FROM sunpeek/poetry:py3.11-slim as lock

WORKDIR /code

COPY ./pyproject.toml /poetry.lock ./
RUN poetry export -f requirements.txt --output requirements.txt
# RUN poetry lock && poetry export -f requirements.txt --without-hashes > requirements.txt

FROM python:3.11-slim as main

COPY --from=lock /code/requirements.txt requirements.txt

# install dependencies
RUN pip install --no-cache-dir -r requirements.txt

# set the working directory in the container
WORKDIR /code

#Copy in the application and entrypoint files
COPY entrypoint.py README.md COPYING COPYING.LESSER NOTICES CITATION.cff alembic.ini ./
COPY sunpeek ./sunpeek
COPY alembic ./alembic

# command to run on container start
CMD ["python", "entrypoint.py"]
EXPOSE 8000/tcp

ARG SUNPEEK_VERSION='0.0.0.dev0'
ENV SUNPEEK_VERSION=$SUNPEEK_VERSION

![Logo_Transparent_wide.svg](https://gitlab.com/sunpeek/sunpeek/-/raw/main/static_assets/Logo_Transparent_wide.svg?inline=false)

**Core:**

![Pipline Status](https://gitlab.com/sunpeek/sunpeek/badges/main/pipeline.svg) 
![Test Coverage](https://gitlab.com/sunpeek/sunpeek/badges/main/coverage.svg) 
![Supported Python](https://img.shields.io/pypi/pyversions/sunpeek)
[![Docker Image Version (latest semver)](https://img.shields.io/docker/v/sunpeek/sunpeek?label=image&logo=docker&sort=semver)](https://hub.docker.com/r/sunpeek/sunpeek)
[![PyPI](https://img.shields.io/pypi/v/sunpeek?logo=PyPi&logoColor=yellow)](https://pypi.org/project/sunpeek/)
![GitLab contributors](https://img.shields.io/gitlab/contributors/sunpeek/sunpeek)
![Open Issues](https://img.shields.io/gitlab/issues/open-raw/sunpeek/sunpeek?gitlab_url=https%3A%2F%2Fgitlab.com) 

**WebUI:**

![Pipline Status](https://gitlab.com/sunpeek/web-ui/badges/main/pipeline.svg)
[![Docker Image Version (latest by date)](https://img.shields.io/docker/v/sunpeek/web-ui?label=image&logo=docker)](https://hub.docker.com/r/sunpeek/sunpeek)
![GitLab contributors](https://img.shields.io/gitlab/contributors/sunpeek/web-ui)
![Open Issues](https://img.shields.io/gitlab/issues/open-raw/sunpeek/web-ui?gitlab_url=https%3A%2F%2Fgitlab.com) 

# About SunPeek
SunPeek implements a dynamic, in situ test methodology for large solar thermal plants, packaged as an open source software 
application and python library. It also includes the first open source implementation of the ISO 24194 procedure 
for checking the performance of solar thermal collector fields.

Full documentation is at [https://docs.sunpeek.org](https://docs.sunpeek.org)

SunPeek was originally developed as part of the HarvestIT research project, see [https://www.collector-array-test.org](https://www.collector-array-test.org)

## A Web Application and a Python Library
SunPeek is available as both a complete, containerised web application - intended to make the ongoing monitoring of one or
several solar thermal plants simple and intuitive - and as a python library, for use by researchers and for building into 
other tools. To install the python library, simply run `pip install sunpeek`. To set up the web application, see below.

## License
Except where specifically noted otherwise, SunPeek is made available under the GNU Lesser General Public License. This means
that you can use the software, copy it, redistribute it and include it in other software, including commercial, proprietary 
software, for free, as long as you abide by the terms of the GNU GPL, with the exceptions provided by the LGPL. In particular, 
if you redistribute a modified version of the software, you must make the source code of your modifications available, and
if you include the software in another piece of software or physical product, you must give users notice that SunPeek is 
used, and inform them where to obtain a copy of the SunPeek source code and license.

Note that the [SunPeek WebUI](https://gitlab.com/sunpeek/web-ui), is covered by a separate licence, the BSD-3-Clause, see:
[https://opensource.org/licenses/BSD-3-Clause](https://opensource.org/licenses/BSD-3-Clause)

Copyright (c) 2020-2022, AEE - Institut für Nachhaltige Technologien, SOLID Solar Energy Systems GmbH, GASOKOL GmbH, Schneid Gesellschaft m.b.H.  
Copyright (c) 2023, SunPeek Open Source Contributors

SunPeek is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 

# Running SunPeek
## Prerequisites
* Ideally, a modern linux environment, although running on Windows and MacOS is also possible, with _at least_:
  * 5GB free disk space (this is needed for unpacking the application, after installation it will use around 2 GB)
  * A 7th Gen i5 or better processor.
  * 8GB RAM
* Docker and Docker compose installed ([see below](#get-docker)), version 2.20 or newer. 

## Get Docker
In order to provide a consistent environment and allow SunPeek to work across a wide range of install environments, 
it is provided as a set of Docker images (essentially, very lightweight virtual machines). A docker compose 
configuration is also provided, if you are installing sunpeek on a single machine, this is probably what you want to 
use, follow the links below for instructions depending on your environment.

### Linux   
To install docker on Linux go to https://docs.docker.com/engine/install/ select the relevant platform and follow the 
instructions.  
### Windows 10 and 11  
On desktop windows, the easiest way to get Docker is to install [docker desktop](https://www.docker.com/products/docker-desktop/).  
### Windows server   
To install the Docker Engine on Windows Server, see [this guide from Microsoft](https://learn.microsoft.com/en-us/virtualization/windowscontainers/quick-start/set-up-environment?tabs=dockerce)

## Using the easy setup GUI or scripts
For convenience, a bash script for Linux or a simple graphical utility for use on Windows is provided. These will do 
some basic configuration for running SunPeek on a single machine, optionally accessible over a local network.
Docker is still required.   
```{note}
If you have previously set up SunPeek using the default configuration, you must first 
remove all stored data by running the command `docker volume rm harvestit_hit_postgres_data` in a terminal/command 
prompt, _this will also remove uploaded data._ You do not need to do this to update the software, see 
[Upgrading to a new version of SunPeek](#upgrading-to-a-new-version-of-sunpeek)
```

#### On Linux
1. In the location you want to store sunpeek configuration, run 
`curl https://gitlab.com/sunpeek/sunpeek/-/raw/main/deploy/quick-setup.sh?inline=false -o quick-setup.sh`,
2. Run `quick-setup.sh` (usually just with the command `./quick-setup.sh`) and enter the url which sunpeek can be accessed
at when prompted.
3. Run `docker compose up -d`
4. After at most 2 minutes (usually a few seconds), the web UI should be accessible at http://localhost, or the url set 
in step 2.

#### On Windows
1. Download [this file](https://gitlab.com/sunpeek/sunpeek/-/raw/main/sunpeek_easy_installer.zip?inline=false), and unzip
it to a temporary location.
2. Run `sunpeek_easy_installer.exe`
3. You should then get a small window with 2 fields. You must select a location to store the configuration files for the
application, if you are running the application only for access from the local machine, leave the default in the url field. 
4. Click setup. 
5. Once the window closes open the directory you specified, and double-click the start.bat file. A command prompt will 
open to display the startup process.
6. Docker compose will download the required application components, this may take several minutes, once you see all 
components listed as 'started' or 'healthy' you can close the command prompt.
7. Open a browser and go to the url specified in the setup tool, probably http://localhost to see the web-UI for the tool.
8. To stop the application, assuming no other processes are running under docker on your machine, simply shut down the 
docker engine. If you are using Docker Desktop, this can be done by right-clicking the Docker icon the system try and 
selecting Quit

## Advanced Configuration
```{danger}
It is strongly recommended that you don't make SunPeek accessible from the public internet. At present there are NO 
 built-in access controls
```
Configuration is via environment variables, which can be set by any configuration management system you use, however the
default setup uses `.env` files. To deploy the application on a single host, only the external URL needs to be set. The 
easy installer does this for you, or you can set the value of `HIT_API_BASE_URL` in the `ui.env` file to 
<external.url>/api/v1. Other configuration variables are documented at 
[docs.sunpeek.org/configuration-variables.html](https://docs.sunpeek.org/configuration-variables.html)
```{note}
This setup is designed to deploy all containers on a single machine where docker compose is running. 
The configuration for the Traefik reverse proxy is stored in a directory which is *bind mounted* to the container. For 
other deployment approaches (e.g. using Kubernetes), a more appropriate Traefik dynamic 
[configuration provider](https://doc.traefik.io/traefik/providers/overview/) should be selected.
```

### Database Backends
SunPeek supports SQLite and PostgreSQL as backend databases for storing plant configurations, collector and fluid types 
etc. For single host installs with small number of plants, SQLite should be sufficient and this is therefore the default
configuration. To use the PostgreSQL backend you can add `HIT_DB_TYPE=postgresql` to `api.env`, and set the values of
the `HIT_DB_PW` and `POSTGRES_PASSWORD` variables in the `api.env` and `db.env` files to *the same* random, unique 
password string. The docker compose setup includes an optional postgres service, which can be started along with the 
other containers with `docker compose --profile postgres up` 

## Upgrading to a new version of SunPeek
Updates to SunPeek are accomplished by pulling newer versions of the docker images used to run the application. These 
are used to create new containers in place of the old ones. If you used the default configuration, a persistent docker 
volume called `sunpeek_postgres_data` will have been created, this _should_ avoid data loss, however keeping backups is 
always recommended. The update process is as follows, and is the same on all operating systems:
1. Open a terminal/command prompt in the configuration folder selected during setup.
2. Run `docker compose pull` to download the latest images
3. Run `docker compose up -d` to recreate any containers which have updated images.  


## Technical Details - What does compose do?
[Docker Compose](https://docs.docker.com/compose/) is a tool for *orchestrating* docker containers, to create 
applications made up of several docker containers. When the HarvestIT application is started with the default docker-compose 
file, the following things happen:
1. Compose checks if each of the images defined in the compose file, is available with the correct tag locally, if not it
pulls them from the relevant registry
2. A virtual network is created, for the containers to communicate with each other. This is segregated from the host machine's
main network interfaces.
3. The database container (using image `timescale/timescaledb:latest-pg14`), is started, with a healthcheck defined. 
Alongside this container, a [Docker Volume](https://docs.docker.com/storage/volumes/) is created called `hit_postgres_data`, 
which is mapped to the default data directory in the database container, to ensure that database data is persisted when 
the containers are recreated (e.g. during an update).
4. The reverse proxy container (using image `traefik:v2.8`) is started. As well as being attached to the virtual network 
created by docker compose, this has port 80 exposed to the host, so that it can be accessed at `localhost/` or from an
extrnal connection. This routes web requests to either the web-ui or api containers, depending on the path in the request 
URL. It can also [be configured](https://doc.traefik.io/traefik/https/overview/) to terminate TLS (HTTPS) encrypted 
connections and obtain certificates automatically, to secure connections to the application.
5. Compose waits until the database container reports a "healthy" status, then starts the api container (using image 
`sunpeek:latest)`, this is the main HarvestIT application.
6. The `harvestit` container runs a database initialization scripts to get the database ready. 
7. Once the api container has started, the webui container is started. 

# Developing
For information on contributing to sunpeek, see CONTRIBUTING.md, for developer documentation see 
[https://docs.sunpeek.org/developing.html](https://docs.sunpeek.org/developing.html)

# Maintainers and Steering Committee
SunPeek is developed as an open source project, with contributions gladly accepted from interested members of the community.
The overall direction of the project is managed by a **steering committee**, which currently consists of: 
* Daniel Tschopp <d.tschopp@aee.at>
* Philip Ohnewein <p.ohnewein@aee.at>
* Marnoch Hamilton-Jones <m.hamilton-jones@aee.at>
* Lukas Feierl <l.feierl@solid.at>
* Maria Moser <m.moser@solid.at>

The steering committee appoints the project maintainers, and makes final decisions on which contributors have commit 
privileges on the official repository as well as ongoing implementation of new features and updates. The maintainers are
responsible for reviewing and merging any merge (pull) requests. The current **maintainers** are:
* Marnoch Hamilton-Jones <m.hamilton-jones@aee.at>
* Lukas Feierl <l.feierl@solid.at>
* Philip Ohnewein <p.ohnewein@aee.at>

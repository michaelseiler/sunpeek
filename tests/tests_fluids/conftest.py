from collections import namedtuple
from typing import List, Tuple, Optional

import numpy as np
from matplotlib import pyplot as plt
from matplotlib.offsetbox import TextArea, AnnotationBbox, VPacker
from sqlalchemy.orm import sessionmaker
from sunpeek.common import utils, plot_utils as pu
from sunpeek.common.unit_uncertainty import to_s, Q, to_numpy
from sunpeek.components import CoolPropFluidDefinition, FluidFactory
from sunpeek.definitions import FluidProps
from sunpeek.definitions.fluid_definitions import get_definition

S = sessionmaker(utils.db_engine)


def plot_density_heat_capacity(prop_to_plot: FluidProps,
                               concentration: float,
                               fluid_names: List[str],
                               te_range: Tuple[int, int],
                               te_step: Optional[int] = 5,
                               settings: Optional[pu.PlotSettings] = None,
                               ) -> Optional[pu.PlotResult]:
    """Plot fluid property (density, heat capacity) of given fluid names over given temperature range.
    Works at a fixed concentration.
    """
    if prop_to_plot is None or prop_to_plot not in list(FluidProps):
        raise ValueError(f'Invalid "prop_to_plot": {prop_to_plot}')
    if not fluid_names:
        raise ValueError('No fluids passed. Quitting.')

    if settings is None:
        settings = pu.PlotSettings()

    # Data to plot
    te_min, te_max = te_range
    te = to_s(range(te_min, te_max + 1, te_step), 'degC')
    concentration = Q(concentration)

    if prop_to_plot == 'density':
        title_str = 'Fluid Density'
        unit_str = 'kg m**-3'
        y_label = 'Density'
        legend_loc = 'upper right'
        get_fluid_prop = lambda fluid, te: fluid.get_density(te)
    else:
        title_str = 'Fluid Heat Capacity'
        unit_str = 'J kg**-1 K**-1'
        y_label = 'Heat Capacity'
        legend_loc = 'lower right'
        get_fluid_prop = lambda fluid, te: fluid.get_heat_capacity(te)

    fluids = []
    Fluid = namedtuple('Fluid', ['name', 'te', 'fluid_def', 'is_coolprop'])
    for fluid_name in fluid_names:
        fluid_def = get_definition(fluid_name)
        is_coolprop = True if isinstance(fluid_def, CoolPropFluidDefinition) else False
        fluids.append(Fluid(fluid_name, te, fluid_def, is_coolprop))

    fluid_prop = dict()
    for fluid in fluids:
        fl = FluidFactory(fluid=fluid.fluid_def, concentration=concentration)
        fluid_prop[fluid.name] = get_fluid_prop(fl, te)

    # Plot Title
    fig, params = pu.prepare_figure(settings=settings)
    box = VPacker(children=[pu.box_title(f'{title_str} of selected fluids'),
                            ], pad=0, sep=pu.Defaults.sep_major.value)
    artist = pu.annotation_bbox(box, xy=pu.Defaults.xy_topleft.value)
    fig.add_artist(artist)

    # Data plot
    rect = pu.get_rectangle_below(artist, vsep=pu.Defaults.sep_huge.value, bottom=0.45)
    ax = fig.add_axes(rect)
    colors = [pu.Colors[k].value for k in ['blue', 'cyan', 'green', 'yellow', 'red', 'purple', 'gray']]

    for i, fluid in enumerate(fluids):
        x = to_numpy(te, 'degC')
        y = to_numpy(fluid_prop[fluid.name], unit_str)
        color = colors[i % len(colors)]
        legend_prefix = f'[CoolProp, concentration={concentration.m:.0%}] ' if fluid.is_coolprop else ''
        mask = ~np.isinf(y)
        ax.plot(x[mask], y[mask], 'o-',
                label=f"{legend_prefix}{fluid.fluid_def.description}",
                markersize=pu.Defaults.marker_size_plot.value,
                alpha=pu.Defaults.marker_alpha.value,
                color=color,
                zorder=2.5,
                )

    ax.set_xlabel(f'Temperature [{te.pint.units:~P}]')
    ax.set_ylabel(f'{y_label} [{fluid_prop[fluids[0].name].pint.units:~P}]')
    ax.grid()
    ax.set_axisbelow('line')
    ax.legend(loc=legend_loc).set_zorder(3)

    return fig

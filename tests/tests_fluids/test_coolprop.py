import matplotlib.figure
import pytest
import os
from pathlib import Path
import numpy as np
import pandas as pd
import CoolProp.CoolProp as Cp

from sunpeek.common.unit_uncertainty import to_s, Q, to_numpy
from sunpeek.definitions import FluidProps
from sunpeek.definitions.fluid_definitions import get_definition
from sunpeek.components import CoolPropFluid, CoolPropFluidDefinition, FluidFactory
from sunpeek.common import plot_utils as pu
from tests.tests_fluids.conftest import plot_density_heat_capacity


def test_coolprop_interface():
    # does this return all fluids, also incompressible link?

    # heat capacity of water
    Cp.PropsSI("C", "P", 101325, "T", 300, "Water")
    # density of water
    Cp.PropsSI("D", "P", 101325, "T", 300, "Water")

    # same for 50% (mass) ethylene glycol/water
    Cp.PropsSI("C", "T", 300, "P", 101325, "INCOMP::MEG-50%")
    Cp.PropsSI("D", "T", 300, "P", 101325, "INCOMP::MEG-50%")

    Cp.PropsSI('H', 'T', 300, 'P', 101325, 'INCOMP::LiBr-23%')
    Cp.PropsSI('H', 'T', 300, 'P', 101325, 'INCOMP::LiBr[0.23]')

    # Also works on arrays
    te = np.linspace(280, 380, 11)
    Cp.PropsSI("C", "P", 101325, "T", te, "Water")


@pytest.mark.parametrize('prop', ['density', 'heat_capacity'])
@pytest.mark.parametrize('temperature', [20, [20, 30]])
@pytest.mark.parametrize(
    'fluid_name, concentration', [
        ('water', None),
        # All CoolProp Propylene glycols
        ('Antifrogen L', Q(0.3)),
        ('ASHRAE, Propylene Glycol', Q(0.3)),
        ('Pekasol L', Q(0.3)),
        ('Zitrec FC', Q(0.3)),
        ('Zitrec LC', Q(0.3)),
    ])
def test_return_types(fluid_name, concentration, temperature, prop):
    te = to_s(temperature, 'degC')
    fl = CoolPropFluid(get_definition(fluid_name), concentration=concentration)

    if prop == 'density':
        val = fl.get_density(te)
    elif prop == 'heat_capacity':
        val = fl.get_heat_capacity(te)
    else:
        raise ValueError(f'Invalid prop "{prop}".')

    assert isinstance(val, pd.Series)
    assert val.dtype.__str__()[:4] == 'pint'
    assert isinstance(val[0], Q)
    assert isinstance(val[0].m, np.float64)


@pytest.mark.parametrize(
    'fluid_name, concentration, temperature, expected', [
        ('water', None, 20, 998.2071504679284),
        ('water', None, 1, 999.9018375606109),
        ('water', None, 99, 959.0660595594433),
        # All CoolProp Propylene glycols
        ('Antifrogen L', Q(0.3), 20, 1029.2448346134374),
        ('ASHRAE, Propylene Glycol', Q(0.3), 20, 1028.3492563225548),
        ('Pekasol L', Q(0.3), 20, 1027.8822691507014),
        ('Zitrec FC', Q(0.3), 20, 1029.1478606012502),
        ('Zitrec LC', Q(0.3), 20, 1025.7905872946249),
    ])
def test_density__numerically(fluid_name, concentration, temperature, expected):
    fl = CoolPropFluid(get_definition(fluid_name), concentration=concentration)
    val = fl.get_density(to_s(temperature, 'degC'))

    assert pytest.approx(val[0].to('kg m**-3'), abs=0.01) == expected


@pytest.mark.parametrize(
    'fluid_name, concentration, temperature, expected', [
        ('water', None, 20, 4184.050924523541),
        ('water', None, 1, 4216.113470946228),
        ('water', None, 99, 4214.528625231976),
        # All CoolProp Propylene glycols
        ('Antifrogen L', Q(0.3), 20, 3880.8493805909375),
        ('ASHRAE, Propylene Glycol', Q(0.3), 20, 3847.894800208916),
        ('Pekasol L', Q(0.3), 20, 3946.2917615629003),
        ('Zitrec FC', Q(0.3), 20, 3883.00168114507),
        ('Zitrec LC', Q(0.3), 20, 3880.25034885875),
    ])
def test_heat_capacity__numerically(fluid_name, concentration, temperature, expected):
    fl = CoolPropFluid(get_definition(fluid_name), concentration=concentration)
    val = fl.get_heat_capacity(to_s(temperature, 'degC'))

    assert pytest.approx(val[0].to('J kg**-1 K**-1'), abs=0.1) == expected


# Goal of this test is to find out, for practical knowledge, the working temperature and concentration ranges of all
# propylene glycol fluids. Done via 2D grid search over defined temperature and concentration ranges.
@pytest.mark.parametrize(
    'fluid_name, te_min, te_max, c_min, c_max', [
        ('Antifrogen L', 0, 80, 10, 15),
        ('Antifrogen L', -5, 80, 20, 20),
        ('Antifrogen L', -10, 80, 25, 30),
        ('Antifrogen L', -15, 80, 35, 35),
        ('Antifrogen L', -20, 80, 40, 40),
        ('Antifrogen L', -25, 80, 45, 45),
        ('Antifrogen L', -30, 80, 50, 60),
        ('ASHRAE, Propylene Glycol', 0, 100, 10, 10),
        ('ASHRAE, Propylene Glycol', -5, 100, 15, 25),
        ('ASHRAE, Propylene Glycol', -10, 100, 30, 30),
        ('ASHRAE, Propylene Glycol', -15, 100, 35, 35),
        ('ASHRAE, Propylene Glycol', -20, 100, 40, 60),
        ('Pekasol L', 0, 100, 10, 15),
        ('Pekasol L', -5, 100, 20, 25),
        ('Pekasol L', -10, 100, 30, 30),
        ('Pekasol L', -15, 100, 35, 35),
        ('Pekasol L', -20, 100, 40, 60),
        ('Zitrec FC', -10, 100, 30, 30),
        ('Zitrec FC', -15, 100, 35, 35),
        ('Zitrec FC', -20, 100, 40, 60),
        ('Zitrec LC', -10, 100, 30, 30),
        ('Zitrec LC', -15, 100, 35, 35),
        ('Zitrec LC', -20, 100, 40, 65),
    ])
# Temperature and concentration grid sizes
@pytest.mark.parametrize('te_step', [5])
@pytest.mark.parametrize('c_step', [5])
def test_temperature_and_concentration_ranges_propylene_glycols(fluid_name, te_min, te_max, c_min, c_max,
                                                                te_step, c_step):
    te_range = range(te_min, te_max + 1, te_step)
    c_range = range(c_min, c_max + 1, c_step)

    rho = []
    cp = []
    for c in c_range:
        fl = CoolPropFluid(get_definition(fluid_name), concentration=Q(c * 0.01))
        rho_vals = fl.get_density(to_s(te_range, 'degC'))
        rho.append(to_numpy(rho_vals, 'kg m**-3'))
        cp_vals = fl.get_heat_capacity(to_s(te_range, 'degC'))
        cp.append(to_numpy(cp_vals, 'J kg**-1 K**-1'))

    rho_df = pd.DataFrame(data=rho, index=c_range, columns=te_range).transpose().rename_axis('temperature')
    assert rho_df.notna().all().all()

    cp_df = pd.DataFrame(data=cp, index=c_range, columns=te_range).transpose().rename_axis('temperature')
    assert cp_df.notna().all().all()


# @pytest.mark.skip('Development plot for selected fluid properties')
@pytest.mark.parametrize('prop_to_plot', [FluidProps.density, FluidProps.heat_capacity])
@pytest.mark.parametrize('concentration', [0.36])
def test_plot_propylene_glycols__fixed_concentration(prop_to_plot, concentration):
    # All CoolProp Propylene glycols
    fluids = [
        'Antifrogen L',
        'ASHRAE, Propylene Glycol',
        'Pekasol L',
        'Zitrec FC',
        'Zitrec LC',
        'Pekasolar_FHW',
    ]

    fig = plot_density_heat_capacity(prop_to_plot=prop_to_plot,
                                     concentration=concentration,
                                     fluid_names=fluids,
                                     te_range=(30, 130),
                                     )

    assert isinstance(fig, matplotlib.figure.Figure)


def test_concentration_pure_fluid():
    fluid_def = CoolPropFluidDefinition(model_type='CoolProp',
                                        name='water',
                                        is_pure=True)
    with pytest.raises(ValueError, match="Non-None concentration was given for a pure fluid"):
        FluidFactory(fluid=fluid_def, concentration=Q(50, 'percent'))


def test_missing_concentration():
    fluid_def = CoolPropFluidDefinition(model_type='CoolProp',
                                        name='AL',
                                        manufacturer=None,
                                        description='Antifrogen L, Propylene Glycol',
                                        is_pure=False)
    with pytest.raises(ValueError, match="None concentration was given a for a non-pure fluid"):
        FluidFactory(fluid=fluid_def)

## Some more fluid definitions:
# Cp.PropsSI("C", "P", self.P_DEFAULT, "T", 300, "Water")
# Cp.PropsSI("C", "P", self.P_DEFAULT, "T", 300, "HEOS::Water")
# # Concentration not required
# Cp.PropsSI("D", "P", 101325, "T", 300, 'INCOMP::AS55')
#
# # Cp.PropsSI("C", "T", 300, "P", 101325, "MEG-50%")
# Cp.PropsSI("C", "T", 300, "P", 101325, "INCOMP::MEG-50%")
# Cp.PropsSI("C", "T", 300, "P", 101325, "INCOMP::MEG[0.5]")
# Cp.PropsSI("D", "P", 101325, "T", 300, 'LiBr')
# Cp.PropsSI("D", "P", 101325, "T", 300, 'AS55')


## Things tried...
# # all_definitions = Cp.get_global_param_string("FluidsList").split(',')
# all_definitions = Cp.get_global_param_string("fluids_list").split(',')
# # CoolProp.get_global_param_string('fluids_list').split(',')
# inc_pure = Cp.get_global_param_string('incompressible_list_pure').split(',')
# inc_sol = Cp.get_global_param_string('incompressible_list_solution').split(',')
#
# all_definitions = Cp.get_global_param_string("parameter_list").split(',')
# all_definitions = Cp.get_global_param_string("predefined_mixtures").split(',')
# Cp.get_incompressible_list_pure()
#
# Cp.get_fluid_param_string()
#
# Cp.get_csv_predefined_mixtures()
# Cp.get_csv_mixture_binary_pairs ()
# Cp.get_csv_parameter_list ()
#
# Cp.get_config_string()
#
# Cp.config_string_to_key()
# Cp.config_key_description()
# Cp.config_key_to_string()
# Cp.get_mixture_binary_pair_pcsaft()
#
#
# Cp.get_config_as_json ()
# Cp.get_config_as_json_string ()

import matplotlib.figure
import pytest
import os
from pathlib import Path
import numpy as np
import pandas as pd
from sklearn.pipeline import Pipeline

from sunpeek.common.unit_uncertainty import Q, to_s, to_numpy
from sunpeek.definitions.fluid_definitions import get_definition, WPDFluids
from sunpeek.components.fluids_wpd_models import ModelFactory, WPDModelPure, WPDModelMixed
from sunpeek.components.fluids import FluidFactory, WPDFluidPure, WPDFluidMixed
from sunpeek.definitions import FluidProps
import sunpeek.common.plot_utils as pu


# Test WPDModel, ModelFactory classes --------------------------------

@pytest.mark.parametrize('prop', [FluidProps.density, FluidProps.heat_capacity])
@pytest.mark.parametrize('fluid_info', [
    WPDFluids.fhw_pekasolar.value,
    WPDFluids.corro_star.value,
])
def test_modelfactory_pure(fluid_info, prop):
    model = ModelFactory.from_info_and_property(fluid_info, prop)
    assert isinstance(model, WPDModelPure)


@pytest.mark.parametrize('prop', [FluidProps.density, FluidProps.heat_capacity])
@pytest.mark.parametrize('fluid_info', [
    WPDFluids.thermum_p.value,
])
def test_modelfactory_mixed(fluid_info, prop):
    model = ModelFactory.from_info_and_property(fluid_info, prop)
    assert isinstance(model, WPDModelMixed)


@pytest.fixture
def csv_model(fluid_info, prop):
    model = ModelFactory.from_info_and_property(fluid_info, prop)
    return model


@pytest.mark.parametrize('prop', [FluidProps.density, FluidProps.heat_capacity])
@pytest.mark.parametrize('fluid_info', [
    WPDFluids.fhw_pekasolar.value,
    WPDFluids.thermum_p.value,
    WPDFluids.corro_star.value,
])
def test_train_return_type(fluid_info, prop, csv_model):
    # Goal = Test successful training & return type of all available fluids and all properties
    expected_type = Pipeline
    assert type(csv_model.sk_model) == expected_type


@pytest.mark.parametrize('prop', [FluidProps.density])
@pytest.mark.parametrize(
    'fluid_info, concentration, temperature, expected', [
        # FHW Pekasolar
        (WPDFluids.fhw_pekasolar.value, None,
         [-20, 20, 80, 120, 130],
         [1056.4524, 1040.5236, 1003.51056, 971.45996, 962.6601]),
        # Thermum at selected concentrations
        (WPDFluids.thermum_p.value, 0,
         [-10, 20, 110, 120, 130],
         [1002.15839617, 998.18788706, 957.25088583, 952.56200055, 948.27611488]),
        (WPDFluids.thermum_p.value, 30,
         [-10, 20, 110, 120, 130],
         [1033.54046536, 1021.4009242, 962.39468778, 956.29420769, 950.71594775]),
        (WPDFluids.thermum_p.value, 36,
         [-10, 20, 110, 120, 130],
         [1039.80447493, 1026.03672219, 963.43342332, 957.05248923, 951.21761919]),
        (WPDFluids.thermum_p.value, 60,
         [-10, 20, 110, 120, 130],
         [1064.83621264, 1044.57426285, 967.63866379, 960.1421306, 953.28703626]),
        # corroStar
        (WPDFluids.corro_star.value, None,
         [-10, 20, 110, 120, 130],
         [1039.80991873, 1026.0290437, 963.43573331, 957.0475599, 951.20159584]),
    ])
def test_density(prop, csv_model, fluid_info, concentration, temperature, expected):
    prop = csv_model.predict(temperature, concentration)
    np.testing.assert_array_almost_equal(prop, expected, decimal=3)


@pytest.mark.parametrize('prop', [FluidProps.heat_capacity])
@pytest.mark.parametrize(
    'fluid_info, concentration, temperature, expected', [
        # FHW Pekasolar
        (WPDFluids.fhw_pekasolar.value, None,
         [-20, 20, 80, 120, 130],
         [3.47003542, 3.73028382, 3.89593563, 4.00779386, 4.05079008]),
        # Thermum at selected concentrations
        (WPDFluids.thermum_p.value, 0,
         [-10, 20, 110, 120, 130],
         [4.24842575, 4.22684375, 4.21184876, 4.21429683, 4.2174844]),
        (WPDFluids.thermum_p.value, 30,
         [-10, 20, 110, 120, 130],
         [3.84176119, 3.8782868, 3.98518536, 3.9963226, 4.00722841]),
        (WPDFluids.thermum_p.value, 36,
         [-10, 20, 110, 120, 130],
         [3.76016964, 3.80838971, 3.93988577, 3.95278515, 3.96525893]),
        (WPDFluids.thermum_p.value, 60,
         [-10, 20, 110, 120, 130],
         [3.43305648, 3.52829747, 3.75891285, 3.77894183, 3.79776848]),
        # corroStar
        (WPDFluids.corro_star.value, None,
         [-10, 20, 110, 120, 130],
         [3760.15956198, 3808.40529638, 3939.8814017, 3952.79588326, 3965.29269009]),
    ])
def test_heat_capacity(prop, csv_model, fluid_info, concentration, temperature, expected):
    prop = csv_model.predict(temperature, concentration)
    np.testing.assert_array_almost_equal(prop, expected, decimal=3)


# Test WPDFluid, FluidFactory (= higher-level fluid behavior) -------------------------

@pytest.mark.parametrize('fluid_name', [
    WPDFluids.fhw_pekasolar.value.name,
    WPDFluids.corro_star.value.name,
    WPDFluids.thermum_p.value.name,
])
def test_get_definition(fluid_name):
    fluid_def = get_definition(fluid_name)
    assert fluid_def is not None


@pytest.mark.parametrize('fluid_name, expected_class', [
    (WPDFluids.fhw_pekasolar.value.name, WPDFluidPure),
    (WPDFluids.corro_star.value.name, WPDFluidPure),
])
def test_fluid_factory_noconcentration(fluid_name, expected_class):
    fluid_def = get_definition(fluid_name)
    fluid = FluidFactory(fluid=fluid_def)

    assert type(fluid) == expected_class


@pytest.mark.parametrize('fluid_name, expected_class, concentration', [
    (WPDFluids.fhw_pekasolar.value.name, WPDFluidPure, None),
    (WPDFluids.thermum_p.value.name, WPDFluidMixed, Q(36, 'percent')),
    (WPDFluids.corro_star.value.name, WPDFluidPure, None),
])
def test_fluid_factory_withconcentration(fluid_name, expected_class, concentration):
    fluid_def = get_definition(fluid_name)
    fluid = FluidFactory(fluid=fluid_def, concentration=concentration)

    assert type(fluid) == expected_class


@pytest.mark.parametrize('temperature', [np.linspace(-10, 120, 14)])
@pytest.mark.parametrize('fluid_name, concentration', [
    (WPDFluids.fhw_pekasolar.value.name, None),
    (WPDFluids.thermum_p.value.name, Q(36, 'percent')),
    (WPDFluids.corro_star.value.name, None),
])
def test_fluid_factory_getproperties(fluid_name, concentration, temperature):
    fluid_def = get_definition(fluid_name)
    fluid = FluidFactory(fluid=fluid_def, concentration=concentration)
    te = to_s(temperature, 'degC')

    fluid.get_density(te)
    fluid.get_heat_capacity(te)


@pytest.mark.parametrize('prop', ['density', 'heat_capacity'])
@pytest.mark.parametrize('temperature', [20, [20, 30]])
@pytest.mark.parametrize('fluid_name, concentration', [
    (WPDFluids.fhw_pekasolar.value.name, None),
    (WPDFluids.thermum_p.value.name, Q(36, 'percent')),
    (WPDFluids.corro_star.value.name, None),
])
def test_return_types(fluid_name, concentration, temperature, prop):
    te = to_s(temperature, 'degC')
    fl = FluidFactory(fluid=get_definition(fluid_name), concentration=concentration)

    if prop == 'density':
        val = fl.get_density(te)
    elif prop == 'heat_capacity':
        val = fl.get_heat_capacity(te)
    else:
        raise ValueError(f'Invalid prop "{prop}".')

    assert isinstance(val, pd.Series)
    assert val.dtype.__str__()[:4] == 'pint'
    assert isinstance(val[0], Q)
    assert isinstance(val[0].m, np.float64)


@pytest.mark.parametrize(
    'fluid_name, concentration, temperature, expected', [
        (WPDFluids.fhw_pekasolar.value.name, None, 20, 1040.5235),
        (WPDFluids.thermum_p.value.name, Q(36, 'percent'), 20, 1026.03672),
        (WPDFluids.corro_star.value.name, None, 20, 1026.0290),
    ]
)
def test_density__numerically(fluid_name, concentration, temperature, expected):
    fluid = FluidFactory(fluid=get_definition(fluid_name), concentration=concentration)
    te = to_s(temperature, 'degC')
    val = fluid.get_density(te)

    assert pytest.approx(val[0].to('kg m**-3'), abs=0.01) == expected


@pytest.mark.parametrize(
    'fluid_name, concentration, temperature, expected', [
        (WPDFluids.fhw_pekasolar.value.name, None, 20, 3730.2838),
        (WPDFluids.thermum_p.value.name, Q(36, 'percent'), 20, 3808.3897),
        (WPDFluids.corro_star.value.name, None, 20, 3808.4052),
    ]
)
def test_heat_capacity__numerically(fluid_name, concentration, temperature, expected):
    fluid = FluidFactory(fluid=get_definition(fluid_name), concentration=concentration)
    te = to_s(temperature, 'degC')
    val = fluid.get_heat_capacity(te)

    assert pytest.approx(val[0].to('J kg**-1 K**-1'), abs=0.1) == expected


@pytest.mark.skip(reason='One-time export only')
@pytest.mark.parametrize('temperature', [np.linspace(-10, 120, 14)])
@pytest.mark.parametrize('fluid_name, concentration', [
    (WPDFluids.thermum_p.name, Q(36, 'percent')),
])
def test_export_thermump_to_corrostar(temperature, fluid_name, concentration):
    # Export Thermum P properties at 36 degC in WPD format -> corroStar properties
    fluid_def = get_definition(fluid_name)
    fluid = FluidFactory(fluid=fluid_def, concentration=concentration)

    te = to_s(temperature, 'degC')
    rho = fluid.get_density(te)
    cp = fluid.get_heat_capacity(te)

    def export_file(val, unit, filename):
        df = pd.DataFrame(data=to_numpy(val, unit), index=temperature, columns=['Y']).rename_axis('X')
        fn = Path(__file__).with_name(f'{filename}.csv')
        df.to_csv(fn, sep=',')

    export_file(rho, 'kg m**-3', FluidProps.density)
    export_file(cp, 'J kg**-1 K**-1', FluidProps.heat_capacity)


@pytest.mark.parametrize('fluid_name, expected_class', [
    (WPDFluids.thermum_p.value.name, WPDFluidMixed),
])
def test_fluid_factory_raises_noconcentration_for_mixed_fluids(fluid_name, expected_class):
    fluid_def = get_definition(fluid_name)
    with pytest.raises(TypeError, match="missing 1 required positional argument: 'concentration'"):
        FluidFactory(fluid=fluid_def)


@pytest.mark.parametrize('fluid_name, concentration', [
    (WPDFluids.fhw_pekasolar.value.name, Q(28, 'percent')),
    (WPDFluids.corro_star.value.name, Q(28, 'percent')),
])
def test_warn_purefluid_concentration_given(fluid_name, concentration):
    # Test that a warning is issued when passing a non-None concentration to a pure fluid.

    fluid_def = get_definition(fluid_name)
    with pytest.warns(UserWarning, match='gracefully ignore the concentration'):
        fluid = FluidFactory(fluid=fluid_def, concentration=concentration)
        assert isinstance(fluid, WPDFluidPure)


@pytest.mark.parametrize('fluid_name', [
    WPDFluids.thermum_p.value.name,
])
def test_error_mixedfluid_noconcentration(fluid_name):
    # Test that an error is issued when not passing a concentration to a mixed fluid.

    fluid_def = get_definition(fluid_name)
    with pytest.raises(TypeError, match="missing 1 required positional argument: 'concentration'"):
        fluid = FluidFactory(fluid=fluid_def)
        assert isinstance(fluid, WPDFluidMixed)

        fluid = FluidFactory(fluid=fluid_def, concentration=None)
        assert isinstance(fluid, WPDFluidMixed)


# Test plotting -------------------


@pytest.mark.parametrize('prop', [FluidProps.density, FluidProps.heat_capacity])
@pytest.mark.parametrize('fluid_info', [
    WPDFluids.fhw_pekasolar.value,
    WPDFluids.thermum_p.value,
    WPDFluids.corro_star.value,
])
def test_plot_wpd_fit(csv_model, prop, fluid_info):
    fig = csv_model.plot_fit(prop_to_plot=prop, fluid_name=fluid_info.name)

    assert isinstance(fig, matplotlib.figure.Figure)

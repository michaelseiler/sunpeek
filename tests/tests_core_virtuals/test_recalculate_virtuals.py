import pytest
import json

from sunpeek.common import config_parser
import sunpeek.demo
from sunpeek.common.utils import DatetimeTemplates
from sunpeek.data_handling.wrapper import use_csv
from sunpeek.core_methods import virtuals


def test_recalculation__plant_without_data(fhw):
    assert not fhw.virtuals_calculation_uptodate


def test_recalculation_no_virtuals(fhw):
    use_csv(fhw, csv_files=sunpeek.demo.DEMO_DATA_PATH_2DAYS, timezone='UTC',
            datetime_template=DatetimeTemplates.year_month_day,
            calculate_virtuals=False)   # <--

    # Virtual sensor calculation NOT called
    assert not fhw.virtuals_calculation_uptodate


def test_recalculation_base(fhw):
    use_csv(fhw, csv_files=sunpeek.demo.DEMO_DATA_PATH_2DAYS, timezone='UTC',
            datetime_template=DatetimeTemplates.year_month_day,
            calculate_virtuals=True)  # <--

    assert fhw.virtuals_calculation_uptodate


def test_config_virtuals_resets_flag(fhw, resources_dir):
    # Data upload should calculate virtuals + reset flag
    full_fn = resources_dir / 'data/data_uploader/csv1.csv'
    use_csv(fhw, full_fn, timezone='Europe/Vienna',
            datetime_template='year_month_day',
            calculate_virtuals=True)  # <--
    assert fhw.virtuals_calculation_uptodate

    # Every config virtuals invalidates virtual sensors
    virtuals.config_virtuals(fhw)
    assert not fhw.virtuals_calculation_uptodate

    # Every calculate virtuals invalidates virtual sensors
    virtuals.calculate_virtuals(fhw)
    assert fhw.virtuals_calculation_uptodate

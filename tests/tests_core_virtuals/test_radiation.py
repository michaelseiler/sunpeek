"""Tests creation and calculation of radiation conversion virtual sensors.
This test suite is limited to all functionality related to radiation calculation and radiation conversion.
Calculation: Tests creation and mapping of virtual sensor as well as everything that happens after call to
calculate_virtuals(plant).
API call triggering call to calculate_virtuals(plant) is not included here.
"""
import pytest
import numpy as np

import sunpeek.demo
from sunpeek.data_handling.wrapper import use_dataframe, use_csv
from sunpeek.common.unit_uncertainty import Q
import sunpeek.core_methods.virtuals as virtuals
import sunpeek.core_methods.virtuals.virtuals_plant as vp
import sunpeek.core_methods.virtuals.virtuals_array as va


## Plants with different input sensors for Plant
# ---------------------------------------------------

@pytest.fixture
def fhw_rad(fhw__2days_data__no_virtuals):
    return fhw__2days_data__no_virtuals


@pytest.fixture
def fhw_vs_rad(fhw_rad, sensor_info):
    """Returns FHW plant without fluid, with all 3 radiation input slots set.
    """
    plant = fhw_rad
    plant.set_sensors(in_global=fhw_rad.get_raw_sensor('rd_gti'),
                      in_beam=fhw_rad.get_raw_sensor('rd_bti'),
                      in_diffuse=fhw_rad.get_raw_sensor('rd_dti'),
                      in_dni=fhw_rad.get_raw_sensor('rd_dni'))
    # TODO Should include tests that make of use of plant.te_amb, is relevant e.g. for pvlib's get_solarposition
    return plant


@pytest.fixture
def fhw_vs_rad_rdgti_different_tilt(fhw_rad, sensor_info):
    """Yields FHW plant with array.in_global sensor info altered.
    """
    a = fhw_rad.arrays[0]
    a.in_global.info = sensor_info['fhw10']

    yield fhw_rad
    a.in_global.info = sensor_info['fhw']


@pytest.fixture
def fhw_vs_rad_rdgti_nosensorinfo(fhw_rad, sensor_info):
    """Yields FHW plant without array.in_global sensor info.
    """
    a = fhw_rad.arrays[0]
    old_info = a.in_global.info

    # Sensor without SensorInfo
    a.in_global.info = {}

    yield fhw_rad
    a.in_global.info = old_info


@pytest.fixture
def sensor_info(fhw_rad):
    a = fhw_rad.arrays[0]
    return {
        # as in FHW JSON config file
        'fhw': {'tilt': a.tilt, 'azim': a.azim, },
        # deliberately altered
        'fhw10': {'tilt': a.tilt - Q(10, 'deg'), 'azim': a.azim}
    }


@pytest.fixture
def fhw_none(fhw_rad):
    return fhw_rad


@pytest.fixture
def fhw_global(fhw_rad, sensor_info):
    plant = fhw_rad
    plant.set_sensors(in_global=fhw_rad.get_raw_sensor('rd_gti'))
    return plant


@pytest.fixture
def fhw_beam(fhw_rad, sensor_info):
    plant = fhw_rad
    plant.set_sensors(in_beam=fhw_rad.get_raw_sensor('rd_bti'))
    return plant


@pytest.fixture
def fhw_diffuse(fhw_rad, sensor_info):
    plant = fhw_rad
    plant.set_sensors(in_diffuse=fhw_rad.get_raw_sensor('rd_dti'))
    return plant


@pytest.fixture
def fhw_dni(fhw_rad, sensor_info):
    plant = fhw_rad
    plant.set_sensors(in_dni=fhw_rad.get_raw_sensor('rd_dni'))
    return plant


@pytest.fixture
def fhw_beam_diffuse(fhw_rad, sensor_info):
    plant = fhw_rad
    plant.set_sensors(in_beam=fhw_rad.get_raw_sensor('rd_bti'),
                      in_diffuse=fhw_rad.get_raw_sensor('rd_dti'))
    return plant


@pytest.fixture
def fhw_global_beam_diffuse(fhw_rad, sensor_info):
    plant = fhw_rad
    plant.set_sensors(in_global=fhw_rad.get_raw_sensor('rd_gti'),
                      in_beam=fhw_rad.get_raw_sensor('rd_bti'),
                      in_diffuse=fhw_rad.get_raw_sensor('rd_dti'))
    return plant


## Config tests
# --------------

def test_get_fixtures(fhw_vs_rad, fhw_global, fhw_beam, fhw_beam_diffuse, fhw_global_beam_diffuse):
    pass


@pytest.mark.skip('Module is currently commented / not needed')
def test_config_horizontal_ok(fhw_global):
    vp.config_virtuals_radiation(fhw_global)


@pytest.mark.skip('Module is currently commented / not needed')
def test_create_horizontal_fails_none(fhw_none):
    vp.config_virtuals_radiation(fhw_none)
    assert not fhw_none.rd_ghi.can_calculate


@pytest.mark.skip('Module is currently commented / not needed')
def test_create_horizontal_fails_beam(fhw_beam):
    # with pytest.warns(UserWarning, match='Cannot calculate radiation components with only 1'):
    vp.config_virtuals_radiation(fhw_beam)
    assert not fhw_beam.rd_ghi.can_calculate


@pytest.mark.skip('Module is currently commented / not needed')
def test_create_horizontal_fails_diffuse(fhw_diffuse):
    # with pytest.warns(UserWarning, match='Cannot calculate radiation components with only 1'):
    vp.config_virtuals_radiation(fhw_diffuse)
    assert not fhw_diffuse.rd_ghi.can_calculate


@pytest.mark.skip('Module is currently commented / not needed')
def test_create_horizontal_fails_dni(fhw_dni):
    # with pytest.warns(UserWarning, match='Cannot calculate radiation components with only 1'):
    vp.config_virtuals_radiation(fhw_dni)
    assert not fhw_dni.rd_ghi.can_calculate


# TODO add array tests


## Horizontal conversion tests
# ---------------------------

@pytest.mark.skip('Module is currently commented / not needed')
def test_horizontal_conversion__global(fhw_global):
    use_dataframe(fhw_global, fhw_global.context.df,
                  calculate_virtuals=False)

    vp.config_virtuals_ambient(fhw_global)
    vp.config_virtuals_radiation(fhw_global)
    vp.calculate_virtuals_ambient(fhw_global)
    vp.calculate_virtuals_radiation(fhw_global)


@pytest.mark.skip('Module is currently commented / not needed')
def test_horizontal_conversion__beam_diffuse(fhw_beam_diffuse):
    use_dataframe(fhw_beam_diffuse, fhw_beam_diffuse.context.df,
                  calculate_virtuals=False)

    vp.config_virtuals_ambient(fhw_beam_diffuse)
    vp.config_virtuals_radiation(fhw_beam_diffuse)

    vp.calculate_virtuals_ambient(fhw_beam_diffuse)
    vp.calculate_virtuals_radiation(fhw_beam_diffuse)


@pytest.mark.skip('Module is currently commented / not needed')
def test_horizontal_conversion__global_beam_diffuse(fhw_global_beam_diffuse):
    use_dataframe(fhw_global_beam_diffuse, fhw_global_beam_diffuse.context.df,
                  calculate_virtuals=False)
    vp.config_virtuals_ambient(fhw_global_beam_diffuse)
    vp.config_virtuals_radiation(fhw_global_beam_diffuse)

    vp.calculate_virtuals_ambient(fhw_global_beam_diffuse)
    vp.calculate_virtuals_radiation(fhw_global_beam_diffuse)


## Tilted / Array tests

def test_array_radiation_feedthrough_global_only(fhw):
    # Array radiation conversion with 'feedthrough' strategy
    a = fhw.arrays[0]
    a.in_beam = None
    a.in_diffuse = None
    a.in_dni = None

    # Test config
    virtuals.config_virtuals(fhw)

    assert a.rd_gti.can_calculate
    assert not a.rd_bti.can_calculate
    assert not a.rd_dti.can_calculate

    # Test calculation
    use_csv(fhw, csv_files=sunpeek.demo.DEMO_DATA_PATH_2DAYS, timezone='UTC', datetime_template='year_month_day',
            calculate_virtuals=False)
    vp.calculate_virtuals_ambient(fhw)
    va.calculate_virtuals_ambient(a)
    va.calculate_virtuals_radiation(a, 'feedthrough')

    assert a.rd_gti.data.notna().sum() == 2880
    assert a.iam.data.notna().sum() == 2880


def test_array_irradiance_feedthrough__beam_diffuse(fhw):
    # Array radiation conversion with 'feedthrough' strategy
    a = fhw.arrays[0]
    a.in_global = None

    # Test config
    virtuals.config_virtuals(fhw)

    assert a.rd_gti.can_calculate
    assert a.rd_bti.can_calculate
    assert a.rd_dti.can_calculate

    # Test calculation
    use_csv(fhw, csv_files=sunpeek.demo.DEMO_DATA_PATH_2DAYS, timezone='UTC', datetime_template='year_month_day',
            calculate_virtuals=False)
    vp.calculate_virtuals_ambient(fhw)
    va.calculate_virtuals_ambient(a)
    va.calculate_virtuals_radiation(a, 'feedthrough')

    assert a.rd_gti.data.notna().sum() >= 2878

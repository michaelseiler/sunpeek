"""Tests creation and calculation of virtual sensors.
Calculation: Tests creation and mapping of virtual sensor as well as everything that happens after call to
virtuals.calculate_virtuals(Plant).
API call triggering call to virtuals.calculate_virtuals(Plant) is not included here.
"""
import pandas as pd
import numpy as np
import pytest
import json
import time

from sunpeek.data_handling.wrapper import use_csv, use_dataframe
from sunpeek.core_methods.virtuals.main import config_virtuals
import sunpeek.demo
from sunpeek.common import config_parser
import sunpeek.common.unit_uncertainty as uu
from sunpeek.common.utils import DatetimeTemplates
import sunpeek.components as cmp
from sunpeek.components.fluids import WPDFluidPure
import sunpeek.components.sensor_types as st
from sunpeek.components.helpers import IsVirtual
from sunpeek.components.base import SensorSlot
from sunpeek.serializable_models import CoreMethodFeedback
from sunpeek.definitions.fluid_definitions import get_definition
import sunpeek.core_methods.virtuals as virtuals
import sunpeek.core_methods.virtuals.virtuals_plant as vp
import sunpeek.core_methods.virtuals.virtuals_array as va


@pytest.mark.skip('Execution time test only')
def test_time_config_virtuals(fhw):
    N = 100
    start_time = time.time()
    for i in range(N):
        config_virtuals(fhw)
    elapsed_time = time.time() - start_time
    print(elapsed_time / N)


@pytest.fixture
def fhw_local(fhw):
    use_csv(fhw, csv_files=sunpeek.demo.DEMO_DATA_PATH_2DAYS, timezone='UTC', calculate_virtuals=False,
            datetime_template=DatetimeTemplates.year_month_day)
    return fhw


@pytest.fixture()
def fhw_vs_extra_slot(fhw_local):
    fhw_local.sensor_slots['abc'] = SensorSlot('abc', st.global_radiation, 'Some name', IsVirtual.possible)
    fhw_local.arrays[0].sensor_slots['abc'] = SensorSlot('abc', st.fluid_temperature, 'Some name', IsVirtual.possible)
    yield fhw_local

    # Clean up
    try:
        fhw_local.sensors['abc'].remove_references()
        fhw_local.arrays[0].sensors['abc'].remove_references()
    except KeyError:
        pass
    del fhw_local.sensor_slots['abc']
    del fhw_local.arrays[0].sensor_slots['abc']


def test_fluid_numerics(fhw_fluid):
    assert isinstance(fhw_fluid, WPDFluidPure)

    te = uu.to_s(np.linspace(-10, 110, 13), 'degC')

    rho = fhw_fluid.get_density(te)
    assert rho.iloc[-1].to('kg m**-3').m == pytest.approx(979.96, abs=1)

    cp = fhw_fluid.get_heat_capacity(te)
    assert cp.iloc[-1].to('J kg**-1 K**-1').m == pytest.approx(3972.43, abs=1)


def test_real_sensors(fhw_local):
    assert isinstance(fhw_local.te_amb, cmp.Sensor)
    assert isinstance(fhw_local.te_amb.data, pd.Series)
    assert isinstance(fhw_local.tp, cmp.Sensor)
    assert fhw_local.tp.is_virtual
    assert isinstance(fhw_local.vf, cmp.Sensor)
    assert isinstance(fhw_local.vf.data, pd.Series)
    assert isinstance(fhw_local.time_index, pd.DatetimeIndex)
    assert isinstance(fhw_local.te_in, cmp.Sensor)
    assert not fhw_local.te_in.is_virtual
    assert isinstance(fhw_local.te_in.data, pd.Series)
    assert isinstance(fhw_local.te_out.data, pd.Series)


def test_add_virtual_sensor(fhw_vs_extra_slot):
    fhw_vs_extra_slot.map_vsensor('abc', CoreMethodFeedback())
    assert isinstance(fhw_vs_extra_slot.abc, cmp.Sensor)
    assert fhw_vs_extra_slot.abc.is_virtual
    assert fhw_vs_extra_slot.abc.sensor_type.name == 'global_radiation'
    assert fhw_vs_extra_slot.abc.plant.name == fhw_vs_extra_slot.name

    fhw_vs_extra_slot.arrays[0].map_vsensor('abc', CoreMethodFeedback())
    assert fhw_vs_extra_slot.arrays[0].abc.is_virtual
    assert fhw_vs_extra_slot.arrays[0].abc.sensor_type.name == 'fluid_temperature'


def test_setting_vsensor_raises(fhw_local):
    p = fhw_local
    virtuals.calculate_virtuals(p)
    with pytest.warns(match='cannot set a virtual sensor directly'):
        p.tp = None
    with pytest.warns(match='cannot set a virtual sensor directly'):
        p.arrays[0].rd_bti = None


@pytest.fixture(scope='module')
def fhw__virtual_error(resources_dir):
    with open(sunpeek.demo.DEMO_CONFIG_PATH) as f:
        conf = json.load(f)
        conf['plant']['rh_amb'] = 'xx__virtual'
        conf['plant']['raw_sensors'][1]['raw_name'] = 'xx__virtual'
    plant, sensors, c = config_parser.make_full_plant(conf)

    # Set sensor values from csv / dataframe
    csv_fn = 'FHW_test_virtual.csv'
    df = pd.read_csv(resources_dir + '/data/' + csv_fn, sep=';', index_col=0, parse_dates=True)

    use_dataframe(plant, df, timezone='UTC')
    return plant


def test_plant_ambient(fhw_local):
    vp.calculate_virtuals_ambient(fhw_local)

    _assert_valid_sensor(fhw_local.sun_azimuth)
    _assert_valid_sensor(fhw_local.sun_zenith)
    _assert_valid_sensor(fhw_local.sun_apparent_zenith)
    _assert_valid_sensor(fhw_local.sun_elevation)
    _assert_valid_sensor(fhw_local.sun_apparent_elevation)

    # Temporarily commented because vsensor code is also commented, see virtuals_plant
    # _assert_valid_sensor(fhw_global.te_dew_amb)
    # _assert_valid_sensor(fhw_global.rd_dni_extra)
    # _assert_valid_sensor(fhw_global.rel_airmass)
    # _assert_valid_sensor(fhw_global.abs_airmass)
    # _assert_valid_sensor(fhw_global.linke_turbidity)
    # _assert_valid_sensor(fhw_global.rd_ghi_clearsky)
    # _assert_valid_sensor(fhw_global.rd_dni_clearsky)


def _assert_valid_sensor(s, type_name=None, test_virtual=True, compatible_unit=None):
    assert s is not None
    assert isinstance(s, cmp.Sensor)
    assert isinstance(s.data, pd.Series)
    assert s.data.dtype.__str__()[:4] == 'pint'
    if type_name is not None:
        assert s.sensor_type.name == type_name
    if test_virtual:
        assert s.is_virtual
    if compatible_unit is not None:
        uu.assert_compatible(s.data.pint.units.__str__(), compatible_unit)


# def test_plant_power(fhw_global, fhw_fluid):
def test_plant_power(fhw_local):
    """Test creation and calculation process of Plant.tp virtual sensor."""
    p = fhw_local
    assert p.tp is not None
    assert isinstance(p.tp, cmp.Sensor)
    assert p.tp.is_virtual

    vp.calculate_virtuals_power(p)
    _assert_valid_sensor(p.tp, type_name='thermal_power')
    _assert_valid_sensor(p.mf, type_name='mass_flow')


def test_array_power(fhw_local, fhw_fluid):
    """Test creation and calculation process of Array.tp virtual sensor."""
    fhw_local.fluid_solar = fhw_fluid
    a = fhw_local.arrays[0]

    assert a.tp is not None
    assert isinstance(a.tp, cmp.Sensor)
    assert a.tp.is_virtual

    va.calculate_virtuals_power(a)
    _assert_valid_sensor(a.tp, type_name='thermal_power')
    _assert_valid_sensor(a.mf, type_name='mass_flow')


def test_array_aoi(fhw_local):
    """Test creationg and calculation process of Array aoi virtual sensor."""
    # Requires calculation of solar position on Plant
    vp.calculate_virtuals_ambient(fhw_local)

    arr = fhw_local.arrays[0]
    is_shadowed_virtual = arr.is_shadowed is None
    va.calculate_virtuals_ambient(arr)

    _assert_valid_sensor(arr.aoi)
    _assert_valid_sensor(arr.is_shadowed, test_virtual=is_shadowed_virtual)
    _assert_valid_sensor(arr.internal_shading_ratio)
    _assert_valid_sensor(arr.shadow_angle)
    _assert_valid_sensor(arr.shadow_angle_midpoint)


def test_array_temperature(fhw_local):
    arr = fhw_local.arrays[0]
    va.calculate_virtuals_temperature(arr)
    _assert_valid_sensor(arr.te_op, compatible_unit='degC')
    _assert_valid_sensor(arr.te_op_deriv, compatible_unit='K/s')


def test_array_radiation_feedthrough(fhw_local):
    """Tests array radiation conversion with 'feedthrough' strategy."""
    vp.calculate_virtuals_ambient(fhw_local)

    arr = fhw_local.arrays[0]
    va.calculate_virtuals_ambient(arr)

    va.calculate_virtuals_radiation(arr)
    _assert_valid_sensor(arr.rd_gti)
    _assert_valid_sensor(arr.rd_bti)
    _assert_valid_sensor(arr.rd_dti)


def test_coolprop_fluid_returns_nan(fhw_coll, fhw__nofluid__nocoll):
    # Test CoolPropFluid behavior with allowed temperature range.
    p = fhw__nofluid__nocoll
    fluid = cmp.CoolPropFluid(get_definition('Antifrogen L'), concentration=uu.Q(40, 'percent'))
    p.fluid_solar = fluid
    use_csv(p, csv_files=sunpeek.demo.DEMO_DATA_PATH_2DAYS, timezone='UTC', calculate_virtuals=True,
            datetime_template=DatetimeTemplates.year_month_day)
    te_mean = 0.5 * (p.te_in.data + p.te_out.data)

    # Make sure fluid properties are not Inf: _some_ temperatures exceed allowed range
    t_test1 = pd.to_datetime('2017-05-01 10:27:00+00:00')  # within allowed temperature range
    t_test2 = pd.to_datetime('2017-05-01 10:28:00+00:00')  # outside allowed temperature range
    te_test = pd.Series(index=[t_test1, t_test2], data=te_mean.loc[t_test1:t_test2]).astype('pint[kelvin]')
    cp = uu.to_numpy(fluid.get_heat_capacity(te_test), 'J kg**-1 K**-1')
    assert np.isfinite(cp[0])
    assert np.isinf(cp[1])

    # Make sure fluid properties are Inf: _all_ temperatures exceed allowed range
    t_test1 = pd.to_datetime('2017-05-01 10:28:00+00:00')  # outside allowed temperature range
    t_test2 = pd.to_datetime('2017-05-01 10:29:00+00:00')  # outside allowed temperature range
    te_test = pd.Series(index=[t_test1, t_test2], data=te_mean.loc[t_test1:t_test2]).astype('pint[kelvin]')
    cp = uu.to_numpy(fluid.get_heat_capacity(te_test), 'J kg**-1 K**-1')
    assert np.isinf(cp).all()

    # Make sure virtual sensor calculation result is not Inf but NaN
    tp = uu.to_numpy(p.tp.data.loc[t_test1:t_test2], 'W')
    assert np.isnan(tp).all()


def test_virtuals(fhw_local):
    # Validate numeric correctness & units of solar angles
    p = fhw_local
    virtuals.calculate_virtuals(p)

    assert not np.isnan(p.arrays[0].tp.data[0])

    t = pd.to_datetime("2017-05-01 12:00:00").tz_localize(tz=p.tz_data)
    assert str(p.sun_azimuth.native_unit) == 'degree'
    assert str(p.sun_zenith.native_unit) == 'degree'
    assert str(p.sun_apparent_zenith.native_unit) == 'degree'
    assert str(p.sun_elevation.native_unit) == 'degree'
    assert str(p.sun_apparent_elevation.native_unit) == 'degree'

    assert p.sun_azimuth.data[t].to('deg').m == pytest.approx(182, 1)
    assert p.sun_zenith.data[t].to('deg').m == pytest.approx(32, 1)
    assert p.sun_apparent_zenith.data[t].to('deg').m == pytest.approx(32, 1)
    assert p.sun_elevation.data[t].to('deg').m == pytest.approx(58, 1)
    assert p.sun_apparent_elevation.data[t].to('deg').m == pytest.approx(58, 1)
# def test_solar_angles(fhw__2days_data):
#     # Validate numeric correctness & units of solar angles
#     p = fhw__2days_data
#     t = pd.to_datetime("2017-05-01 12:00:00").tz_localize(tz=p.tz_data)
#
#     assert str(p.sun_azimuth.native_unit) == 'degree'
#     assert str(p.sun_zenith.native_unit) == 'degree'
#     assert str(p.sun_apparent_zenith.native_unit) == 'degree'
#     assert str(p.sun_elevation.native_unit) == 'degree'
#     assert str(p.sun_apparent_elevation.native_unit) == 'degree'
#
#     assert p.sun_azimuth.data[t].to('deg').m == pytest.approx(182, 1)
#     assert p.sun_zenith.data[t].to('deg').m == pytest.approx(32, 1)
#     assert p.sun_apparent_zenith.data[t].to('deg').m == pytest.approx(32, 1)
#     assert p.sun_elevation.data[t].to('deg').m == pytest.approx(58, 1)
#     assert p.sun_apparent_elevation.data[t].to('deg').m == pytest.approx(58, 1)


# def test_array_tp(fhw__2days_data):
#     assert not np.isnan(fhw__2days_data.arrays[0].tp.data[0])


@pytest.mark.skip(reason='Temporary to speed up tests')
def test_full(fhw_local):
    virtuals.config_virtuals(fhw_local)
    virtuals.calculate_virtuals(fhw_local)

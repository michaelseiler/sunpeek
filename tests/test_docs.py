import pytest
import pathlib
import os
import subprocess
import shutil
# import sunpeek.common.utils


@pytest.mark.skip('This test passes even if docs do not build correctly')
def test_docs_build(tests_root):
    docs_path = os.path.join(pathlib.Path(tests_root).parent, 'docs')
    try:
        shutil.rmtree(os.path.join(docs_path, 'source', '_autosummary'))
    except FileNotFoundError:
        pass

    subprocess.check_output(['sphinx-build', "-b", "html",
                             os.path.join(docs_path, 'source'),
                             os.path.join(docs_path, 'build'),
                             ])

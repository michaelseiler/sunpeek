# Goal of this module is to test that typical config changes on a plant actually reset
# the plant calculation_uptodate flag.

import pytest
from fastapi.testclient import TestClient
import random, string

from sunpeek.api.main import app
from sunpeek.common import utils
import sunpeek.components as cmp
from tests.tests_integration.conftest import plant_with_data

client = TestClient(app)


@pytest.mark.xdist_group(name="integration")
def test_new_plant(session_fixture, random_stored_plant):
    # Test that new plant without data is not uptodate
    _, p, pid = random_stored_plant
    p = session_fixture.query(cmp.Plant).filter(cmp.Plant.id == pid).one()
    assert not p.virtuals_calculation_uptodate

    # Make sure PlantSummary carries the calculation_uptodate flag
    url = f"plants/{pid}/summary"
    response = client.get(url, params={'token': utils.API_TOKEN})
    assert response.status_code == 200
    assert 'virtuals_calculation_uptodate' in response.json()
    # Still not uptodate, because no data uploaded
    assert not response.json()['virtuals_calculation_uptodate']


## Test demo plant --------------------------------

@pytest.fixture
def random_demo_name():
    return ''.join(random.choice(string.ascii_lowercase) for i in range(8))


@pytest.mark.xdist_group(name="integration")
def test_create_demo_plant_no_data(session_fixture, random_demo_name):
    # app.dependency_overrides = {}

    params = {'token': utils.API_TOKEN,
              'include_data': False,  # <--
              'accept_license': True,
              'name': random_demo_name}
    url = f"plants/create_demo_plant"
    response = client.get(url, params=params)

    assert response.status_code == 200
    assert not response.json()['virtuals_calculation_uptodate']


@pytest.mark.xdist_group(name="integration")
def test_create_demo_plant_with_data(session_fixture, random_demo_name):
    # app.dependency_overrides = {}

    url = f"plants/create_demo_plant"
    params = {'token': utils.API_TOKEN,
              'include_data': True,  # <--
              'accept_license': True,
              'name': random_demo_name}
    response = client.get(url, params=params)

    assert response.status_code == 200
    # Data upload in demo triggers updates virtual sensors
    assert response.json()['virtuals_calculation_uptodate']


## Test common API endpoints on plant with data  --------------------------------


@pytest.mark.xdist_group(name="integration")
def test_update_plant(session_fixture, plant_with_data):
    # Update plant
    p, pid = plant_with_data

    url = f"plants/{pid}?token={utils.API_TOKEN}"
    response = client.post(url, json={"latitude": {"magnitude": 40, "units": "deg"}})
    assert response.status_code == 200
    assert response.json()['latitude']['magnitude'] == 40

    # Update invalidated virtual sensors
    session_fixture.refresh(p)
    assert not p.virtuals_calculation_uptodate


@pytest.mark.xdist_group(name="integration")
def test_update_plant_fluid(session_fixture, plant_with_data):
    # Update fluid
    p, pid = plant_with_data
    url = f"plants/{pid}?token={utils.API_TOKEN}"
    assert p.fluid_solar.name == 'Pekasolar_FHW'
    response = client.post(url, json={"fluid_solar": {"fluid": "Gasokol, corroStar mixture"}})

    assert response.status_code == 200
    assert response.json()['fluid_solar']['name'] == 'Gasokol, corroStar mixture'

    # Update invalidated virtual sensors
    session_fixture.refresh(p)
    assert not p.virtuals_calculation_uptodate


@pytest.mark.xdist_group(name="integration")
def test_update_array(session_fixture, plant_with_data):
    # Update array
    p, pid = plant_with_data
    a = session_fixture.query(cmp.Array).filter(cmp.Array.plant_id == pid).one()
    url = f"plants/-/arrays/{a.id}?token={utils.API_TOKEN}"
    response = client.post(url, json={"area_gr": {"magnitude": 1000, "units": "m**2"}})
    assert response.status_code == 200

    # Update invalidated virtual sensors
    session_fixture.refresh(p)
    assert not p.virtuals_calculation_uptodate
    assert not a.plant.virtuals_calculation_uptodate

    # Updating array has no effect, as expected
    session_fixture.refresh(a)
    assert not a.plant.virtuals_calculation_uptodate


@pytest.mark.xdist_group(name="integration")
def test_update_sensor(session_fixture, plant_with_data):
    # Update sensor
    p, pid = plant_with_data
    url = f'plants/{pid}/sensors'
    params = {'token': utils.API_TOKEN,
              'raw_name': 'te_amb'}
    response = client.get(url, params=params)

    assert response.status_code == 200
    sid = response.json()['id']

    url = f"plants/-/sensors/{sid}?token={utils.API_TOKEN}"
    response = client.post(url, json={"native_unit": "degC"})    # <-- change sensor native_unit
    assert response.status_code == 200

    # Update invalidated virtual sensors
    session_fixture.refresh(p)
    assert not p.virtuals_calculation_uptodate


@pytest.mark.xdist_group(name="integration")
def test_update_sensors_bulk(session_fixture, plant_with_data):
    # Update multiple sensors
    p, pid = plant_with_data
    url = f"/plants/-/sensors?token={utils.API_TOKEN}"
    conf = [{"id": p.te_amb.id, "raw_name": "test updated"},
            {"id": p.rh_amb.id, "native_unit": "percent"}]
    response = client.post(url, json=conf)
    assert response.status_code == 200

    # Update invalidated virtual sensors
    session_fixture.refresh(p)
    assert not p.virtuals_calculation_uptodate


@pytest.mark.xdist_group(name="integration")
def test_delete_sensor(session_fixture, plant_with_data):
    # Delete sensor
    p, pid = plant_with_data
    sid = p.te_amb.id
    url = f"/plants/-/sensors/{sid}?token={utils.API_TOKEN}"
    response = client.delete(url)
    assert response.status_code == 200

    response = client.get(url)
    assert response.status_code == 404

    # Update invalidated virtual sensors
    session_fixture.refresh(p)
    assert not p.virtuals_calculation_uptodate


@pytest.mark.xdist_group(name="integration")
def test_delete_sensors_bulk(session_fixture, plant_with_data):
    # Delete multiple / all sensors
    p, pid = plant_with_data
    url = f"/plants/-/sensors?token={utils.API_TOKEN}"
    response = client.get(url)
    assert response.status_code == 200

    sensor_ids = [str(item["id"]) for item in response.json()]
    url = f"/plants/{pid}/sensors?token={utils.API_TOKEN}"
    response = client.delete(url, params={"ids": sensor_ids})
    assert response.status_code == 200

    # Update invalidated virtual sensors
    session_fixture.refresh(p)
    assert not p.virtuals_calculation_uptodate


@pytest.mark.xdist_group(name="integration")
def test_update_collector(session_fixture, plant_with_data):
    # Update collector
    p, pid = plant_with_data
    cid = p.arrays[0].collector.id
    url = f"/config/collectors/{cid}?token={utils.API_TOKEN}"
    response = client.get(url)
    assert response.status_code == 200

    collector = response.json()
    collector["product_name"] = "Test Name"
    url = f"/config/collectors/{cid}?token={utils.API_TOKEN}"
    response = client.post(url, json=collector)
    assert response.status_code == 200
    assert response.json()["product_name"] == "Test Name"

    # Update invalidated virtual sensors
    session_fixture.refresh(p)
    assert not p.virtuals_calculation_uptodate


@pytest.mark.xdist_group(name="integration")
def test_delete_collector(session_fixture, plant_with_data):
    # When trying to delete a collector that is in use (conflit), make sure calculation_updtodate flag is untouched
    p, pid = plant_with_data
    cid = p.arrays[0].collector.id
    url = f"/config/collectors/{cid}?token={utils.API_TOKEN}"
    response = client.get(url)
    assert response.status_code == 200

    url = f"/config/collectors/{cid}?token={utils.API_TOKEN}"
    response = client.delete(url)
    assert response.status_code == 409

    # Update invalidated virtual sensors
    session_fixture.refresh(p)
    assert p.virtuals_calculation_uptodate

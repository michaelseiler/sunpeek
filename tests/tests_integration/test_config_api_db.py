import pytest
import copy
import json
from fastapi.testclient import TestClient

from sunpeek.api.main import app
from sunpeek.common import utils
import sunpeek.components as cmp

client = TestClient(app)


@pytest.mark.xdist_group(name="integration")
def test_get_collectors(session_fixture):
    app.dependency_overrides = {}

    request_url = f"config/collectors?token={utils.API_TOKEN}"
    response = client.get(request_url)
    assert response.status_code == 200

    # check that data is valid as well (only checks some of the data)
    collector_list = response.json()
    assert len(collector_list) >= 21

    coll_idx = [c['name'] for c in collector_list].index('Arcon 3510')
    coll = collector_list[coll_idx]

    assert coll["name"] == "Arcon 3510"
    assert coll["test_type"] == "QDT"
    assert coll["test_reference_area"] == "gross"
    assert coll["gross_length"]["magnitude"] == 2272.0
    assert coll["gross_length"]["units"] == "mm"
    assert coll["manufacturer_name"] == "Arcon-Sunmark A/S"
    assert coll["product_name"] == "HTHEATstore 35/10"
    assert coll["test_report_id"] == "6P02267-C-Rev 1 (2016-07-06), 4P04266-C-Rev 2 (2015-11-10)"
    assert coll["licence_number"] == "SP SC0843-14"
    assert coll["certificate_date_issued"] == "2016-07-14T00:00:00"
    assert coll["certificate_lab"] == "SP Technical Research Institute of Sweden"
    assert "certificate_details" in coll
    assert coll["area_gr"]["magnitude"] == 13.57
    assert type(coll["a1"]["magnitude"]) is float
    assert type(coll["a2"]["magnitude"]) is float
    assert type(coll["a5"]["magnitude"]) is float
    # Make sure collector parameter got correctly converted to default unit (J/m²K for a5)
    assert coll["a5"]["magnitude"] == 7313


@pytest.mark.xdist_group(name="integration")
def test_api_delete_collector(session_fixture, random_stored_plant):
    # Deleting collector is allowed if it is not used by any array.
    pname, p, pid = random_stored_plant
    p = session_fixture.query(cmp.Plant).filter(cmp.Plant.id == pid).one()

    cid = p.arrays[0].collector.id
    assert cid != 9
    cid = 9

    request_url = f"config/collectors/{cid}?token={utils.API_TOKEN}"
    response = client.delete(request_url)
    assert response.status_code == 204

    request_url = f"config/collectors/{cid}?plant_id=0&token={utils.API_TOKEN}"
    response = client.get(request_url)
    assert response.status_code == 404


@pytest.mark.xdist_group(name="integration")
def test_api_delete_collector_fails_if_in_use(session_fixture, random_stored_plant):
    # Deleting collector used in array fails
    # This is because collectors can be used by many arrays and plants, and it's easy to forget about that.
    pname, p, pid = random_stored_plant
    p = session_fixture.query(cmp.Plant).filter(cmp.Plant.id == pid).one()

    cid = p.arrays[0].collector.id
    request_url = f"config/collectors/{cid}?token={utils.API_TOKEN}"
    response = client.delete(request_url)

    expected = 'Cannot delete collector.'
    assert response.status_code == 409
    assert response.json()['error'] == expected


@pytest.mark.xdist_group(name="integration")
def test_add_collector_SST(session_fixture):
    request_url = f"/config/collectors/new?token={utils.API_TOKEN}"
    collector = {
        "name": "TIGI HC1-A",
        "product_name": "TIGI HC1-A",
        "manufacturer_name": "TIGI LTD.",
        "licence_number": "011-7S2636 F",
        "collector_type": "flat_plate",
        "gross_width": {"magnitude": "1030", "units": "mm"},
        "gross_length": {"magnitude": "2027", "units": "mm"},
        "gross_height": {"magnitude": "195", "units": "mm"},
        "area_gr": {"magnitude": "2.09", "units": "m²"},
        "test_reference_area": "gross",
        "test_type": "SST",
        "eta0hem": {"magnitude": "0.678", "units": ""},
        "a1": {"magnitude": "1.900", "units": "W/m²/K"},
        "a2": {"magnitude": "0.009", "units": "W/m²/K²"},
        "a8": {"magnitude": "0", "units": "W/m²/K**4"},
        "ceff": {"magnitude": "6100", "units": "J/m²/K"},
        "iam_method": {"method_type": "IAM_Interpolated",
                       "aoi_reference": {"magnitude": [[0, 10, 20, 30, 40, 50, 60, 70, 80, 90],
                                                       [0, 10, 20, 30, 40, 50, 60, 70, 80, 90]], "units": "°"},
                       "iam_reference": {
                           "magnitude": [[1, "0.99", "0.98", "0.96", "0.93", "0.88", "0.78", "0.65", "0.38", 0],
                                         [1, "1.00", "0.99", ".98", ".95", ".89", ".8", ".65", "0.40", 0]],
                           "units": ""}},
    }
    response = client.post(request_url, json=collector)
    response.json()

    assert response.status_code == 201


@pytest.mark.xdist_group(name="integration")
def test_add_collector_SST_IAMk50(session_fixture):
    request_url = f"/config/collectors/new?token={utils.API_TOKEN}"
    collector = {
        "name": "HT-SA 28-10",
        "product_name": "HT-SA 28-10",
        "manufacturer_name": "ARCON Solar A/S",
        "licence_number": "011-7S1520 F",
        "collector_type": "flat_plate",
        "gross_width": {"magnitude": "5965", "units": "mm"},
        "gross_length": {"magnitude": "2275", "units": "mm"},
        "gross_height": {"magnitude": "140", "units": "mm"},
        "area_gr": {"magnitude": "13.57", "units": "m²"},
        "area_ap": {"magnitude": "12.52", "units": "m²"},
        "test_reference_area": "aperture",
        "test_type": "SST",
        "eta0hem": {"magnitude": "0.804", "units": ""},
        "a1": {"magnitude": "2.564", "units": "W/m²/K"},
        "a2": {"magnitude": "0.005", "units": "W/m²/K²"},
        "a8": {"magnitude": "0", "units": "W/m²/K**4"},
        "ceff": {"magnitude": "6880", "units": "J/m²/K"},
        "iam_method": {"method_type": "IAM_K50", "k50": {"magnitude": "0.94", "units": ""}},
    }
    response = client.post(request_url, json=collector)
    response.json()

    assert response.status_code == 201


@pytest.mark.xdist_group(name="integration")
def test_add_collector_SST_ASHRAE(session_fixture):
    request_url = f"/config/collectors/new?token={utils.API_TOKEN}"
    collector = {
        "name": "HT-SA 28-10 TEST",
        "product_name": "HT-SA 28-10 TEST",
        "manufacturer_name": "ARCON Solar A/S",
        "licence_number": "TEST: 011-7S1520 F",
        "collector_type": "flat_plate",
        "gross_width": {"magnitude": "5965", "units": "mm"},
        "gross_length": {"magnitude": "2275", "units": "mm"},
        "gross_height": {"magnitude": "140", "units": "mm"},
        "area_gr": {"magnitude": "13.57", "units": "m²"},
        "area_ap": {"magnitude": "12.52", "units": "m²"},
        "test_reference_area": "aperture",
        "test_type": "SST",
        "eta0hem": {"magnitude": "0.804", "units": ""},
        "a1": {"magnitude": "2.564", "units": "W/m²/K"},
        "a2": {"magnitude": "0.005", "units": "W/m²/K²"},
        "a8": {"magnitude": "0", "units": "W/m²/K**4"},
        "ceff": {"magnitude": "6880", "units": "J/m²/K"},
        "iam_method": {"method_type": "IAM_ASHRAE", "b": {"magnitude": "0.4", "units": ""}},
    }
    response = client.post(request_url, json=collector)
    response.json()

    assert response.status_code == 201


@pytest.mark.xdist_group(name="integration")
def test_add_collector_SST_QDT(session_fixture):
    request_url = f"/config/collectors/new?token={utils.API_TOKEN}"
    collector = {
        "name": "ökoTech GS",
        "product_name": "ökoTech GS",
        "manufacturer_name": "ökoTech Solarkollektoren GmbH",
        "licence_number": "011-7S2918 F",
        "collector_type": "flat_plate",
        "gross_width": {"magnitude": "7173", "units": "mm"},
        "gross_length": {"magnitude": "2353", "units": "mm"},
        "gross_height": {"magnitude": "127", "units": "mm"},
        "area_gr": {"magnitude": "16.96", "units": "m²"},
        "area_ap": {"magnitude": "15.23", "units": "m²"},
        "test_reference_area": "gross",
        "test_type": "QDT",
        "eta0b": {"magnitude": "0.679", "units": ""},
        "a1": {"magnitude": "3.65", "units": "W/m²/K"},
        "a2": {"magnitude": "0.007", "units": "W/m²/K²"},
        "a8": {"magnitude": "0", "units": "W/m²/K**4"},
        "a5": {"magnitude": "23299", "units": "J/m²/K"},
        "kd": {"magnitude": "0.93", "units": ""},
        "iam_method": {"method_type": "IAM_Interpolated",
                       "aoi_reference": {"magnitude": [[0, 10, 20, 30, 40, 50, 60, 70, 80, 90],
                                                       [0, 10, 20, 30, 40, 50, 60, 70, 80, 90]], "units": "°"},
                       "iam_reference": {
                           "magnitude": [[1, "1.00", "1.00", "0.99", "0.98", "0.97", "0.95", "0.90", "0.45", 0],
                                         [1, "1.00", "1.00", "0.99", "0.98", "0.97", "0.95", "0.90", "0.45", 0]],
                           "units": ""}},
    }
    response = client.post(request_url, json=collector)
    response.json()

    assert response.status_code == 201


@pytest.mark.xdist_group(name="integration")
def test_update_collector(session_fixture):
    request_url = f"/config/collectors?token={utils.API_TOKEN}"
    response = client.get(request_url)
    assert response.status_code == 200
    cid = response.json()[0]['id']

    request_url = f"/config/collectors/{cid}?token={utils.API_TOKEN}"
    response = client.get(request_url)
    assert response.status_code == 200

    collector = response.json()
    original_name = collector["product_name"]
    collector["product_name"] = "Test Name"

    request_url = f"/config/collectors/{cid}?token={utils.API_TOKEN}"
    response = client.post(request_url, json=collector)
    assert response.status_code == 200
    assert response.json()["product_name"] == "Test Name"

    request_url = f"/config/collectors/{cid}?token={utils.API_TOKEN}"
    response = client.get(request_url)
    assert response.status_code == 200
    assert response.json()["product_name"] == "Test Name"

    # revert back to original one
    collector = response.json()
    collector["product_name"] = original_name
    request_url = f"/config/collectors/{cid}?token={utils.API_TOKEN}"
    response = client.post(request_url, json=collector)
    assert response.status_code == 200


@pytest.fixture
def collector_with_ref_area_aperture(session_fixture):
    request_url = f"/config/collectors"
    params = {'token': utils.API_TOKEN,
              'name': 'Greenonetec 3803'}
    response = client.get(request_url, params=params)
    assert response.status_code == 200

    collector = response.json()
    assert 'aperture_parameters' in collector
    assert collector['test_reference_area'] == 'aperture'

    return collector, collector['id']


@pytest.mark.xdist_group(name="integration")
def test_aperture_params_updated(session_fixture, collector_with_ref_area_aperture):
    # Get collector with ref area "aperture", update area_gr, see if parameters updated
    collector, cid = collector_with_ref_area_aperture

    # Assert collector parameters before update
    assert collector['area_gr']['magnitude'] == 7.91
    assert collector['aperture_parameters']['a1']['magnitude'] == 2.102
    # Make sure unit got converted to collector parameter default unit also for aperture params,
    # see serializable_models pydantic validator
    assert collector['aperture_parameters']['a5']['magnitude'] == 9664
    assert collector['a1']['magnitude'] == pytest.approx(2.102 * 7.41 / 7.91)
    old_collector = copy.deepcopy(collector)

    try:
        # Update gross area
        collector['area_gr']['magnitude'] = 10
        request_url = f"/config/collectors/{cid}?token={utils.API_TOKEN}"
        response = client.post(request_url, json=collector)
        assert response.status_code == 200

        # Assert parameters updated
        collector = response.json()
        assert collector["area_gr"]['magnitude'] == 10
        assert collector['aperture_parameters']['a1']['magnitude'] == 2.102
        assert collector['a1']['magnitude'] == pytest.approx(2.102 * 7.41 / 10)

    finally:
        # Undo collector update
        request_url = f"/config/collectors/{cid}?token={utils.API_TOKEN}"
        response = client.post(request_url, json=old_collector)
        assert response.status_code == 200
        collector = response.json()
        assert collector['area_gr']['magnitude'] == 7.91
        assert collector['aperture_parameters']['a1']['magnitude'] == 2.102
        assert collector['a1']['magnitude'] == pytest.approx(2.102 * 7.41 / 7.91)


@pytest.mark.xdist_group(name="integration")
def test_aperture_params_deleted_if_ref_area_changed(session_fixture, collector_with_ref_area_aperture):
    # Get collector with ref area "aperture", assert aperture params dict is set, change to "gross", assert is deleted.
    collector, cid = collector_with_ref_area_aperture

    # Assert collector parameters before update
    assert collector['aperture_parameters']['a1']['magnitude'] == 2.102
    assert collector['a1']['magnitude'] == pytest.approx(2.102 * 7.41 / 7.91)
    old_collector = copy.deepcopy(collector)

    try:
        # Update gross area
        collector['test_reference_area'] = 'gross'
        request_url = f"/config/collectors/{cid}?token={utils.API_TOKEN}"
        response = client.post(request_url, json=collector)
        assert response.status_code == 200

        # Assert parameters updated
        collector = response.json()
        assert not collector['aperture_parameters']
        assert collector['a1']['magnitude'] == pytest.approx(2.102 * 7.41 / 7.91)

    finally:
        # Undo collector update
        request_url = f"/config/collectors/{cid}?token={utils.API_TOKEN}"
        response = client.post(request_url, json=old_collector)
        assert response.status_code == 200
        collector = response.json()
        assert collector['test_reference_area'] == 'aperture'
        assert collector['aperture_parameters']['a1']['magnitude'] == 2.102
        assert collector['a1']['magnitude'] == pytest.approx(2.102 * 7.41 / 7.91)


@pytest.mark.xdist_group(name="integration")
def test_ping(session_fixture):
    request_url = f"config/ping_backend?token={utils.API_TOKEN}"
    response = client.get(request_url)

    assert response.status_code == 200


@pytest.mark.xdist_group(name="integration")
def test_ping_database(session_fixture):
    request_url = f"config/ping_database?token={utils.API_TOKEN}"
    response = client.get(request_url)

    assert response.status_code == 200


def test_update_fluid_solar(session_fixture, random_stored_plant):
    request_url = f'plants/{random_stored_plant[2]}?token={utils.API_TOKEN}'
    data = {"fluid_solar": {"fluid": "Gasokol, corroStar mixture"}}
    response = client.post(request_url, json=data)
    assert response.status_code == 200
    assert response.json()['fluid_solar']['name'] == 'Gasokol, corroStar mixture'

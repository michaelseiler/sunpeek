from pathlib import Path

import pandas as pd
import pytest
from unittest import TestCase
import random, string
import sqlalchemy.exc
from fastapi.testclient import TestClient
import datetime as dt

from sunpeek.api.main import app
from sunpeek.common import utils
import sunpeek.components as cmp
from sunpeek.core_methods import virtuals
import sunpeek

client = TestClient(app)


@pytest.mark.xdist_group(name="integration")
def test_api_duplicate_key_error_db(random_stored_plant):
    app.dependency_overrides = {}
    request_url = f"plants/new?token={utils.API_TOKEN}"
    config = {"name": random_stored_plant[0],
              "latitude": {"magnitude": 47.047201, "units": "deg"},
              "longitude": {"magnitude": 15.436428, "units": "deg"}}
    response = client.post(request_url, json=config)
    assert response.status_code == 409
    assert response.json()['message'] == 'Item with duplicate identifier (e.g. name or id) exists'


@pytest.mark.xdist_group(name="integration")
def test_api_change_array_collector(S, random_stored_plant):
    app.dependency_overrides = {}
    plant = random_stored_plant[1]
    with S.begin() as session:
        session.add(plant)
        array_id = plant.arrays[0].id
    request_url = f"plants/-/arrays/{array_id}?token={utils.API_TOKEN}"
    config = {"collector": "powerSol 55"}

    response = client.post(request_url, json=config)

    assert response.status_code == 200
    assert response.json()['collector'] == "powerSol 55"


@pytest.mark.xdist_group(name="integration")
def test_api_create_demo_plant(session_fixture):
    app.dependency_overrides = {}
    letters = string.ascii_lowercase
    name = ''.join(random.choice(letters) for i in range(8))
    request_url = f"plants/create_demo_plant?include_data=false&accept_license=true&name={name}&token={utils.API_TOKEN}"
    response = client.get(request_url)

    assert response.status_code == 200


@pytest.mark.xdist_group(name="integration")
def test_api_create_demo_plant_no_name(session_fixture):
    app.dependency_overrides = {}
    request_url = f"plants/create_demo_plant?include_data=false&accept_license=true&token={utils.API_TOKEN}"
    response = client.get(request_url)

    assert response.status_code == 200


@pytest.mark.xdist_group(name="integration")
def test_api_delete_plant(session_fixture, random_stored_plant):
    # Delete plant
    pname, p, pid = random_stored_plant
    request_url = f"plants/{pid}?token={utils.API_TOKEN}"
    response = client.delete(request_url)
    assert response.status_code == 200

    # Try to get deleted plant -> 404
    request_url = f"plants?name={pname}&token={utils.API_TOKEN}"
    response = client.get(request_url)
    assert response.status_code == 404

    # Plant not in db
    with pytest.raises(sqlalchemy.exc.NoResultFound):
        session_fixture.query(cmp.Plant).filter(cmp.Plant.id == pid).one()


@pytest.mark.xdist_group(name="integration")
def test_api_delete_all_data(session_fixture, plant_with_data):
    # Save parquet folders for later
    p, pid = plant_with_data
    p = session_fixture.query(cmp.Plant).filter(cmp.Plant.id == pid).one()
    fldrs = [p.raw_data_path, p.calc_data_path]

    # Delete all data
    request_url = f"plants/{pid}/data/all?token={utils.API_TOKEN}"
    response = client.delete(request_url)
    assert response.status_code == 200

    # Plant still in db
    session_fixture.query(cmp.Plant).filter(cmp.Plant.id == pid).one()

    # Assert data folders deleted
    for f in fldrs:
        assert not Path(f).exists()


@pytest.mark.xdist_group(name="integration")
def test_api_delete_plant__deletes_data_folders(session_fixture, plant_with_data):
    # Save parquet folders for later
    p, pid = plant_with_data
    p = session_fixture.query(cmp.Plant).filter(cmp.Plant.id == pid).one()
    fldrs = [p.raw_data_path, p.calc_data_path]

    # Delete plant
    request_url = f"plants/{pid}?token={utils.API_TOKEN}"
    response = client.delete(request_url)
    assert response.status_code == 200

    # Try to get deleted plant -> 404
    request_url = f"plants?name={p.name}&token={utils.API_TOKEN}"
    response = client.get(request_url)
    assert response.status_code == 404

    # Plant not in db
    with pytest.raises(sqlalchemy.exc.NoResultFound):
        session_fixture.query(cmp.Plant).filter(cmp.Plant.id == pid).one()

    # Assert data folders deleted
    for f in fldrs:
        assert not Path(f).exists()


@pytest.mark.xdist_group(name="integration")
def test_delete_sensor_db(session_fixture, random_stored_plant):
    request_url = f"/plants/-/sensors/1?token={utils.API_TOKEN}"

    response = client.get(request_url)
    assert response.status_code == 200
    assert response.json()['id'] == 1

    response = client.delete(request_url)
    assert response.status_code == 200

    response = client.get(request_url)
    assert response.status_code == 404

    with pytest.raises(sqlalchemy.exc.NoResultFound):
        p = session_fixture.query(cmp.Sensor).filter(cmp.Sensor.id == 1).one()


@pytest.mark.xdist_group(name="integration")
def test_delete_multiple_sensors_db(session_fixture, random_stored_plant):
    request_url = f"/plants/-/sensors?token={utils.API_TOKEN}"
    response = client.get(request_url)
    assert response.status_code == 200

    # construct the query
    # Exclude virtual sensors -> they are re-generated in delete sensors endpoint anyway
    ids = [str(item["id"]) for item in response.json() if not item['is_virtual']]
    plant_id = response.json()[0]["plant_id"]

    # perform delete
    request_url = f"/plants/{plant_id}/sensors?token={utils.API_TOKEN}"
    response = client.delete(request_url, params={"ids": ids})
    # print(response.json())
    assert response.status_code == 200

    # check results
    response = client.get(request_url)
    new_ids = [item["id"] for item in response.json()]

    # Assert ids of deleted real sensors are not included in current sensor ids, new_ids
    for deleted_id in ids:
        assert deleted_id not in new_ids


@pytest.mark.xdist_group(name="integration")
def test_change_array_collector(S, random_stored_plant):
    with S.begin() as session:
        session.add(random_stored_plant[1])
        array_id = random_stored_plant[1].arrays[0].id
    request_url = f"/plants/-/arrays/{array_id}"

    r = client.get(request_url, params={'token': "harvestIT"})
    assert r.json()['collector'] != 'powerSol 120'

    client.post(request_url, params={'token': "harvestIT"}, json={'collector': "powerSol 120"})

    r = client.get(request_url, params={'token': "harvestIT"})
    assert r.json()['collector'] == 'powerSol 120'


def test_create_operational_event(session_fixture, random_stored_plant):
    request_url = f"plants/1/operational_events"
    plant_id = random_stored_plant[2]
    response = client.post(request_url, params={'plant_id': plant_id, 'event_start': '2017-5-1 00:00',
                                                'event_end': '2017-5-2 00:00', 'timezone': 'Europe/Vienna',
                                                'description': 'test', 'ignored_range': True, 'token': 'harvestIT'})
    assert response.status_code == 200
    assert response.json()['event_start'] == '2017-04-30T22:00:00+00:00'
    assert response.json()['event_end'] == '2017-05-01T22:00:00+00:00'
    assert response.json()['original_timezone'] == 'Europe/Vienna'


def test_operational_events_db(random_stored_plant):
    plant_id = random_stored_plant[2]
    request_url = f"plants/{plant_id}/operational_events"

    params = {'event_start': '2017-5-1 00:00', 'event_end': '2017-5-2 00:00',
              'timezone': 'Europe/Vienna', 'description': 'test', 'ignored_range': True, 'token': 'harvestIT'}
    client.post(request_url, params=params)
    params['event_start'] = '2017-5-15 00:00'
    params['event_end'] = '2017-5-16 00:00'
    client.post(request_url, params=params)

    r = client.get(request_url, params={'token': 'harvestIT'})
    assert len(r.json()) == 2

    r = client.get(request_url, params={'token': 'harvestIT', 'search_start': '2017-1-1 05:00',
                                        'search_end': '2017-5-14 23:59', 'search_timezone': 'Europe/Vienna'})
    assert len(r.json()) == 1

    r = client.get(request_url, params={'token': 'harvestIT', 'search_start': '2017-5-15 05:00',
                                        'search_end': '2017-5-15 23:59', 'search_timezone': 'Europe/Vienna'})
    assert len(r.json()) == 1

    r = client.get(request_url,
                   params={'token': 'harvestIT', 'search_start': '2017-5-1 05:00', 'search_timezone': 'Europe/Vienna'})
    assert len(r.json()) > 1

    test_id = r.json()[0]["id"]
    r = client.get(f'plants/-/operational_events/{test_id}', params={'token': 'harvestIT'})
    assert r.json()['id'] == test_id
    assert r.json()['plant']['name'] == random_stored_plant[0]

    client.delete(f'plants/-/operational_events/{test_id}', params={'token': 'harvestIT'})
    r = client.get(f'plants/-/operational_events/{test_id}', params={'token': 'harvestIT'})
    assert r.status_code == 404
    assert r.json()['error'] == 'NoResultFound'


def test_operational_events_db__timezone(random_stored_plant):
    plant_id = random_stored_plant[2]
    request_url = f"plants/{plant_id}/operational_events"

    params = {'search_start': '2017-5-1 00:00', 'search_end': '2017-5-2 00:00'}
    r = client.post(request_url, params=params)
    assert r.status_code == 422


def test_api_update_data_upload_defaults(db_test_plant):
    request_url = f"plants/{db_test_plant[1]}?token={utils.API_TOKEN}"

    response = client.get(request_url)
    d_id = response.json()["data_upload_defaults"]['id']
    TestCase().assertDictEqual(
        response.json()["data_upload_defaults"],
        {
            "id": d_id,
            "datetime_template": None,
            "datetime_format": None,
            "timezone": None,
            "csv_separator": None,
            "csv_decimal": None,
            "csv_encoding": None,
            "index_col": None
        })

    data = {"data_upload_defaults": {
        "datetime_template": "day_month_year",
        "datetime_format": None,
        "timezone": None,
        "csv_separator": ";",
        "csv_decimal": ".",
        "csv_encoding": 'utf-8',
        "index_col": 0
    }}

    response = client.post(request_url, json=data)
    assert response.status_code == 200
    d_id = response.json()["data_upload_defaults"]['id']
    TestCase().assertDictEqual(
        response.json()['data_upload_defaults'],
        {
            "id": d_id,
            "datetime_template": "day_month_year",
            "datetime_format": None,
            "timezone": None,
            "csv_separator": ";",
            "csv_decimal": ".",
            "csv_encoding": 'utf-8',
            "index_col": 0
        })


def test_get_pc_method_settings(db_test_plant):
    request_url = f"plants/{db_test_plant[1]}/evaluations/pc_method_settings?token=harvestIT"
    response = client.get(request_url)
    print(response.json())
    assert response.status_code == 200

    data = response.json()
    assert "formula" in data
    assert "evaluation_mode" in data
    assert "safety_uncertainty" in data
    assert "safety_others" in data
    assert "safety_pipes" in data
    assert "wind_used" in data
    assert data["evaluation_mode"] == "ISO"


@pytest.mark.parametrize("formula", [1, 2, None])
@pytest.mark.parametrize("evaluation_mode", ["ISO", "extended"])
@pytest.mark.parametrize("wind_used", [True, False, None])
@pytest.mark.parametrize("safety_uncertainty", [.93, 1.0, None])
@pytest.mark.parametrize("safety_pipes", [None])  # for the sake of simplicity only tested on safety_uncertainty
@pytest.mark.parametrize("safety_others", [None])  # for the sake of simplicity only tested on safety_uncertainty
def test_updated_pc_method_settings(db_test_plant, formula, evaluation_mode, wind_used, safety_others,
                                    safety_uncertainty, safety_pipes):
    request_url = f"plants/{db_test_plant[1]}/evaluations/pc_method_settings?token=harvestIT"
    json = {
        "formula": formula,
        "evaluation_mode": evaluation_mode,
        "wind_used": wind_used,
        "safety_pipes": safety_pipes,
        "safety_uncertainty": safety_uncertainty,
        "safety_others": safety_others,
    }
    response = client.post(request_url, json=json)
    assert response.status_code == 200

    data = response.json()
    assert data["formula"] == formula
    assert data["evaluation_mode"] == evaluation_mode
    assert data["safety_uncertainty"] == safety_uncertainty
    assert data["safety_others"] == safety_others
    assert data["safety_pipes"] == safety_pipes
    assert data["wind_used"] == wind_used


def test_update_array_db(S, random_stored_plant):
    p = random_stored_plant[1]
    with S.begin() as session:
        session.add(p)
        array_id = p.arrays[0].id
    request_url = f"plants/-/arrays/{array_id}?token={utils.API_TOKEN}"

    response = client.post(request_url, json={"name": "Arcon South 2", "area_gr": {"magnitude": 5000, "units": "m2"}})
    assert response.status_code == 200
    assert response.json()['name'] == "Arcon South 2"
    assert response.json()['area_gr']["magnitude"] == 5000

    assert len(client.get(f"plants?token={utils.API_TOKEN}").json()) >= 1


def test_delete_component_deletes_vsensors(S, random_stored_plant):
    plant = random_stored_plant[1]
    with S.begin() as session:
        session.add(plant)
        array_id = plant.arrays[0].id
    len_o = len(client.get(f"plants/{random_stored_plant[2]}/sensors?token={utils.API_TOKEN}").json())

    response = client.delete(f"plants/-/arrays/{array_id}?token={utils.API_TOKEN}")
    assert response.status_code == 200

    assert len(client.get(f"plants/{random_stored_plant[2]}/sensors?token={utils.API_TOKEN}").json()) < len_o


def test_virtual_sensor_has_problems_after_plant_load(S, random_stored_plant):
    # Make sure virtual sensor .problems is calculated after plant load, and sets .problems for all other vsensors, too.
    plant = random_stored_plant[1]
    with S.begin() as session:
        session.add(plant)
        vsensor_id = plant.tp.id

    request_url = f"plants/-/sensors/{vsensor_id}?token={utils.API_TOKEN}"
    response = client.get(request_url)

    assert response.status_code == 200
    # Need to re-attach the plant to a session
    with S.begin() as session:
        session.add(plant)
        assert plant.tp.problems is not None
        assert plant.tp.problems.success
        # Make sure loading one sensor also populates .problems for other virtual sensors...
        assert plant.sun_azimuth.problems is not None
        assert plant.sun_azimuth.problems.success
        # ...for all components of the plant
        assert plant.arrays[0].iam.problems is not None
        assert plant.arrays[0].iam.problems.success


def test_get_sensor_data_works(session_fixture, db_test_plant_with_data):
    plant_id = db_test_plant_with_data[1]
    session_fixture.add(db_test_plant_with_data[0])
    sensors = db_test_plant_with_data[0].raw_sensors
    sensor_id = sensors[0].id
    request_url = f"plants/{plant_id}/sensors/{sensor_id}/data?token={utils.API_TOKEN}"
    r = client.get(request_url)

    assert r.status_code == 200

    data = r.json()
    timestamps = list(data.keys())
    values = list(data.values())
    assert timestamps[0] == "1493593200000"
    assert values[0] == pytest.approx(282.10, 0.01)
    assert timestamps[111] == "1493599860000"
    assert values[111] == pytest.approx(282.01, 0.01)


def test_get_sensor_data_noSensorReturnsNotFound(db_test_plant_with_data):
    plant_id = db_test_plant_with_data[1]
    request_url = f"plants/{plant_id}/sensors/{2000000}/data?token={utils.API_TOKEN}"
    r = client.get(request_url)
    assert r.status_code == 404


# @pytest.mark.skip(reason="Hangs due to untraced problem with DB connection. Possibly due to messy connection "
#                          "management in tests")
def test_get_sensor_data_returns404ifNoSensor(S, db_test_plant_with_data):
    plant_id = db_test_plant_with_data[1]

    # Just testing that we get a 404 with a non-existant ID should be enough here
    request_url = f"plants/{plant_id}/sensors/{9999}/data?token={utils.API_TOKEN}"
    r = client.get(request_url)
    assert r.status_code == 404


def test_recalculate_virtual_sensors(db_test_plant_with_data):
    plant_id = db_test_plant_with_data[1]
    request_url = f"plants/{plant_id}/sensors/recalculate_virtuals?token={utils.API_TOKEN}"
    r = client.get(request_url)

    assert r.status_code == 200


def test_recalculate_virtual_sensors_noflush_is_inconsistent(session_fixture, db_test_plant_with_data):
    # If recalculate virtuals does not flush to parquet, old results are returned by vsensor.data,
    # SunPeek is left in an inconsistent state.
    p = session_fixture.query(cmp.Plant).filter(cmp.Plant.id == db_test_plant_with_data[1]).one()
    a = p.arrays[0]
    s = a.rd_gti

    # make sure some vsensor has values & can_calculate
    assert s.can_calculate
    assert not s.data.isna().all()
    assert a.in_global.info['tilt'].m == 30

    # Do some config change that results in can_calculate==False
    # Then recalc virtuals _without_ flushing.
    a.in_global.info = {}
    virtuals.calculate_virtuals(p)

    # After recalc: Make sure sensor cache is correct
    assert not s.can_calculate  # config_virtuals did its job
    assert s.data.isna().all()  # could not calculate, due to config change: all-nan in cache

    p.reset_cache()
    # Data are now returned by parquet: old data
    assert not s.data.isna().all()  # inconsistent with cache


def test_recalculate_virtual_sensors__flushes_parquet(S, db_test_plant_with_data):
    # Make sure recalculate virtuals flushes virtuals to parquet, and leaves SunPeek in a consistent state.
    with S.begin() as session:
        plant_id = db_test_plant_with_data[1]
        p = session.query(cmp.Plant).filter(cmp.Plant.id == plant_id).one()
        a = p.arrays[0]
        s = a.rd_gti
        s_id = s.id
        # Make sure some vsensor has values & can_calculate
        assert s.can_calculate
        assert not s.data.isna().all()
        assert a.in_global.info['tilt'].m == 30

        check_id = a.in_global.id

    # Do some config change that results in s.can_calculate==False (reset input sensor's .info).
    request_url = f'/plants/-/sensors/{check_id}?token={utils.API_TOKEN}'
    r = client.post(request_url, json={'info': {}})
    assert r.status_code == 200
    assert r.json()['info'] == {}

    # Recalc virtuals via API (flushes new sensor data).
    request_url = f"plants/{plant_id}/sensors/recalculate_virtuals?token={utils.API_TOKEN}"
    r = client.get(request_url)
    assert r.status_code == 200

    with S.begin() as session:
        # After API recalc: make sure all-nans in sensor cache, but parquet still has old data
        s_updated = session.query(cmp.Sensor).filter(cmp.Sensor.id == s_id).one()
        assert not s_updated.can_calculate  # config_virtuals did its job -> can't calculate vsensor without sensor info
        assert s_updated.data.isna().all()  # could not calculate, due to config change: all-nan in cache

    p.reset_cache()
    # Data are now returned by parquet: old data
    with S.begin() as session:
        session.add(s)
        assert s.data.isna().all()  # parquet is consistent with cache
        session.rollback()


def test_get_data_start_end(db_test_plant_with_data):
    plant_id = db_test_plant_with_data[1]
    request_url = f"plants/{plant_id}/data_start_end?token={utils.API_TOKEN}"

    response = client.get(request_url)
    assert response.status_code == 200

    start_end = response.json()

    assert isinstance(start_end, dict)
    assert start_end['start'] == '2017-05-01T00:00:00+01:00'
    assert start_end['end'] == '2017-05-31T23:59:00+01:00'


def test_get_data_start_end__no_data(db_test_plant_clean):
    # Test plant with no data uploaded
    plant_id = db_test_plant_clean[1]
    request_url = f"plants/{plant_id}/data_start_end?token={utils.API_TOKEN}"

    response = client.get(request_url)
    assert response.status_code == 200

    start_end = response.json()

    assert isinstance(start_end, dict)
    assert start_end['start'] is None
    assert start_end['end'] is None


def test_get_nan_report(db_test_plant_with_data):
    plant_id = db_test_plant_with_data[1]
    request_url = f"plants/{plant_id}/sensors/nan_report?token={utils.API_TOKEN}"

    response = client.get(request_url)
    assert response.status_code == 200

    nan_report = response.json()['nan_report']

    assert isinstance(nan_report, dict)
    assert len(nan_report) >= 30
    assert 'vf' in nan_report
    assert sum(key.startswith('sun_azimuth__virtual__plant_') for key in nan_report.keys())


## Export / Import with data  ----------------------------------------------------------------------------------

def test_plant_export_import(db_test_plant_with_data):
    # Test export + import with new name
    p, pid = db_test_plant_with_data
    request_url = f"/plants/{pid}/export_config?token={utils.API_TOKEN}"
    response = client.get(request_url)
    assert response.status_code == 200

    # Export config
    config_export = response.json()
    plant_name = config_export['plant']['name']
    new_plant_name = f'{plant_name}__new'

    # Import
    request_url = f"/plants/import"
    params = {'token': utils.API_TOKEN,
              'new_plant_name': new_plant_name}
    response = client.post(request_url, params=params, json=response.json())
    assert response.status_code == 201
    assert response.json()[0]['name'] == new_plant_name


def test_export_demo__import_run_pc(session_fixture):
    # Full test involving export of demo plant, import as new plant, add data to imported plant, run PC method.

    # Create demo plant
    request_url = f"plants/create_demo_plant?include_data=true&accept_license=true&token={utils.API_TOKEN}"
    response = client.get(request_url)
    assert response.status_code == 200

    # Export config
    request_url = f"/plants/{response.json()['id']}/export_config?token={utils.API_TOKEN}"
    response = client.get(request_url)
    assert response.status_code == 200

    # Import
    request_url = f"/plants/import"
    new_plant_name = f'{response.json()["plant"]["name"]}__new'
    params = {'token': utils.API_TOKEN,
              'new_plant_name': new_plant_name}
    response = client.post(request_url, params=params, json=response.json())
    assert response.status_code == 201

    # PC method
    new_plant = response.json()[0]

    timezone = "Europe/Vienna"
    datetime_template = "year_month_day"
    plant_id = new_plant['id']

    with open(sunpeek.demo.DEMO_DATA_PATH_2DAYS, "rb") as f:
        request_url = f"plants/{plant_id}/data"
        single_file = [("files", ("test.csv", f, "text/csv"))]
        params = {
            "token": utils.API_TOKEN,
            "timezone": timezone,
            "datetime_template": datetime_template,
        }
        response = client.post(request_url, files=single_file, params=params)

    assert response.status_code == 201

    request_url = f"plants/{new_plant['id']}/evaluations/pc_method"
    params = {'token': utils.API_TOKEN,
              'method': 'ISO',
              'formula': 1}
    response = client.get(request_url, params=params)
    assert response.status_code == 200

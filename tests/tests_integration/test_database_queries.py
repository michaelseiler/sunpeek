import os.path
import random, string
import pandas as pd
import pytest
from fastapi import UploadFile
import sqlalchemy, sqlalchemy.exc
from sqlalchemy.orm import selectinload
import pyarrow as pa

import sunpeek.demo
from sunpeek.common.unit_uncertainty import Q
from sunpeek.common import config_parser
from sunpeek.common.errors import ConfigurationError
from sunpeek.common.utils import DatetimeTemplates
from sunpeek.data_handling.context import Context
import sunpeek.components as cmp
from sunpeek.components.fluids import WPDFluidPure
from sunpeek.db_utils import crud, DATETIME_COL_NAME
from sunpeek.api.routers import files
from sunpeek.definitions.fluid_definitions import WPDFluids


def randomword(length):
    letters = string.ascii_lowercase
    return ''.join(random.choice(letters) for i in range(length))


@pytest.mark.xdist_group(name="integration")
def test_data_access(session_fixture, db_test_plant_with_data):
    p = session_fixture.query(cmp.Plant).filter(cmp.Plant.name == 'FHW Arcon South _Test_').one()
    assert isinstance(p.te_amb.data, pd.Series)
    assert isinstance(p.te_amb.data.index, pd.DatetimeIndex)
    assert len(p.te_amb.data) > 1


@pytest.mark.xdist_group(name="integration")
def test_time_index_access(session_fixture, db_test_plant_with_data):
    p = session_fixture.query(cmp.Plant).filter(cmp.Plant.name == 'FHW Arcon South _Test_').one()
    p.context = Context(plant=p, datasource='pq')

    assert isinstance(p.time_index, pd.DatetimeIndex)
    assert DATETIME_COL_NAME in p.context.cache
    assert len(p.time_index) == len(p.te_amb.data.index)


@pytest.mark.xdist_group(name="integration")
def test_start_end(session_fixture, db_test_plant_with_data):
    p = session_fixture.query(cmp.Plant).filter(cmp.Plant.name == 'FHW Arcon South _Test_').one()
    p.context = Context(plant=p, datasource='pq',
                        eval_start=pd.to_datetime('2017-5-6', utc=True),
                        eval_end=pd.to_datetime('2017-5-9', utc=True))

    assert p.time_index[0] == pd.to_datetime('2017-05-06 00:00', utc=True)
    assert p.time_index[-1] == pd.to_datetime('2017-05-09 00:00', utc=True)


@pytest.mark.xdist_group(name="integration")
def test_query_collector_types(session_fixture):
    coll = session_fixture.query(cmp.Collector).filter(cmp.Collector.name == 'Arcon 3510').one()

    assert coll.name == 'Arcon 3510'
    assert coll.product_name == 'HTHEATstore 35/10'


@pytest.mark.xdist_group(name="integration")
def test_fluid_creation_with_db(session_fixture):
    try:
        def_water = cmp.FluidDefinition.get_definition(fluid_string="water", session=session_fixture)
        def_pekasol_2000 = cmp.FluidDefinition.get_definition(fluid_string="Pekasol 2000", session=session_fixture)
        def_antifrogen_l = cmp.FluidDefinition.get_definition(fluid_string="AL", session=session_fixture)
        def_pekasol_fhw = cmp.FluidDefinition.get_definition(fluid_string=WPDFluids.fhw_pekasolar.value.name,
                                                             session=session_fixture)

        with pytest.raises(ConfigurationError, match=".*is not unique.*"):
            cmp.FluidDefinition.get_definition(fluid_string="PEK", session=session_fixture)
    except sqlalchemy.exc.OperationalError as e:
        print(e)
        if "Is the server running" in str(e):
            pytest.skip("Database not available")
        else:
            print(e)

    water = cmp.FluidFactory(fluid=def_water)
    pekasol_2000 = cmp.FluidFactory(fluid=def_pekasol_2000, concentration=Q(36, '%'))
    def_Antifrogen_l = cmp.FluidFactory(fluid=def_antifrogen_l, concentration=Q(42, '%'))
    pekasol_fhw = cmp.FluidFactory(fluid=def_pekasol_fhw)

    assert isinstance(water, cmp.CoolPropFluid)
    assert water.fluid.is_pure
    assert not pekasol_2000.fluid.is_pure
    assert isinstance(def_Antifrogen_l, cmp.CoolPropFluid)
    assert isinstance(pekasol_2000, cmp.CoolPropFluid)
    assert isinstance(pekasol_fhw, cmp.WPDFluid)
    assert def_Antifrogen_l.concentration == Q(42, 'percent')


@pytest.mark.xdist_group(name="integration")
def test_query_fluids(session_fixture):
    fluids = list(session_fixture.query(cmp.FluidDefinition).all())
    assert len(fluids) >= 15


@pytest.mark.xdist_group(name="integration")
def test_get_fluid_defs_coolprop(session_fixture):
    pk2000 = cmp.FluidDefinition.get_definition('Pekasol 2000', session_fixture)
    assert pk2000.name == 'PK2'
    assert not pk2000.is_pure


@pytest.mark.xdist_group(name="integration")
def test_get_fluid_defs_coolprop2(session_fixture):
    fhw = cmp.FluidDefinition.get_definition('Pekasolar_FHW', session_fixture)
    assert fhw.name == 'Pekasolar_FHW'
    assert fhw.is_pure
    assert fhw.density_unit_te == 'degC'


@pytest.mark.xdist_group(name="integration")
def test_sensor_info_db(session_fixture):
    example_plant = cmp.Plant('Test')
    rd_gti = cmp.Sensor(raw_name='rd_gti', native_unit='W/m**2', info={'tilt': Q(35, 'deg'), 'azim': Q(10, 'deg')},
                        plant=example_plant)
    example_plant.set_sensors(in_global=rd_gti)
    session_fixture.add(example_plant)
    session_fixture.commit()

    p = session_fixture.query(cmp.Plant).filter(cmp.Plant.name == 'Test').one()
    assert isinstance(p.in_global.info['tilt'], Q)
    assert p.in_global.info['tilt'].magnitude == 35


@pytest.mark.xdist_group(name="integration")
def test_upload_measure_data(session_fixture, random_stored_plant):
    with open(sunpeek.demo.DEMO_DATA_PATH_2DAYS, 'rb') as f:
        fu = UploadFile(filename=f.name, file=f)
        files.upload_measure_data(random_stored_plant[2], files=[fu], timezone='UTC', sess=session_fixture, crd=crud,
                                  datetime_template=DatetimeTemplates.year_month_day)

    p = session_fixture.query(cmp.Plant).filter(cmp.Plant.name == random_stored_plant[0]).one()

    assert {DATETIME_COL_NAME, "ve_wind", "vf", "te_amb", "rd_bti", "rd_dti", "rd_gti", "te_out", "te_in"}.issubset(
        set(pa.dataset.dataset(os.environ['SUNPEEK_RAW_DATA_PATH']).schema.names))


# @pytest.mark.xdist_group(name="integration")
# def test_new_sensor_cols_created(session, db_test_plant, resources_dir):
#     plant = session.query(cmp.Plant).where(cmp.Plant.id == db_test_plant.id).one()
#     plant.raw_sensors.append(cmp.Sensor('te_out_row2', 'degC'))
#
#     with open(os.path.join(resources_dir, 'data', 'data_uploader', 'csv1.csv'), 'rb') as f:
#         fu = UploadFile(filename=f.name, file=f)
#         files.upload_measure_data(plant.id, files=[fu], timezone='UTC', sess=session, crd=crud,
#                                   datetime_template=DatetimeTemplates.year_month_day)
#
#     s = plant.get_raw_sensor('te_out_row2', True)
#     assert s.data.notna().sum() > 0


@pytest.mark.xdist_group(name="integration")
def test_make_full_plant(conf_clean, session_fixture):
    name = randomword(8)
    conf_clean['plant']['name'] = name
    p = config_parser.make_full_plant(conf_clean, session_fixture)
    assert p.name == name
    assert isinstance(p.fluid_solar, WPDFluidPure)
    assert p.arrays[0].te_in.sensor_type.name == 'fluid_temperature'
    assert isinstance(p.arrays[0], cmp.Array)
    assert p.arrays[0].collector.name == "Arcon 3510"


@pytest.mark.xdist_group(name="integration")
def test_load_plant(random_stored_plant, session_fixture):
    print(session_fixture.query(cmp.Plant).all())
    p = session_fixture.query(cmp.Plant).options(selectinload('*')).filter(cmp.Plant.name == random_stored_plant[0]).one()
    assert isinstance(p, cmp.Plant)
    assert p.te_amb.sensor_type.name == 'ambient_temperature'
    # arrays = p.arrays
    assert len(p.arrays) == 1
    assert p.arrays[0].name == 'test array'
    assert isinstance(p.arrays[0].te_in, cmp.Sensor)
    assert p.arrays[0].te_out.sensor_type.name == 'fluid_temperature'


@pytest.fixture()
@pytest.mark.xdist_group(name="integration")
def db_test_plant_reset_datasource_after_use(db_test_plant):
    yield db_test_plant
    db_test_plant.context.datasource = 'pq'


# @pytest.mark.xdist_group(name="integration")
# def test_copy_to_batch_insert_correct_column_order(session, db_test_plant_reset_datasource_after_use):
#     test_plant = db_test_plant_reset_datasource_after_use
#     test_plant.use_csv(sunpeek.demo.DEMO_DATA_PATH_2DAYS, timezone='UTC', csv_separator=';',
#                        datetime_template=DatetimeTemplates.year_month_day)
#     df = test_plant.context.df
#     up = DataUploader_db(plant=test_plant,
#                          files=sunpeek.demo.DEMO_DATA_PATH_2DAYS,
#                          datetime_template=DatetimeTemplates.year_month_day,
#                          timezone='UTC',
#                          session=session)
#     up.do_upload()
#
#     for col in df.columns:
#         stmt = select(column(col)).select_from(table(test_plant.raw_table_name)).where(column(DATETIME_COL_NAME) == str(df.index[0]))
#         db_val = session.execute(stmt).one()[0]
#         if np.isnan(df[col][0]):
#             assert db_val is None
#         else:
#             assert round(float(db_val), 2) == round(df[col][0], 2)


@pytest.mark.xdist_group(name="integration")
def test_get_raw_sensors_db(random_stored_plant, session_fixture):
    p = session_fixture.query(cmp.Plant).options(selectinload('*')).filter(cmp.Plant.name == random_stored_plant[0]).one()
    # used to be 39, now 30 -> some radiation-related vsensors temporarily commented.
    assert len(p.raw_sensors) >= 30


# @pytest.mark.xdist_group(name="integration")
# def test_db_data_operations_error_display(session):
#     con_params = db_data_operations.get_db_connection_dict()
#     con_params['port'] = 5000
#     db_data_operations.connect(con_params)
#
#     with open(utils.sp_logger.handlers[1].baseFilename, 'r') as f:
#         lines = f.readlines()[-10:]
#
#     checks = []
#     for l in lines:
#         if "port 5000 failed: Connection refused" in l:
#             checks.append(True)
#         else:
#             checks.append(False)
#
#     assert any(checks)


# @pytest.mark.xdist_group(name="integration")
# def test_db_sensor_cols_lowercase(session, db_test_plant_reset_datasource_after_use):
#     test_plant = db_test_plant_reset_datasource_after_use
#     test_plant.use_csv(sunpeek.demo.DEMO_DATA_PATH_2DAYS, timezone='UTC', csv_separator=';',
#                        datetime_template=DatetimeTemplates.year_month_day)
#     df = test_plant.context.df
#     df = df.rename(columns={'te_amb': 'teAmb'})
#     f = io.BytesIO()
#     df.to_csv(f, sep=';')
#     up = DataUploader_db(plant=test_plant,
#                          datetime_template=DatetimeTemplates.year_month_day,
#                          timezone=None,
#                          session=session)
#
#     up.do_upload(files=[f])
#
#     result = session.execute(f"SELECT * FROM {test_plant.raw_table_name}")
#     assert 'teAmb' not in result.keys()
#     assert 'teamb' not in result.keys()

import pytest

from sunpeek.demo import demo_plant
from sunpeek.common.errors import DuplicateNameError


@pytest.mark.xdist_group(name="integration")
def test_demo_plant_monotonic(session_fixture):
    plant = demo_plant.create_demoplant(session_fixture)
    demo_plant.add_demo_data(plant, session=session_fixture)

    assert plant.time_index.is_monotonic_increasing


@pytest.mark.xdist_group(name="integration")
def test_demo_plant_no_name(session_fixture):
    demo_plant.create_demoplant(session_fixture, name=None)


@pytest.mark.xdist_group(name="integration")
def test_demo_plant_unique_name(session_fixture):
    plant_name = 'my_favorite_plant_name'
    demo_plant.create_demoplant(session_fixture, name=plant_name)
    with pytest.raises(DuplicateNameError, match=f'Plant with name "{plant_name}" already exists.'):
        demo_plant.create_demoplant(session_fixture, name=plant_name)


def test_demo_plant_correctly_synchronizes(session_fixture):
    plant_name = "the_best_plant_name_ever2"
    plant = demo_plant.create_demoplant(session_fixture, name=plant_name)

    # Should be calculatable as all information is there to calculate the virtual sensor.
    assert plant.sensors["tp"].can_calculate

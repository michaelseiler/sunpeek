import sunpeek.components as cmp
from sunpeek.db_utils import crud
from sunpeek.common.errors import ConfigurationError

import pytest


@pytest.mark.xdist_group(name="integration")
def test_get_plants(session_fixture, db_test_plant, random_stored_plant):
    assert len(crud.get_plants(session_fixture)) >= 2
    p1 = crud.get_plants(session_fixture, plant_name="FHW Arcon South _Test_")
    assert isinstance(p1, cmp.Plant)
    assert p1.name == crud.get_plants(session_fixture, plant_id=p1.id).name


@pytest.mark.xdist_group(name="integration")
def test_get_sensors(session_fixture, db_test_plant, random_stored_plant):
    p1_name = "FHW Arcon South _Test_"
    assert len(crud.get_sensors(session_fixture)) > len(crud.get_sensors(session_fixture, plant_name=p1_name))
    p1 = crud.get_plants(session_fixture, plant_name=p1_name)
    l = crud.get_sensors(session_fixture, plant_id=p1.id)
    for sensor in l:
        assert sensor.plant.name == p1_name


@pytest.mark.xdist_group(name="integration")
def test_get_arrays(session_fixture, db_test_plant, random_stored_plant):
    p1_name = "FHW Arcon South _Test_"
    assert len(crud.get_components(session_fixture, cmp.Array)) > len(crud.get_components(session_fixture, cmp.Array, plant_name=p1_name))
    p1 = crud.get_plants(session_fixture, plant_name=p1_name)
    l = crud.get_components(session_fixture, cmp.Array, plant_id=p1.id)
    # assert len(l) == 1
    A = l[0]
    assert isinstance(A, cmp.Array)
    assert A.plant.name == p1_name
    A = crud.get_components(session_fixture, cmp.Array, name ="Arcon South", plant_id=p1.id)
    assert isinstance(A, cmp.Array)
    A = crud.get_components(session_fixture, cmp.Array, name ="Arcon South", plant_name=p1.name)
    assert isinstance(A, cmp.Array)


@pytest.mark.xdist_group(name="integration")
def test_get_fluids(session_fixture, db_test_plant, random_stored_plant):
    p1_name = "FHW Arcon South _Test_"
    assert len(crud.get_components(session_fixture, cmp.Fluid)) > len(crud.get_components(session_fixture, cmp.Fluid, plant_name=p1_name))
    p1 = crud.get_plants(session_fixture, plant_name=p1_name)
    l = crud.get_components(session_fixture, cmp.Fluid, plant_id=p1.id)
    assert len(l) == 1
    A = l[0]
    assert isinstance(A, cmp.Fluid)
    assert A.plant.name == p1_name
    F = crud.get_components(session_fixture, cmp.Fluid, plant_id=p1.id)[0]
    assert isinstance(A, cmp.Fluid)
    A = crud.get_components(session_fixture, cmp.Fluid, id=F.id)
    assert isinstance(A, cmp.Fluid)


@pytest.mark.xdist_group(name="integration")
def test_delete_sensor_removes_mappings(session_fixture):
    example_plant = crud.create_component(session_fixture, cmp.Plant('Test Plant'))
    s = crud.create_component(session_fixture, cmp.Sensor('Test Sensor', 'K'))
    crud.create_component(session_fixture, cmp.SensorMap('te_amb', s, 'ambient_temperature', example_plant))

    assert s.plant == example_plant
    assert example_plant.sensors["te_amb"] == s
    assert example_plant.sensor_map["te_amb"].sensor == s

    crud.delete_component(session_fixture, s)

    assert s not in example_plant.sensors.values()
    assert s not in example_plant.raw_sensors
    assert "te_amb" not in example_plant.sensor_map
    assert "te_amb" not in example_plant.sensors

# -*- coding: utf-8 -*-
# Note: This module is outdated and should currently not be used!


import pytest
import json
from fastapi.testclient import TestClient

import sunpeek.demo
from sunpeek.common import utils
from sunpeek.data_handling.wrapper import use_csv
from sunpeek.core_methods.pc_method.wrapper import run_performance_check
import sunpeek.components as cmp
from sunpeek.api.main import app
from sunpeek.definitions.fluid_definitions import get_definition


@pytest.mark.skip('Adding fluid should not be necessary')
def test_first_run__walk_through():
    # STEP 1: Make Plant from Config
    with open(sunpeek.demo.DEMO_CONFIG_PATH) as f:
        conf = json.load(f)
    plant = sunpeek.common.config_parser.make_full_plant(conf=conf)

    # uninitialized fluid, workaround:
    plant.fluid_solar = cmp.FluidFactory(fluid=get_definition('FHW, Pekasolar'))

    # STEP 2: Submit Data
    # ------ COMPANY SPECIFIC CODE ------
    use_csv(plant, sunpeek.demo.DEMO_DATA_PATH_1MONTH, timezone='Europe/Vienna')
    # ------------------------------------

    # STEP 3: Run PC Method
    pc_output = run_performance_check(plant).output

    # STEP 4: Print Results
    assert pc_output.target_actual_slope > 1.0
    assert pc_output.target_actual_slope <= 1.2
    print("measured yield", pc_output.tp_sp_measured)
    print("estimated yield (as measured)", pc_output.tp_sp_estimated)
    print("estimated yield (incl. safety)", pc_output.tp_sp_estimated_safety)


@pytest.mark.skip('This never worked, we can fix it later')
def test_first_run__walkthrough_with_api(session_fixture):
    # =============================
    # PLANT SETUP
    # =============================

    # First, we need to gather all required information about the plant
    # we can do so by passing a json structure to the API
    plant_name = "Test_Plant"

    # COMPANY CODE
    with open(sunpeek.demo.DEMO_CONFIG_PATH) as f:
        conf = json.load(f)
        conf["plant"]["name"] = plant_name

    # def_water = cmp.FluidDefinition.get_definition(fluid_string="water", session=session)

    # This simulates a graphical frontend or other program connecting via the API
    client = TestClient(app)
    request_url = f"config/create_plant?token={utils.API_TOKEN}"
    response = client.post(request_url, json=conf)
    print(response)
    # END OF COMPANY CODE

    # =============================
    # ADD Sensor Data
    # =============================
    # There is not data yet. Thus, we need to add data.
    # It is expected that each company using Harvest IT has a script that sends csv-files via an api
    # or that the data is submitted via the Visualisation (with csv files)

    # -------------------------------------------------------------
    # COMPANY CODE

    # The company chose to store their data as csv file here
    measure_timezone = "UTC"

    # Inside the application we can load the plant from database
    # plant = config_parser.make_and_store_plant(conf, session)
    plant = session_fixture.query(cmp.Plant).filter(cmp.Plant.name == plant_name).one()
    with open(sunpeek.demo.DEMO_DATA_PATH_1MONTH, "rb") as f:
        request_url = f"plants/{plant.id}/data?timezone={measure_timezone}&token={utils.API_TOKEN}"
        response = client.post(request_url, files={"files": f})
    assert response.status_code == 200

    # END OF COMPANY CODE
    # ---------------------------------------

    # To access plant data, we can now do...
    # print(plant.te_amb.data)
    # ... and get some data back :)

    # =============================
    # Apply PC-Method
    # =============================
    # Finally, all is ready for the PC method

    # We can call it by simply running
    pc_output = run_performance_check(plant, method=['ISO']).output

    # gather the results:
    assert pc_output.target_actual_slope > 1.0
    assert pc_output.target_actual_slope <= 1.2
    print("measured yield", pc_output.tp_sp_measured)
    print("estimated yield (as measured)", pc_output.tp_sp_estimated)
    print("estimated yield (incl. safety)", pc_output.tp_sp_estimated_safety)

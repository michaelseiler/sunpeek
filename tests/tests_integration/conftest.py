import copy

import pytest
import os
import json
from fastapi import UploadFile
import sqlalchemy
import random, string
import time
import subprocess
from filelock import FileLock

import sunpeek.demo
from sunpeek import components as cmp
from sunpeek.api.routers import files
from sunpeek.common import config_parser, utils
from sunpeek.common.utils import DatetimeTemplates
from sunpeek.db_utils import crud
import sunpeek.db_utils.init_db
from sunpeek.api.routers.files import upload_measure_data
from sunpeek.data_handling.context import Context
from sunpeek.common import utils
import sunpeek.components as cmp
from sunpeek.common.unit_uncertainty import Q


def randomword(length):
    letters = string.ascii_lowercase
    return ''.join(random.choice(letters) for i in range(length))


@pytest.mark.xdist_group(name="integration")
@pytest.fixture(scope='session')
def setup_db(tmp_path_factory, worker_id):
    sunpeek.db_utils.init_db.init_db()


@pytest.mark.xdist_group(name="integration")
@pytest.fixture(scope='session')
def start_local_db_session(request, tmp_path_factory, worker_id):
    start = time.time()
    # request.getfixturevalue('docker_services')  # pytest-xdist not active
    #
    # if "healthy" in str(subprocess.Popen(['docker', 'inspect', "--format='{{json .State.Health}}'", f'hit_test-db-1'],
    #                                      stdout=subprocess.PIPE).communicate()):
    #     pass
    # else:
    #     while "healthy" not in str(
    #             subprocess.Popen(['docker', 'inspect', "--format='{{json .State.Health}}'", f'hit_test-db-1'],
    #                              stdout=subprocess.PIPE).communicate()):
    #         time.sleep(1)
    #
    #     time.sleep(3)

    prev_db_host = os.environ.get('HIT_DB_HOST')
    db_p = tmp_path_factory.mktemp("db_data")
    os.environ['HIT_DB_HOST'] = f"/{os.path.join(str(db_p), 'test_db.sqlite')}"
    utils.create_db_engine()
    request.getfixturevalue("setup_db")

    print(f'db up in {int(time.time() - start)} seconds')
    db_str = utils.get_db_conection_string()
    yield db_str
    if prev_db_host is not None:
        os.environ['HIT_DB_HOST'] = prev_db_host


@pytest.mark.xdist_group(name="integration")
@pytest.fixture()
def session_fixture(request, tmp_path_factory, worker_id):
    if (os.environ.get('CI_PIPELINE_SOURCE') is None and not os.environ.get('SUNPEEK_TEST_DB_MIGRATION', False)) or \
            os.environ.get('HIT_DB_TYPE') == 'sqlite':
        # Normal test session running locally
        db_str = request.getfixturevalue('start_local_db_session')
        local_db_session = utils.S()
        yield local_db_session
        local_db_session.close()
    elif os.environ.get('CI_PIPELINE_SOURCE') is not None and not os.environ.get('SUNPEEK_TEST_DB_MIGRATION', False):
        # Normal test session running in CI
        request.getfixturevalue("setup_db")
        session = utils.S()
        yield session
        session.close()
    else:
        # DB migration test session, local or CI
        session = utils.S()
        yield session
        session.close()


@pytest.mark.xdist_group(name="integration")
@pytest.fixture(scope="session")
def S(request, tmp_path_factory, worker_id):
    if os.environ.get('CI_PIPELINE_SOURCE') is None and not os.environ.get('SUNPEEK_TEST_DB_MIGRATION', False) or \
            os.environ.get('HIT_DB_TYPE') == 'sqlite':
        db_str = request.getfixturevalue('start_local_db_session')
        return utils.S
    elif os.environ.get('CI_PIPELINE_SOURCE') is not None and not os.environ.get('SUNPEEK_TEST_DB_MIGRATION', False):
        request.getfixturevalue("setup_db")
    return utils.S


# @pytest.fixture
# def psycopg2_con_params():
#     return {"dbname": os.environ.get('HIT_DB_NAME'), "user": os.environ.get('HIT_DB_USER'),
#             "password": os.environ.get('HIT_DB_PW')}


@pytest.mark.xdist_group(name="integration")
@pytest.fixture(scope='session')
def conf():
    """Returns FHW plant config JSON file"""
    with open(sunpeek.demo.DEMO_CONFIG_PATH) as f:
        return json.load(f)


@pytest.mark.xdist_group(name="integration")
@pytest.fixture(scope='session')
def conf_clean():
    """Returns FHW plant config JSON file"""
    with open(sunpeek.demo.DEMO_CONFIG_PATH) as f:
        conf = json.load(f)
        return conf


@pytest.mark.xdist_group(name="integration")
@pytest.fixture
def db_test_plant_clean(S, conf):
    conf_local = copy.deepcopy(conf)
    conf_local['plant']['name'] = 'FHW Arcon South _Test clean_'  # will re-use old data otherwise
    with S.begin() as session:
        p = config_parser.make_and_store_plant(conf_local, session)
        p.context = Context(plant=p, datasource='pq')
        id = p.id

    yield p, id


@pytest.mark.xdist_group(name="integration")
@pytest.fixture(scope='session')
def db_test_plant(S, conf, tmp_path_factory, worker_id):
    with S.begin() as session:
        p = config_parser.make_and_store_plant(conf, session)
        p.context = Context(plant=p, datasource='pq')
        id = p.id

    yield p, id


@pytest.mark.xdist_group(name="integration")
@pytest.fixture
def db_test_plant__no_fluid(S, conf, tmp_path_factory):
    conf_copy = copy.deepcopy(conf)
    conf_copy['plant']['name'] = 'FHW Arcon South _Test no-fluid_'
    del conf_copy['plant']['fluid_solar']

    with S.begin() as session:
        p = config_parser.make_and_store_plant(conf_copy, session)
        p.context = Context(plant=p, datasource='pq')
        id = p.id

    yield p, id



@pytest.mark.xdist_group(name="integration")
@pytest.fixture(scope='session')
def db_test_plant_with_data(db_test_plant, S, conf, tmp_path_factory, worker_id):
    with open(sunpeek.demo.DEMO_DATA_PATH_1MONTH, 'rb') as f:
        fu = UploadFile(filename=f.name, file=f)

        if worker_id == "master":
            with S.begin() as s:
                upload_measure_data(db_test_plant[1], [fu], timezone='UTC', sess=s, crd=crud,
                                    datetime_template=DatetimeTemplates.year_month_day)


        else:
            root_tmp_dir = tmp_path_factory.getbasetemp().parent
            fn = root_tmp_dir / "plant.data_added"
            with FileLock(str(fn) + ".lock"):
                if fn.is_file():
                    pass
                else:
                    with S.begin() as s:
                        upload_measure_data(db_test_plant.id, [fu], timezone='UTC', sess=s, crd=crud,
                                            datetime_template=DatetimeTemplates.year_month_day)
                    open(str(fn), 'a').close()
    yield db_test_plant[0], db_test_plant[1]


@pytest.fixture(scope='session')
def run_migrations(db_test_plant, db_test_plant_with_data):
    if os.environ.get('SUNPEEK_TEST_DB_MIGRATION', False):
        subprocess.Popen(['alembic', 'upgrade', 'head'],
                         cwd=os.path.join(os.path.dirname(os.path.realpath(__file__)), '..', '..'))


@pytest.mark.xdist_group(name="integration")
@pytest.fixture()
def random_stored_plant(conf_clean, S):
    conf_clean['plant']['name'] = randomword(8)
    conf_clean['plant']['arrays'][0]['name'] = 'test array'
    with S.begin() as session:
        try:
            p = config_parser.make_and_store_plant(conf_clean, session)
            id = p.id
        except (sqlalchemy.exc.IntegrityError, sqlalchemy.exc.OperationalError) as e:
            print(e)
            session.rollback()
            raise
    yield conf_clean['plant']['name'], p, id


@pytest.fixture(scope='session', autouse=True)
def temp_data_dir(tmp_path_factory):
    prev_raw = os.environ.get('SUNPEEK_RAW_DATA_PATH')
    prev_calc = os.environ.get('SUNPEEK_CALC_DATA_PATH')
    data_p = tmp_path_factory.mktemp("data")
    os.environ['SUNPEEK_RAW_DATA_PATH'] = os.path.join(data_p, 'raw_data')
    os.environ['SUNPEEK_CALC_DATA_PATH'] = os.path.join(data_p, 'calc_data')
    yield
    if prev_raw is not None:
        os.environ['SUNPEEK_RAW_DATA_PATH'] = prev_raw
    if prev_calc is not None:
        os.environ['SUNPEEK_CALC_DATA_PATH'] = prev_calc


@pytest.fixture(scope='session')
def plant_noarrays_nopower_id(S):
    plant = cmp.Plant(name='example_plant', latitude=Q(1, "deg"), longitude=Q(1, "deg"))
    plant.set_sensors(te_amb=cmp.Sensor('te_amb', 'degC'))
    with S.begin() as s:
        s.add(plant)
        s.flush()
        id = plant.id
    with S.begin() as s:
        with open(sunpeek.demo.DEMO_DATA_PATH_2DAYS, 'rb') as f:
            fu = UploadFile(filename=f.name, file=f)
            upload_measure_data(id, [fu], timezone='UTC', sess=s, crd=crud,
                                datetime_template=DatetimeTemplates.year_month_day)

    return id


@pytest.fixture
def plant_with_data(session_fixture, random_stored_plant):
    _, plant, plant_id = random_stored_plant
    # Note: Tested with just 10 minutes of FHW data, no speed-up in tests
    with open(sunpeek.demo.DEMO_DATA_PATH_2DAYS, 'rb') as f:
        fu = UploadFile(filename=f.name, file=f)
        files.upload_measure_data(plant_id, files=[fu], timezone='UTC', sess=session_fixture, crd=crud,
                                  datetime_template=utils.DatetimeTemplates.year_month_day)

    plant = session_fixture.query(cmp.Plant).filter(cmp.Plant.id == plant_id).one()
    # New plant with data is uptodate
    assert plant.virtuals_calculation_uptodate

    return plant, plant_id

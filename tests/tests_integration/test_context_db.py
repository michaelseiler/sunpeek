"""
Goal of this module is to have tests for the Context class that rely on the database.
The rest of the Context-related unit tests is in tests_backend_general/test_context.
"""

import pytest
import pandas as pd
from sunpeek.data_handling.context import Context, DataSources


@pytest.mark.xdist_group(name="integration")
@pytest.mark.parametrize('datasource_in', ['pq'])
def test_datasource(db_test_plant_with_data, datasource_in):
    ctxt = Context(plant=db_test_plant_with_data, datasource=datasource_in)
    assert ctxt.datasource == DataSources.parquet



## --- reset cache

@pytest.mark.xdist_group(name="integration")
def test_cache_add_operational_event_ignored_false(session_fixture, db_test_plant_with_data):
    p = db_test_plant_with_data[0]
    session_fixture.add(p)
    c = p.context
    data = p.te_amb.data
    if c.cache.is_empty:
        pytest.fail('Assuming non-empty context.cache here, but cache is empty.', pytrace=True)
    old_cache = c.cache.cache.copy()   # copy the dict
    p.add_operational_event(pd.to_datetime('2021-09-13', utc=True),
                            pd.to_datetime('2021-09-14', utc=True),
                            description='foo', ignored_range=False)
    assert old_cache == p.context.cache.cache


@pytest.mark.xdist_group(name="integration")
def test_cache_add_operational_event_ignored_true(db_test_plant_with_data):
    p = db_test_plant_with_data[0]
    c = p.context
    data = p.plant.te_amb.data
    if c.cache.is_empty:
        pytest.fail('Assuming non-empty context.cache here, but cache is empty.', pytrace=True)
    p.add_operational_event(pd.to_datetime('2021-09-13', utc=True),
                            pd.to_datetime('2021-09-14', utc=True),
                            description='foo', ignored_range=True)
    assert p.context.cache.is_empty


@pytest.mark.xdist_group(name="integration")
def test_sensor_replacement_clears_cache(session_fixture, db_test_plant_with_data):
    p = db_test_plant_with_data[0]
    session_fixture.add(p)
    c = p.context
    sensor = p.te_amb
    data = sensor.data  # Fill the context.cache
    if c.cache.is_empty:
        pytest.fail('Assuming non-empty context.cache here, but cache is empty.', pytrace=True)
    sensor.value_replacements = {}
    assert p.context.cache.is_empty

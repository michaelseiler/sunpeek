import io
import time
import tarfile
from fastapi.testclient import TestClient
import pytest

from sunpeek.api.main import app
from sunpeek.common import utils


app.dependency_overrides = {}
client = TestClient(app)


@pytest.mark.xdist_group(name="integration")
def test_export_config(session_fixture, db_test_plant_with_data):
    request_url = f"/plants/{db_test_plant_with_data[1]}/export_config?data=true&token={utils.API_TOKEN}"

    response = client.get(request_url)
    assert response.status_code == 200


@pytest.mark.xdist_group(name="integration")
def test_export_config__no_fluid(session_fixture, db_test_plant__no_fluid):
    request_url = f"/plants/{db_test_plant__no_fluid[1]}/export_config?data=true&token={utils.API_TOKEN}"

    response = client.get(request_url)
    assert response.status_code == 200


@pytest.mark.xdist_group(name="integration")
def test_export_w_data(session_fixture, db_test_plant_with_data):
    request_url = f"/plants/{db_test_plant_with_data[1]}/export_complete?data=true&token={utils.API_TOKEN}"
    response = client.post(request_url)
    assert response.status_code == 202
    status_url = response.json()['href'] + f"?token={utils.API_TOKEN}"
    i = 0
    while client.get(status_url).json()['status'] != 'done':
        if i > 30:
            raise TimeoutError('Export Job did not complete within 60 seconds')
        i += i
        time.sleep(2)
        response = client.get(status_url)
        assert response.json()['status'] != 'failed'

    result_url = client.get(status_url).json()['result_url'] + f"?token={utils.API_TOKEN}"
    response = client.get(result_url)
    tf = tarfile.open(fileobj=io.BytesIO(response.content))

    plant = db_test_plant_with_data[0]
    session_fixture.add(plant)
    assert tf.getnames() == \
           [f'configuration_{plant.name}.json', f'rawdata_{plant.name}_2017.csv']

import pytest
from sqlalchemy.orm import Session
from sqlalchemy.exc import IntegrityError
import sunpeek.components as cmp
import sunpeek.components.sensor_types as st

def test_mapping_sensors_combines_plant_map_sensor(session_fixture, random_stored_plant):
    p = random_stored_plant[1]
    session_fixture.add(p)
    s = cmp.Sensor('Test sensor 1', 'K')

    # initial
    assert s not in p.raw_sensors
    assert s.plant is None

    p.map_sensor(s, 'te_amb')
    assert p in session_fixture
    session_fixture.commit()

    # sensor mapping combines sensor, plant, and mapping
    assert s in p.raw_sensors
    assert s.plant == p
    assert p.sensors["te_amb"] == s
    assert p.sensor_map["te_amb"].sensor == s


def test_delete_sensor_via_plant_removes_mapping(session_fixture, random_stored_plant):
    p = random_stored_plant[1]
    session_fixture.add(p)
    s = cmp.Sensor('Test sensor 1', 'K')
    p.map_sensor(s, 'te_amb')

    assert p in session_fixture
    session_fixture.commit()

    # sensor mapping combines sensor, plant, and mapping
    assert s in p.raw_sensors
    assert s.plant == p
    assert p.sensors["te_amb"] == s
    assert p.sensor_map["te_amb"].sensor == s

    # removing sensor (deleting from plant) reverts that
    p.raw_sensors.remove(s)
    session_fixture.commit()

    assert s not in p.raw_sensors
    assert s.plant is None
    assert "te_amb" not in p.sensors
    assert "te_amb" not in p.sensor_map


def test_delete_sensor_via_sensor_removes_mapping(session_fixture, random_stored_plant):
    p = random_stored_plant[1]
    session_fixture.add(p)
    s = cmp.Sensor('Test sensor 1', 'K')
    p.map_sensor(s, 'te_amb')

    session_fixture.commit()

    # sensor mapping combines sensor, plant, and mapping
    assert s in p.raw_sensors
    assert s.plant == p
    assert p.sensors["te_amb"] == s
    assert p.sensor_map["te_amb"].sensor == s

    # removing sensor (deleting from plant) reverts that
    s.plant = None
    session_fixture.commit()

    assert s not in p.raw_sensors
    assert s.plant is None
    assert "te_amb" not in p.sensor_map
    assert "te_amb" not in p.sensors


# def test_delete_sensor_via_sensor_removes_mapping(session, make_and_store_plant):
#     p = make_and_store_plant[1]
#     session = Session.object_session(p)
#     s = cmp.Sensor('Test sensor 1', 'K')
#     cmp.SensorMap('te_amb', s, 'ambient_temperature', p)
#     with pytest.raises(IntegrityError):
#         session.commit()


def test_mapping_sensor_after_load(session_fixture, random_stored_plant):
    p = random_stored_plant[1]
    s = cmp.Sensor('Test sensor 1', 'K')
    session_fixture.add(s)
    session_fixture.commit()
    s_id = s.id
    del s

    s = session_fixture.query(cmp.Sensor).filter(cmp.Sensor.id == s_id).one()
    assert s._sensor_type is None
    assert len(s.mappings) == 0
    session_fixture.add(p)
    p.te_amb = s
    assert len(s.mappings) == 1
    assert p.sensors['te_amb'] is s
    assert s.sensor_type.name == 'ambient_temperature'
    assert s.sensor_type is st.ambient_temperature

import pytest
import numpy as np

from sunpeek.common.unit_uncertainty import Q, to_s
from sunpeek.common.errors import ConfigurationError
from sunpeek.components import CoolPropFluidDefinition, CoolPropFluid, WPDFluidDefinition, FluidFactory


@pytest.mark.xdist_group(name="integration")
@pytest.mark.parametrize('fluid_info', [
    ('water', None, 'water'),
    # ('DSf', None, 'DSF'),  # excluded from fluids list, see https://gitlab.com/sunpeek/web-ui/-/issues/110
    # ('LiBr', Q(0.5), 'LiBr'),
    ('Glykosol', Q(10, 'percent'), 'GKN'),
    ('lYko sol', Q(0.28, ''), 'GKN'),
    ('Pekasol 2000', Q(0), 'PK2'),
    ('Pekasol L', Q(100, 'percent'), 'PKL'),
    ('Antifrogen N', Q(1), 'AN'),
    ('AntifrogenL', Q(30, 'percent'), 'AL')
])
def test_creation(fluid_info, session_fixture):
    str_user_in, conc, coolprop_fluid = fluid_info
    fluid_def = CoolPropFluidDefinition.get_definition(str_user_in, session_fixture)
    fl = CoolPropFluid(fluid_def, concentration=conc)
    assert fl.fluid.name == coolprop_fluid
    assert fl.concentration == conc
    if conc is not None:
        assert not fl.fluid.is_pure


get_definition = CoolPropFluidDefinition.get_definition


@pytest.mark.xdist_group(name="integration")
@pytest.mark.parametrize('item', [
    ('water', None, to_s(300, 'K'), 996.5569352652021)])
def test_density(item, session_fixture):
    fl = CoolPropFluid(get_definition(item[0], session_fixture), concentration=item[1])
    val = fl.get_density(item[2])
    assert pytest.approx(val[0].to('kg m**-3'), 0.01) == item[3]


@pytest.mark.xdist_group(name="integration")
@pytest.mark.parametrize('item', [
    ('water', None, to_s(300, 'K'), 4180.6357765560715),
    ('AntifrogenL', Q(0.28), to_s(60, 'degC'), 3979.7673195)])
def test_heat_capacity(item, session_fixture):
    fl = CoolPropFluid(get_definition(item[0], session_fixture), concentration=item[1])
    val = fl.get_heat_capacity(item[2])
    assert pytest.approx(val[0].to('J kg**-1 K**-1'), 0.01) == item[3]


@pytest.mark.xdist_group(name="integration")
def test_invalid(session_fixture):
    with pytest.raises(ConfigurationError) as e:
        fl_def = CoolPropFluidDefinition.get_definition('pEka', session_fixture)
    assert 'more than 1 result found' in str(e.value).lower()
    with pytest.raises(ConfigurationError) as e:
        fl_def = CoolPropFluidDefinition.get_definition('xyz', session_fixture)
    assert 'no entry found for xyz' in str(e.value).lower()


@pytest.mark.xdist_group(name="integration")
@pytest.mark.parametrize('fluid_name, concentration', [('FHW, Pekasolar', None), ('Gasokol, corroStar mixture', None)])
def test_property_access_database(fluid_name, concentration, session_fixture):
    """Loads fluids from the database and tests property access (density, heat capacity)."""
    fluid_def = WPDFluidDefinition.get_definition(fluid_name, session_fixture)
    fluid = FluidFactory(fluid=fluid_def, concentration=concentration)

    # te = Q(np.linspace(-10, 110, 13), 'degC')
    te = to_s(np.linspace(-10, 110, 13), 'degC')
    rho = fluid.get_density(te)
    cp = fluid.get_heat_capacity(te)


@pytest.mark.xdist_group(name="integration")
@pytest.mark.parametrize('fluid_name, concentration', [('FHW, Pekasolar', Q(28, 'percent'))])
def test_warn_pure_concentration(fluid_name, concentration, session_fixture):
    """Test that a warning is issued when passing a non-None concentration to a pure fluid."""
    fluid_def = WPDFluidDefinition.get_definition(fluid_name, session_fixture)
    with pytest.warns(UserWarning, match='gracefully ignore the concentration'):
        fluid = FluidFactory(fluid=fluid_def, concentration=concentration)

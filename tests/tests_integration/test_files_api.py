import pytest
import json
import pandas as pd
from fastapi.testclient import TestClient
import numpy as np

import sunpeek.demo
from sunpeek.api.main import app
from sunpeek.common import utils
from sunpeek.common.utils import sp_logger, to_utc, to_unix_str, utc_str, json_to_df
from sunpeek.common import config_parser
import sunpeek.components as cmp
from sunpeek.db_utils import crud as crd

client = TestClient(app)


@pytest.mark.xdist_group(name="integration")
def test_upload_measure_file(session_fixture, random_stored_plant):
    timezone = "Europe/Vienna"
    datetime_template = "year_month_day"
    plant_name, plant, plant_id = random_stored_plant

    with open(sunpeek.demo.DEMO_DATA_PATH_2DAYS, "rb") as f:
        request_url = f"plants/{plant_id}/data"
        single_file = [("files", ("test.csv", f, "text/csv"))]
        params = {
            "token": utils.API_TOKEN,
            "timezone": timezone,
            "datetime_template": datetime_template,
        }
        response = client.post(request_url, files=single_file, params=params)

    assert response.status_code == 201


@pytest.mark.xdist_group(name="integration")
def test_upload_measure_files(session_fixture, random_stored_plant, resources_dir):
    timezone = "Europe/Vienna"
    datetime_template = "year_month_day"
    plant_name, plant, plant_id = random_stored_plant

    request_url = f"plants/{plant_id}/data?timezone={timezone}&datetime_template={datetime_template}&token={utils.API_TOKEN}"
    measure_file_path = resources_dir / "data/data_uploader"
    multiple_files = [
        ("files", ("csv1.csv", open(measure_file_path / "csv1.csv", "rb"), "text/csv")),
        ("files", ("csv2.csv", open(measure_file_path / "csv2.csv", "rb"), "text/csv")),
    ]
    response = client.post(request_url, files=multiple_files)

    assert response.status_code == 201


@pytest.mark.xdist_group(name="integration")
def test_upload_measure_file_twice_mixedcase_sensornames(S, resources_dir):
    timezone = "Europe/Vienna"
    datetime_template = "year_month_day"

    with open(sunpeek.demo.DEMO_CONFIG_PATH, "r") as f:
        conf = json.load(f)
    conf["plant"]["name"] = "_test_double_upload_"
    i = [sensor["raw_name"] for sensor in conf["plant"]["raw_sensors"]].index("te_amb")
    conf["plant"]["raw_sensors"][i]["raw_name"] = "T_amb"
    conf["plant"]["sensor_map"]["te_amb"] = "T_amb"
    with S.begin() as session:
        plant = config_parser.make_and_store_plant(conf, session)
        plant_id = plant.id

    with open(str(resources_dir) + "/data/FHW_test_mixed_case.csv", "rb") as f:
        request_url = (
            f"plants/{plant_id}/data?timezone={timezone}&datetime_template={datetime_template}&"
            f"token={utils.API_TOKEN}"
        )
        multiple_files = [("files", ("test.csv", f, "text/csv"))]

        response = client.post(request_url, files=multiple_files)
        sp_logger.info(
            f"[test_upload_measure_file] Response contents: {response.content}, Status code: {response.status_code}"
        )

        response = client.post(request_url, files=multiple_files)
        sp_logger.info(
            f"[test_upload_measure_file] Response contents: {response.content}, Status code: {response.status_code}"
        )

    assert response.status_code == 201


def test_overlapping_uploads__no_duplicates(session_fixture, random_stored_plant, resources_dir):
    # Test that multiple uploads with overlapping data don't lead to duplicates in data,
    # and that the newer upload overwrites the older one.
    pname, p, pid = random_stored_plant
    p = session_fixture.query(cmp.Plant).filter(cmp.Plant.id == pid).one()
    assert p.raw_sensors[2].raw_name == "ve_wind"
    sid = p.raw_sensors[2].id

    # Upload 2 overlapping files, in 2 separate uploads. 1st upload:
    url = f"plants/{pid}/data"
    measure_file_path = resources_dir / "data/data_uploader"
    params = {"token": utils.API_TOKEN,
              "timezone": "UTC",
              "datetime_template": "year_month_day"}
    csv1 = [("files", ("csv1.csv", open(measure_file_path / "csv1.csv", "rb"), "text/csv"))]
    r = client.post(url, files=csv1, params=params)

    assert r.status_code == 201
    assert p.context.datasource == 'pq'

    s = crd.get_sensors(session_fixture, plant_id=pid, id=sid)
    data = s.data
    assert len(data) == 5

    # 2nd upload:
    csv1_2_ = [("files", ("csv1_2_.csv", open(measure_file_path / "csv1_2_.csv", "rb"), "text/csv"))]
    r = client.post(url, files=csv1_2_, params=params)
    assert r.status_code == 201

    # Assert sensor data not overlapping
    p.reset_cache()  # Otherwise old values are accessed via cache
    data = s.data
    expected_index = pd.date_range(start="2017-05-01 00:00:00", end="2017-05-01 00:07:00", freq="1T", tz="UTC")
    expected_data = [0.235, 0.183333333, 10, 10, 10, 10, 10, 10]

    assert all(s.data.index == expected_index)
    assert [x.magnitude for x in data] == expected_data


@pytest.fixture
def plant_for_delete_data(session_fixture, random_stored_plant):
    # Upload data
    plant_name, plant, plant_id = random_stored_plant
    with open(sunpeek.demo.DEMO_DATA_PATH_2DAYS, "rb") as f:
        url = f"plants/{plant_id}/data"
        params = {
            "token": utils.API_TOKEN,
            "timezone": "UTC",
            "datetime_template": "year_month_day",
        }
        response = client.post(
            url, params=params, files=[("files", ("some data", f, "text/csv"))]
        )
    assert response.status_code == 201

    # Assert data before deletion
    session_fixture.add(plant)
    sensor_id = plant.raw_sensors[0].id
    url = f"plants/{plant_id}/sensors/{sensor_id}/data?token={utils.API_TOKEN}"
    response = client.get(url)
    assert response.status_code == 200
    data = response.json()

    df = json_to_df(data)
    print(f"Data uploaded. {df.index[0]} --> {df.index[-1]}")

    t = to_unix_str("2017-05-01 12:00:00")
    print(f"- Assert has data: {utc_str(t)}")
    assert t in data.keys()
    assert not np.isnan(data[t])

    return plant_id, sensor_id


@pytest.mark.xdist_group(name="integration")
def test_delete_data__in_between_api(plant_for_delete_data):
    # Delete some data in the middle of available data
    plant_id, sensor_id = plant_for_delete_data

    # Delete data
    url = f"plants/{plant_id}/data"
    params = {
        "token": utils.API_TOKEN,
        "start": to_utc("2017-05-01 10:00:00"),
        "end": to_utc("2017-05-01 14:00:00"),
    }
    response = client.delete(url, params=params)
    assert response.status_code == 200
    print(f'Deleted: {utc_str(params["start"])} --> {utc_str(params["end"])}')

    # Get data after deletion
    url = f"plants/{plant_id}/sensors/{sensor_id}/data?token={utils.API_TOKEN}"
    response = client.get(url)
    assert response.status_code == 200
    data = response.json()
    df = json_to_df(data)
    print(f"Returned after delete: {df.index[0]} --> {df.index[-1]}")

    # Data outside deleted period ok?
    t = to_unix_str("2017-05-01 09:59:00")
    print(f"- Assert has data: {utc_str(t)}.")
    assert not np.isnan(data[t])

    t = to_unix_str("2017-05-01 14:01:00")
    print(f"- Assert has data: {utc_str(t)}.")
    assert not np.isnan(data[t])

    # Data actually deleted?
    t = to_unix_str("2017-05-01 12:00:00")
    print(f"- Assert has NO data: {utc_str(t)}.")
    assert t not in data

    # Data start & end correct?
    url = f"plants/{plant_id}/data_start_end?token={utils.API_TOKEN}"
    response = client.get(url)
    expected = {
        "start": "2017-05-01T00:00:00+01:00",
        "end": "2017-05-02T23:59:00+01:00",
    }

    assert response.json() == expected


@pytest.mark.xdist_group(name="integration")
@pytest.mark.parametrize("start, end, t_ok, t_deleted, expected", [
    ("2017-05-01 10:00:00", "2017-05-03 14:00:00",
     "2017-05-01 09:59:00", "2017-05-01 10:01:00",
     {"start": "2017-05-01T00:00:00+01:00", "end": "2017-05-01T10:59:00+01:00"}),
    ("2017-04-01 10:00:00", "2017-05-01 10:00:00",
     "2017-05-01 10:01:00", "2017-05-01 09:59:00",
     {"start": "2017-05-01T11:01:00+01:00", "end": "2017-05-02T23:59:00+01:00"}),
])
def test_delete_data__overlapping_start_end(plant_for_delete_data, start, end, t_ok, t_deleted, expected):
    # Delete some data overlapping with start / end of available data
    plant_id, sensor_id = plant_for_delete_data

    # Delete data
    url = f"plants/{plant_id}/data"
    params = {"token": utils.API_TOKEN, "start": to_utc(start), "end": to_utc(end)}
    response = client.delete(url, params=params)
    assert response.status_code == 200
    print(f'Deleted: {utc_str(params["start"])} --> {utc_str(params["end"])}')

    # Get data after deletion
    url = f"plants/{plant_id}/sensors/{sensor_id}/data?token={utils.API_TOKEN}"
    response = client.get(url)
    assert response.status_code == 200
    data = response.json()
    df = json_to_df(data)
    print(f"Returned after delete: {df.index[0]} --> {df.index[-1]}")

    # Data outside deleted period ok?
    t = to_unix_str(t_ok)
    print(f"- Assert has data: {utc_str(t)}.")
    assert not np.isnan(data[t])

    # Data actually deleted?
    t = to_unix_str(t_deleted)
    print(f"- Assert has NO data: {utc_str(t)}.")
    assert t not in data

    # Data start & end correct?
    url = f"plants/{plant_id}/data_start_end?token={utils.API_TOKEN}"
    response = client.get(url)
    assert response.json() == expected


@pytest.mark.xdist_group(name="integration")
@pytest.mark.parametrize(
    "start, end",
    [
        ("2017-04-30 23:00:00", "2017-05-02 22:59:00"),  # exact start & end dates
        ("2017-01-01 00:00:00", "2017-05-02 22:59:00"),  # overlapping
        ("2017-01-01 00:00:00", "2018-01-01 00:00:00"),
    ],
)
def test_delete_data__all(plant_for_delete_data, start, end):
    # Delete all data
    plant_id, sensor_id = plant_for_delete_data

    request_url = f"plants/{plant_id}/data"
    params = {"token": utils.API_TOKEN, "start": to_utc(start), "end": to_utc(end)}
    response = client.delete(request_url, params=params)
    assert response.status_code == 200
    print(f'Deleted: {utc_str(params["start"])} --> {utc_str(params["end"])}')

    # Get data after deletion
    url = f"plants/{plant_id}/sensors/{sensor_id}/data?token={utils.API_TOKEN}"
    response = client.get(url)
    assert response.status_code == 200
    data = response.json()
    assert len(data) == 0

    # Data actually deleted?
    t = to_unix_str("2017-05-01 10:00:00")
    print(f"- Assert has NO data: {utc_str(t)}.")
    assert t not in data

    # Data start & end correct?
    request_url = f"plants/{plant_id}/data_start_end?token={utils.API_TOKEN}"
    response = client.get(request_url)

    expected_start_end = {"start": None, "end": None}
    assert response.json() == expected_start_end


def test_delete_data__inverted_fails(plant_for_delete_data):
    start, end = ("2018-01-01 00:00:00", "2017-01-01 00:00:00")
    plant_id, sensor_id = plant_for_delete_data

    request_url = f"plants/{plant_id}/data"
    params = {"token": utils.API_TOKEN, "start": to_utc(start), "end": to_utc(end)}
    with pytest.raises(ValueError, match='Timestamp "start" must be equal or less than "end"'):
        client.delete(request_url, params=params)


@pytest.mark.xdist_group(name="integration")
def test_parse_warnings(resources_dir, db_test_plant_with_data):
    datetime_template = "year_month_day"
    plant_id = db_test_plant_with_data[1]

    # Generates warning because timezone missing in request_url
    request_url = f"plants/{plant_id}/data?datetime_template={datetime_template}&token={utils.API_TOKEN}"
    measure_file_path = resources_dir / "data/data_uploader"
    multiple_files = [
        ("files", ("csv1.csv", open(measure_file_path / "csv1.csv", "rb"), "text/csv")),
        ("files", ("csv2.csv", open(measure_file_path / "csv2.csv", "rb"), "text/csv")),
    ]

    response = client.post(request_url, files=multiple_files)
    assert response.status_code == 201
    assert response.headers["x-sunpeek-warnings"] == (
        '["Failed to read csv file using pandas read_csv. No timezone information '
        'found: \'timezone\' is set to \'None\' but no timezone information was found in the '
        'provided data.", "Failed to read csv file using pandas read_csv. No '
        'timezone information found: \'timezone\' is set to \'None\' but no timezone '
        'information was found in the provided data.", \'Reading csv files '
        'resulted in a DataFrame with less than 2 rows.\', \'Cannot set DataFrame in '
        'Context: DataFrame is None.\']')

@pytest.mark.xdist_group(name="integration")
def test_parse_warnings_timezone_string_none(resources_dir, db_test_plant_with_data):
    timezone = "UTC offset included in data"
    datetime_template = "year_month_day"
    plant_id = db_test_plant_with_data[1]

    # Generates warning because timezone missing in request_url
    request_url = f"plants/{plant_id}/data?datetime_template={datetime_template}&timezone={timezone}&token={utils.API_TOKEN}"
    measure_file_path = resources_dir / "data/data_uploader"
    multiple_files = [
        ("files", ("csv1.csv", open(measure_file_path / "csv1.csv", "rb"), "text/csv")),
        ("files", ("csv2.csv", open(measure_file_path / "csv2.csv", "rb"), "text/csv")),
    ]

    response = client.post(request_url, files=multiple_files)
    assert response.status_code == 201
    assert response.headers["x-sunpeek-warnings"] == (
        "[\"Failed to read csv file using pandas read_csv. No timezone information "
        "found: \'timezone\' is set to \'UTC offset included in data\' but no timezone information was found in the "
        "provided data.\", \"Failed to read csv file using pandas read_csv. No "
        "timezone information found: \'timezone\' is set to \'UTC offset included in data\' but no timezone "
        "information was found in the provided data.\", \'Reading csv files "
        "resulted in a DataFrame with less than 2 rows.\', \'Cannot set DataFrame in "
        "Context: DataFrame is None.\']")


@pytest.mark.xdist_group(name="integration")
def test_get_upload_history(session_fixture, random_stored_plant):
    timezone = "UTC"
    datetime_template = "year_month_day"
    plant_name, plant, plant_id = random_stored_plant

    with open(sunpeek.demo.DEMO_DATA_PATH_2DAYS, "rb") as f:
        request_url = f"plants/{plant_id}/data"
        multiple_files = [("files", ("test.csv", f, "text/csv"))]
        params = {
            "token": utils.API_TOKEN,
            "timezone": timezone,
            "datetime_template": datetime_template,
        }
        response = client.post(request_url, files=multiple_files, params=params)
        sp_logger.info(
            f"[test_upload_measure_file] Response contents: {response.content}, Status code: {response.status_code}"
        )

    assert response.status_code == 201

    request_url = f"plants/{plant_id}/data/history?token={utils.API_TOKEN}"
    response = client.get(request_url)
    assert response.status_code == 201

    data = response.json()
    latest_data = data[-1]
    assert latest_data["name"] == "test.csv"
    assert latest_data["size_bytes"] == 684537
    assert latest_data["error_cause"] is None
    assert pd.to_datetime(latest_data["start"]) == pd.to_datetime("2017-04-30T23:00:00")
    assert pd.to_datetime(latest_data["end"]) == pd.to_datetime("2017-05-02T22:59:00")
    assert latest_data["missing_columns"] == []


@pytest.mark.xdist_group(name="integration")
def test_get_upload_history_missingColumns(session_fixture, random_stored_plant):
    timezone = "UTC"
    datetime_template = "year_month_day"
    plant_name, plant, plant_id = random_stored_plant

    # add new sensor, which is not in data
    # --------------------------------------
    plant = session_fixture.query(cmp.Plant).get(plant_id)
    new_sensor = cmp.Sensor("Test Sensor", native_unit="W/m²", plant=plant)
    plant.map_sensor(new_sensor, "in_global")
    session_fixture.commit()
    # --------------------------------------

    with open(sunpeek.demo.DEMO_DATA_PATH_2DAYS, "rb") as f:
        request_url = f"plants/{plant_id}/data"
        multiple_files = [("files", ("test.csv", f, "text/csv"))]
        params = {
            "token": utils.API_TOKEN,
            "timezone": timezone,
            "datetime_template": datetime_template,
        }
        response = client.post(request_url, files=multiple_files, params=params)
        sp_logger.info(
            f"[test_upload_measure_file] Response contents: {response.content}, Status code: {response.status_code}"
        )
    assert response.status_code == 201

    request_url = f"plants/{plant_id}/data/history?token={utils.API_TOKEN}"
    response = client.get(request_url)
    assert response.status_code == 201

    data = response.json()
    latest_data = data[-1]
    assert latest_data["name"] == "test.csv"
    assert latest_data["size_bytes"] == 684537
    assert latest_data["error_cause"] is None
    assert pd.to_datetime(latest_data["start"]) == pd.to_datetime("2017-04-30T23:00:00")
    assert pd.to_datetime(latest_data["end"]) == pd.to_datetime("2017-05-02T22:59:00")
    assert latest_data["missing_columns"] == ["Test Sensor"]


@pytest.mark.xdist_group(name="integration")
def test_delete_history_entry(session_fixture, random_stored_plant):
    timezone = "UTC"
    datetime_template = "year_month_day"
    plant_name, plant, plant_id = random_stored_plant

    with open(sunpeek.demo.DEMO_DATA_PATH_2DAYS, "rb") as f:
        request_url = f"plants/{plant_id}/data"
        multiple_files = [("files", ("test.csv", f, "text/csv"))]
        params = {
            "token": utils.API_TOKEN,
            "timezone": timezone,
            "datetime_template": datetime_template,
        }
        response = client.post(request_url, files=multiple_files, params=params)
        sp_logger.info(
            f"[test_upload_measure_file] Response contents: {response.content}, Status code: {response.status_code}"
        )
    assert response.status_code == 201

    entry_id = response.json()["response_per_file"][0]["id"]

    request_url = f"plants/{plant_id}/data/history/{entry_id}?token={utils.API_TOKEN}"
    response = client.delete(request_url)
    assert response.status_code == 201

    request_url = f"plants/{plant_id}/data/history?token={utils.API_TOKEN}"
    response = client.get(request_url)
    assert response.status_code == 201
    assert len(response.json()) == 0

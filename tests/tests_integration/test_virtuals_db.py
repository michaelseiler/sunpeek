import pytest

from sunpeek.core_methods.virtuals import calculations as algos
import sunpeek.core_methods.virtuals as virtuals
import sunpeek.components as cmp


@pytest.mark.xdist_group(name="integration")
def test_cache_populated(session_fixture, db_test_plant_with_data):
    plant = session_fixture.query(cmp.Plant).filter(cmp.Plant.id == db_test_plant_with_data[1]).one()
    sensor = plant.te_amb
    sensor.data    # this retrieves data and stores in context.cache
    assert sensor.raw_name in plant.context.cache


@pytest.mark.xdist_group(name="integration")
@pytest.mark.skip(reason='This test is very slow, we need to speed things up a lot when updating sensors with '
                         'db backend, but for now we are skipping the test')
def test_stored_data_dimension_mismatch(db_test_plant_with_data):
    # Virtual sensors have been calculated with dataframe backend in fixture db_test_plant_with_data
    # Store another vsensor calculation result in database, make sure dimensions are ok
    # So far, everything fine, store data length
    p = db_test_plant_with_data

    l = len(p.te_amb.data)
    virtuals.config_virtuals(p)

    azimuth, zenith, apparent_zenith, elevation, apparent_elevation = algos.SolarPosition(p).run()
    p.sun_azimuth.update(azimuth)

    assert len(p.sun_azimuth.data) == l
    assert len(p.te_amb.data) == l

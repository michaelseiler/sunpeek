import pytest
from fastapi.testclient import TestClient
import datetime as dt

import sunpeek.demo
from sunpeek.common import utils
from sunpeek.api.main import app
import sunpeek.components as cmp

client = TestClient(app)


@pytest.mark.xdist_group(name="integration")
def test_run_pc_no_dates(session_fixture, random_stored_plant):
    plant_name, plant, plant_id = random_stored_plant

    # Upload data
    with open(sunpeek.demo.DEMO_DATA_PATH_2DAYS, "rb") as f:
        request_url = f"plants/{plant_id}/data"
        params = {'token': utils.API_TOKEN,
                  'timezone': 'UTC',
                  'datetime_template': 'year_month_day',
                  }
        multiple_files = [('files', ('some data', f, 'text/csv'))]
        response = client.post(request_url, files=multiple_files, params=params)

    assert response.status_code == 201

    # Run PC method
    request_url = f"plants/{plant_id}/evaluations/pc_method"
    params = {'token': utils.API_TOKEN,
              'method': 'ISO',
              'formula': 1,
              }
    response = client.get(request_url, params=params)

    assert response.status_code == 200


@pytest.mark.xdist_group(name="integration")
def test_run_pc_no_data_uploaded(session_fixture, random_stored_plant):
    plant_name, plant, plant_id = random_stored_plant

    # Run PC method
    request_url = f"plants/{plant_id}/evaluations/pc_method"
    params = {'token': utils.API_TOKEN,
              'method': 'ISO',
              'formula': 1,
              }
    response = client.get(request_url, params=params)
    expected = {'error': 'Cannot run Performance Check.',
                'message': 'Cannot run Performance Check analysis. No data have been uploaded to the plant.'}

    assert response.status_code == 400
    assert response.json() == expected


@pytest.fixture(scope="session")
def demo_plant(S):
    request_url = f"plants/create_demo_plant?include_data=true&accept_license=true&token={utils.API_TOKEN}"
    response = client.get(request_url)
    assert response.status_code == 200
    plant = response.json()
    return plant


@pytest.mark.parametrize("method", [None, 'ISO', 'iso', 'Extended'])
@pytest.mark.parametrize("formula", [None, 1, 2])
@pytest.mark.parametrize("ignore_wind", [None, False, True])
@pytest.mark.xdist_group(name="integration")
def test_run_pc_method__demo_plant(session_fixture, demo_plant, method, formula, ignore_wind):
    request_url = f"plants/{demo_plant['id']}/evaluations/pc_method"
    params = {'token': utils.API_TOKEN,
              'method': method,
              'formula': formula,
              'ignore_wind': ignore_wind,
              }
    params = {k: v for k, v in params.items() if v is not None}
    response = client.get(request_url, params=params)

    assert response.status_code == 200


def test_run_pc_method__fails(session_fixture, plant_noarrays_nopower_id):
    request_url = f"plants/{plant_noarrays_nopower_id}/evaluations/pc_method"
    params = {'token': utils.API_TOKEN,
              'method': 'ISO',
              'formula': 1,
              'ignore_wind': True,
              }
    params = {k: v for k, v in params.items() if v is not None}
    response = client.get(request_url, params=params)
    expected = {
        'error': 'Could not calculate Performance Check.',
        'message': 'None of the chosen Performance Check strategies (1) was successful.',
        'detail': '\n'
           '> Thermal Power Check with Mode: ISO, Formula: 1, Ignoring wind\n'
           '  - Plant has no arrays. To run a Performance Check analysis, the plant needs one or more arrays.\n'
           '  - "Thermal power" (tp) in plant: Sensor missing or virtual sensor calculation failed.\n'
           '\n'
           'Virtual Sensors:\n'
           '> "Thermal power" (tp) in plant: Virtual sensor calculation failed. Details:\n'
           '  > Calculate thermal power from real sensor\n'
           '    - "Thermal power" (tp) in plant: Sensor missing.\n'
           '  > Calculate thermal power from volume flow\n'
           '    - "fluid_solar" in plant: Fluid is missing (None) or invalid (UninitialisedFluid). In case you defined a fluid, this is an internal SunPeek error. Please report it.\n'
           '    - "Volume flow" (vf) in plant: Sensor missing.\n'
           '    - "Inlet temperature" (te_in) in plant: Sensor missing.\n'
           '    - "Outlet temperature" (te_out) in plant: Sensor missing or virtual sensor calculation failed.\n',
    }

    assert response.status_code == 400
    assert response.json() == expected


@pytest.mark.parametrize('method', [None, 'ISO', 'Extended'])
@pytest.mark.parametrize('formula', [None, 1, 2])
@pytest.mark.parametrize('ignore_wind', [None, False, True])
@pytest.mark.xdist_group(name="integration")
def test_list_pc_problems__demo_plant(session_fixture, demo_plant, method, formula, ignore_wind):
    request_url = f"plants/{demo_plant['id']}/evaluations/pc_method_feedback"
    params = {'token': utils.API_TOKEN,
              'method': method,
              'formula': formula,
              'ignore_wind': ignore_wind,
              }
    params = {k: v for k, v in params.items() if v is not None}
    response = client.get(request_url, params=params)

    assert response.status_code == 200


@pytest.mark.parametrize("method", [None])
@pytest.mark.parametrize("formula", [None])
@pytest.mark.parametrize("ignore_wind", [None])
@pytest.mark.xdist_group(name="integration")
def test_pc_method_pdf_report__demo_plant(session_fixture, demo_plant, method, formula, ignore_wind):
    request_url = f"plants/{demo_plant['id']}/evaluations/pc_method_report"
    params = {'token': utils.API_TOKEN,
              'method': method,
              'formula': formula,
              'ignore_wind': ignore_wind,
              }
    params = {k: v for k, v in params.items() if v is not None}
    response = client.get(request_url, params=params)

    assert response.status_code == 200
    assert response.headers["content-type"] == "application/pdf"


# note: this test requires Python 3.11+ (https://stackoverflow.com/questions/55542280/why-does-python-3-find-this-iso8601-date-2019-04-05t165526z-invalid)
@pytest.mark.parametrize("method", [None])
@pytest.mark.parametrize("formula", [None])
@pytest.mark.parametrize("ignore_wind", [None])
@pytest.mark.parametrize("with_interval_plots", [None, True])
@pytest.mark.parametrize("eval_start, eval_end", [
    (None, None),
    ('2017-05-09T00:00:00.000Z', '2017-06-09T00:00:00.000Z')
])
@pytest.mark.xdist_group(name="integration")
def test_pc_method_pdf_report__demo_plant__with_dates(session_fixture, demo_plant, method, formula, ignore_wind,
                                                      with_interval_plots, eval_start, eval_end):
    request_url = f"plants/{demo_plant['id']}/evaluations/pc_method_report"
    params = {'token': utils.API_TOKEN,
              'method': method,
              'formula': formula,
              'ignore_wind': ignore_wind,
              'eval_start': None if eval_start is None else dt.datetime.fromisoformat(eval_start),
              'eval_end': None if eval_end is None else dt.datetime.fromisoformat(eval_end),
              'with_interval_plots': with_interval_plots,
              }
    params = {k: v for k, v in params.items() if v is not None}
    response = client.get(request_url, params=params)

    assert response.status_code == 200
    assert response.headers["content-type"] == "application/pdf"


@pytest.mark.parametrize("method", ['ISO'])
@pytest.mark.parametrize("formula", [1])
@pytest.mark.parametrize("ignore_wind", [False])
@pytest.mark.parametrize("eval_start, eval_end", [('2017-05-09T00:00:00.000Z', '2017-05-10T00:00:00.000Z')])
@pytest.mark.xdist_group(name="integration")
def test_pc_method__no_intervals(session_fixture, demo_plant, method, formula, ignore_wind, eval_start, eval_end):
    request_url = f"plants/{demo_plant['id']}/evaluations/pc_method"
    params = {'token': utils.API_TOKEN,
              'method': method,
              'formula': formula,
              'ignore_wind': ignore_wind,
              'eval_start': None if eval_start is None else dt.datetime.fromisoformat(eval_start),
              'eval_end': None if eval_end is None else dt.datetime.fromisoformat(eval_end),
              }
    params = {k: v for k, v in params.items() if v is not None}
    response = client.get(request_url, params=params)

    assert response.status_code == 200
    assert response.json()['plant_output']['n_intervals'] == 0


def test_pc_method_pdf_report__fails(session_fixture, plant_noarrays_nopower_id):
    request_url = f"plants/{plant_noarrays_nopower_id}/evaluations/pc_method"
    params = {'token': utils.API_TOKEN,
              'method': 'ISO',
              'formula': 1,
              'ignore_wind': True,
              }
    response = client.get(request_url, params=params)

    assert response.status_code == 400

# -*- coding: utf-8 -*-
import importlib
import os
import pytest

from sunpeek.definitions import collectors, fluid_definitions
from entrypoint import initialise_database


@pytest.mark.skipif(os.environ.get('SUNPEEK_TEST_DB_MIGRATION', False), reason='not applicable during migration tests')
def test_database_setup_works_if_already_initialised(session_fixture):
    importlib.reload(collectors)
    importlib.reload(fluid_definitions)

    # the initialise_database in the entrypoint MUST succeed even when the database already exists
    initialise_database()

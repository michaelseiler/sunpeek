import json
import pytest
import os
import datetime as dt
from pathlib import Path

import sunpeek.demo
from sunpeek import common
from sunpeek.common.unit_uncertainty import Q
import sunpeek.db_utils.init_db
import sunpeek.components.iam_methods as iam
from sunpeek.components.components_factories import CollectorQDT, CollectorTypes
from sunpeek.data_handling.wrapper import use_csv
from sunpeek.common.utils import DatetimeTemplates
from sunpeek.components.fluids import FluidFactory
import sunpeek.definitions
import sunpeek.definitions.fluid_definitions as fdefs
import sunpeek.definitions.collectors as cdefs


@pytest.fixture()
def tests_root():
    return Path(__file__).parent


@pytest.fixture()
def resources_dir():
    return Path(__file__).parent / 'resources'


# @pytest.fixture()
# def psycopg2_connection_local(docker_services, psycopg2_con_params):
#     return sunpeek.db_utils.hit_db_connection.connect(psycopg2_con_params)
#
#
# @pytest.fixture
# def psycopg2_connection(request, psycopg2_con_params):
#     if os.environ.get('CI_PIPELINE_SOURCE') is None:
#         db_con = request.getfixturevalue("psycopg2_connection_local")
#     else:
#         db_con = sunpeek.db_utils.hit_db_connection.connect(psycopg2_con_params)
#     yield db_con
#     sunpeek.db_utils.hit_db_connection.disconnect_db(db_con)


@pytest.fixture(scope="session")
def docker_compose_file(pytestconfig):
    return os.path.join(str(pytestconfig.rootdir), "docker-compose.yml")


@pytest.fixture()
def fhw_fluid():
    """Returns FHW fluid (Pekasolar) without database."""
    fluid_def = fdefs.get_definition(fdefs.WPDFluids.fhw_pekasolar.value.name)
    return FluidFactory(fluid=fluid_def)


@pytest.fixture(scope='session')
def fhw_coll():
    return cdefs.get_definition(name='Arcon 3510')


@pytest.fixture()
def fhw__nofluid(resources_dir):
    """Returns FHW plant built from JSON, without fluid, without data."""
    with open(resources_dir / 'plant_config_FHW_Arcon_South_with_Collector.json') as f:
        conf = json.load(f)
    return common.config_parser.make_full_plant(conf)


@pytest.fixture()
def fhw__nofluid__nocoll():
    """Returns FHW plant built from JSON, without fluid, without data."""
    with open(sunpeek.demo.DEMO_CONFIG_PATH) as f:
        conf = json.load(f)
    return common.config_parser.make_full_plant(conf)


@pytest.fixture()
def fhw(fhw__nofluid__nocoll, fhw_fluid, fhw_coll):
    fhw__nofluid__nocoll.fluid_solar = fhw_fluid
    fhw__nofluid__nocoll.arrays[0].collector = fhw_coll
    return fhw__nofluid__nocoll


@pytest.fixture(scope='session')
def fhw_session(fhw_coll):
    with open(sunpeek.demo.DEMO_CONFIG_PATH) as f:
        conf = json.load(f)
    fhw = common.config_parser.make_full_plant(conf)
    fhw.fluid_solar = FluidFactory(fluid=fdefs.get_definition(fdefs.WPDFluids.fhw_pekasolar.value.name))
    fhw.arrays[0].collector = fhw_coll
    return fhw


def fhw_fun():
    # Deliberately not defined as fixture, to avoid that the "fhw__" session-scoped fixtures
    # use the same shared fhw fixture instance, and changes made to one fixture affect others.
    with open(sunpeek.demo.DEMO_CONFIG_PATH) as f:
        conf = json.load(f)
    fhw = common.config_parser.make_full_plant(conf)
    fhw.fluid_solar = FluidFactory(fluid=fdefs.get_definition(fdefs.WPDFluids.fhw_pekasolar.value.name))
    coll = cdefs.get_definition(name='Arcon 3510')
    fhw.arrays[0].collector = coll

    return fhw


@pytest.fixture(scope='session')
def fhw__2days_data():
    plant = fhw_fun()
    use_csv(plant, csv_files=sunpeek.demo.DEMO_DATA_PATH_2DAYS, timezone='UTC', calculate_virtuals=True,
            datetime_template=DatetimeTemplates.year_month_day)
    return plant


@pytest.fixture(scope='session')
def fhw__1month_data():
    plant = fhw_fun()
    use_csv(plant, csv_files=sunpeek.demo.DEMO_DATA_PATH_1MONTH, timezone='UTC', calculate_virtuals=True,
            datetime_template=DatetimeTemplates.year_month_day)
    return plant


@pytest.fixture(scope='session')
def fhw__1year_data():
    plant = fhw_fun()
    use_csv(plant, csv_files=sunpeek.demo.DEMO_DATA_PATH_1YEAR, timezone='UTC', calculate_virtuals=True,
            datetime_template=DatetimeTemplates.year_month_day)
    return plant


@pytest.fixture(scope='session')
def fhw__2days_data__no_virtuals():
    # Yields a normal FHW plant, but with virtual sensors _not_ calculated, so calculation can be tested here.
    plant = fhw_fun()
    use_csv(plant, csv_files=sunpeek.demo.DEMO_DATA_PATH_2DAYS, timezone='UTC', calculate_virtuals=False,
            datetime_template=DatetimeTemplates.year_month_day)
    return plant


@pytest.fixture(scope='session')
def dummy_concentrating__collector():
    return CollectorQDT(
        name='concentrating_test_collector',
        collector_type=CollectorTypes.concentrating,
        test_reference_area="gross",
        eta0b=Q(0.7),
        a1=Q(1, "W m**-2 K**-1"),
        a2=Q(0, "W m**-2 K**-2"),
        a5=Q(1500, "J m**-2 K**-1"),
        a8=Q(5e-9, "W m**-2 K**-4"),
        kd=Q(0.1),
        gross_length=Q(2, "m"),
        area_gr=Q(5, "m**2"),
        iam_method=iam.IAM_Interpolated(aoi_reference=Q([10, 20, 30, 40, 50, 60, 70, 80, 90], 'deg'),
                                        iam_reference=Q([[1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 0],
                                                         [0.99, 0.99, 0.98, 0.96, 0.91, 0.77, 0.53, 0.18, 0]]))
    )


@pytest.fixture(scope='session')
def fhw__concentrating(dummy_concentrating__collector):
    # FHW with dummy concentrating collector, no data
    with open(sunpeek.demo.DEMO_CONFIG_PATH) as f:
        conf = json.load(f)
    fhw = common.config_parser.make_full_plant(conf)
    fhw.fluid_solar = FluidFactory(fluid=fdefs.get_definition(fdefs.WPDFluids.fhw_pekasolar.value.name))
    fhw.arrays[0].collector = dummy_concentrating__collector

    return fhw


@pytest.fixture(scope='session')
def fhw__concentrating__2days_data(fhw__concentrating):
    fhw = fhw__concentrating
    use_csv(fhw, csv_files=sunpeek.demo.DEMO_DATA_PATH_2DAYS, timezone='UTC', calculate_virtuals=True,
            datetime_template=DatetimeTemplates.year_month_day)

    return fhw


@pytest.fixture(scope='session')
def fhw__concentrating__1month_data(fhw__concentrating):
    fhw = fhw__concentrating
    use_csv(fhw, csv_files=sunpeek.demo.DEMO_DATA_PATH_1MONTH, timezone='UTC', calculate_virtuals=True,
            datetime_template=DatetimeTemplates.year_month_day)

    return fhw

import pytest

from sunpeek.common.unit_uncertainty import Q
from sunpeek.components import Plant, Array, Collector, CollectorTypes, CollectorQDT, iam_methods, Sensor
from sunpeek.components.helpers import AlgoCheckMode
from sunpeek.components.fluids import UninitialisedFluid
from sunpeek.serializable_models import CoreMethodFeedback, ProblemType
from sunpeek.core_methods.common.main import is_valid_fluid, is_valid_collector
from sunpeek.components.types import UninitialisedCollector


@pytest.fixture
def r() -> CoreMethodFeedback:
    return CoreMethodFeedback()


@pytest.fixture
def p() -> Plant:
    return Plant(latitude=Q(47, "deg"), longitude=Q(15, "deg"))


@pytest.fixture
def a(p) -> Plant:
    s = Sensor('te_in_raw_name', 'K')
    a = Array(name='my_array', tilt=Q(30, 'deg'), plant=p)
    a.set_sensors(te_in=s)
    return a


@pytest.fixture
def c() -> Collector:
    return CollectorQDT(name='c', collector_type=CollectorTypes.concentrating, iam_method=iam_methods.IAM_K50(Q(0.9)),
                        test_reference_area='gross', area_gr=Q(1, 'm**2'), gross_length=Q(1, 'm'), kd=Q(0),
                        eta0b=Q(0), a1=Q(0, 'W m**-2 K**-1'), a2=Q(0, 'W m**-2 K**-2'), a5=Q(1, 'J m**-2 K**-1'))


def test_add_missing_component(p):
    r = CoreMethodFeedback()
    description = 'No arrays defined'
    r.add_missing_component(p, 'arrays', description)

    expected_type = ProblemType.component_missing

    assert len(r.own_feedback) == 1
    assert r.own_feedback[0].problem_type == expected_type
    assert description in r.parse()


# Missing real or virtual sensors ---------------------------

@pytest.mark.parametrize('check_mode', [AlgoCheckMode.config_only, AlgoCheckMode.config_and_data])
def test_add_missing_sensor__real_sensor(p, check_mode):
    r = CoreMethodFeedback()
    r.add_missing_sensor(p, 'te_amb', check_mode=check_mode)

    expected_type = ProblemType.real_sensor_missing
    expected_str = '"Ambient temperature" (te_amb) in plant: Sensor missing'

    assert len(r.own_feedback) == 1
    assert r.own_feedback[0].problem_type == expected_type
    assert expected_str in r.parse()


@pytest.mark.parametrize('check_mode', [AlgoCheckMode.config_only, AlgoCheckMode.config_and_data])
def test_add_missing_sensor__virtual_sensor(r, p, check_mode):
    r.add_missing_sensor(p, 'rd_ghi', check_mode=check_mode)

    expected_type = ProblemType.virtual_sensor_missing
    expected_str = '"Global radiation" (rd_ghi) in plant: Virtual sensor calculation failed'

    assert len(r.own_feedback) == 1
    assert r.own_feedback[0].problem_type == expected_type
    assert expected_str in r.parse()


# @pytest.mark.parametrize('check_mode', [AlgoCheckMode.config_only, AlgoCheckMode.config_and_data])
@pytest.mark.parametrize('check_mode', [AlgoCheckMode.config_only])
def test_add_missing_sensor__possibly_virtual_sensor(r, p, check_mode):
    r.add_missing_sensor(p, 'tp', check_mode=check_mode)

    expected_type = ProblemType.real_or_virtual_sensor_missing
    expected_str = '"Thermal power" (tp) in plant: Sensor missing or virtual sensor calculation failed'

    assert len(r.own_feedback) == 1
    assert r.own_feedback[0].problem_type == expected_type
    assert expected_str in r.parse()


def test_add_missing_sensor_info(r, a):
    info_name = 'some_info_name'
    if a.te_in.is_info_missing(info_name):
        r.add_missing_sensor_info(a, 'te_in', info_name)

    expected_type = ProblemType.sensor_info
    expected_str = (f'Sensor info "{info_name}" missing for sensor "{a.te_in.raw_name}" (te_in in array "{a.name}"). '
                    f'This can be fixed on the Sensor Details page.')

    assert len(r.own_feedback) == 1
    assert r.own_feedback[0].problem_type == expected_type
    assert expected_str in r.parse()


def test_add_missing_attrib(r, a):
    description = 'Here is a very good explanation.'
    if a.is_attrib_missing('azim'):
        r.add_missing_attrib(a, 'azim', description)

    expected_type = ProblemType.component_slot
    expected_str = f'Missing information "azim" in array "my_array". {description}'

    assert len(r.own_feedback) == 1
    assert r.own_feedback[0].problem_type == expected_type
    assert expected_str in r.parse()


def test_add_wrong_collector_type(r, a, c):
    expected = CollectorTypes.flat_plate
    if c.collector_type != CollectorTypes.flat_plate.value:
        r.add_wrong_collector_type(a, expected=expected,
                                   received=c.collector_type)

    expected_type = ProblemType.collector_type
    expected_str = 'Wrong collector type: Expected a collector of type flat_plate, but received "concentrating"'

    assert len(r.own_feedback) == 1
    assert r.own_feedback[0].problem_type == expected_type
    assert expected_str in r.parse()


def test_add_wrong_collector_type_list(r, a, c):
    expected = [CollectorTypes.flat_plate, CollectorTypes.WISC]
    if c.collector_type != CollectorTypes.flat_plate.value:
        r.add_wrong_collector_type(a, expected=expected,
                                   received=c.collector_type)

    expected_type = ProblemType.collector_type
    expected_str = 'Wrong collector type: Expected a collector of type flat_plate or WISC, but received "concentrating"'

    assert len(r.own_feedback) == 1
    assert r.own_feedback[0].problem_type == expected_type
    assert expected_str in r.parse()


def test_add_missing_collector(r):
    a = Array(name='my_array')
    a.collector = UninitialisedCollector('', parent=a, attribute='collector')

    assert is_valid_collector(a.collector, check_mode=AlgoCheckMode.config_only)

    if not is_valid_collector(a.collector, check_mode=AlgoCheckMode.config_and_data):
        r.add_missing_collector(a, 'collector')

    expected_type = ProblemType.component_missing
    expected_str = ('Collector is missing (None) or invalid (UninitialisedCollector). In case you defined a collector, '
                    'this is an internal SunPeek error. Please report it.')

    assert len(r.own_feedback) == 1
    assert r.own_feedback[0].problem_type == expected_type
    assert expected_str in r.parse()


def test_add_missing_fluid(r, p):
    p.fluid_solar = UninitialisedFluid('', [])
    assert is_valid_fluid(p.fluid_solar, check_mode=AlgoCheckMode.config_only)

    if not is_valid_fluid(p.fluid_solar, check_mode=AlgoCheckMode.config_and_data):
        r.add_missing_fluid(p, 'fluid_solar')

    expected_type = ProblemType.fluid_missing
    expected_str = ('"fluid_solar" in plant: Fluid is missing (None) or invalid (UninitialisedFluid). '
                    'In case you defined a fluid, this is an internal SunPeek error. Please report it.')

    assert len(r.own_feedback) == 1
    assert r.own_feedback[0].problem_type == expected_type
    assert expected_str in r.parse()


def test_add_generic_slot_problem(r, p):
    txt = 'something really bad happened'
    r.add_generic_slot_problem(p, txt)

    expected_type = ProblemType.component_slot
    expected_str = txt

    assert len(r.own_feedback) == 1
    assert r.own_feedback[0].problem_type == expected_type
    assert expected_str in r.parse()

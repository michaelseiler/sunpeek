import pytest
from typing import List
import numpy as np
import pandas as pd
import json

from sunpeek.common.unit_uncertainty import Q
from sunpeek.components import Plant, Array, Sensor
from sunpeek.data_handling.wrapper import use_dataframe
from sunpeek.components.helpers import AlgoCheckMode
from sunpeek.core_methods import CoreStrategy, CoreAlgorithm
from sunpeek.core_methods.common.main import VirtualSensorStrategy, AlgoResult, StrategyErrorBehavior
from sunpeek.core_methods.pc_method.wrapper import PCStrategy
from sunpeek.core_methods.pc_method.wrapper import get_feedback, get_successful_strategy, list_feedback
from sunpeek.core_methods.pc_method import PCFormulae, PCMethods
from sunpeek.serializable_models import CoreMethodFeedback, PCMethodFeedback, CoreProblem, ProblemType
from sunpeek.common.errors import AlgorithmError
from sunpeek.core_methods.virtuals import calculations as algos
import sunpeek.demo
from sunpeek.common.config_parser import make_full_plant


@pytest.fixture
def pr_plant_noarrays_nopower():
    plant = Plant(name='example_plant', latitude=Q(1, "deg"), longitude=Q(1, "deg"))
    plant.set_sensors(te_amb=Sensor('te_amb', 'degC'))
    idx = pd.date_range(start='2022-09-12 11:16+01', end='2022-09-13 21:16+01')
    columns = ['te_amb']
    df = pd.DataFrame(data=np.random.randn(len(idx), len(columns)), columns=columns, index=idx)
    use_dataframe(plant, df, calculate_virtuals=True)

    return plant


## Test CoreStrategy -----

@pytest.fixture
def pr_plant_noarray_nodata():
    plant = Plant(name='pr_plant_nodata', latitude=Q(1, "deg"), longitude=Q(1, "deg"))
    plant.set_sensors(te_amb=Sensor('te_amb', 'degC'))

    return plant


@pytest.fixture
def pr_plant_nodata():
    plant = Plant(name='pr_plant_nodata', latitude=Q(1, "deg"), longitude=Q(1, "deg"))
    plant.set_sensors(te_amb=Sensor('te_amb', 'degC'))
    array = Array(plant=plant, tilt=Q(0, "deg"), azim=Q(0, "deg"), area_gr=Q(1, "m**2"), row_spacing=Q(3, 'm'))
    array.set_sensors(te_in=Sensor('te_in', 'degC'))

    return plant


@pytest.fixture
def pr_plant_vf_nodata(pr_plant_nodata):
    p = pr_plant_nodata
    p.set_sensors(vf=Sensor('vf', 'l s**-1'))
    return p


@pytest.fixture
def strategy_no_calc():
    # noinspection PyArgumentList
    class TestStrategy(CoreStrategy):
        name = 'test strategy no_problems calc_ok'
        feedthrough_real_sensor = False

        def _calc(self):
            raise NotImplementedError

        def _get_feedback(self, check_mode: AlgoCheckMode) -> CoreMethodFeedback:
            fb = CoreMethodFeedback()
            if self.component.is_sensor_missing('vf', check_mode):
                fb.add_own(CoreProblem(ProblemType.component_slot, self.component, 'vf', 'Sensor missing.'))
            return fb

    return TestStrategy


@pytest.fixture
def strategy_no_problems_calc_ok():
    class TestStrategy(CoreStrategy):
        name = 'test strategy no_problems calc_ok'
        feedthrough_real_sensor = False

        def _calc(self):
            return 1

        def _get_feedback(self, check_mode: AlgoCheckMode) -> CoreMethodFeedback:
            return CoreMethodFeedback()  # Deliberately no problems

    return TestStrategy


@pytest.fixture
def strategy_calc_fails():
    class TestStrategy(CoreStrategy):
        name = 'test strategy calc fails'

        def _calc(self):
            return {'output': 1 / 0}

        def _get_feedback(self, check_mode: AlgoCheckMode) -> CoreMethodFeedback:
            return CoreMethodFeedback()

    return TestStrategy


@pytest.fixture
def strategy_with_problems():
    # noinspection PyArgumentList
    class TestStrategy(CoreStrategy):
        name = 'test strategy with problems'

        def _calc(self):
            return 0

        def _get_feedback(self, check_mode: AlgoCheckMode) -> CoreMethodFeedback:
            fb = CoreMethodFeedback()
            fb.add_own(CoreProblem(ProblemType.component_attrib, self.component, 'test_attribute'))
            return fb

    return TestStrategy


def test_strategy__calc_fails(strategy_calc_fails, pr_plant_noarrays_nopower):
    # CoreStrategies raise exceptions that occur in execute() / _calc().
    # Only CoreAlgorithms sandbox these errors and continue with other strategies.
    with pytest.raises(ZeroDivisionError):
        strategy_calc_fails(component=pr_plant_noarrays_nopower).execute()


def test_strategy__problem_report_wrong_type(pr_plant_nodata):
    class TestStrategy(CoreStrategy):
        name = 'test strategy wrong feedback type'

        def _calc(self):
            return 0

        def _get_feedback(self, check_mode: AlgoCheckMode) -> CoreMethodFeedback:
            return 1

    with pytest.raises(AlgorithmError, match='returned problems with invalid type'):
        TestStrategy(component=pr_plant_nodata).get_feedback(check_mode=AlgoCheckMode.config_only)


## Test CoreAlgorithm -----

def test_corealgo__component_tuples(pr_plant_noarray_nodata):
    class TestAlgo(CoreAlgorithm):
        def allowed_components(self):
            return [Plant, Array]

        def define_strategies(self):
            return []

    with pytest.raises(AlgorithmError, match='returned invalid type. Expected tuple'):
        TestAlgo(component=pr_plant_noarray_nodata)


def test_corealgo__invalid_allowed_component_raises(pr_plant_nodata):
    class TestAlgo(CoreAlgorithm):
        def allowed_components(self):
            return Plant, Sensor

        def define_strategies(self):
            return []

    with pytest.raises(AlgorithmError, match='Allowed components must be subclasses of "Component"'):
        TestAlgo(component=pr_plant_nodata)


def test_strategy_invalid_component_noraise(strategy_no_calc, pr_plant_nodata):
    # Only CoreAlgorithm has the concept of allowed components, and checks and raises the component of its strategies.
    strategy_no_calc(component=pr_plant_nodata.arrays[0])
    assert True


def test_corealgo__invalid_component_raises(pr_plant_nodata):
    class TestAlgo(CoreAlgorithm):
        def define_strategies(self):
            return []

    with pytest.raises(AlgorithmError, match='got a component of invalid type'):
        TestAlgo(component=pr_plant_nodata.arrays[0])


def test_corealgo__strategy_wrong_object_raises(strategy_no_calc, pr_plant_nodata):
    class TestAlgo(CoreAlgorithm):
        def define_strategies(self):
            return [strategy_no_calc(self.component), self.component]

    with pytest.raises(AlgorithmError, match='Expected "CoreStrategy" object, but got'):
        TestAlgo(component=pr_plant_nodata)


def test_corealgo__duplicate_strategies_raises(strategy_no_calc, pr_plant_nodata):
    class TestAlgo(CoreAlgorithm):
        def define_strategies(self):
            return [strategy_no_calc(self.component), strategy_no_calc(self.component)]

    with pytest.raises(AlgorithmError, match='Cannot add strategies with duplicate names'):
        TestAlgo(component=pr_plant_nodata)


def test_corealgo__strategies_added_ok(strategy_no_calc, strategy_calc_fails, pr_plant_nodata):
    class TestAlgo(CoreAlgorithm):
        def define_strategies(self):
            return [strategy_no_calc(self.component), strategy_calc_fails(self.component)]

    algo = TestAlgo(pr_plant_nodata)

    assert strategy_no_calc.name in [s.name for s in algo.strategies]
    assert strategy_calc_fails.name in [s.name for s in algo.strategies]
    assert len(algo.strategies) == 2


def test_corealgo__strategies_ok(strategy_no_problems_calc_ok, pr_plant_nodata):
    s1 = strategy_no_problems_calc_ok(component=pr_plant_nodata)
    elapsed_time, output = s1.execute()

    assert isinstance(elapsed_time, float)
    assert output == 1


def test_corealgo_catches_strategy_calc_fails(strategy_calc_fails, pr_plant_noarrays_nopower):
    class TestAlgo(CoreAlgorithm):
        def define_strategies(self, *args, **kwargs) -> List[VirtualSensorStrategy]:
            return [strategy_calc_fails(component=self.component)]

    # Failing strategy does not raise, see comment in test_strategy__calc_fails() above
    algo_result = TestAlgo(pr_plant_noarrays_nopower).run()
    p = algo_result.feedback

    assert isinstance(p, CoreMethodFeedback)
    assert not p.success
    assert isinstance(p.sub_feedback, dict)
    assert strategy_calc_fails.name in p.sub_feedback.keys()
    assert isinstance(p.sub_feedback[strategy_calc_fails.name], CoreMethodFeedback)

    assert isinstance(p.own_feedback, list)
    assert len(p.own_feedback) == 1
    ap = p.own_feedback[0]
    assert isinstance(ap, CoreProblem)
    assert ap.problem_type == ProblemType.unexpected_in_calc
    assert ap.affected_component is None
    assert ap.affected_item_name is None
    assert 'unexpected calculation error' in ap.description
    assert 'ZeroDivisionError' in ap.description


def test_corealgo_catches_strategy_calc_problem(strategy_no_calc, pr_plant_noarrays_nopower):
    class TestAlgo(CoreAlgorithm):
        def define_strategies(self, *args, **kwargs) -> List[VirtualSensorStrategy]:
            return [strategy_no_calc(component=self.component)]

    # Failing strategy does not raise, see comment in test_strategy__calc_fails() above
    algo_result = TestAlgo(pr_plant_noarrays_nopower).run()
    p = algo_result.feedback

    assert isinstance(p, CoreMethodFeedback)
    assert not p.success
    assert p.own_feedback is None
    assert isinstance(p.sub_feedback, dict)
    assert strategy_no_calc.name in p.sub_feedback.keys()
    fb = p.sub_feedback[strategy_no_calc.name]
    assert isinstance(fb, CoreMethodFeedback)
    assert not fb.success
    assert len(fb.own_feedback) == 1

    ap = fb.own_feedback[0]
    assert isinstance(ap, CoreProblem)
    assert ap.problem_type == ProblemType.component_slot
    assert ap.affected_component.name == pr_plant_noarrays_nopower.name
    assert ap.affected_item_name == 'vf'
    assert ap.description == 'Sensor missing.'


def test_corealgo__mix_working_and_failing__working_first(strategy_no_problems_calc_ok, strategy_calc_fails,
                                                          pr_plant_vf_nodata,
                                                          mocker):
    class TestAlgo(CoreAlgorithm):
        def define_strategies(self, *args, **kwargs) -> List[VirtualSensorStrategy]:
            return [strategy_no_problems_calc_ok(self.component), strategy_calc_fails(self.component)]

    # Make sure failing strategy was not called (because working one got there before)
    mock_failing = mocker.Mock()
    strategy_calc_fails._calc = mock_failing

    algo = TestAlgo(pr_plant_vf_nodata)
    ar = algo.run()

    mock_failing.assert_not_called()
    assert isinstance(ar, AlgoResult)
    assert ar.success
    assert isinstance(ar.feedback, CoreMethodFeedback)
    assert ar.successful_strategy == algo.strategies[0]
    assert ar.output == 1


def test_corealgo__mix_working_and_failing__failing_first(strategy_with_problems, strategy_no_problems_calc_ok,
                                                          strategy_calc_fails, pr_plant_vf_nodata, mocker):
    class TestAlgo(CoreAlgorithm):
        def define_strategies(self, *args, **kwargs) -> List[CoreStrategy]:
            return [strategy_with_problems(self.component),
                    strategy_calc_fails(self.component),
                    strategy_no_problems_calc_ok(self.component)]

    algo = TestAlgo(pr_plant_vf_nodata)
    s = algo.strategies[0]
    mock_problems_calc = mocker.Mock()
    s._calc = mock_problems_calc

    s = algo.strategies[2]
    mock_working_problems = mocker.Mock(side_effect=lambda self: CoreMethodFeedback())
    s._get_feedback = mock_working_problems
    mock_working_calc = mocker.Mock()
    s._calc = mock_working_calc

    ar = algo.run()

    mock_problems_calc.assert_not_called()
    mock_working_problems.assert_called()
    mock_working_calc.assert_called()

    op = algo.feedback.own_feedback
    assert len(op) == 1
    assert op[0].problem_type == ProblemType.unexpected_in_calc

    assert isinstance(ar, AlgoResult)
    assert ar.success
    assert isinstance(ar.feedback, CoreMethodFeedback)
    assert ar.successful_strategy == algo.strategies[2]


def test_corealgo_run__on_strategy_error(pr_plant_noarrays_nopower,
                                         strategy_calc_fails, strategy_no_problems_calc_ok):
    class TestAlgo(CoreAlgorithm):
        def define_strategies(self):
            return [strategy_calc_fails(self.component), strategy_no_problems_calc_ok(self.component)]

    algo = TestAlgo(component=pr_plant_noarrays_nopower)
    # on_strategy_error=='skip' skips failing strategy
    ar = algo.run(on_strategy_error=StrategyErrorBehavior.skip)
    assert ar.success
    assert ar.successful_strategy == algo.strategies[1]

    # Default is on_strategy_error=='skip'
    ar = algo.run()
    assert ar.success
    assert ar.successful_strategy == algo.strategies[1]

    # on_strategy_error=='error' shall raise failing strategy
    with pytest.raises(ZeroDivisionError):
        ar = algo.run(on_strategy_error=StrategyErrorBehavior.error)


## Test VirtualSensorStrategy ---------


@pytest.mark.parametrize('calc_output, feedthrough, expected', [
    (None, True, 'Expected dict from call to execute'),
    (None, False, 'Expected dict from call to execute'),
    ({'output': None}, False, 'is None, expected pd.Series'),
    ({'output': 123}, False, 'expected pd.Series'),
    ({'output': pd.Series(123)}, False, 'incompatible with size of "Plant.time_index'),
    ({'output': pd.Series([123, 456])}, False, 'is not unit-aware'),
])
def test_virtual_output(pr_plant_noarrays_nopower, calc_output, feedthrough, expected):
    class TestStrategy(VirtualSensorStrategy):
        name = 'test virtual strategy output'
        feedthrough_real_sensor = feedthrough

        def _calc(self):
            return calc_output

        def _get_feedback(self, check_mode: AlgoCheckMode) -> CoreMethodFeedback:
            return CoreMethodFeedback()

    with pytest.raises(AlgorithmError, match=expected):
        TestStrategy(pr_plant_noarrays_nopower).execute()


def test_virtual_fhw__tp_strategy(fhw):
    # FHW has no tp real sensor, so strategy should report, and calculation via fluid + volume flow should work
    algo = algos.ThermalPower(fhw)
    pr = algo.get_config_feedback()

    assert pr.success
    assert pr.own_feedback is None
    assert isinstance(pr.sub_feedback, dict)
    for s in algo.strategies:
        assert s.name in pr.sub_feedback.keys()
    assert not pr.sub_feedback[algos.StrategyPowerFromSensor.name].success
    assert pr.successful_strategy_str == algos.StrategyPowerFromVolumeFlow.name


def test_virtual_fhw__tp_fluid_missing():
    # fluid missing: no successful strategy
    with open(sunpeek.demo.DEMO_CONFIG_PATH) as f:
        conf = json.load(f)
    pl = make_full_plant(conf)
    pl.fluid_solar = None

    pr = algos.ThermalPower(pl).get_config_feedback()

    assert not pr.success
    assert pr.own_feedback is None


def test_virtual__success_with_problem_slots(pr_plant_noarrays_nopower):
    class TestStrategy(CoreStrategy):
        name = 'test strategy problem slots'
        feedthrough_real_sensor = False

        def _calc(self):
            return 1

        def _get_feedback(self, check_mode: AlgoCheckMode) -> CoreMethodFeedback:
            fb = CoreMethodFeedback()
            fb.problem_slots.extend(['missing_test_sensor'])
            fb.success = True
            return fb

    class TestAlgo(CoreAlgorithm):
        def define_strategies(self):
            return [TestStrategy(self.component)]

    ar = TestAlgo(component=pr_plant_noarrays_nopower).run()

    assert ar.success
    assert ar.output == 1
    assert ar.feedback.sub_feedback[TestStrategy.name].problem_slots[0] == 'missing_test_sensor'


def test_virtual_strategy__output_no_dict(pr_plant_noarrays_nopower):
    class TestStrategy(VirtualSensorStrategy):
        def _calc(self):
            return 1

        def _get_feedback(self, check_mode: AlgoCheckMode) -> CoreMethodFeedback:
            return CoreMethodFeedback()

    with pytest.raises(AlgorithmError, match='Expected dict from call to execute'):
        TestStrategy(component=pr_plant_noarrays_nopower).execute()


def test_strategy__problemslots(pr_plant_noarrays_nopower):
    # noinspection PyArgumentList
    class TestStrategy(VirtualSensorStrategy):
        name = 'test virtual strategy problemslots'
        feedthrough_real_sensor = False

        def _calc(self):
            raise NotImplementedError()

        def _get_feedback(self, check_mode: AlgoCheckMode) -> CoreMethodFeedback:
            fb = CoreMethodFeedback()
            if self.component.is_real_sensor_missing('te_amb', check_mode):
                fb.add_own(CoreProblem(ProblemType.component_slot, self.component, 'te_amb'))
            fb.problem_slots.extend(['tp'])
            return fb

    class TestAlgo(CoreAlgorithm):
        def allowed_components(self):
            return Plant,

        def define_strategies(self):
            return [TestStrategy(self.component)]

    p = pr_plant_noarrays_nopower
    algo = TestAlgo(component=p)
    problems = algo.get_config_feedback()
    p.map_vsensor('tp', problems)

    assert problems.success
    assert not p.tp.can_calculate
    assert 'tp' in problems.sub_feedback[TestStrategy.name].problem_slots


def test_strategy__feedthrough(pr_plant_noarrays_nopower):
    # feedthrough_real_sensor is only used in VirtualSensorStrategy
    # noinspection PyArgumentList
    class TestStrategy(VirtualSensorStrategy):
        name = 'test strategy feedthrough'
        feedthrough_real_sensor = True

        def _calc(self):
            return {'output': None}

        def _get_feedback(self, check_mode: AlgoCheckMode) -> CoreMethodFeedback:
            fb = CoreMethodFeedback()
            if self.component.is_real_sensor_missing('te_amb', check_mode):
                fb.add_own(CoreProblem(ProblemType.component_slot, self.component, 'te_amb'))
            return fb

    class TestAlgo(CoreAlgorithm):
        def allowed_components(self):
            return Plant,

        def define_strategies(self):
            return [TestStrategy(self.component)]

    algo = TestAlgo(component=pr_plant_noarrays_nopower)
    ar = algo.run()

    assert ar.success
    assert ar.output['output'] is None


def test_strategy__feedthrough_and_problemslots(pr_plant_noarrays_nopower):
    # noinspection PyArgumentList
    class TestStrategy(VirtualSensorStrategy):
        name = 'test virtual strategy feedthrough and problemslots'
        feedthrough_real_sensor = True

        def _calc(self):
            return {'output': None}

        def _get_feedback(self, check_mode: AlgoCheckMode) -> CoreMethodFeedback:
            fb = CoreMethodFeedback()
            if self.component.is_real_sensor_missing('te_amb', check_mode):
                fb.add_own(CoreProblem(ProblemType.component_slot, self.component, 'te_amb'))
            fb.problem_slots.extend(['tp'])
            return fb

    class TestAlgo(CoreAlgorithm):
        def allowed_components(self):
            return Plant,

        def define_strategies(self):
            return [TestStrategy(self.component)]

    algo = TestAlgo(component=pr_plant_noarrays_nopower)
    ar = algo.run()

    assert ar.success
    assert ar.output['output'] is None


## Test problem report parsing etc.  --------------


@pytest.mark.parametrize(
    "plant_fixture,"
    "method, formula, use_wind, expected_strategy, "
    "include_successful_strategies, include_problem_slots,"
    "expected", [
        ('fhw',
         ['ISO'], [1], [True],
         'Thermal Power Check with Mode: ISO, Formula: 1, Using wind',
         False, True,
         ''),

        ('fhw',
         ['extended'], [1], [True],
         'Thermal Power Check with Mode: extended, Formula: 1, Using wind',
         False, True,
         ''),

        ('fhw',
         ['ISO'], [1], [True],
         'Thermal Power Check with Mode: ISO, Formula: 1, Using wind',
         True, True,
         ('\n> Thermal Power Check with Mode: ISO, Formula: 1, Using wind\n'
          '  - No problems found.\n')
         ),

        ('pr_plant_noarrays_nopower',
         ['ISO'], [1], [True], '',
         True, True,
         ('\n'
          '> Thermal Power Check with Mode: ISO, Formula: 1, Using wind\n'
          '  - Plant has no arrays. To run a Performance Check analysis, the plant needs one or more arrays.\n'
          '  - "Thermal power" (tp) in plant: Sensor missing or virtual sensor calculation failed.\n'
          '\n'
          'Virtual Sensors:\n'
          '> "Thermal power" (tp) in plant: Virtual sensor calculation failed. Details:\n'
          '  > Calculate thermal power from real sensor\n'
          '    - "Thermal power" (tp) in plant: Sensor missing.\n'
          '  > Calculate thermal power from volume flow\n'
          '    - "fluid_solar" in plant: Fluid is missing (None) or invalid (UninitialisedFluid). In case you defined a fluid, this is an internal '
          'SunPeek error. Please report it.\n'
          '    - "Volume flow" (vf) in plant: Sensor missing.\n'
          '    - "Inlet temperature" (te_in) in plant: Sensor missing.\n'
          '    - "Outlet temperature" (te_out) in plant: Sensor missing or virtual sensor calculation failed.\n')
         ),

        ('pr_plant_noarrays_nopower',
         ['ISO'], [1], [True], 'Evaluation mode: ISO, Formula: 1, Using wind',
         True, False,
         ('\n'
          '> Thermal Power Check with Mode: ISO, Formula: 1, Using wind\n'
          '  - Plant has no arrays. To run a Performance Check analysis, the plant needs one or more arrays.\n'
          '  - "Thermal power" (tp) in plant: Sensor missing or virtual sensor calculation failed.\n'
          '\n'
          'Virtual Sensors:\n'
          '> "Thermal power" (tp) in plant: Virtual sensor calculation failed. Details:\n'
          '  > Calculate thermal power from real sensor\n'
          '    - "Thermal power" (tp) in plant: Sensor missing.\n'
          '  > Calculate thermal power from volume flow\n'
          '    - "fluid_solar" in plant: Fluid is missing (None) or invalid (UninitialisedFluid). In case you defined a fluid, this is an internal '
          'SunPeek error. Please report it.\n'
          '    - "Volume flow" (vf) in plant: Sensor missing.\n'
          '    - "Inlet temperature" (te_in) in plant: Sensor missing.\n'
          '    - "Outlet temperature" (te_out) in plant: Sensor missing or virtual sensor calculation failed.\n')
         ),

        ('pr_plant_noarrays_nopower',
         ['ISO'], [1], [True], 'Evaluation mode: iso, Formula: 1, Using wind',
         False, True,
         ('\n'
          '> Thermal Power Check with Mode: ISO, Formula: 1, Using wind\n'
          '  - Plant has no arrays. To run a Performance Check analysis, the plant needs one or more arrays.\n'
          '  - "Thermal power" (tp) in plant: Sensor missing or virtual sensor calculation failed.\n'
          '\n'
          'Virtual Sensors:\n'
          '> "Thermal power" (tp) in plant: Virtual sensor calculation failed. Details:\n'
          '  > Calculate thermal power from real sensor\n'
          '    - "Thermal power" (tp) in plant: Sensor missing.\n'
          '  > Calculate thermal power from volume flow\n'
          '    - "fluid_solar" in plant: Fluid is missing (None) or invalid (UninitialisedFluid). In case you defined a fluid, this is an internal '
          'SunPeek error. Please report it.\n'
          '    - "Volume flow" (vf) in plant: Sensor missing.\n'
          '    - "Inlet temperature" (te_in) in plant: Sensor missing.\n'
          '    - "Outlet temperature" (te_out) in plant: Sensor missing or virtual sensor calculation failed.\n')
         ),

        ('pr_plant_noarrays_nopower',
         ['Extended'], [2, 1], [False], '',
         False, False,
         ('\n'
          '> Thermal Power Check with Mode: extended, Formula: 2, Ignoring wind\n'
          '  - Plant has no arrays. To run a Performance Check analysis, the plant needs one or more arrays.\n'
          '  - "Thermal power" (tp) in plant: Sensor missing or virtual sensor calculation failed.\n'
          '\n'
          '> Thermal Power Check with Mode: extended, Formula: 1, Ignoring wind\n'
          '  - Plant has no arrays. To run a Performance Check analysis, the plant needs one or more arrays.\n'
          '  - "Thermal power" (tp) in plant: Sensor missing or virtual sensor calculation failed.\n'
          '\n'
          'Virtual Sensors:\n'
          '> "Thermal power" (tp) in plant: Virtual sensor calculation failed. Details:\n'
          '  > Calculate thermal power from real sensor\n'
          '    - "Thermal power" (tp) in plant: Sensor missing.\n'
          '  > Calculate thermal power from volume flow\n'
          '    - "fluid_solar" in plant: Fluid is missing (None) or invalid (UninitialisedFluid). In case you defined a fluid, this is an internal '
          'SunPeek error. Please report it.\n'
          '    - "Volume flow" (vf) in plant: Sensor missing.\n'
          '    - "Inlet temperature" (te_in) in plant: Sensor missing.\n'
          '    - "Outlet temperature" (te_out) in plant: Sensor missing or virtual sensor calculation failed.\n')
         ),

        ('pr_plant_noarrays_nopower',
         [None], [1], [False], '',
         False, False,
         ('\n'
          '> Thermal Power Check with Mode: ISO, Formula: 1, Ignoring wind\n'
          '  - Plant has no arrays. To run a Performance Check analysis, the plant needs one or more arrays.\n'
          '  - "Thermal power" (tp) in plant: Sensor missing or virtual sensor calculation failed.\n'
          '\n'
          '> Thermal Power Check with Mode: extended, Formula: 1, Ignoring wind\n'
          '  - Plant has no arrays. To run a Performance Check analysis, the plant needs one or more arrays.\n'
          '  - "Thermal power" (tp) in plant: Sensor missing or virtual sensor calculation failed.\n'
          '\n'
          'Virtual Sensors:\n'
          '> "Thermal power" (tp) in plant: Virtual sensor calculation failed. Details:\n'
          '  > Calculate thermal power from real sensor\n'
          '    - "Thermal power" (tp) in plant: Sensor missing.\n'
          '  > Calculate thermal power from volume flow\n'
          '    - "fluid_solar" in plant: Fluid is missing (None) or invalid (UninitialisedFluid). In case you defined a fluid, this is an internal '
          'SunPeek error. Please report it.\n'
          '    - "Volume flow" (vf) in plant: Sensor missing.\n'
          '    - "Inlet temperature" (te_in) in plant: Sensor missing.\n'
          '    - "Outlet temperature" (te_out) in plant: Sensor missing or virtual sensor calculation failed.\n')
         ),
    ])
def test_pc_run__feedback_parsing__problem(plant_fixture, method, formula, use_wind, expected_strategy,
                                           include_successful_strategies, include_problem_slots,
                                           expected, request):
    plant = request.getfixturevalue(plant_fixture)
    pc_feedback = get_feedback(plant=plant,
                             method=method,
                             formula=formula,
                             use_wind=use_wind,
                             safety_pipes=1,
                             safety_uncertainty=0.9,
                             safety_others=1,
                             )
    assert isinstance(pc_feedback, CoreMethodFeedback)
    if pc_feedback.success:
        assert pc_feedback.successful_strategy_str == expected_strategy

    fb = pc_feedback.parse(include_successful_strategies=include_successful_strategies,
                        include_problem_slots=include_problem_slots)

    assert fb == expected


@pytest.mark.parametrize(
    "method, formula, use_wind, "
    "expected", [
        (['ISO', 'ISO'], [1, 1, 1, PCFormulae.one], [True],
         'Thermal Power Check with Mode: ISO, Formula: 1, Using wind'),
        (['ISO'], [PCFormulae.one], [True, None, True],
         'Thermal Power Check with Mode: ISO, Formula: 1, Using wind'),
        ([PCMethods.iso], [PCFormulae.one], [True],
         'Thermal Power Check with Mode: ISO, Formula: 1, Using wind'),
        (['ISO'], [2], [True, False],
         'Thermal Power Check with Mode: ISO, Formula: 2, Using wind'),
        (['ISO'], [1, 2], [None],
         'Thermal Power Check with Mode: ISO, Formula: 1, Using wind'),
        (['ISO'], [1, PCFormulae.two], [None],
         'Thermal Power Check with Mode: ISO, Formula: 1, Using wind'),
        (['ISO'], [2, 1], [None, False],
         'Thermal Power Check with Mode: ISO, Formula: 2, Ignoring wind'),
        (['ISO'], [2, None], [None, False],
         'Thermal Power Check with Mode: ISO, Formula: 2, Ignoring wind'),
        (['ISO'], [None], [None, False],
         'Thermal Power Check with Mode: ISO, Formula: 2, Ignoring wind'),
        (['Extended'], [1], [True],
         'Thermal Power Check with Mode: extended, Formula: 1, Using wind'),
        (['Extended'], [2], [True, False],
         'Thermal Power Check with Mode: extended, Formula: 2, Using wind'),
        (['Extended'], [1, 2], [None],
         'Thermal Power Check with Mode: extended, Formula: 1, Using wind'),
        (['Extended'], [2, 1], [None, False],
         'Thermal Power Check with Mode: extended, Formula: 2, Ignoring wind'),
    ])
def test_pc__successful_strategy(fhw, method, formula, use_wind, expected):
    successful_strategy = get_successful_strategy(plant=fhw,
                                                  method=method,
                                                  formula=formula,
                                                  use_wind=use_wind,
                                                  )

    assert isinstance(successful_strategy, PCStrategy)
    assert successful_strategy.name == expected


@pytest.mark.parametrize('method', [None, 'ISO', 'Extended'])
@pytest.mark.parametrize('formula', [None, 1, 2])
@pytest.mark.parametrize('use_wind', [None, False, True])
def test_list_pc_problems(fhw, method, formula, use_wind):
    out = list_feedback(plant=fhw,
                        method=method,
                        formula=formula,
                        use_wind=use_wind)

    assert isinstance(out, list)
    assert all(isinstance(x, PCMethodFeedback) for x in out)

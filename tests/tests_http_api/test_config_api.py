import os
import pytest
import json

from sunpeek.common import utils


def test_new_collector(test_client, resources_dir):
    request_url = f"config/collectors/new?token={utils.API_TOKEN}"
    with open(os.path.join(resources_dir, 'plant_config_FHW_Arcon_South_with_Collector.json')) as f:
        col_type = json.load(f)['collectors'][0]
    response = test_client.post(request_url, json=col_type)

    assert response.status_code == 201


def test_list_collectors(test_client):
    request_url = f"config/collectors/?token={utils.API_TOKEN}"
    response = test_client.get(request_url)
    assert response.status_code == 200
    assert len(response.json()) >= 14

    request_url = f"config/collectors/?plant_id=0&token={utils.API_TOKEN}"
    response = test_client.get(request_url)
    assert response.status_code == 200
    assert len(response.json()) == 1


def test_get_collector(test_client):
    request_url = f"config/collectors/0?token={utils.API_TOKEN}"
    response = test_client.get(request_url)

    assert response.status_code == 200
    assert len(response.json()['gross_length']) == 2

    calc_params = response.json()['calculation_info']
    assert isinstance(calc_params, dict)
    assert 'eta0hem' in calc_params


def test_list_slots_api(test_client):
    r = test_client.get(f"/config/sensor_slots?token={utils.API_TOKEN}&component_type=plant")
    assert r.status_code == 200

    returned_slot_names = [d['name'] for d in r.json()]
    assert returned_slot_names == ['tp', 'vf', 'mf', 'te_in', 'te_out', 'te_amb', 've_wind', 'rh_amb', 'pr_amb',
                                   'te_dew_amb', 'in_global', 'in_beam', 'in_diffuse', 'in_dni']

    r = test_client.get(f"/config/sensor_slots?token={utils.API_TOKEN}&component_type=array")

    assert r.status_code == 200
    assert len(r.json()) >= 10


def test_list_sensor_types(test_client):
    r = test_client.get(f"/config/sensor_types?token={utils.API_TOKEN}")

    assert r.status_code == 200
    assert len(r.json()) >= 20
    assert isinstance(r.json()[0]['name'], str)


def test_get_single_sensor_type(test_client):
    r = test_client.get(f"/config/sensor_types?name=ambient_temperature&token={utils.API_TOKEN}")

    assert r.status_code == 200
    assert r.json()['name'] == 'ambient_temperature'
    assert r.json()['compatible_unit_str'] == '°C'


def test_delete_collector(test_client):
    request_url = f"config/collectors/0?token={utils.API_TOKEN}"
    r = test_client.get(request_url)
    assert r.status_code == 200

    request_url = f"config/collectors/0?token={utils.API_TOKEN}"
    r = test_client.delete(request_url)
    assert r.status_code == 204


def test_list_sensor_types_api(test_client):
    r = test_client.get(f"/config/sensor_types?token={utils.API_TOKEN}")

    assert r.status_code == 200

    # returned_slot_names = [d['name'] for d in r.json()]
    # assert returned_slot_names == ['tp', 'vf', 'mf', 'te_in', 'te_out', 'te_amb', 've_wind', 'rh_amb', 'pr_amb',
    #                                'te_dew_amb', 'in_global', 'in_beam', 'in_diffuse', 'in_dni']
    # r = ac.get(f"/config/sensor_slots?token={utils.API_TOKEN}&component_type=array")
    # assert r.status_code == 200
    # assert len(r.json()) >= 10

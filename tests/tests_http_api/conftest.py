import pytest
from fastapi.testclient import TestClient

from sunpeek.common import utils
from sunpeek.api.main import app
from sunpeek.api.dependencies import crud
from sunpeek.api.dependencies import session
import tests.tests_http_api.mock_crud as mc
from tests.tests_http_api.mock_session import Session


def mock_crud():
    yield mc


def mock_session():
    yield Session()


@pytest.fixture()
def app_with_overrides():
    app.dependency_overrides[crud] = mock_crud
    app.dependency_overrides[session] = mock_session
    yield app
    del app.dependency_overrides[crud]
    del app.dependency_overrides[session]


@pytest.fixture()
def test_client(app_with_overrides):
    return TestClient(app=app_with_overrides, base_url=utils.API_LOCAL_BASE_URL)

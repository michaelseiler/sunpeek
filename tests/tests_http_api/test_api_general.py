from sunpeek.common import utils


def test_api_error_returns(test_client):
    request_url = f"plants/1?token={utils.API_TOKEN}"

    data = {"sensor_map": {"te_in": "T_Auß"}}

    response = test_client.post(request_url, json=data)
    assert response.status_code == 400
    assert response.json()['error'] == "ConfigurationError"


def test_api_list_timezones(test_client):
    request_url = f"available_timezones?token={utils.API_TOKEN}"

    response = test_client.get(request_url)

    assert "Africa/Addis_Ababa" in response.json()
    assert "Europe/Vienna" in response.json()
    assert "UTC+6" in response.json()
    assert "UTC offset included in data" in response.json()



class DummyContextManager:
    def __enter__(self):
        pass

    def __exit__(self, exc_type, exc_value, exc_tb):
        pass


class Session:
    no_autoflush = DummyContextManager()
    def add(self, object):
        return None

    def rollback(self):
        return

    def commit(self):
        return

# -*- coding: utf-8 -*-


import os
import pytest
import json
# from fastapi.testclient import TestClient
import sqlalchemy.exc

import sunpeek.demo
from sunpeek.common import utils
import sunpeek.components as cmp


@pytest.mark.parametrize('fn', ['csv1'])
def test_get_sensor_names(test_client, fn, resources_dir):
    # same method as data_inspection - onl kept for backward compatibility
    expected_tst = "timestamps_UTC"
    expected_cols = ["vf", "te_in", "te_out", "te_out_row1", "te_out_row2", "te_out_row3", "te_out_row4", "rd_ghi",
                     "rd_bhi", "rd_gti", "rd_bti", "rd_dti", "rd_dni", "te_amb", "ve_wind", "rh_amb", "is_shadowed"]

    csv_path = os.path.join(os.path.join(resources_dir, 'data', 'data_uploader'), fn + '.csv')
    with open(csv_path, "rb") as f:
        r = test_client.post(f"/plants/0/data/columns?token={utils.API_TOKEN}",
                             files={"files": ("filename", f, "text/csv")},
                             params={"datetime_template": "day_month_year", "timezone": "Europe/Vienna"})

        assert r.status_code == 201
        assert r.json()['sensors'] == expected_cols
        assert r.json()['index'] == expected_tst


@pytest.mark.parametrize('fn', ['csv1'])
def test_get_data_inspection(test_client, fn, resources_dir):
    expected_tst = "timestamps_UTC"
    expected_cols = ["vf", "te_in", "te_out", "te_out_row1", "te_out_row2", "te_out_row3", "te_out_row4", "rd_ghi",
                     "rd_bhi", "rd_gti", "rd_bti", "rd_dti", "rd_dni", "te_amb", "ve_wind", "rh_amb", "is_shadowed"]
    expected_dtypes = ['float64']*8 + ['int64']*5 + ['float64']*3 + ["int64"]
    expected_settings = {
        "datetime_format": None,
        "csv_decimal": ".",
        "csv_encoding": "utf-8",
        "csv_separator": ";",
        "datetime_template": "day_month_year",
        "index_col": 0,
        'timezone': 'Europe/Vienna'
    }

    params = {"datetime_template": "day_month_year", "timezone": "Europe/Vienna"}
    csv_path = os.path.join(os.path.join(resources_dir, 'data', 'data_uploader'), fn + '.csv')
    with open(csv_path, "rb") as f:
        r = test_client.post(f"/plants/0/data/inspection?token={utils.API_TOKEN}", params=params,
                             files={"files": ("filename", f, "text/csv")})

        print(r.json())
        assert r.status_code == 201

        request_data = r.json()
        assert request_data['sensors'] == expected_cols
        assert request_data['index'] == expected_tst
        assert request_data['dtypes'] == expected_dtypes
        assert request_data['settings'] == expected_settings

        table_data = request_data["data"]
        assert len(table_data["index"]) == 5
        assert len(table_data["columns"]) == 17
        assert len(table_data["data"]) == 5


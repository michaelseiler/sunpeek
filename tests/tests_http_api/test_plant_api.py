import pytest
import json

import sunpeek.demo
from sunpeek.common import utils


## Sensors -----------------------------------------------------------------

def test_config_sensors_get_single(test_client):
    r = test_client.get(f"/plants/0/sensors?token={utils.API_TOKEN}&id=1")

    assert r.status_code == 200
    assert r.json()['id'] == 1
    assert r.json()['native_unit'] == 'kelvin'
    assert r.json()['formatted_unit'] == 'K'
    assert r.json()['is_infos_set']


def test_config_sensors_get_list(test_client):
    r = test_client.get(f"/plants/0/sensors?token={utils.API_TOKEN}")

    assert r.status_code == 200
    assert len(r.json()) == 2
    assert r.json()[1]['id'] == 1
    assert r.json()[1]['formatted_unit'] == 'kW'
    assert r.json()[1]['native_unit'] == 'kilowatt'


def test_api_new_sensor(test_client):
    request_url = f"/plants/0/sensors/new?token={utils.API_TOKEN}"
    conf = {
        "raw_name": "rd_Gti",
        "native_unit": "W m**-2",
        "info": {
            "tilt": {"magnitude": 30, "units": "deg"},
            "azim": {"magnitude": 180, "units": "deg"}
        }
    }
    response = test_client.post(request_url, json=conf)

    assert response.json()[0]['raw_name'] == 'rd_Gti'
    assert response.json()[0]['formatted_unit'] == 'W/m²'
    assert isinstance(response.json()[0]['info'], dict)


def test_api_new_sensor_missing_info(test_client):
    request_url = f"/plants/0/sensors/new?token={utils.API_TOKEN}"
    conf = {
        "raw_name": "rd_Gti",
        "sensor_type": "global_radiation",
        "native_unit": "W m**-2",
        "info": {
            "tilt": {"magnitude": 30, "units": "deg"},
            # Azimuth is missing
        }
    }
    response = test_client.post(request_url, json=conf)
    assert response.status_code == 201
    assert response.json()[0]['raw_name'] == 'rd_Gti'
    assert response.json()[0]['formatted_unit'] == 'W/m²'
    assert not response.json()[0]['is_infos_set']
    assert isinstance(response.json()[0]['info'], dict)


def test_api_new_sensor_dimensionless(test_client):
    request_url = f"/plants/0/sensors/new?token={utils.API_TOKEN}"
    conf = {
        "raw_name": "test_boolean",
        "native_unit": "dimensionless",
        "info": {
            "tilt": {"magnitude": 30, "units": "deg"},
            "azim": {"magnitude": 180, "units": "deg"}
        }
    }
    response = test_client.post(request_url, json=conf)

    assert response.json()[0]['raw_name'] == 'test_boolean'
    assert response.json()[0]['formatted_unit'] == 'None'
    assert isinstance(response.json()[0]['info'], dict)


def test_api_new_sensor__no_unit(test_client):
    request_url = f"/plants/0/sensors/new?token={utils.API_TOKEN}"
    conf = {
        "raw_name": "test_boolean",
        "native_unit": None,
        "info": {
            "tilt": {"magnitude": 30, "units": "deg"},
            "azim": {"magnitude": 180, "units": "deg"}
        }
    }
    response = test_client.post(request_url, json=conf)

    assert response.json()[0]['raw_name'] == 'test_boolean'
    assert response.json()[0]['formatted_unit'] is None
    assert isinstance(response.json()[0]['info'], dict)


def test_api_update_sensor_single(test_client):
    request_url = f"/plants/-/sensors/1?token={utils.API_TOKEN}"
    conf = {"info": {"azim": {"magnitude": 100, "units": "deg"}}}
    response = test_client.post(request_url, json=conf)

    assert isinstance(response.json()['info'], dict)
    assert response.json()['info']['azim']['magnitude'] == 100


def test_api_update_sensor_change_type(test_client):
    # Assumes an unmapped sensor is returned by mock_crud
    request_url = f"/plants/-/sensors/9999?token={utils.API_TOKEN}"  # NOTE 9999 used to indicate to mock_crud we need a sensor with type
    response = test_client.get(request_url)
    assert response.json()['sensor_type'] == 'ambient_temperature'

    conf = {"sensor_type": 'fluid_temperature'}
    response = test_client.post(request_url, json=conf)

    assert response.json()['sensor_type'] == 'fluid_temperature'


def test_api_update_sensor_include_unchanged_data(test_client):
    # Assumes an unmapped sensor is returned by mock_crud
    request_url = f"/plants/-/sensors/1?token={utils.API_TOKEN}"
    conf = {"id": 1, 'is_virtual': False, 'native_unit': 'degC'}
    response = test_client.post(request_url, json=conf)

    assert response.json()['native_unit'] == 'degree_Celsius'
    assert response.json()['formatted_unit'] == '°C'
    assert not response.json()['is_virtual']


def test_api_update_sensor_bulk(test_client):
    request_url = f"/plants/-/sensors?token={utils.API_TOKEN}"
    conf = [{"id": 0, "raw_name": "Test Updated"}, {"id": 1, "native_unit": "K"}]
    response = test_client.post(request_url, json=conf)

    assert response.json()[0]['raw_name'] == "Test Updated"
    assert response.json()[1]["native_unit"].lower() == 'kelvin'


def test_api_delete_sensor(test_client):
    request_url = f"/plants/-/sensors/1?token={utils.API_TOKEN}"

    response = test_client.delete(request_url)
    assert response.status_code == 200


def test_api_get_sensors(test_client):
    request_url = f"/plants/0/sensors?token={utils.API_TOKEN}"
    response = test_client.get(request_url)
    assert response.status_code == 200

    arbitrary_sensor = response.json()[0]
    assert "is_mapped" in arbitrary_sensor
    assert "is_virtual" in arbitrary_sensor
    assert "raw_name" in arbitrary_sensor
    assert "formatted_unit" in arbitrary_sensor


## Plant -----------------------------------------------------------------

def test_api_get_plant_summary(test_client):
    request_url = f"/plants/summary/?token={utils.API_TOKEN}"
    response = test_client.get(request_url)

    assert response.status_code == 200


def test_api_new_plant(test_client):
    # timezone = "UTC"
    plant_name = "TEST"

    with open(sunpeek.demo.DEMO_CONFIG_PATH) as f:
        conf = json.load(f)
        conf['plant']['name'] = plant_name
        conf = conf['plant']
    request_url = f"plants/new?token={utils.API_TOKEN}"
    response = test_client.post(request_url, json=conf)

    assert response.status_code == 201
    assert response.json()['name'] == 'TEST'
    assert len(response.json()['raw_sensors']) >= 12
    assert not response.json()['virtuals_calculation_uptodate']


def test_api_get_plants_list(test_client):
    request_url = f"/plants?token={utils.API_TOKEN}"
    response = test_client.get(request_url)

    assert response.status_code == 200
    assert isinstance(response.json(), list)
    assert len(response.json()) > 1


def test_api_get_plant_single(test_client):
    request_url = f"/plants/0?token={utils.API_TOKEN}"
    response = test_client.get(request_url)

    assert response.status_code == 200
    assert response.json()['name'] == 'test'


def test_api_get_plant_by_name(test_client):
    plant_name = "FHW Arcon South"
    request_url = f"/plants?name={plant_name}&token={utils.API_TOKEN}"
    response = test_client.get(request_url)

    assert response.status_code == 200
    assert response.json()[0]['name'] == 'FHW Arcon South'
    assert response.json()[0]['tz_data_offset'] == 60


def test_api_update_plant(test_client):
    request_url = f"plants/1?token={utils.API_TOKEN}"
    data = {"name": "Test Update Plant Name"}
    response = test_client.post(request_url, json=data)

    assert response.status_code == 200
    assert response.json()['name'] == "Test Update Plant Name"


def test_api_sensor_map(test_client):
    request_url = f"plants/1?token={utils.API_TOKEN}"
    data = {"sensor_map": {"te_amb": "T_Auß"}}
    response = test_client.post(request_url, json=data)

    assert response.status_code == 200
    assert response.json()['sensor_map']["te_amb"] == "T_Auß"

    data = {"sensor_map": {"te_in": None}}
    response = test_client.post(request_url, json=data)

    assert "te_in" not in response.json()['sensor_map']


def test_api_update_plant_summary(test_client):
    request_url = f"plants/1/summary?token={utils.API_TOKEN}"
    data = {"name": "Test Update Plant Name", "latitude": {"magnitude": 0, "units": "deg"}}
    response = test_client.post(request_url, json=data)

    assert response.status_code == 200
    assert response.json()['name'] == "Test Update Plant Name"
    assert response.json()["latitude"]["magnitude"] == 0


def test_api_duplicate_key_error(test_client):
    request_url = f"plants/new?token={utils.API_TOKEN}"
    config = {"name": "test_duplicate_plant_name",
              "latitude": {"magnitude": 47.047201, "units": "deg"},
              "longitude": {"magnitude": 15.436428, "units": "deg"}}
    response = test_client.post(request_url, json=config)

    assert response.status_code == 409
    assert response.json()['message'] == 'Item with duplicate identifier (e.g. name or id) exists'


## Array, Fluid -----------------------------------------------------------------

def test_api_new_array(test_client):
    request_url = f"/plants/0/arrays/new?token={utils.API_TOKEN}"
    conf = {
        "name": "Arcon South",
        "area_gr": {"magnitude": 515.66, "units": "m**2"},
        "area_ap": {"magnitude": 478.8, "units": "m**2"},
        "collector": "Arcon 3510",
        "azim": {"magnitude": 180, "units": "deg"},
        "tilt": {"magnitude": 30, "units": "deg"}
    }
    response = test_client.post(request_url, json=conf)

    assert response.status_code == 201
    assert response.json()[0]['name'] == 'Arcon South'
    assert response.json()[0]['collector'] == 'Arcon 3510'


def test_api_update_array(test_client):
    request_url = f"plants/-/arrays/1?token={utils.API_TOKEN}"
    response = test_client.post(request_url, json={"name": "Arcon South 2"})

    assert response.status_code == 200
    assert response.json()['name'] == "Arcon South 2"


def test_api_get_fluids(test_client):
    request_url = f"/plants/0/fluids/?token={utils.API_TOKEN}"
    response = test_client.get(request_url)

    assert response.status_code == 200

    request_url = f"/plants/0/fluids/?token={utils.API_TOKEN}"
    response = test_client.get(request_url, params={'plant_id': 0})

    assert response.status_code == 200


def test_api_get_fluid_by_id(test_client):
    request_url = f"/plants/0/fluids/0?token={utils.API_TOKEN}"
    response = test_client.get(request_url)

    assert response.status_code == 200


## Import / Export  -----------------------------------------------------------------

def test_plant_export(test_client):
    request_url = f"/plants/0/export_config?token={utils.API_TOKEN}"
    response = test_client.get(request_url)

    assert response.status_code == 200
    assert "plant" in response.json()
    assert len(response.json()['fluid_definitions']) == 1
    assert response.json()['collectors'][0]['name'] == 'Arcon 3510'


def test_plant_export_import(test_client):
    # Test export + import with new name
    request_url = f"/plants/0/export_config?token={utils.API_TOKEN}"
    response = test_client.get(request_url)
    assert response.status_code == 200

    # Export config
    config_export = response.json()
    plant_name = config_export['plant']['name']
    new_plant_name = f'{plant_name}__new'

    # Import
    request_url = f"/plants/import"
    params = {'token': utils.API_TOKEN,
              'new_plant_name': new_plant_name}
    response = test_client.post(request_url, params=params, json=config_export)

    assert response.status_code == 201
    assert response.json()[0]['name'] == new_plant_name

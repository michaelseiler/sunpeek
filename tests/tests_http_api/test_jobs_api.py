import uuid

from sunpeek.common import utils


# from sunpeek.api.dependencies import session


def test_single_job_api(test_client):
    request_url = f"jobs/444cdf1f-3983-4883-9508-a0cf49f492b7?token={utils.API_TOKEN}"

    response = test_client.get(request_url)
    assert response.status_code == 200
    assert response.json()['id'] == "444cdf1f-3983-4883-9508-a0cf49f492b7"
    assert response.json()['status'] == 'running'


def test_jobs_api(test_client):
    request_url = f"jobs?token={utils.API_TOKEN}"

    response = test_client.get(request_url)
    assert response.status_code == 200
    assert len(response.json()) >= 3
    for job in response.json():
        assert uuid.UUID(job['id']).version == 4
        assert job['status'] in ["pending", "running", "done", "failed"]

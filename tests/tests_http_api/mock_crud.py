import uuid
from typing import Type
import sqlalchemy.exc

from sunpeek.common.unit_uncertainty import Q
import sunpeek.components as cmp
from sunpeek.components.helpers import ResultStatus
from sunpeek.definitions.collectors import all_definitions
from sunpeek.definitions.fluid_definitions import all_definitions as fluids
import sunpeek.components.sensor_types as st
from sunpeek.definitions.fluid_definitions import WPDFluids
from sunpeek.components.fluids import WPDFluidDefinition


def fluid_test():
    return cmp.FluidFactory(fluid=WPDFluidDefinition.from_fluid_info(WPDFluids.fhw_pekasolar.value))


def get_components(session, component: Type[cmp.Component], id: int = None, name: str = None, plant_id: int = None,
                   plant_name: str = None):
    if component is cmp.Collector.name:
            return [(col.name,) for col in all_definitions]
    if component is cmp.FluidDefinition.name:
        return [(fluid.name,) for fluid in fluids]
    if component == cmp.Sensor:
        if id == 9999:
            return cmp.Sensor(raw_name="Test", native_unit="kelvin", sensor_type=st.ambient_temperature)
        elif id is not None or isinstance(name, str):
            s = cmp.Sensor(raw_name="Test", native_unit="kelvin")
            s.id = id
            return s
        elif isinstance(plant_id, int) or isinstance(plant_name, str):
            s1 = cmp.Sensor(raw_name="Test", native_unit="kelvin")
            s1.id = 0
            s2 = cmp.Sensor(raw_name="Test2", native_unit="kW")
            s2.id = 1
            return [s1, s2]
    if component == cmp.Array:
        if name is not None:
            comp = cmp.Array(name=name)
        else:
            comp = cmp.Array(name="Test")
        if isinstance(id, int):
            comp.id = id
        return comp
    if component == cmp.Collector:
        defs = []
        if id is not None:
            col = all_definitions[id]
            col.id = id
            return col
        for i, col in enumerate(all_definitions):
            col.id = i
            defs.append(col)
        return defs
    if component == cmp.SensorType:
        defs = [st.__dict__[key] for key in st.__dict__ if isinstance(st.__dict__[key], cmp.SensorType)]
        return defs
    if component == cmp.Fluid:
        return fluid_test()

    if component == "Job":
        if id is not None:
            job = cmp.Job(status=ResultStatus.running)
            job.id = id
            return job
        else:
            return [cmp.Job(status=ResultStatus.pending), cmp.Job(status=ResultStatus.running),
                    cmp.Job(status=ResultStatus.done)]


def get_sensors(session, id: int = None, raw_name: str = None, plant_id: int = None, plant_name: str = None):
    return get_components(session, cmp.Sensor, id, raw_name, plant_id, plant_name)


def get_plants(session=None, plant_id: int = None, plant_name: str = 'test'):
    collector = all_definitions[0]
    fluid = fluid_test()
    raw_sensors = [cmp.Sensor('T_Auß', 'degC', sensor_type=st.ambient_temperature),
                   cmp.Sensor('T_inlet', 'degC', sensor_type=st.fluid_temperature)]
    p = cmp.Plant(plant_name, latitude=Q(47, 'deg'), longitude=Q(15, 'deg'),
                  arrays=[cmp.Array('Test Array', collector=collector)], fluid_solar=fluid,
                  raw_sensors=raw_sensors,
                  sensor_map={'te_in': 'T_inlet'})
    if plant_id is not None:
        p.id = plant_id
    else:
        p.id = 0
    if plant_id is None and plant_name is None:
        p2 = cmp.Plant('test', arrays=[cmp.Array('Test Array', collector=collector)], fluid_solar=fluid)
        p2.id = 42
        return [p, p2]
    return p


def create_component(session, component: cmp.Component, commit=True):
    collector = all_definitions[0]
    component.id = 0
    if isinstance(component, cmp.Plant):
        if component.name == "test_duplicate_plant_name":
            raise sqlalchemy.exc.IntegrityError("duplicate key value violates unique constraint",
                                                orig="duplicate key value violates unique constraint", params={})
        collector = all_definitions[0]
        fluid = fluid_test()
        component.arrays[0].collector = collector
        component.fluid_solar = fluid
    elif isinstance(component, cmp.Array):
        component.collector = collector
    elif isinstance(component, cmp.Job):
        component.id = uuid.uuid4()
    return component


def update_component(session, component: cmp.helpers.ORMBase, commit=True):
    # if isinstance(component, cmp.Plant):
    #     return get_plants(session, plant_id=component.id, plant_name=getattr(component, 'name', 'test'))
    # return get_components(session, component=component.__class__, id=component.id, name=getattr(component, 'name', None))
    return component


def delete_component(session, component: cmp.helpers.ORMBase):
    return None

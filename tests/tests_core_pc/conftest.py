import pytest
import pandas as pd
import datetime as dt
import numpy as np

from sunpeek.common.unit_uncertainty import Q, to_dict
from sunpeek.components import Plant, Array, Sensor, Collector, CollectorTypes, CollectorQDT
import sunpeek.components.iam_methods as iam
import sunpeek.core_methods.virtuals as virtuals
from sunpeek.data_handling.wrapper import use_dataframe


@pytest.fixture(scope='session')
def example_data():
    start = dt.datetime(2020, 1, 1)
    end = dt.datetime(2020, 2, 1)
    columns_needed = {'rd_gti': 'W m**-2',
                      'rd_bti': 'W m**-2',
                      'rd_dti': 'W m**-2',
                      'tp': 'kW',
                      'te_out': 'degC', 'te_in': 'degC', 'te_amb': 'degC',
                      've_wind': 'm/s',
                      'is_shadowed': 'dimensionless'}
    columns_needed = {k: f'pint[{v}]' for k, v in columns_needed.items()}
    index = pd.date_range(start=start, end=end, freq="1T", tz='UTC')
    M = len(index)
    N = len(columns_needed)
    data = pd.DataFrame(np.random.randn(M, N), columns=columns_needed.keys(), index=index)
    data["is_shadowed"] = data["is_shadowed"]
    return data


@pytest.fixture(scope='session')
def params_for_pc():
    return {'lat': 47.047201,
            "long": 15.436428,
            "ground_tilt": 0.0,
            "safety_factor": 0.92,

            "collectors": [{
                "tilt": 30.0,
                "azim": 180.0,
                'a1_ap': 2.2261261904761906,
                "a2_ap": 0.009692857142857142,
                "a5_ap": 7875.984920634921,
                "a8_ap": 0,
                "eta0_hem": 0.7939284662698414,
                "eta0_b": 0.8023531746031747,
                "k50": 0.9,
                "Kd": 0.93,
                "area_gr": 515.66,
                "area_ap": 478.8,
                "name": "Collector A",
            }]
            }


@pytest.fixture(scope='session')
def plant_for_pc(params_for_pc) -> Plant:
    plant = Plant(latitude=Q(params_for_pc["lat"], "deg"), longitude=Q(params_for_pc["long"], "deg"))
    for col_params in params_for_pc["collectors"]:
        col = Collector(
            name='Test Collector',
            eta0hem=Q(col_params["eta0_hem"]),
            eta0b=Q(col_params["eta0_b"]),
            a1=Q(col_params["a1_ap"], "W m**-2 K**-1"),
            a2=Q(col_params["a2_ap"], "W m**-2 K**-2"),
            a5=Q(col_params["a5_ap"], "J m**-2 K**-1"),
            a8=Q(col_params["a8_ap"], "W m**-2 K**-4"),
            kd=Q(col_params["Kd"]),
            test_type='SST',
            test_reference_area="gross",
            gross_length=Q(1, "m"),
            area_gr=Q(3, "m**2"),
            iam_method=iam.IAM_K50(k50=Q(col_params['k50'])),
            collector_type=CollectorTypes.flat_plate.value,
        )

        array = Array(
            name=col_params['name'],
            plant=plant,
            collector=col,
            tilt=Q(col_params["tilt"], "deg"),
            azim=Q(col_params["azim"], "deg"),
            area_gr=Q(col_params["area_gr"], "m²"),
            row_spacing=Q(3.08, "m"),
        )
        array.set_sensors(
            te_in=Sensor("te_in", "degC", plant=plant),
            te_out=Sensor("te_out", "degC", plant=plant),
            in_global=Sensor("rd_gti", "W m**-2", plant=plant, info={'azim': array.azim, 'tilt': array.tilt}),
            in_beam=Sensor("rd_bti", "W m**-2", plant=plant, info={'azim': array.azim, 'tilt': array.tilt}),
            in_diffuse=Sensor("rd_dti", "W m**-2", plant=plant, info={'azim': array.azim, 'tilt': array.tilt})
        )

    plant.set_sensors(
        te_amb=Sensor("te_amb", "degC", plant=plant),
        tp=Sensor("tp", "kW", plant=plant),
        ve_wind=Sensor("ve_wind", "m/s", plant=plant),
    )

    return plant


@pytest.fixture(scope='session')
def plant_for_pc_with_example_data(plant_for_pc, example_data):
    use_dataframe(plant_for_pc, example_data, calculate_virtuals=False)
    return plant_for_pc


@pytest.fixture(scope='session')
def plant_for_pc_with_example_data_virtuals(plant_for_pc_with_example_data):
    virtuals.calculate_virtuals(plant_for_pc_with_example_data)
    return plant_for_pc_with_example_data


@pytest.fixture(scope='session')
def fhw_like__concentrating(dummy_concentrating__collector, example_data):
    plant = Plant(latitude=Q(47, "deg"), longitude=Q(15, "deg"),
                  te_amb=Sensor("te_amb", "degC"),
                  tp=Sensor("tp", "kW"),
                  ve_wind=Sensor("ve_wind", "m/s"),
                  )
    azim_tilt = {'tilt': Q(30, 'deg'),
                 'azim': Q(180, 'deg')}
    sensor_info = {k: to_dict(v) for k, v in azim_tilt.items()}
    array = Array(
        name='main',
        plant=plant,
        collector=dummy_concentrating__collector,
        tilt=azim_tilt['tilt'],
        azim=azim_tilt['azim'],
        area_gr=Q(500, "m²"),
        row_spacing=Q(3, "m"),
        te_in=Sensor("te_in", "degC"),
        te_out=Sensor("te_out", "degC"),
        in_global=Sensor("rd_gti", "W m**-2", info=sensor_info),
        in_beam=Sensor("rd_bti", "W m**-2", info=sensor_info),
        in_diffuse=Sensor("rd_dti", "W m**-2", info=sensor_info)
    )

    use_dataframe(plant, example_data, calculate_virtuals=True)

    return plant

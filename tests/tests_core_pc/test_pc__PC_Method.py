import pytest
import datetime as dt
import pandas as pd
# from unittest.mock import patch, MagicMock

import sunpeek.demo
from sunpeek.common.unit_uncertainty import Q
from sunpeek.components.components_factories import CollectorSST, CollectorTypes
import sunpeek.components.iam_methods as iam
from sunpeek.core_methods.pc_method.main import PCMethod, PCMethodISO, PCMethodExtended
from sunpeek.core_methods.pc_method.wrapper import run_performance_check
from sunpeek.data_handling.wrapper import use_csv
from sunpeek.common.utils import DatetimeTemplates
import sunpeek.common.errors as err


# Testing return values

def test_pc_handler_returns_pc_method_iso(plant_for_pc):
    pc = PCMethodISO(plant_for_pc, formula=1)
    assert issubclass(type(pc), PCMethodISO)


def test_pc_handler_returns_pc_method_extended(plant_for_pc):
    pc = PCMethodExtended(plant_for_pc, formula=1)
    assert issubclass(type(pc), PCMethodExtended)


def test_pc_warns_if_virtuals_not_uptodate(plant_for_pc_with_example_data_virtuals):
    # Manually setting virtuals_uptodate flag on plant:
    # Need virtuals in plant, otherwise problem report will fail on virtual sensors.
    p = plant_for_pc_with_example_data_virtuals
    p.virtuals_calculation_uptodate = False

    with pytest.warns(UserWarning, match='results might be outdated or inconsistent with the plant configuration'):
        run_performance_check(plant=p)


# Test all parameters are understood

def test_run_pc_method_iso_with_all_parameters(plant_for_pc_with_example_data_virtuals):
    pc_output = run_performance_check(plant=plant_for_pc_with_example_data_virtuals,
                                      method=['ISO'],
                                      formula=[1],
                                      use_wind=[True],
                                      safety_pipes=0.95,
                                      safety_uncertainty=0.9,
                                      safety_others=0.99,
                                      accuracy_level="II",
                                      max_gap_in_interval=dt.timedelta(minutes=10),
                                      max_nan_density=0.2,
                                      ).output

    assert pc_output.plant_output.n_intervals == 0


def test_run_pc_method_extended_with_all_parameters(plant_for_pc_with_example_data_virtuals):
    pc_output = run_performance_check(plant=plant_for_pc_with_example_data_virtuals,
                                      method=['Extended'],
                                      formula=[1],
                                      use_wind=[True],
                                      safety_pipes=0.95,
                                      safety_uncertainty=0.9,
                                      safety_others=0.99,
                                      accuracy_level="II",
                                      interval_length=dt.timedelta(minutes=30),
                                      min_data_in_interval=10,
                                      max_gap_in_interval=dt.timedelta(minutes=10),
                                      max_nan_density=0.2,
                                      ).output
    assert pc_output.plant_output.n_intervals == 0


@pytest.mark.parametrize('formula', [1, 2])
def test_run_pc_method_without_kd(formula, plant_for_pc_with_example_data_virtuals):
    pl = plant_for_pc_with_example_data_virtuals
    # using a collector without Kd in its Solar Keymark database information
    pl.arrays[0].collector = CollectorSST(name="Savo SF500-15DG",
                                          manufacturer_name="Savo Solar Oyj",
                                          product_name="Savo SF500-15DG",
                                          licence_number='011-7S2689 F',
                                          test_report_id="C1705LPEN (2016-12-15), C1705QPEN (2016-12-15)",
                                          certificate_date_issued=dt.datetime(2016, 12, 15),
                                          certificate_lab='SPF, Switzerland',
                                          collector_type=CollectorTypes.flat_plate.value,
                                          test_reference_area="gross",
                                          area_gr=Q(15.96, 'm**2'),
                                          gross_width=Q(6158, "mm"),
                                          gross_length=Q(2591, "mm"),
                                          gross_height=Q(213, "mm"),
                                          eta0hem=Q(0.793, ''),
                                          a1=Q(2.520, 'W m**-2 K**-1'),
                                          a2=Q(0.004, 'W m**-2 K**-2'),
                                          a8=Q(0, 'W m**-2 K**-4'),
                                          ceff=Q(12.0, "kJ m**-2 K**-1"),
                                          iam_method=iam.IAM_Interpolated(
                                              aoi_reference=Q([10, 20, 30, 40, 50, 60, 70, 80, 90], 'deg'),
                                              iam_reference=Q([[1, 1, 0.99, 0.98, 0.96, 0.87, 0.68, 0.38, 0],
                                                               [1, 1, 1.00, 0.99, 0.96, 0.91, 0.78, 0.53, 0]]))
                                          )
    run_performance_check(plant=pl)


@pytest.mark.parametrize(
    "method, interval_length", [
        ('Extended', dt.timedelta(hours=2)),
        ('extended', dt.timedelta(minutes=30)),
        ('ISO', dt.timedelta(hours=1)),
    ])
@pytest.mark.parametrize("formula", [1, 2])
def test_interval_length_ok(method, formula, interval_length, plant_for_pc):
    PCMethod.create(plant=plant_for_pc,
                    method=method,
                    formula=formula,
                    interval_length=interval_length)


@pytest.mark.parametrize(
    "method, interval_length", [
        ('ISO', dt.timedelta(minutes=30)),
        ('ISO', dt.timedelta(hours=2)),
    ])
@pytest.mark.parametrize("formula", [1, 2])
def test_interval_length_bad(method, formula, interval_length, plant_for_pc):
    with pytest.raises(err.PCMethodError, match='fixed to 1 hour'):
        PCMethod.create(plant=plant_for_pc,
                        method=method,
                        formula=formula,
                        interval_length=interval_length)


@pytest.mark.parametrize(
    "method, interval_length, max_gap, expected_length, expected_gap", [
        ('ISO', None, dt.timedelta(minutes=30), dt.timedelta(minutes=60), dt.timedelta(minutes=30)),
        ('ISO', None, dt.timedelta(minutes=20), dt.timedelta(minutes=60), dt.timedelta(minutes=20)),
        ('ISO', None, dt.timedelta(hours=2), dt.timedelta(minutes=60), dt.timedelta(minutes=60)),
        ('Extended', dt.timedelta(hours=2), dt.timedelta(minutes=90), dt.timedelta(hours=2), dt.timedelta(minutes=90)),
        ('Extended', dt.timedelta(hours=1), dt.timedelta(minutes=90), dt.timedelta(hours=1), dt.timedelta(minutes=60)),
    ])
@pytest.mark.parametrize('formula', [1, 2])
def test_max_gap(plant_for_pc, method, formula, interval_length, max_gap,
                 expected_length, expected_gap):
    pc = PCMethod.create(plant=plant_for_pc,
                         method=method,
                         formula=formula,
                         interval_length=interval_length,
                         max_gap_in_interval=max_gap)

    assert pc.settings.interval_length == expected_length
    assert pc.settings.max_gap_in_interval == expected_gap


# safety factors: see test_pc__params_safety_factors.py

@pytest.mark.parametrize('method', ['ISO', 'Extended'])
@pytest.mark.parametrize('formula', [1, 2])
def test_nan_density_ok(plant_for_pc, method, formula):
    # default
    pc = PCMethod.create(method=method, plant=plant_for_pc, formula=formula)
    expected = 0.1
    assert pc.settings.max_nan_density == expected

    pc = PCMethod.create(method=method, plant=plant_for_pc, formula=formula,
                         max_nan_density=0.5)
    expected = 0.5
    assert pc.settings.max_nan_density == expected


@pytest.mark.parametrize('method', ['ISO', 'Extended'])
@pytest.mark.parametrize('formula', [1, 2])
@pytest.mark.parametrize('nan_dens', [-1, 1.2])
def test_nan_density_bad(plant_for_pc, method, formula, nan_dens):
    with pytest.raises(err.PCMethodError, match='None or a float between 0 and 1'):
        PCMethod.create(method=method, plant=plant_for_pc, formula=formula,
                        max_nan_density=nan_dens)


## Test PC method limits to available data

def test_pc__no_data_uploaded(fhw):
    # Test what happens in the backend when running PC method on a plant without any data uploaded
    with pytest.raises(err.NoDataError, match='Context has a None datasource'):
        run_performance_check(plant=fhw, method=['iso'], formula=[1])


def test_pc__no_dates(fhw__2days_data):
    pc_output = run_performance_check(plant=fhw__2days_data, method=['iso'], formula=[1]).output

    assert pc_output.datetime_eval_start == fhw__2days_data.time_index[0]
    assert pc_output.datetime_eval_end == fhw__2days_data.time_index[-1]


def to_utc(ds: str) -> dt.datetime:
    return pd.to_datetime(ds, utc=True).to_pydatetime()


@pytest.mark.parametrize('start, end', [
    ('2017-03-01', '2017-04-01'),
    ('2017-05-02 23:00:00', '2017-06-02'),
    ('2017-06-01', '2017-06-02'),
])
def test_pc__no_overlap(fhw__2days_data, start, end):
    # Selected eval interval does not overlap with uploaded data
    with pytest.raises(err.NoDataError, match='No measurements available in the range'):
        run_performance_check(plant=fhw__2days_data, method=['iso'], formula=[1],
                              eval_start=to_utc(start), eval_end=to_utc(end))


@pytest.mark.parametrize('start, end', [
    ('2017-03-01', '2017-04-30 23:00:00'),
    ('2017-05-02 22:59:00', '2017-05-10'),
])
def test_pc__touches_one_side(fhw__2days_data, start, end):
    # Selected eval interval touches uploaded data at left or right side -> not enough data, only 1 value
    with pytest.raises(err.NoDataError, match='No measurements available for the plant in the selected range'):
        run_performance_check(plant=fhw__2days_data, method=['iso'], formula=[1],
                              eval_start=to_utc(start), eval_end=to_utc(end))


@pytest.mark.parametrize('start, end, expected', [
    ('2017-04-01', '2017-05-02 00:00:00', ('2017-04-30 23:00:00', '2017-05-02 00:00:00')),
    ('2017-05-02 00:00:00', '2017-05-10', ('2017-05-02 00:00:00', '2017-05-02 22:59:00')),
])
def test_pc__overlap_one_side(fhw__2days_data, start, end, expected):
    # Selected eval interval overlaps uploaded data at left or right side
    pc_output = run_performance_check(plant=fhw__2days_data, method=['iso'], formula=[1],
                                      eval_start=to_utc(start), eval_end=to_utc(end)).output

    assert pc_output.datetime_eval_start == to_utc(expected[0])
    assert pc_output.datetime_eval_end == to_utc(expected[1])


@pytest.mark.parametrize('start, end', [
    ('2017-04-30 23:00:00', '2017-05-02 23:00:00'),
    ('2017-04-01', '2017-06-01'),
])
def test_pc__overlap_both_sides(fhw__2days_data, start, end):
    # Selected eval interval is superset of uploaded data
    expected = ('2017-04-30 23:00:00', '2017-05-02 22:59:00')
    pc_output = run_performance_check(plant=fhw__2days_data, method=['iso'], formula=[1],
                                      eval_start=to_utc(start), eval_end=to_utc(end)).output

    assert pc_output.datetime_eval_start == to_utc(expected[0])
    assert pc_output.datetime_eval_end == to_utc(expected[1])


@pytest.fixture
def fhw__with_gap(fhw):
    # Artificial test plant, with gap in-between / no data available in some interval
    use_csv(fhw, csv_files=sunpeek.demo.DEMO_DATA_PATH_2DAYS, timezone='UTC', calculate_virtuals=True,
            datetime_template=DatetimeTemplates.year_month_day)
    fhw.context.delete_sensor_data(start=pd.to_datetime('2017-05-01 12:00', utc=True).to_pydatetime(),
                                   end=pd.to_datetime('2017-05-02 12:00', utc=True).to_pydatetime())
    return fhw


@pytest.mark.parametrize('start, end, expected', [
    ('2017-04-01', '2017-05-02 06:00:00', ('2017-04-30 23:00:00', '2017-05-02 06:00:00')),
])
def test_pc__gap(fhw__with_gap, start, end, expected):
    expected = [pd.to_datetime(x, utc=True) for x in expected]
    pc_output = run_performance_check(plant=fhw__with_gap, method=['iso'], formula=[1],
                                      eval_start=to_utc(start), eval_end=to_utc(end)).output

    assert pc_output.datetime_eval_start == expected[0]
    assert pc_output.datetime_eval_end == expected[1]


def test_pc__gap_no_dates(fhw__with_gap):
    pc_output = run_performance_check(plant=fhw__with_gap, method=['iso'], formula=[1]).output

    assert pc_output.datetime_eval_start == fhw__with_gap.time_index[0]
    assert pc_output.datetime_eval_end == fhw__with_gap.time_index[-1]


@pytest.mark.parametrize('start, end', [
    ('2017-05-01 14:00:00', '2017-05-02 10:00:00'),
])
def test_pc__gap__no_data(fhw__with_gap, start, end):
    with pytest.raises(err.NoDataError, match='No measurements available for the plant in the selected range'):
        run_performance_check(plant=fhw__with_gap, method=['iso'], formula=[1],
                              eval_start=to_utc(start), eval_end=to_utc(end))

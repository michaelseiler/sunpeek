import pytest

from sunpeek.core_methods.pc_method import PCFormulae, PCMethods
import sunpeek.core_methods.pc_method.wrapper as pc_wrapper
from sunpeek.serializable_models import CoreMethodFeedback


@pytest.mark.parametrize(
    "method, formula, use_wind, expected_success, expected_strategy", [
        ('ISO', 1, True, True,
         'Thermal Power Check with Mode: ISO, Formula: 1, Using wind'),
        ('ISO', 1, False, True,
         'Thermal Power Check with Mode: ISO, Formula: 1, Ignoring wind'),
        ('ISO', 2, True, True,
         'Thermal Power Check with Mode: ISO, Formula: 2, Using wind'),
        ('ISO', 2, False, True,
         'Thermal Power Check with Mode: ISO, Formula: 2, Ignoring wind'),
        ('Extended', 1, True, True,
         'Thermal Power Check with Mode: extended, Formula: 1, Using wind'),
        ('Extended', 1, False, True,
         'Thermal Power Check with Mode: extended, Formula: 1, Ignoring wind'),
        ('Extended', 2, True, True,
         'Thermal Power Check with Mode: extended, Formula: 2, Using wind'),
        ('Extended', 2, False, True,
         'Thermal Power Check with Mode: extended, Formula: 2, Ignoring wind'),
        (None, None, None, True,
         'Thermal Power Check with Mode: ISO, Formula: 2, Using wind'),
        ('Extended', 1, None, True,
         'Thermal Power Check with Mode: extended, Formula: 1, Using wind'),
        (None, 1, None, True,
         'Thermal Power Check with Mode: ISO, Formula: 1, Using wind'),
        (None, None, False, True,
         'Thermal Power Check with Mode: ISO, Formula: 2, Ignoring wind'),
    ]
)
def test_all_combinations(fhw__2days_data, method, formula, use_wind, expected_success, expected_strategy):
    pc_result = pc_wrapper.run_performance_check(plant=fhw__2days_data,
                                                 method=[method],
                                                 formula=[formula],
                                                 use_wind=[use_wind],
                                                 )
    assert pc_result.success
    assert pc_result.successful_strategy_str == expected_strategy


@pytest.mark.parametrize(
    "method, formula, use_wind, expected_success, expected_strategy", [
        (['ISO'], [1], [True], True,
         'Thermal Power Check with Mode: ISO, Formula: 1, Using wind'),
        (['ISO', 'ISO'], [1, 1, 1, PCFormulae.one], [True], True,
         'Thermal Power Check with Mode: ISO, Formula: 1, Using wind'),
        ('ISO', PCFormulae.one, [True, None, True], True,
         'Thermal Power Check with Mode: ISO, Formula: 1, Using wind'),
        (PCMethods.iso, PCFormulae.one, [True], True,
         'Thermal Power Check with Mode: ISO, Formula: 1, Using wind'),
        (['ISO'], [2], [True, False], True,
         'Thermal Power Check with Mode: ISO, Formula: 2, Using wind'),
        (['ISO'], [1, 2], [None], True,
         'Thermal Power Check with Mode: ISO, Formula: 1, Using wind'),
        (['ISO'], [1, PCFormulae.two], [None], True,
         'Thermal Power Check with Mode: ISO, Formula: 1, Using wind'),
        (['ISO'], [2, 1], [None, False], True,
         'Thermal Power Check with Mode: ISO, Formula: 2, Ignoring wind'),
        (['ISO'], [2, None], [None, False], True,
         'Thermal Power Check with Mode: ISO, Formula: 2, Ignoring wind'),
        (['ISO'], None, [None, False], True,
         'Thermal Power Check with Mode: ISO, Formula: 2, Ignoring wind'),
        (['Extended'], [1], [True], True,
         'Thermal Power Check with Mode: extended, Formula: 1, Using wind'),
        (['Extended'], [2], [True, False], True,
         'Thermal Power Check with Mode: extended, Formula: 2, Using wind'),
        (['Extended'], [1, 2], [None], True,
         'Thermal Power Check with Mode: extended, Formula: 1, Using wind'),
        (['Extended'], [2, 1], [None, False], True,
         'Thermal Power Check with Mode: extended, Formula: 2, Ignoring wind'),
    ])
def test_pc_inputs_to_strategies_mapper(fhw, method, formula, use_wind, expected_success, expected_strategy):
    pc_feedback = pc_wrapper.get_feedback(plant=fhw,
                                        method=method,
                                        formula=formula,
                                        use_wind=use_wind,
                                        safety_pipes=1,
                                        safety_uncertainty=0.9,
                                        safety_others=1,
                                        )

    assert isinstance(pc_feedback, CoreMethodFeedback)
    assert pc_feedback.success == expected_success
    assert pc_feedback.successful_strategy_str == expected_strategy


@pytest.mark.parametrize(
    "method, formula, use_wind, listlen, success", [
        (['ISO'], [1], [True], 1, [True]),
        (None, [1, PCFormulae.one], [True], 2, [True, True]),
        (['ISO', 'Extended'], [1, 2], [True, None, True], 4, [True for i in range(4)]),
        (None, None, [None], 12, [True, True, True, True, False, False, True, True, True, True, False, False]),
        # False because formula 3 not working for classical FHW setup
    ])
def test_list_pc_problems(fhw, method, formula, use_wind, listlen, success):
    plist = pc_wrapper.list_feedback(fhw, method, formula, use_wind)

    assert isinstance(plist, list)
    assert len(plist) == listlen
    for p, s in zip(plist, success):
        assert p.success == s

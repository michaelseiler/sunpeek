import pytest
from io import StringIO
import pandas as pd

import sunpeek.core_methods.pc_method.wrapper as pc_wrapper
import sunpeek.core_methods.pc_method as pc
from sunpeek.components import PCMethodOutput


@pytest.mark.parametrize('method', ['ISO', 'extended'])
@pytest.mark.parametrize('formula', [None, 1, 2])
@pytest.mark.parametrize('use_wind', [None, True, False])
# @pytest.mark.parametrize('formula', [1])
# @pytest.mark.parametrize('method', ['ISO'])
# @pytest.mark.parametrize('use_wind', [False])
def test_formula12(fhw__2days_data, method, formula, use_wind):
    pc_result = pc_wrapper.run_performance_check(plant=fhw__2days_data,
                                                 method=[method],
                                                 formula=[formula],
                                                 use_wind=[use_wind])
    pc_output = pc_result.output

    assert isinstance(pc_output, PCMethodOutput)


@pytest.mark.parametrize('method', ['ISO', 'Extended'])
@pytest.mark.parametrize('formula', [None, 3])
@pytest.mark.parametrize('use_wind', [None, True, False])
# @pytest.mark.parametrize('formula', [3])
# @pytest.mark.parametrize('method', ['ISO'])
# @pytest.mark.parametrize('use_wind', [False])
def test_formula3(fhw__concentrating__2days_data, method, formula, use_wind):
    pc_result = pc_wrapper.run_performance_check(plant=fhw__concentrating__2days_data,
                                                 method=[method],
                                                 formula=[formula],
                                                 use_wind=[use_wind])
    pc_output = pc_result.output

    assert isinstance(pc_output, PCMethodOutput)


@pytest.mark.parametrize(
    "method, formula, use_wind", [
        (['isoxx'], [1], [True]),
        (['ISO_'], [1, 1, 1, pc.PCFormulae.one], [True]),
        (['IssO '], [2], [123]),
    ])
def test_pc_invalid_inputs__method(fhw__2days_data, method, formula, use_wind):
    with pytest.raises(ValueError, match='is not a valid PCMethods'):
        pc_wrapper.list_feedback(plant=fhw__2days_data,
                                 method=method,
                                 formula=formula,
                                 use_wind=use_wind,
                                 )


@pytest.mark.parametrize(
    "method, formula, use_wind", [
        (['ISO'], [22], [True]),
        (['IsO'], ['hi'], [False]),
        (['ISO'], [False], [True]),
    ])
def test_pc_invalid_inputs__formula(fhw__2days_data, method, formula, use_wind):
    with pytest.raises(ValueError, match='is not a valid PCFormulae'):
        pc_wrapper.list_feedback(plant=fhw__2days_data,
                                 method=method,
                                 formula=formula,
                                 use_wind=use_wind,
                                 )


@pytest.mark.parametrize(
    "method, formula, use_wind", [
        (['ISO'], [2], [123]),
        (['ISO'], [2], ['test invalid']),
        (['ISO'], [2], ['True']),
    ])
def test_pc_invalid_inputs__wind(fhw__2days_data, method, formula, use_wind):
    with pytest.raises(TypeError, match='is not a valid bool'):
        pc_wrapper.list_feedback(plant=fhw__2days_data,
                                 method=method,
                                 formula=formula,
                                 use_wind=use_wind,
                                 )


@pytest.mark.skip('PC method profiling test')
def test_profiler_dummy(fhw__1month_data):
    pc_wrapper.run_performance_check(plant=fhw__1month_data, formula=[1])


## Test numeric results

@pytest.mark.parametrize('iso, formula, use_wind, expected', [
    ('ISO', 1, True,
     'datetime,tp_sp_measured,tp_sp_estimated,tp_sp_estimated_safety\r\n'
     '2017-05-01 10:00:00+01:00,224.3717,479.8609,431.8749\r\n'
     '2017-05-02 10:00:00+01:00,256.7811,540.4965,486.4469\r\n'
     ),
    ('ISO', 2, True,
     'datetime,tp_sp_measured,tp_sp_estimated,tp_sp_estimated_safety\r\n'
     '2017-05-02 10:00:00+01:00,256.7811,538.7691,484.8922\r\n'
     ),
    ('Extended', 1, True,
     'datetime,tp_sp_measured,tp_sp_estimated,tp_sp_estimated_safety\r\n'
     '2017-05-01 09:59:00+01:00,223.2987,479.6638,431.6974\r\n'
     '2017-05-01 11:15:00+01:00,197.0413,436.9318,393.2386\r\n'
     '2017-05-01 12:25:00+01:00,212.0191,448.1538,403.3385\r\n'
     '2017-05-02 08:36:00+01:00,190.6403,400.7302,360.6572\r\n'
     '2017-05-02 10:02:00+01:00,257.7010,548.8257,493.9431\r\n'
     '2017-05-02 11:06:00+01:00,301.5674,635.4084,571.8676\r\n'
     '2017-05-02 13:34:00+01:00,207.0383,433.9689,390.5720\r\n'
     ),
    ('Extended', 2, True,
     'datetime,tp_sp_measured,tp_sp_estimated,tp_sp_estimated_safety\r\n'
     '2017-05-02 08:36:00+01:00,190.6403,400.3949,360.3554\r\n'
     '2017-05-02 10:02:00+01:00,257.7010,542.4520,488.2068\r\n'
     '2017-05-02 11:06:00+01:00,301.5674,632.7685,569.4917\r\n'
     '2017-05-02 13:47:00+01:00,209.4557,438.9388,395.0449\r\n'
     ),
])
def test_pc_output_numerically(fhw__2days_data, iso, formula, use_wind, expected):
    out = pc_wrapper.run_performance_check(plant=fhw__2days_data,
                                           method=[iso],
                                           formula=[formula],
                                           use_wind=[use_wind],
                                           ).output.plant_output
    df = pd.DataFrame({
        # 'datetime': out.datetime_intervals_start,
        'tp_sp_measured': out._tp_measured_mag,
        'tp_sp_estimated': out._tp_sp_estimated_mag,
        'tp_sp_estimated_safety': out._tp_sp_estimated_safety_mag,
    }, index=out.datetime_intervals_start)

    df_expected = pd.read_csv(StringIO(expected), index_col=0, parse_dates=True)
    df_expected.index.name = None

    pd.testing.assert_frame_equal(df, df_expected, atol=1e-3, check_index_type=False)

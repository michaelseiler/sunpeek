import matplotlib.figure
import pytest
import os
import shutil
import datetime as dt
from pathlib import Path

from sunpeek.core_methods.pc_method.wrapper import run_performance_check
import sunpeek.common.plot_utils as pu
import sunpeek.core_methods.pc_method.plotting as pcp
from sunpeek.components import PCMethodOutput

# pytest.skip('Generate and save PC development plots, not required in CI/CD pipeline.', allow_module_level=True)

skip_individual_tests = True
skip_reason = 'Only testing overall pdf report generation, see test_plot_fhw__create_pdf_report__no_pc_creation().'


@pytest.fixture(scope='module', params=[
    # (method, formula, use_wind, safety_uncertainty, interval_length)
    ('ISO', 1, True, 0.9, dt.timedelta(minutes=60)),
    ('ISO', 2, True, 0.9, dt.timedelta(minutes=60)),
    ('extended', 1, True, 0.9, dt.timedelta(minutes=60)),
    ('Extended', 2, True, 0.9, dt.timedelta(minutes=60)),
])
def pc_output_fhw(request, fhw__1month_data) -> PCMethodOutput:
    # Create PC method output for the tests in this module
    method, formula, use_wind, safety_uncertainty, interval_length = request.param
    plant = fhw__1month_data

    pc_result = run_performance_check(plant=plant,
                                      method=[method],
                                      formula=[formula],
                                      use_wind=[use_wind],
                                      safety_pipes=1,
                                      safety_uncertainty=safety_uncertainty,
                                      safety_others=1,
                                      interval_length=interval_length,
                                      )
    return pc_result.output


@pytest.fixture(scope='module', params=[
    # (method, formula, use_wind, safety_uncertainty, interval_length)
    ('ISO', 3, True, 0.9, dt.timedelta(minutes=60)),
    ('Extended', 3, True, 0.9, dt.timedelta(minutes=60)),
])
def pc_output_fhw__like_concentrating(request, fhw__concentrating__1month_data) -> PCMethodOutput:
    # Create PC method output for the tests in this module
    method, formula, use_wind, safety_uncertainty, interval_length = request.param
    plant = fhw__concentrating__1month_data

    pc_result = run_performance_check(plant=plant,
                                      method=[method],
                                      formula=[formula],
                                      use_wind=[use_wind],
                                      safety_pipes=1,
                                      safety_uncertainty=safety_uncertainty,
                                      safety_others=1,
                                      interval_length=interval_length,
                                      )
    return pc_result.output


@pytest.fixture
def tmp_export_folder(tmp_path_factory):
    export_folder = tmp_path_factory.mktemp('pc_plots')
    yield export_folder
    shutil.rmtree(export_folder, ignore_errors=True)


# --------
# Fixtures to control several of the tests

# Central control over export folder and whether to open produced plot / pdf
@pytest.fixture
def export_folder(tmp_export_folder):
    return tmp_export_folder
    # return 'D:/Desktop/xx'


@pytest.fixture
def open_pdf():
    return False
    # return True


# @pytest.fixture(params=['print', 'presentation'])
@pytest.fixture(params=['print'])
def target(request):
    return request.param


# @pytest.fixture(params=[False, True])
@pytest.fixture(params=[False])
def anonymize(request):
    return request.param


## Test Create pdf  -------------

@pytest.mark.parametrize('with_interval_plots', [False])
# @pytest.mark.parametrize('with_interval_plots', [True])
# @pytest.mark.parametrize('with_interval_plots', [False, True])
def test_create_pdf__fhw(pc_output_fhw, with_interval_plots, anonymize, export_folder):
    settings = pu.PlotSettings(with_interval_plots=with_interval_plots, anonymize=anonymize)
    pc_output = pc_output_fhw
    full_fn = pcp.create_pdf_report(pc_output=pc_output,
                                    settings=settings,
                                    export_folder=export_folder,
                                    filename=None)
    assert Path(full_fn).is_file()

    expected_filename = (f'PC_report, {pc_output.plant.name}, '
                         f'{pc_output_fhw.evaluation_mode}, '
                         f'formula_{pc_output_fhw.formula}, '
                         f'wind_{"used" if pc_output_fhw.wind_used else "ignored"}'
                         f'{"" if not with_interval_plots else ", with_interval_plots"}.pdf')
    assert full_fn.name == expected_filename


@pytest.mark.parametrize('with_interval_plots', [False])
# @pytest.mark.parametrize('with_interval_plots', [True])
# @pytest.mark.parametrize('with_interval_plots', [False, True])
def test_create_pdf__fhw_like_concentrating(pc_output_fhw__like_concentrating, with_interval_plots, anonymize, export_folder):
    settings = pu.PlotSettings(with_interval_plots=with_interval_plots, anonymize=anonymize)
    pc_output = pc_output_fhw__like_concentrating
    full_fn = pcp.create_pdf_report(pc_output=pc_output,
                                    settings=settings,
                                    export_folder=export_folder,
                                    filename=None)
    assert Path(full_fn).is_file()

    expected_filename = (f'PC_report, {pc_output.plant.name}, '
                         f'{pc_output.evaluation_mode}, '
                         f'formula_{pc_output.formula}, '
                         f'wind_{"used" if pc_output.wind_used else "ignored"}'
                         f'{"" if not with_interval_plots else ", with_interval_plots"}.pdf')
    assert full_fn.name == expected_filename


# Individual tests -----------------------------------------------------------------------------

@pytest.mark.skipif(skip_individual_tests, reason=skip_reason)
@pytest.mark.parametrize('anonymize', [False])
# @pytest.mark.parametrize('with_interval_plots', [True])
@pytest.mark.parametrize('with_interval_plots', [False])
def test_plot_fhw__create_pdf_report(pc_output_fhw, target, anonymize, with_interval_plots, export_folder, open_pdf):
    # Goal: Create pdf report for a specific PC method evaluation.
    plot_settings = pu.PlotSettings(anonymize=anonymize, with_interval_plots=with_interval_plots)
    full_fn = pcp.create_pdf_report(pc_output=pc_output_fhw,
                                    settings=plot_settings,
                                    export_folder=export_folder)
    assert Path(full_fn).is_file()
    if open_pdf:
        os.startfile(full_fn)


# @pytest.mark.skipif(skip_individual_tests, reason=skip_reason)
@pytest.mark.parametrize('anonymize', [False])
# @pytest.mark.parametrize('with_interval_plots', [True])
@pytest.mark.parametrize('with_interval_plots', [False])
def test_plot_fhw_like_concentrating__create_pdf_report(pc_output_fhw__like_concentrating, target, anonymize, with_interval_plots, export_folder, open_pdf):
    # Goal: Create pdf report for a specific PC method evaluation.
    plot_settings = pu.PlotSettings(anonymize=anonymize, with_interval_plots=with_interval_plots)
    full_fn = pcp.create_pdf_report(pc_output=pc_output_fhw__like_concentrating,
                                    settings=plot_settings,
                                    export_folder=export_folder)
    assert Path(full_fn).is_file()
    if open_pdf:
        os.startfile(full_fn)


## Plot all ----------

@pytest.mark.skipif(skip_individual_tests, reason=skip_reason)
@pytest.mark.parametrize('with_interval_plots', [False, True])
# @pytest.mark.parametrize('with_interval_plots', [False])
def test_plot_fhw__all(pc_output_fhw, target, anonymize, with_interval_plots, export_folder):
    plot_settings = pu.PlotSettings(target=target, anonymize=anonymize, with_interval_plots=with_interval_plots)
    fig_list = pcp.plot_all(pc_output=pc_output_fhw, settings=plot_settings)

    assert isinstance(fig_list, list)
    assert all(isinstance(f, matplotlib.figure.Figure) for f in fig_list)


## Plot cover -------------

@pytest.mark.skipif(skip_individual_tests, reason=skip_reason)
@pytest.mark.parametrize('include_creation_date', [True])
def test_plot_fhw__cover(pc_output_fhw, target, anonymize, export_folder, include_creation_date, open_pdf):
    plot_settings = pu.PlotSettings(target=target, anonymize=anonymize, include_creation_date=include_creation_date)
    fig_list = pcp.plot_cover(pc_output=pc_output_fhw, settings=plot_settings)

    full_fn = pcp.create_pdf_report(pc_output=pc_output_fhw,
                                    settings=plot_settings,
                                    fig_list=fig_list,
                                    filename='cover',
                                    export_folder=export_folder)

    assert Path(full_fn).is_file()

    if open_pdf:
        os.startfile(full_fn)


## Plot bars -------------

@pytest.mark.skipif(skip_individual_tests, reason=skip_reason)
def test_plot_fhw__bars(pc_output_fhw, target, anonymize, export_folder, open_pdf):
    plot_settings = pu.PlotSettings(target=target, anonymize=anonymize)
    fig_list = pcp.plot_bars(pc_output=pc_output_fhw, settings=plot_settings)

    full_fn = pcp.create_pdf_report(pc_output=pc_output_fhw,
                                    settings=plot_settings,
                                    fig_list=fig_list,
                                    filename='bars',
                                    export_folder=export_folder)

    assert Path(full_fn).is_file()

    if open_pdf:
        os.startfile(full_fn)


## Shadow / interval plot -------------

@pytest.mark.skipif(skip_individual_tests, reason=skip_reason)
def test_plot_fhw__shadow_and_intervals(pc_output_fhw, target, anonymize, export_folder, open_pdf):
    plot_settings = pu.PlotSettings(target=target, anonymize=anonymize)
    fig_list = pcp.plot_shadow_and_intervals(pc_output=pc_output_fhw, settings=plot_settings)

    full_fn = pcp.create_pdf_report(pc_output=pc_output_fhw,
                                    settings=plot_settings,
                                    fig_list=fig_list,
                                    filename='shadow',
                                    export_folder=export_folder)

    assert Path(full_fn).is_file()

    if open_pdf:
        os.startfile(full_fn)


## Plot square -------------

@pytest.mark.skipif(skip_individual_tests, reason=skip_reason)
def test_plot_fhw__square(pc_output_fhw, target, anonymize, export_folder, open_pdf):
    plot_settings = pu.PlotSettings(target=target, anonymize=anonymize)
    fig_list = pcp.plot_square(pc_output=pc_output_fhw, settings=plot_settings)

    full_fn = pcp.create_pdf_report(pc_output=pc_output_fhw,
                                    settings=plot_settings,
                                    fig_list=fig_list,
                                    filename='square',
                                    export_folder=export_folder)

    assert Path(full_fn).is_file()

    if open_pdf:
        os.startfile(full_fn)


## Plot time -------------

@pytest.mark.skipif(skip_individual_tests, reason=skip_reason)
def test_plot_fhw__time(pc_output_fhw, target, anonymize, export_folder, open_pdf):
    plot_settings = pu.PlotSettings(target=target, anonymize=anonymize)
    fig_list = pcp.plot_time(pc_output=pc_output_fhw, settings=plot_settings)

    full_fn = pcp.create_pdf_report(pc_output=pc_output_fhw,
                                    settings=plot_settings,
                                    fig_list=fig_list,
                                    filename='time',
                                    export_folder=export_folder)

    assert Path(full_fn).is_file()

    if open_pdf:
        os.startfile(full_fn)


## Plot plant overview -------------

@pytest.mark.skipif(skip_individual_tests, reason=skip_reason)
def test_plot_fhw__plant_overview(pc_output_fhw, target, anonymize, export_folder, open_pdf):
    plot_settings = pu.PlotSettings(target=target, anonymize=anonymize)
    fig_list = pcp.plot_plant_overview(pc_output=pc_output_fhw, settings=plot_settings)

    full_fn = pcp.create_pdf_report(pc_output=pc_output_fhw,
                                    settings=plot_settings,
                                    fig_list=fig_list,
                                    filename='plant_overview',
                                    export_folder=export_folder)

    assert Path(full_fn).is_file()

    if open_pdf:
        os.startfile(full_fn)


## Plot collector overview -------------

@pytest.mark.skipif(skip_individual_tests, reason=skip_reason)
def test_plot_fhw__collector_overview(pc_output_fhw, target, anonymize, export_folder, open_pdf):
    plot_settings = pu.PlotSettings(target=target, anonymize=anonymize)
    fig_list = pcp.plot_collector_overview(pc_output=pc_output_fhw, settings=plot_settings)

    full_fn = pcp.create_pdf_report(pc_output=pc_output_fhw,
                                    settings=plot_settings,
                                    fig_list=fig_list,
                                    filename='collector_overview',
                                    export_folder=export_folder)

    assert Path(full_fn).is_file()

    if open_pdf:
        os.startfile(full_fn)


## Plot data overview -------------

@pytest.mark.skipif(skip_individual_tests, reason=skip_reason)
def test_plot_fhw__data_overview(pc_output_fhw, target, anonymize, export_folder, open_pdf):
    plot_settings = pu.PlotSettings(target=target, anonymize=anonymize)
    fig_list = pcp.plot_data_overview(pc_output=pc_output_fhw, settings=plot_settings)

    full_fn = pcp.create_pdf_report(pc_output=pc_output_fhw,
                                    settings=plot_settings,
                                    fig_list=fig_list,
                                    filename='data_overview',
                                    export_folder=export_folder)

    assert Path(full_fn).is_file()

    if open_pdf:
        os.startfile(full_fn)


## Plot symbols overview -------------

@pytest.mark.skipif(skip_individual_tests, reason=skip_reason)
def test_plot_fhw__symbols_overview(pc_output_fhw, target, anonymize, export_folder, open_pdf):
    plot_settings = pu.PlotSettings(target=target, anonymize=anonymize)
    fig_list = pcp.plot_symbols_overview(pc_output=pc_output_fhw, settings=plot_settings)

    full_fn = pcp.create_pdf_report(pc_output=pc_output_fhw,
                                    settings=plot_settings,
                                    fig_list=fig_list,
                                    filename='symbols_overview',
                                    export_folder=export_folder)

    assert Path(full_fn).is_file()

    if open_pdf:
        os.startfile(full_fn)


## Plot Interval ----------

@pytest.mark.skipif(skip_individual_tests, reason=skip_reason)
def test_plot_fhw__intervals(pc_output_fhw, target, anonymize, export_folder, open_pdf):
    plot_settings = pu.PlotSettings(target=target, anonymize=anonymize)
    fig_list = pcp.plot_intervals(pc_output=pc_output_fhw, settings=plot_settings)

    full_fn = pcp.create_pdf_report(pc_output=pc_output_fhw,
                                    settings=plot_settings,
                                    fig_list=fig_list,
                                    filename='intervals',
                                    export_folder=export_folder)

    assert Path(full_fn).is_file()

    if open_pdf:
        os.startfile(full_fn)


## Other plot tests ---------

@pytest.mark.skipif(skip_individual_tests, reason=skip_reason)
def test_plot_fhw__kwargs(pc_output_fhw, anonymize):
    # Test specifically the effect of the kwargs that can be passed to create_pdf_report().
    # For the rest, use default settings.
    pcp.create_pdf_report(pc_output_fhw, square_axis_range=[0, 600])
    pcp.create_pdf_report(pc_output_fhw, y_ratio_limits=[0, 2])
    pcp.create_pdf_report(pc_output_fhw, axes_limits_interval_plots=dict(
        te_max=150, rd_max=1500, tp_max=1500, vf_sp_max=40))

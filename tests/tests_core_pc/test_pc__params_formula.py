import pytest
import datetime as dt
import numpy as np
import pandas as pd

from sunpeek.common.unit_uncertainty import Q
import sunpeek.components.iam_methods as iam
from sunpeek.core_methods.pc_method.main import PCMethod
from sunpeek.core_methods.pc_method.wrapper import get_successful_strategy, get_feedback
from sunpeek.data_handling.wrapper import use_dataframe
from sunpeek.common.errors import PCMethodError
from sunpeek.core_methods.virtuals import config_virtuals
from sunpeek.serializable_models import ProblemType
from sunpeek.components import Plant, Array, Collector, CollectorTypes, CollectorQDT, iam_methods, Sensor


@pytest.fixture
def p() -> Plant:
    return Plant(latitude=Q(47, "deg"), longitude=Q(15, "deg"))


@pytest.fixture
def c_flat() -> Collector:
    return CollectorQDT(name='coll_flat', collector_type=CollectorTypes.flat_plate,
                        iam_method=iam_methods.IAM_K50(Q(0.9)),
                        test_reference_area='gross', area_gr=Q(1, 'm**2'), gross_length=Q(1, 'm'), kd=Q(0),
                        eta0b=Q(0.8), a1=Q(1, 'W m**-2 K**-1'), a2=Q(0.1, 'W m**-2 K**-2'), a5=Q(1, 'J m**-2 K**-1'))


@pytest.fixture
def c_concentrating() -> Collector:
    return CollectorQDT(name='coll_flat', collector_type=CollectorTypes.concentrating,
                        iam_method=iam_methods.IAM_K50(Q(0.9)),
                        test_reference_area='gross', area_gr=Q(1, 'm**2'), gross_length=Q(1, 'm'), kd=Q(0),
                        eta0b=Q(0.7), a1=Q(0, 'W m**-2 K**-1'), a2=Q(0, 'W m**-2 K**-2'), a5=Q(1, 'J m**-2 K**-1'),
                        a8=Q(1e-4, 'W m**-2 K**-4'))

@pytest.fixture
def a(p) -> Plant:
    s = Sensor('te_in_raw_name', 'K')
    a = Array(name='my_array', tilt=Q(30, 'deg'), plant=p)
    a.set_sensors(te_in=s)
    return a

# ==================================
# Tests for the formula parameter
# ==================================

@pytest.mark.parametrize('method, formula, expected_method, expected_formula', [
    (None, None, "PC Method 'ISO 24194'", 2),
    ('ISO', 1, "PC Method 'ISO 24194'", 1),
    ('iso', 1, "PC Method 'ISO 24194'", 1),
    ('Iso', 1, "PC Method 'ISO 24194'", 1),
    ('ISO', 2, "PC Method 'ISO 24194'", 2),
    ('ISO', None, "PC Method 'ISO 24194'", 2),
    ('Extended', 1, "PC Method 'ISO 24194' extended", 1),
    ('extended', 2, "PC Method 'ISO 24194' extended", 2),
    ('Extended', None, "PC Method 'ISO 24194' extended", 2),
    ('extended', None, "PC Method 'ISO 24194' extended", 2),
])
def test_formula12__strategy(plant_for_pc_with_example_data_virtuals,
                             method, formula, expected_method, expected_formula):
    p = plant_for_pc_with_example_data_virtuals
    strategy = get_successful_strategy(plant=p, method=[method], formula=[formula])

    assert strategy.pc.method_name == expected_method
    assert strategy.pc.formula.id == expected_formula


@pytest.mark.parametrize('method, formula, expected_method, expected_formula', [
    ('ISO', 3, "PC Method 'ISO 24194'", 3),
    ('IsO', None, "PC Method 'ISO 24194'", 3),
    ('extended', 3, "PC Method 'ISO 24194' extended", 3),
    ('extended', None, "PC Method 'ISO 24194' extended", 3),
])
def test_formula3__strategy(fhw_like__concentrating, c_concentrating,
                            method, formula, expected_method, expected_formula):
    strategy = get_successful_strategy(plant=fhw_like__concentrating, method=[method], formula=[formula])
    r = get_feedback(fhw_like__concentrating, method=[method], formula=[formula])

    assert strategy.pc.method_name == expected_method
    assert strategy.pc.formula.id == expected_formula


# Test problems are reported correctly -------------------------------


@pytest.mark.parametrize('coll_type', [CollectorTypes.concentrating, CollectorTypes.WISC])
@pytest.mark.parametrize('method', ['iso', 'extended'])
@pytest.mark.parametrize('use_wind', [True, False])
@pytest.mark.parametrize('formula', [1])
def test_formula1__wrong_collector_type(a, coll_type, method, use_wind, formula):
    coll = CollectorQDT(name='c', collector_type=coll_type, iam_method=iam_methods.IAM_K50(Q(0.9)),
                        test_reference_area='gross', area_gr=Q(1, 'm**2'), gross_length=Q(1, 'm'), kd=Q(0.9),
                        eta0b=Q(0.6), a1=Q(1, 'W m**-2 K**-1'), a2=Q(0.01, 'W m**-2 K**-2'),
                        a5=Q(1000, 'J m**-2 K**-1'))
    a.collector = coll
    r = get_feedback(a.plant, method=[method], use_wind=[use_wind], formula=[formula])

    assert not r.own_feedback
    assert len(r.sub_feedback) == 1

    sr = next(iter(r.sub_feedback.values()))
    assert not sr.success
    assert 1 == sum([ap.problem_type == ProblemType.collector_type for ap in sr.own_feedback])


@pytest.mark.parametrize('method', ['iso', 'extended'])
@pytest.mark.parametrize('use_wind', [True, False])
@pytest.mark.parametrize('formula', [1, 2])
def test_formula12__a1_zero(a, c_flat, method, use_wind, formula):
    c_flat.a1 = Q(0, 'W m**-2 K**-1')
    a.collector = c_flat
    r = get_feedback(a.plant, method=[method], use_wind=[use_wind], formula=[formula])

    assert not r.own_feedback
    assert len(r.sub_feedback) == 1

    sr = next(iter(r.sub_feedback.values()))
    assert not sr.success
    assert 1 == sum([ap.problem_type == ProblemType.collector_param and 'a1' in ap.description
                     for ap in sr.own_feedback])


@pytest.mark.parametrize('method', ['iso', 'extended'])
@pytest.mark.parametrize('use_wind', [True, False])
@pytest.mark.parametrize('formula', [1, 2])
def test_formula12__a8_nonzero(a, c_flat, method, use_wind, formula):
    c_flat.a8 = Q(1e-3, 'W m**-2 K**-4')
    a.collector = c_flat
    r = get_feedback(a.plant, method=[method], use_wind=[use_wind], formula=[formula])

    assert not r.own_feedback
    assert len(r.sub_feedback) == 1

    sr = next(iter(r.sub_feedback.values()))
    assert not sr.success
    assert 1 == sum([ap.problem_type == ProblemType.collector_param and 'a8' in ap.description
                     for ap in sr.own_feedback])


@pytest.mark.parametrize('coll_type', [CollectorTypes.WISC])
@pytest.mark.parametrize('method', ['iso', 'extended'])
@pytest.mark.parametrize('use_wind', [True, False])
@pytest.mark.parametrize('formula', [2])
def test_formula2__wrong_collector_type(a, coll_type, method, use_wind, formula):
    coll = CollectorQDT(name='c', collector_type=coll_type, iam_method=iam_methods.IAM_K50(Q(0.9)),
                        test_reference_area='gross', area_gr=Q(1, 'm**2'), gross_length=Q(1, 'm'), kd=Q(0.9),
                        eta0b=Q(0.6), a1=Q(1, 'W m**-2 K**-1'), a2=Q(0.01, 'W m**-2 K**-2'),
                        a5=Q(1000, 'J m**-2 K**-1'))
    a.collector = coll
    r = get_feedback(a.plant, method=[method], use_wind=[use_wind], formula=[formula])

    assert not r.own_feedback
    assert len(r.sub_feedback) == 1

    sr = next(iter(r.sub_feedback.values()))
    assert not sr.success
    assert 1 == sum([ap.problem_type == ProblemType.collector_type for ap in sr.own_feedback])


@pytest.mark.parametrize('coll_type', [CollectorTypes.flat_plate, CollectorTypes.WISC])
@pytest.mark.parametrize('method', ['iso', 'extended'])
@pytest.mark.parametrize('use_wind', [True, False])
@pytest.mark.parametrize('formula', [3])
def test_formula3__wrong_collector_type(a, coll_type, method, use_wind, formula):
    coll = CollectorQDT(name='c', collector_type=coll_type, iam_method=iam_methods.IAM_K50(Q(0.9)),
                        test_reference_area='gross', area_gr=Q(1, 'm**2'), gross_length=Q(1, 'm'), kd=Q(0.9),
                        eta0b=Q(0.6), a1=Q(1, 'W m**-2 K**-1'), a2=Q(0.01, 'W m**-2 K**-2'),
                        a5=Q(1000, 'J m**-2 K**-1'))
    a.collector = coll
    r = get_feedback(a.plant, method=[method], use_wind=[use_wind], formula=[formula])

    assert not r.own_feedback
    assert len(r.sub_feedback) == 1

    sr = next(iter(r.sub_feedback.values()))
    assert not sr.success
    assert 1 == sum([ap.problem_type == ProblemType.collector_type for ap in sr.own_feedback])


@pytest.mark.parametrize('method', ['iso', 'extended'])
@pytest.mark.parametrize('use_wind', [True, False])
@pytest.mark.parametrize('formula', [3])
def test_formula3__a8_zero(a, method, use_wind, formula):
    coll = CollectorQDT(name='coll_concentrating', collector_type=CollectorTypes.concentrating,
                        iam_method=iam_methods.IAM_K50(Q(0.9)),
                        test_reference_area='gross', area_gr=Q(1, 'm**2'), gross_length=Q(1, 'm'), kd=Q(0),
                        eta0b=Q(0), a1=Q(0, 'W m**-2 K**-1'), a2=Q(0, 'W m**-2 K**-2'), a5=Q(1, 'J m**-2 K**-1'))
    a.collector = coll
    r = get_feedback(a.plant, method=[method], use_wind=[use_wind], formula=[formula])

    assert not r.own_feedback
    assert len(r.sub_feedback) == 1

    sr = next(iter(r.sub_feedback.values()))
    assert not sr.success
    assert 1 == sum([ap.problem_type == ProblemType.collector_param and 'a8' in ap.description
                     for ap in sr.own_feedback])


@pytest.mark.parametrize('method', ['iso', 'extended'])
@pytest.mark.parametrize('use_wind', [True, False])
@pytest.mark.parametrize('formula', [2])
def test_formula2__CR_too_large(a, method, use_wind, formula):
    coll = CollectorQDT(name='coll_concentrating', collector_type=CollectorTypes.concentrating,
                        iam_method=iam_methods.IAM_K50(Q(0.9)),
                        test_reference_area='gross', area_gr=Q(1, 'm**2'), gross_length=Q(1, 'm'), kd=Q(0.2),
                        eta0b=Q(0.7), a1=Q(1, 'W m**-2 K**-1'), a2=Q(0, 'W m**-2 K**-2'), a5=Q(1000, 'J m**-2 K**-1'),
                        a8=Q(5e-9, 'W m**-2 K**-4'), concentration_ratio=Q(21))
    a.collector = coll
    r = get_feedback(a.plant, method=[method], use_wind=[use_wind], formula=[formula])

    assert not r.own_feedback
    assert len(r.sub_feedback) == 1

    sr = next(iter(r.sub_feedback.values()))
    assert not sr.success
    assert 1 == sum([ap.problem_type == ProblemType.component_slot and 'concentration_ratio' in ap.description
                     for ap in sr.own_feedback])


@pytest.mark.parametrize('method', ['iso', 'extended'])
@pytest.mark.parametrize('use_wind', [True, False])
@pytest.mark.parametrize('formula', [3])
def test_formula3__CR_too_small(a, method, use_wind, formula):
    coll = CollectorQDT(name='coll_concentrating', collector_type=CollectorTypes.concentrating,
                        iam_method=iam_methods.IAM_K50(Q(0.9)),
                        test_reference_area='gross', area_gr=Q(1, 'm**2'), gross_length=Q(1, 'm'), kd=Q(0.2),
                        eta0b=Q(0.7), a1=Q(1, 'W m**-2 K**-1'), a2=Q(0, 'W m**-2 K**-2'), a5=Q(1000, 'J m**-2 K**-1'),
                        a8=Q(5e-9, 'W m**-2 K**-4'), concentration_ratio=Q(19))
    a.collector = coll
    r = get_feedback(a.plant, method=[method], use_wind=[use_wind], formula=[formula])

    assert not r.own_feedback
    assert len(r.sub_feedback) == 1

    sr = next(iter(r.sub_feedback.values()))
    assert not sr.success
    assert 1 == sum([ap.problem_type == ProblemType.component_slot and 'concentration_ratio' in ap.description
                     for ap in sr.own_feedback])


def test_noformula__altered_plant(plant_for_pc_with_example_data_virtuals):
    # This test sets virtual sensor data to None, so plant is different afterwards
    p = plant_for_pc_with_example_data_virtuals
    for array in p.arrays:
        array.in_beam = None
        array.in_diffuse = None
    config_virtuals(p)

    strategy = get_successful_strategy(plant=p, method=['ISO'])
    expected_formula = 1

    assert strategy.pc.formula.id == expected_formula


def test_formula_only_plant_fails_for_wrong_input(plant_for_pc_with_example_data):
    wrong_input = ["heya", 7, 20, 0.1, dt.datetime(2020, 1, 1), "eins", "Eq1", "eq2"]
    for formula in wrong_input:
        with pytest.raises(ValueError, match='is not a valid PCFormulae'):
            PCMethod.create(method='ISO',
                            plant=plant_for_pc_with_example_data,
                            formula=formula)


# ==================================
# Tests for the formula parameter
# ==================================

@pytest.fixture
def plant_rdglobal():
    def dummy_plant(collector):
        plant = Plant(
            latitude=Q(1, "deg"),
            longitude=Q(1, "deg"),
        )
        plant.set_sensors(
            tp=Sensor('tp', 'W'),
            te_amb=Sensor('te_amb', 'degC')
        )
        array = Array(
            plant=plant,
            tilt=Q(0, "deg"),
            azim=Q(0, "deg"),
            collector=collector,
            area_gr=Q(1, "m²"),
            row_spacing=Q(3, 'm')
        )
        array.set_sensors(
            te_in=Sensor('te_in', 'degC'),
            te_out=Sensor('te_out', 'degC'),
            in_global=Sensor('rd_gti', 'W m**-2', info={'azim': array.azim, 'tilt': array.tilt}),
        )
        return plant

    return dummy_plant


@pytest.fixture
def plant_rdbeamdiffuse():
    def dummy_plant(collector):
        plant = Plant(
            latitude=Q(1, "deg"),
            longitude=Q(1, "deg"),
        )
        plant.set_sensors(
            tp=Sensor('tp', 'W'),
            te_amb=Sensor('te_amb', 'degC')
        )
        array = Array(
            plant=plant,
            tilt=Q(0, "deg"),
            azim=Q(0, "deg"),
            collector=collector,
            area_gr=Q(1, "m²"),
            row_spacing=Q(3, 'm')
        )
        array.set_sensors(
            te_in=Sensor('te_in', 'degC'),
            te_out=Sensor('te_out', 'degC'),
            in_global=Sensor('rd_gti', 'W m**-2', info={'azim': array.azim, 'tilt': array.tilt}),
            in_beam=Sensor('rd_bti', 'W m**-2', info={'azim': array.azim, 'tilt': array.tilt}),
            in_diffuse=Sensor('rd_dti', 'W m**-2', info={'azim': array.azim, 'tilt': array.tilt}),
        )
        return plant

    return dummy_plant


@pytest.fixture
def collector_qdt():
    return Collector(gross_length=Q(2.3, 'm'),
                     test_type='QDT',
                     test_reference_area="gross",
                     area_gr=Q(5, 'm**2'),
                     a1=Q(2.3, 'W m**-2 K**-1'),
                     a2=Q(0.01, 'W m**-2 K**-2'),
                     a5=Q(7500, 'J m**-2 K**-1'),
                     a8=Q(0, 'W m**-2 K**-4'),
                     eta0b=Q(0.81, ''),
                     kd=Q(0.93, ''),
                     iam_method=iam.IAM_ASHRAE(b=Q(0.15)),
                     collector_type=CollectorTypes.flat_plate.value,
                     )


@pytest.fixture
def collector_sst():
    return Collector(
        test_reference_area="gross",
        test_type='SST',
        a1=Q(1, "W m**-2 K**-1"),
        a2=Q(1, "W m**-2 K**-2"),
        a5=Q(1, "kJ m**-2 K**-1"),
        a8=Q(0, "W m**-2 K**-4"),
        eta0hem=Q(0.72),
        gross_length=Q(1, "m"),
        area_gr=Q(3, "m**2"),
        kd=Q(0.9),
        iam_method=iam.IAM_K50(k50=Q(0.98)),
        collector_type=CollectorTypes.flat_plate.value,
    )


@pytest.fixture
def collector_sst__nokd():
    return Collector(
        test_reference_area="gross",
        area_gr=Q(3, "m**2"),
        test_type='SST',
        a1=Q(1, "W m**-2 K**-1"),
        a2=Q(1, "W m**-2 K**-2"),
        a5=Q(1, "kJ m**-2 K**-1"),
        a8=Q(0, "W m**-2 K**-4"),
        eta0hem=Q(0.72),
        gross_length=Q(1, "m"),
        iam_method=iam.IAM_K50(k50=Q(0.98)),
        collector_type=CollectorTypes.flat_plate.value,
    )


@pytest.fixture(scope='module')
def dummy_idx():
    return pd.date_range(start='2022-09-12 11:16+01', end='2022-09-13 21:16+01')


@pytest.fixture
def dummy_data(dummy_idx):
    columns = ['tp', 'te_amb', 'te_in', 'te_out', 'rd_gti', 'rd_bti', 'rd_dti']
    df = pd.DataFrame(data=np.random.randn(len(dummy_idx), len(columns)), columns=columns, index=dummy_idx)
    # use_dataframe(plant, df, calculate_virtuals=False)

    return df


@pytest.mark.parametrize('collector', ['collector_qdt'])
def test_formula2_if_satisified(dummy_idx, collector, plant_rdbeamdiffuse, dummy_data, request):
    collector = request.getfixturevalue(collector)
    p = plant_rdbeamdiffuse(collector)
    use_dataframe(p, dummy_data, calculate_virtuals=False)

    strategy = get_successful_strategy(plant=p, method=['ISO'], use_wind=[False], formula=[2])
    expected_formula = 2

    assert strategy.pc.formula.id == expected_formula


@pytest.mark.parametrize('collector', ['collector_sst'])
def test_formula2_if_calculatable(dummy_idx, collector, plant_rdbeamdiffuse, dummy_data, request):
    collector = request.getfixturevalue(collector)
    p = plant_rdbeamdiffuse(collector)
    use_dataframe(p, dummy_data, calculate_virtuals=False)

    strategy = get_successful_strategy(plant=p, method=['ISO'], use_wind=[False])
    expected_formula = 2

    assert strategy.pc.formula.id == expected_formula


@pytest.mark.parametrize('collector', ['collector_sst__nokd'])
def test_formula1_if_kd_not_set(dummy_idx, collector, plant_rdbeamdiffuse, dummy_data, request):
    collector = request.getfixturevalue(collector)
    p = plant_rdbeamdiffuse(collector)
    use_dataframe(p, dummy_data, calculate_virtuals=False)

    strategy = get_successful_strategy(plant=p, method=['ISO'], use_wind=[False])
    # Since Kd can be calculated based on the given parameter eta0b (see types._set_collector_parameters),
    # expected formula id is now 2:
    expected_formula = 2

    assert strategy.pc.formula.id == expected_formula


@pytest.mark.parametrize('collector', ['collector_sst'])
def test_formula1_if_radiation_not_set(dummy_idx, collector, plant_rdglobal, dummy_data, request):
    collector = request.getfixturevalue(collector)
    p = plant_rdglobal(collector)
    use_dataframe(p, dummy_data, calculate_virtuals=False)

    strategy = get_successful_strategy(plant=p, method=['ISO'], use_wind=[False])
    expected_formula = 1

    assert strategy.pc.formula.id == expected_formula

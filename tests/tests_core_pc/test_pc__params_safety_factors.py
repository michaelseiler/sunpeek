import pytest
import numpy as np

from sunpeek.core_methods.pc_method.main import PCMethod, PCAccuracyClasses, PCFormulae
import sunpeek.core_methods.pc_method.main as main
from sunpeek.common.errors import PCMethodError
import sunpeek.core_methods.virtuals as virtuals


# Tests for the safety factor parameters

@pytest.mark.parametrize("method", ['ISO', 'Extended'])
@pytest.mark.parametrize("formula", [1, 2])
@pytest.mark.parametrize("use_wind", [True, False])
def test_safety_factor_rounds_correctly(plant_for_pc, method, formula, use_wind):
    # Example taken from ISO 24194 page 12
    f_pipe = 0.97
    f_uncertainty = 0.95
    f_others = 0.95
    pc = PCMethod.create(method=method, plant=plant_for_pc,
                         formula=formula, use_wind=use_wind,
                         safety_pipes=f_pipe,
                         safety_uncertainty=f_uncertainty,
                         safety_others=f_others)

    f_combined_expected = np.round(f_pipe * f_uncertainty * f_others, 2)
    assert pc.settings.safety_combined == f_combined_expected


@pytest.mark.parametrize("method", ['ISO', 'Extended'])
@pytest.mark.parametrize("formula", [1, 2])
@pytest.mark.parametrize("use_wind", [True, False])
@pytest.mark.parametrize("safety", [0, -1, 2])
def test_invalid_safety_factor(plant_for_pc, method, formula, use_wind, safety):
    expected_match = 'floats between 0 and 1'
    with pytest.raises(PCMethodError, match=expected_match):
        PCMethod.create(method=method, plant=plant_for_pc,
                        formula=formula, use_wind=use_wind,
                        safety_pipes=safety)
    with pytest.raises(PCMethodError, match=expected_match):
        PCMethod.create(method=method, plant=plant_for_pc,
                        formula=formula, use_wind=use_wind,
                        safety_uncertainty=safety)
    with pytest.raises(PCMethodError, match=expected_match):
        PCMethod.create(method=method, plant=plant_for_pc,
                        formula=formula, use_wind=use_wind,
                        safety_others=safety)


@pytest.mark.parametrize("method", ['ISO', 'extended'])
@pytest.mark.parametrize("formula", [1, 2])
@pytest.mark.parametrize("use_wind", [True, False])
def test_calculate_safety_factor_individual_params(plant_for_pc_with_example_data_virtuals, method, formula, use_wind):
    f_pipes = 0.9
    f_others = 0.1
    f_uncertainty = 0.5
    f_combined_expected = np.round(0.9 * 0.1 * 0.5, 2)

    pc = PCMethod.create(method=method, plant=plant_for_pc_with_example_data_virtuals,
                         formula=formula, use_wind=use_wind,
                         safety_pipes=f_pipes,
                         safety_uncertainty=f_uncertainty,
                         safety_others=f_others)

    assert pc.settings.safety_combined == pytest.approx(f_combined_expected)
    assert pc.settings.safety_pipes == pytest.approx(f_pipes)
    assert pc.settings.safety_others == pytest.approx(f_others)
    assert pc.settings.safety_uncertainty == pytest.approx(f_uncertainty)


@pytest.mark.parametrize("method", ['ISO', 'Extended'])
@pytest.mark.parametrize("formula", [1, 2])
@pytest.mark.parametrize("use_wind", [True, False])
def test_calculate_safety_factor_default_uncertaintyfactor(plant_for_pc_with_example_data_virtuals, method, formula,
                                                           use_wind):
    f_pipes = 0.9
    f_others = 0.1
    f_uncertainty = None
    f_combined_expected = np.round(0.9 * 0.1 * main.F_UNCERTAINTY, 2)

    pc = PCMethod.create(method=method, plant=plant_for_pc_with_example_data_virtuals,
                         formula=formula, use_wind=use_wind,
                         safety_pipes=f_pipes,
                         safety_uncertainty=f_uncertainty,
                         safety_others=f_others)

    assert pc.settings.safety_combined == pytest.approx(f_combined_expected)
    assert pc.settings.safety_pipes == pytest.approx(f_pipes)
    assert pc.settings.safety_others == pytest.approx(f_others)
    assert pc.settings.safety_uncertainty == pytest.approx(main.F_UNCERTAINTY)


@pytest.mark.parametrize("method", ['ISO', 'Extended'])
@pytest.mark.parametrize("use_wind", [True, False])
def test_calculate_safety_factor_others_eq1(plant_for_pc_with_example_data, method, use_wind):
    formula = PCFormulae.one

    f_pipes = 0.9
    f_others = None
    f_uncertainty = 0.9
    f_combined_expected = np.round(0.9 * main.F_OTHERS * 0.9, 2)

    virtuals.calculate_virtuals(plant_for_pc_with_example_data)
    pc = PCMethod.create(method=method, plant=plant_for_pc_with_example_data,
                         formula=formula, use_wind=use_wind,
                         safety_pipes=f_pipes,
                         safety_uncertainty=f_uncertainty,
                         safety_others=f_others)

    assert pc.settings.safety_combined == pytest.approx(f_combined_expected)
    assert pc.settings.safety_pipes == pytest.approx(f_pipes)
    assert pc.settings.safety_others == pytest.approx(main.F_OTHERS)
    assert pc.settings.safety_uncertainty == pytest.approx(f_uncertainty)


@pytest.mark.parametrize("method", ['ISO', 'Extended'])
@pytest.mark.parametrize("use_wind", [True, False])
def test_calculate_safety_factor_others_eq2(plant_for_pc_with_example_data, method, use_wind):
    formula = PCFormulae.one

    f_pipes = 0.9
    f_others = None
    f_uncertainty = 0.9
    f_combined_expected = np.round(0.9 * main.F_OTHERS * 0.9, 2)

    virtuals.calculate_virtuals(plant_for_pc_with_example_data)
    pc = PCMethod.create(method=method, plant=plant_for_pc_with_example_data,
                         formula=formula, use_wind=use_wind,
                         safety_pipes=f_pipes,
                         safety_uncertainty=f_uncertainty,
                         safety_others=f_others)

    assert pc.settings.safety_combined == pytest.approx(f_combined_expected)
    assert pc.settings.safety_pipes == pytest.approx(f_pipes)
    assert pc.settings.safety_others == pytest.approx(main.F_OTHERS)
    assert pc.settings.safety_uncertainty == pytest.approx(f_uncertainty)


@pytest.mark.parametrize("method", ['ISO', 'Extended'])
@pytest.mark.parametrize("formula", [1, 2])
@pytest.mark.parametrize("use_wind", [True, False])
def test_calculate_safety_factor_default_pipes(plant_for_pc_with_example_data_virtuals, method, formula, use_wind):
    f_pipes = None
    f_others = 0.1
    f_uncertainty = 0.9
    f_combined_expected = np.round(main.F_PIPES * 0.1 * 0.90, 2)

    pc = PCMethod.create(method=method, plant=plant_for_pc_with_example_data_virtuals,
                         formula=formula, use_wind=use_wind,
                         safety_pipes=f_pipes,
                         safety_uncertainty=f_uncertainty,
                         safety_others=f_others)

    assert pc.settings.safety_combined == pytest.approx(f_combined_expected)
    assert pc.settings.safety_pipes == pytest.approx(main.F_PIPES)
    assert pc.settings.safety_others == pytest.approx(f_others)
    assert pc.settings.safety_uncertainty == pytest.approx(f_uncertainty)

import pytest
import os
import json
import pvlib

from sunpeek.common.unit_uncertainty import Q
from sunpeek.common import config_parser
from sunpeek.common.errors import CollectorDefinitionError
import sunpeek.definitions.collectors
import sunpeek.components.iam_methods as iam
from sunpeek.components import Collector, CollectorTypes
from sunpeek.components.types import calculate_eta0hem, calculate_eta0b, calculate_kd_Hess_and_Hanby


@pytest.fixture
def test_coll(request):
    # Return collector definition from collectors.py, given collector name
    coll_name = request.param
    return sunpeek.definitions.collectors.get_definition(name=coll_name)


def test_all_collectors():
    all_collectors = sunpeek.definitions.collectors.all_definitions

    assert isinstance(all_collectors, list)
    assert all([isinstance(c, Collector) for c in all_collectors])
    assert len(all_collectors) >= 10


def test_error_quantity():
    with pytest.raises(TypeError):
        Collector(name='Arcon',
                  test_reference_area='gross',
                  test_type='QDT', area_gr=Q(5, 'm**2'),
                  a1=2.3, a2=0.01, a5=7500, a8=0, eta0b=0.81, kd=0.93,
                  gross_length=Q(1, 'm'),
                  iam_method=iam.IAM_ASHRAE(b=Q(0.15)),
                  collector_type=CollectorTypes.flat_plate.value,
                  )


def test_area_gr_none():
    with pytest.raises(CollectorDefinitionError, match='Collector parameter is None, but must be specified: "area_gr"'):
        Collector(name="test_gr_none",
                  test_reference_area='aperture',
                  test_type='QDT',
                  a1=Q(2.27, 'W m**-2 K**-1'),
                  a2=Q(0.018, 'W m**-2 K**-2'),
                  a5=Q(5980, 'J m**-2 K**-1'),
                  a8=Q(0, 'W m**-2 K**-4'),
                  kd=Q(0.92),
                  eta0b=Q(0.78),
                  gross_length=Q(1, 'm'),
                  iam_method=iam.IAM_K50(k50=Q(0.97)),
                  collector_type=CollectorTypes.flat_plate.value,
                  )


def test_reference_area_ap():
    with pytest.raises(CollectorDefinitionError, match='both gross and aperture collector areas must be given'):
        Collector(name="test_area_ap",
                  test_reference_area='aperture',
                  test_type='QDT',
                  area_gr=Q(5, 'm**2'),
                  a1=Q(2.27, 'W m**-2 K**-1'),
                  a2=Q(0.018, 'W m**-2 K**-2'),
                  a5=Q(5980, 'J m**-2 K**-1'),
                  a8=Q(0, 'W m**-2 K**-4'),
                  kd=Q(0.92),
                  eta0b=Q(0.78),
                  gross_length=Q(1, 'm'),
                  iam_method=iam.IAM_K50(k50=Q(0.97)),
                  collector_type=CollectorTypes.flat_plate.value,
                  )


def test_area_gr_smaller_than_ap():
    with pytest.raises(CollectorDefinitionError,
                       match='must be smaller than gross area'):
        Collector(name="test_gr_smaller",
                  test_reference_area='aperture',
                  test_type='QDT',
                  area_gr=Q(3, 'm**2'),
                  area_ap=Q(4, 'm**2'),
                  a1=Q(2.27, 'W m**-2 K**-1'),
                  a2=Q(0.018, 'W m**-2 K**-2'),
                  a5=Q(5980, 'J m**-2 K**-1'),
                  a8=Q(0, 'W m**-2 K**-4'),
                  kd=Q(0.92),
                  eta0b=Q(0.78),
                  gross_length=Q(1, 'm'),
                  iam_method=iam.IAM_K50(k50=Q(0.97)),
                  collector_type=CollectorTypes.flat_plate.value,
                  )


def test_area_gr_ap_ok():
    # Correctly defined
    Collector(name="test_gr_ap_ok",
              test_reference_area='aperture',
              test_type='QDT',
              area_gr=Q(5, 'm**2'),
              area_ap=Q(4, 'm**2'),
              eta0b=Q(0.9),
              a1=Q(2.27, 'W m**-2 K**-1'),
              a2=Q(0.018, 'W m**-2 K**-2'),
              a5=Q(5980, 'J m**-2 K**-1'),
              a8=Q(0, 'W m**-2 K**-4'),
              kd=Q(0.92),
              gross_length=Q(1, 'm'),
              iam_method=iam.IAM_K50(k50=Q(0.97)),
              collector_type=CollectorTypes.flat_plate.value,
              )


def test_collector_setup():
    # from :https://www.dincertco.tuv.com/search?locale=de&q=Sonnenkollektoren
    Collector(
        name="Savo SF500-15DG",
        test_type='SST',
        test_reference_area="gross",
        area_gr=Q(15.96, 'm**2'),
        eta0hem=Q(0.793, ''),
        a1=Q(2.520, 'W m**-2 K**-1'),
        a2=Q(0.004, 'W m**-2 K**-2'),
        a5=Q(12.0, "kJ m**-2 K**-1"),
        a8=Q(0, 'W m**-2 K**-4'),
        licence_number="011-7S2689 F",
        gross_length=Q(2591, "mm"),
        iam_method=iam.IAM_K50(k50=Q(0.96)),
        collector_type=CollectorTypes.flat_plate.value,
    )

    Collector(
        eta0b=Q(0.782, ''),
        a1=Q(2.27, 'W m**-2 K**-1'),
        a2=Q(0.018, 'W m**-2 K**-2'),
        a5=Q(5980, 'J m**-2 K**-1'),
        a8=Q(0, 'W m**-2 K**-4'),
        kd=Q(0.92, ''),
        licence_number="011-7S2819 F",
        manufacturer_name="GREENoneTec",
        name="GK HT 13,6",
        test_type="QDT",
        test_reference_area="gross",
        gross_length=Q(2280, "mm"),
        iam_method=iam.IAM_K50(k50=Q(0.97)),
        collector_type=CollectorTypes.flat_plate.value,
        area_gr=Q(15.96, 'm**2'),
    )


def test_collector_creation_from_json(resources_dir):
    # Test if collector definition from json file works
    with open(os.path.join(resources_dir, 'plant_config_FHW_Arcon_South_with_Collector.json')) as f:
        conf = json.load(f)
    plant = config_parser.make_full_plant(conf)

    assert plant.arrays[0].collector.name == 'Arcon HT-SA 35/10'


## Test SST <--> QDT Parameter and Aperture Conversion -----------------------------------------------------------------


def test_calculate_eta0hem__like_kbbgigaplus():
    # based on K5 Giga+ values which states both the eta0b and the eta0hem
    eta0b = 0.752
    kd = 0.964
    eta0hem = 0.748
    eta0hem_estimated, _info = calculate_eta0hem(eta0b=eta0b, kd=kd)

    assert eta0hem_estimated == pytest.approx(eta0hem, 0.001)


def test_calculate_eta0b__like_kbbgigaplus():
    # based on K5 Giga+ values which states both the eta0b and the eta0hem
    eta0b = 0.752
    kd = 0.964
    eta0hem = 0.748
    eta0b_estimated, _info = calculate_eta0b(eta0hem=eta0hem, kd=kd)

    assert eta0b_estimated == pytest.approx(eta0b, 0.001)


# Test Eta0hem and eta0b Calculation ----------------------------------------------------------------------------------

def test_calculate_eta__SST():
    eta0b = Q(0.752)
    kd = Q(0.964)
    eta0hem = Q(0.748)

    # irrelevant for test and arbitrarily chosen:
    gross_length = Q(0.1, "m")
    k50 = Q(0.94)
    a1 = Q(2.3, "W/m²/K")
    a2 = Q(0.004, "W/m²/K²")
    a5 = Q(10, "kJ/m²/K")
    a8 = Q(0, "W/m²/K**4")

    coll = Collector(eta0hem=eta0hem, test_type="SST", test_reference_area="gross", kd=kd,
                     gross_length=gross_length, area_gr=Q(3, 'm**2'),
                     a1=a1, a2=a2, a5=a5, a8=a8,
                     iam_method=iam.IAM_K50(k50=k50),
                     collector_type=CollectorTypes.flat_plate.value
                     )
    assert eta0b == pytest.approx(coll.eta0b, 0.001)


def test_calculate_eta0hem__QDT():
    eta0b = Q(0.752)
    kd = Q(0.964)
    eta0hem = Q(0.748)

    # irrelevant for test and arbitrarily chosen:
    gross_length = Q(0.1, "m")
    k50 = Q(0.94)
    a1 = Q(2.3, "W/m²/K")
    a2 = Q(0.004, "W/m²/K²")
    a5 = Q(10, "kJ/m²/K")
    a8 = Q(0, 'W m**-2 K**-4')

    coll = Collector(eta0b=eta0b, test_type="QDT", test_reference_area="gross", kd=kd,
                     gross_length=gross_length, area_gr=Q(3, 'm**2'),
                     a1=a1, a2=a2, a5=a5, a8=a8,
                     iam_method=iam.IAM_K50(k50=k50),
                     collector_type=CollectorTypes.flat_plate.value
                     )
    assert eta0hem == pytest.approx(coll.eta0hem, 0.001)


@pytest.mark.parametrize('test_coll', ['CONA CCS+'], indirect=True)
def test_calculate_eta0b__CONA(test_coll):
    # Test aperture to gross conversion
    cf = 1.92 / 2.05  # conversion factor aperture / gross
    assert test_coll.a1.m == pytest.approx(11.247 * cf)
    assert test_coll.a2.m == pytest.approx(0)
    assert test_coll.a5.m == pytest.approx(16.2 * cf)
    assert test_coll.a8 is None

    # Not converted parameters
    assert test_coll.eta0hem.m == pytest.approx(0.772)
    # Calculated parameters
    assert test_coll.kd.m == calculate_kd_Hess_and_Hanby(iam_method=test_coll.iam_method)[0].m
    assert test_coll.eta0b == calculate_eta0b(eta0hem=test_coll.eta0hem, kd=test_coll.kd)[0].m

    # calculation_info
    expected = 'Converted from aperture to gross area by SunPeek.'
    for k in ['a1', 'a2', 'a5']:
        assert k in test_coll.calculation_info
        assert test_coll.calculation_info[k] == expected

    expected = ('Collector parameter "eta0b" (beam peak collector efficiency) is calculated '
                'by SunPeek based on "eta0hem" and "Kd" (diffuse incidence angle modifier), '
                'using the formula: eta0b = eta0hem / (0.85 + 0.15 * Kd). ')
    assert test_coll.calculation_info['eta0b'] == expected


@pytest.mark.parametrize('test_coll', ['Arcon 3510'], indirect=True)
def test_calculate_eta0hem__Arcon(test_coll):
    # Assert parameters None / not None
    for k in ['a1', 'a2', 'a5', 'eta0b', 'eta0hem', 'kd', 'f_prime']:
        assert test_coll.__getattribute__(k) is not None
    for k in ['a8', 'concentration_ratio']:
        assert test_coll.__getattribute__(k) is None

    # Calculated parameters
    assert test_coll.eta0b.m == pytest.approx(0.745)
    assert test_coll.eta0hem == calculate_eta0hem(eta0b=test_coll.eta0b, kd=test_coll.kd)[0].m

    # calculation_info
    expected = ('Collector parameter "eta0hem" (hemispherical peak collector efficiency) is calculated by SunPeek '
                'based on "eta0b" and "Kd" (diffuse incidence angle modifier), '
                'using the formula: eta0hem = eta0b * (0.85 + 0.15 * Kd). ')
    assert test_coll.calculation_info['eta0hem'] == expected


# Test Kd diffuse IAM Calculation -------------------------------------------------------------------------------------


def test_calculate_kd__like_k5giga():
    # using values of K5Giga (011-7S2438 F) in comparison to pvlib
    real_kd = 0.970
    theta_array = [10, 20, 30, 40, 50, 60, 70, 80, 90]
    kb = [1.00, 1.00, 0.99, 0.98, 0.96, 0.87, 0.66, 0.33, 0.]

    # comparison with pvlib 1D integration method
    def iam_function(aoi):
        pvlib_iam = pvlib.iam.interp(aoi, theta_ref=theta_array, iam_ref=kb)
        return pvlib_iam

    pvlib_result = pvlib.iam.marion_integrate(iam_function, surface_tilt=0, region="sky")

    assert real_kd == pytest.approx(pvlib_result, 0.15)


def test_calculate_kd__like_GK3133():
    # using values of GK3133 (011-7S2565 F) in comparison to pvlib
    theta_array = [10, 20, 30, 40, 50, 60, 70, 80, 90]
    kb = [1., 0.99, 0.98, 0.96, 0.91, 0.82, 0.53, 0.27, 0.]
    real_kd = 0.931

    # comparison with pvlib 1D integration method
    def iam_function(aoi):
        pvlib_iam = pvlib.iam.interp(aoi, theta_ref=theta_array, iam_ref=kb)
        return pvlib_iam

    pvlib_result = pvlib.iam.marion_integrate(iam_function, surface_tilt=0, region="sky")

    assert real_kd == pytest.approx(pvlib_result, 0.15)


def test_calculate_kd__like_Ensol():
    # using values of K5Giga (011-7S2438 F) in comparison to pvlib
    real_kd = 0.91
    theta_array = [10, 20, 30, 40, 50, 60, 70, 80, 90]
    kb = [1.00, 0.99, 0.98, 0.95, 0.91, 0.84, 0.70, 0.35, 0.0]

    # comparison with pvlib 1D integration method
    def iam_function(aoi):
        pvlib_iam = pvlib.iam.interp(aoi, theta_ref=theta_array, iam_ref=kb)
        return pvlib_iam

    pvlib_result = pvlib.iam.marion_integrate(iam_function, surface_tilt=0, region="sky")

    assert real_kd == pytest.approx(pvlib_result, 0.15)


@pytest.mark.parametrize('test_coll', ['Greenonetec HT 13,6'], indirect=True)
def test_calculate_kd__kd_given(test_coll):
    # Kd given in Solar Keymark data sheet: see if given and calculated values match approximately
    kd_calc, info = calculate_kd_Hess_and_Hanby(test_coll.iam_method)

    assert test_coll.kd == pytest.approx(kd_calc, 0.1)
    # Make sure we use the data sheet value, not the calculated one
    assert 'kd' not in test_coll.calculation_info


@pytest.mark.parametrize('test_coll', ['Savo SF500-15DG'], indirect=True)
def test_calculate_kd__kd_unknown(test_coll):
    # Kd not given in Solar Keymark data sheet: see if calculation works
    kd_calc, info = calculate_kd_Hess_and_Hanby(test_coll.iam_method)

    expected_info = (
        'Collector parameter "Kd" (incidence angle modifier for diffuse radiation) is calculated by SunPeek '
        'based on integration of the values of "eta0b" (beam IAM) over the hemispherical plane '
        '(Hess & Hanby method), as described in doi: 10.1016/j.egypro.2014.02.011')
    assert info == expected_info
    assert 0.9 <= kd_calc.m <= 1.0

    # Make sure we use the right thing for the collector
    assert test_coll.kd == kd_calc
    assert 'kd' in test_coll.calculation_info


@pytest.mark.parametrize('test_coll', ['Savo SF500-15DG'], indirect=True)
def test_calculated_parameters(test_coll):
    # Make sure collector parameters are calculated if not given in the Solar Keymark data sheet / at object creation
    # Savo collector: Kd and eta0b not given
    for p in ['kd', 'eta0b']:
        assert test_coll.__getattribute__(p) is not None
        assert p in test_coll.calculation_info

    kd_calc, info = calculate_kd_Hess_and_Hanby(test_coll.iam_method)
    assert kd_calc == pytest.approx(test_coll.kd, 0.01)
    assert test_coll.calculation_info['kd'] == info


@pytest.mark.parametrize('test_coll', ['Greenonetec 3803'], indirect=True)
def test_parameter_conversion_ap_gr(test_coll):
    expected_keys = ['a1', 'a2', 'a5', 'a8']
    expected_txt = 'Converted from aperture to gross area by SunPeek'
    assert all([k in test_coll.aperture_parameters for k in expected_keys])
    assert all([expected_txt in test_coll.calculation_info[k] for k in expected_keys])

    assert 'eta0b' not in test_coll.calculation_info
    assert 'eta0hem' in test_coll.calculation_info

    cf = 7.41 / 7.91
    assert test_coll.aperture_parameters['a1']['magnitude'] == 2.102
    assert test_coll.a1.magnitude == pytest.approx(2.102 * cf)

    # Make sure update_parameters() doesn't mess things up
    test_coll.update_parameters()
    assert test_coll.aperture_parameters['a1']['magnitude'] == 2.102
    assert test_coll.a1.magnitude == pytest.approx(2.102 * cf)

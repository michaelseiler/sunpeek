import pytest
import pytz

import sunpeek.common.time_zone as tz
from sunpeek.common.unit_uncertainty import Q


@pytest.fixture(scope='session')
def lat():
    return Q(45, 'deg')


@pytest.fixture(scope='session')
def lng():
    return Q(10, 'deg')


def test_offset(lat, lng):
    actual = tz.get_timezone_offset_minutes(lat, lng)
    expected = 60

    assert expected == actual


def test_data_timezone(lat, lng):
    offset = tz.get_timezone_offset_minutes(lat, lng)
    actual = tz.get_data_timezone(offset)
    expected = pytz.FixedOffset(60)

    assert expected == actual


def test_timezone_string(lat, lng):
    actual = tz.get_timezone_string(lat, lng)
    expected = 'Europe/Rome'

    assert expected == actual


def test_timezone_string_plots(lat, lng):
    actual = tz._get_timezone_string_plots(lat, lng)
    expected = 'UTC+1'

    assert expected == actual


def test_timezone_data(fhw):
    assert fhw.tz_data == pytz.FixedOffset(60)
    assert fhw.local_tz_string_with_DST == 'Europe/Vienna'


def test_process_timezone(fhw):
    assert tz.process_timezone('Europe/Vienna') == pytz.timezone('Europe/Vienna')
    assert tz.process_timezone(None) is None
    assert tz.process_timezone(tz.TIMEZONE_NONE) is None
    assert tz.process_timezone(tz.TIMEZONE_LOCAL_NO_DST, fhw) == pytz.FixedOffset(60)
    assert tz.process_timezone(tz.TIMEZONE_LOCAL_WITH_DST, fhw) == pytz.timezone('Europe/Vienna')
    assert tz.process_timezone('UTC-5') == pytz.FixedOffset(-300)
    assert tz.process_timezone('UTC+2') == pytz.FixedOffset(120)

    for timezone in [pytz.FixedOffset(120), pytz.timezone('Europe/Vienna')]:
        assert tz.process_timezone(timezone) == timezone

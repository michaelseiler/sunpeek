from sunpeek.common import utils

import os
import pytest
from unittest import mock


@mock.patch.dict(os.environ, {"TEST": "OK"})
def test_get_env():
    assert utils.get_env('TEST') == 'OK'


@mock.patch.dict(os.environ, {"TEST": "OK"})
def test_get_env_exept():
    with pytest.raises(Exception):
        utils.get_env('T') == 'OK'
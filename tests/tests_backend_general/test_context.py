import pytest
import json
import pandas as pd
import numpy as np

import sunpeek.demo
import sunpeek.common.errors as err
from sunpeek.common import config_parser
from sunpeek.common.unit_uncertainty import Q
from sunpeek.data_handling.wrapper import use_dataframe, use_csv
from sunpeek.data_handling.context import Context, DataSources
from sunpeek.components import OperationalEvent
from sunpeek.data_handling.context import NanReportResponse, sanitize_index
import sunpeek.core_methods.virtuals as virtuals


@pytest.fixture
def example_context(example_plant):
    return Context(plant=example_plant, datasource='pq')


@pytest.fixture
def dummy_df():
    index = pd.date_range(start=pd.to_datetime('2020-01-01', utc=True),
                          end=pd.to_datetime('2020-01-05', utc=True), freq="1T")
    df = pd.DataFrame(np.linspace([-10, -50, 400], [1000, 50, 600], num=len(index)),
                      index=index, columns=['pwSolHtmHst', 'teAmb', 'rd_gti'])
    return df


@pytest.fixture
def example_plant_df(example_plant, dummy_df):
    example_plant.context = Context(plant=example_plant, datasource='df', dataframe=dummy_df)
    return example_plant


## --- init

def test_empty_plant():
    with pytest.raises(err.ConfigurationError, match='Context parameter "plant" must not be None'):
        Context(plant=None, datasource='pq')


@pytest.mark.parametrize('datasource', ['abc'])
def test_datasource_invalid(example_plant, datasource):
    with pytest.raises(ValueError, match="is not a valid DataSources"):
        Context(plant=example_plant, datasource=datasource)


def test_datasource_df(example_plant, dummy_df):
    c = Context(plant=example_plant, datasource='df', dataframe=dummy_df)

    assert c.datasource == DataSources.df
    pd.testing.assert_frame_equal(dummy_df, c.df)


def test_datasource_df_frame_none(example_plant):
    c = Context(plant=example_plant, datasource='df', dataframe=None)

    assert c.datasource == DataSources.df
    assert c._df is None


def test_datasource_df_no_frame(example_plant):
    c = Context(plant=example_plant, datasource='df')

    assert c.datasource == DataSources.df
    assert c._df is None


## --- sanitize_index

def test_sort():
    idx = pd.date_range(start='2023-09-01', periods=3, freq='D', tz='UTC')
    idx_unsorted = pd.DatetimeIndex([idx[i] for i in [0, 2, 1]])
    df = pd.DataFrame(np.random.rand(3, 2), index=idx_unsorted, columns=['A', 'B'])

    df_out, n = sanitize_index(df)

    assert len(df_out == 3)
    assert n == 0
    assert df_out.index.equals(idx)
    assert df_out.index.is_monotonic_increasing


def test_treat_duplicates():
    idx = pd.date_range(start='2023-09-01', periods=3, freq='D', tz='UTC')
    df = pd.DataFrame(np.random.rand(3, 2), index=idx, columns=['A', 'B'])
    df = pd.concat([df, df.iloc[-1:]], axis=0)

    df_out, n = sanitize_index(df)

    assert len(df_out == 2)
    assert n == 2
    assert df_out.index.is_monotonic_increasing


@pytest.mark.parametrize('idx', [
    pd.Index(['A', 'B', 'C']),
    pd.RangeIndex(start=1, stop=4),
    pd.MultiIndex.from_tuples([(1, 'A'), (1, 'B'), (2, 'A')], names=['Type4_1', 'Type4_2']),
])
def test_wrong_index_type(idx):
    df = pd.DataFrame(np.random.rand(3, 2), index=idx, columns=['A', 'B'])
    with pytest.raises(err.TimeIndexError, match='Expected DatetimeIndex'):
        sanitize_index(df)


@pytest.mark.parametrize('idx', [
    pd.date_range(start='2023-09-01', periods=3, freq='D', tz='UTC')
])
def test_datetime_index_ok(idx):
    df = pd.DataFrame(np.random.rand(3, 2), index=idx, columns=['A', 'B'])
    df, n = sanitize_index(df)

    assert n == 0


## --- eval start end

def test_eval_start_none(example_plant):
    c = Context(plant=example_plant, datasource='pq', eval_start=None)
    assert c.eval_start == pd.to_datetime('1970-01-01 00:00', utc=True)


def test_eval_both_none(example_plant):
    c = Context(plant=example_plant, datasource='pq')
    assert c.eval_start == pd.to_datetime('1970-01-01 00:00', utc=True)
    assert c.eval_end == pd.to_datetime('2200-01-01 00:00', utc=True)


def test_set_eval_interval(example_context):
    example_context.set_eval_interval(eval_start=pd.to_datetime('2022-01-01 00:00', utc=True))
    example_context.set_eval_interval(eval_end=pd.to_datetime('2022-01-01 00:00', utc=True))
    example_context.set_eval_interval(eval_start=pd.to_datetime('2022-01-01 00:00', utc=True),
                                      eval_end=pd.to_datetime('2023-01-01 00:00', utc=True))


def test_eval_interval_string(example_context):
    left = '2022-09-12 11:16'
    right = '2022-09-13 21:16'
    with pytest.raises(TypeError, match='expected to be of type datetime'):
        example_context.set_eval_interval(eval_start=left, eval_end=right)


def test_eval_interval_string_tz(example_context):
    df = pd.DataFrame(data=np.random.randn(2, 1), index=pd.to_datetime(['2022-09-12 11:16+01', '2022-09-13 21:16+02']))
    with pytest.raises(AssertionError, match='index expected to be a pandas DatetimeIndex'):
        example_context.use_dataframe(df)


def test_eval_interval_datetime(example_context):
    left = pd.to_datetime('2022-09-12 11:16', utc=True)
    right = pd.to_datetime('2022-09-13 21:16', utc=True)
    df = pd.DataFrame(data=np.random.randn(2, 1), index=[left, right])
    example_context.use_dataframe(df)

    assert example_context.eval_start == Context.EvalInterval.DEFAULT_START
    assert example_context.eval_end == Context.EvalInterval.DEFAULT_END


## --- use dataframe

def test_dataframe_raises_no_datetimeindex(example_context):
    df = pd.DataFrame
    with pytest.raises(AssertionError, match='index expected to be a pandas DatetimeIndex'):
        example_context.use_dataframe(df)


def test_dataframe_ok(example_context, dummy_df):
    example_context.use_dataframe(dummy_df)

    assert example_context.datasource == 'df'
    pd.testing.assert_frame_equal(dummy_df, example_context.df)


def test_get_time_index_dataframe(example_context, dummy_df):
    example_context.use_dataframe(dummy_df)
    pd.testing.assert_index_equal(dummy_df.index, example_context.df.index)


def test_delete_all_data(example_context, dummy_df):
    example_context.use_dataframe(dummy_df)
    pd.testing.assert_frame_equal(example_context.df, dummy_df)

    example_context.delete_all_data()
    assert example_context.df is None


## --- process data

def test_ignored_range(example_plant_df):
    example_plant_df.add_operational_event(pd.to_datetime('2020-01-02', utc=True),
                                           pd.to_datetime('2020-01-03', utc=True),
                                           description='foo', ignored_range=True)
    data = example_plant_df.te_amb.data
    assert np.isnan(data[pd.to_datetime('2020-01-02 12:00', utc=True)].magnitude)

    OperationalEvent(event_start=pd.to_datetime('2020-01-02', utc=True),
                     event_end=pd.to_datetime('2020-01-03', utc=True),
                     ignored_range=True, description='test', plant=example_plant_df)


def test_ignored_range_alt_creation(example_plant_df):
    OperationalEvent(event_start=pd.to_datetime('2020-01-02', utc=True),
                     event_end=pd.to_datetime('2020-01-03', utc=True),
                     ignored_range=True, description='test', plant=example_plant_df)
    data = example_plant_df.te_amb.data
    assert np.isnan(data[pd.to_datetime('2020-01-02 12:00', utc=True)].magnitude)


def test_replace_lower_full(example_plant_df):
    s = example_plant_df.te_amb
    s.value_replacements = {'lower': (Q(-49.99, 'degC'), Q(0, 'degC'), Q(-1, 'degC'))}
    assert pd.isna(s.data[0])
    assert s.data[1] == Q(-1, 'degC')
    assert s.data[-1] == Q(50, 'degC')
    s.value_replacements = {'lower': (Q(-49.99, 'degC'), Q(0, 'degC'), Q(-1, 'degC')),
                            'upper': (Q(0, 'degC'), Q(49.99, 'degC'), Q(1000, 'degC'))}
    assert pd.isna(s.data[-1])
    assert s.data[-2] == Q(1000, 'degC')
    assert s.data[-2] == Q(1000, 'degC')  # on purpose testing multiple access to s.data
    assert pd.isna(s.data[0])
    assert s.data[1] == Q(-1, 'degC')


def test_replace_inherited_from_sensortype(example_plant_df):
    s = example_plant_df.te_amb
    s.value_replacements = {}
    assert pd.isna(s.data[0])


def test_replace_outer_only__lower(example_plant_df):
    s = example_plant_df.te_amb
    s.value_replacements = {'lower': (Q(-49.99, 'degC'), None, None)}
    s.value_replacements
    assert pd.isna(s.data[0])
    assert not pd.isna(s.data[1])


def test_replace_outer_only__upper(example_plant_df):
    s = example_plant_df.te_amb
    s.value_replacements = {'upper': (None, Q(49.99, 'degC'), None)}
    assert pd.isna(s.data[-1])
    assert not pd.isna(s.data[-2])


def test_replace_inner_only__lower(example_plant_df):
    s = example_plant_df.te_amb
    s.value_replacements = {'lower': (None, Q(-49.99, 'degC'), None)}
    assert pd.isna(s.data[0])


def test_replace_inner_only__upper(example_plant_df):
    s = example_plant_df.te_amb
    s.value_replacements = {'upper': (Q(49.98, 'degC'), None, None)}
    assert pd.isna(s.data[-1])


def test_replace_outer_missing__lower(example_plant_df):
    s = example_plant_df.te_amb
    s.value_replacements = {'lower': (None, Q(-49.99, 'degC'), Q(55, 'degC'))}
    assert s.data[0] == Q(55, 'degC')


def test_replace_outer_missing__upper(example_plant_df):
    s = example_plant_df.te_amb
    s.value_replacements = {'upper': (Q(49.98, 'degC'), None, Q(1000, 'degC'))}
    assert s.data[-1] == Q(1000, 'degC')


## --- Get Data

def test_get_data_withNativeSensor(example_plant_df):
    s = example_plant_df.te_amb
    data = s.data
    assert len(data) >= 10
    assert isinstance(data, pd.Series)


def test_get_data_noNativeSensor(example_plant_df):
    s = example_plant_df.te_amb

    # Drop native unit from sensor (requires that also not mapped to plant - else the native_unit is automatically set
    # based on the unit!)
    example_plant_df.te_amb = None
    assert s.sensor_type is None
    s._native_unit = None
    assert s.native_unit is None

    data = s.data
    assert len(data) >= 10
    assert isinstance(data, pd.Series)


## --- Sensor validation / NaN reports

@pytest.fixture(scope='session')
def nan_report_2days(fhw__2days_data):
    return fhw__2days_data.context.get_nan_report()


@pytest.fixture(scope='session')
def nan_report_2days__virtuals(fhw__2days_data):
    return fhw__2days_data.context.get_nan_report(include_virtuals=True)


@pytest.fixture(scope='session')
def nan_report_2days__pq(fhw__2days_data):
    return fhw__2days_data.context.get_nan_report()


def test_nan_report__return_types(nan_report_2days):
    assert isinstance(nan_report_2days, NanReportResponse)
    for v in nan_report_2days.nan_report.values():
        assert isinstance(v, pd.DataFrame)


def test_nan_report__return_types__pq(nan_report_2days__pq):
    assert isinstance(nan_report_2days__pq, NanReportResponse)
    for v in nan_report_2days__pq.nan_report.values():
        assert isinstance(v, pd.DataFrame)


def test_nan_report__sensor_names(fhw, nan_report_2days):
    actual = list(nan_report_2days.nan_report.keys())
    expected = fhw.get_raw_names(include_virtuals=False)

    assert sorted(actual) == sorted(expected)


def test_nan_report__sensor_names_virtuals(fhw, nan_report_2days__virtuals):
    actual = list(nan_report_2days__virtuals.nan_report.keys())
    expected = fhw.get_raw_names(include_virtuals=True)

    assert sorted(actual) == sorted(expected)


def test_nan_report__dataframe_columns(nan_report_2days):
    sensor_name = next(iter(nan_report_2days.nan_report))
    df = nan_report_2days.nan_report[sensor_name]
    actual = list(df.columns)

    expected = [Context.N_TOTAL_TIMESTAMPS, Context.N_AVAILABLE_TIMESTAMPS, Context.NAN_DENSITY_IN_AVAILABLE]

    assert sorted(actual) == sorted(expected)


def test_index(nan_report_2days):
    sensor_name = next(iter(nan_report_2days.nan_report))
    actual = nan_report_2days.nan_report[sensor_name].index

    assert len(actual) == 2
    assert actual[0] == pd.to_datetime('2017-05-01')
    assert actual[1] == pd.to_datetime('2017-05-02')


@pytest.fixture
def example_plant_nan_report(example_plant_df):
    # prepare plant for sensor validation, add some nans and an ignored range
    df = example_plant_df.context.df
    # make sure we have some nan values in the data
    df.loc[::3, :] = np.nan
    use_dataframe(example_plant_df, df, calculate_virtuals=False)
    example_plant_df.add_operational_event(pd.to_datetime('2020-01-02 21:00', utc=True),
                                           pd.to_datetime('2020-01-04 2:00', utc=True),
                                           description='foo', ignored_range=True)
    return example_plant_df


def test_nan_report__numerically(example_plant_nan_report):
    p = example_plant_nan_report
    nan_report = p.context.get_nan_report().nan_report

    assert all([x in nan_report.keys() for x in ['pwSolHtmHst', 'teAmb']])
    assert isinstance(nan_report['teAmb'], pd.DataFrame)

    actual = nan_report['teAmb']
    expected = pd.DataFrame(data={
        Context.N_TOTAL_TIMESTAMPS: [1380, 1440, 1440, 1440, 61],
        Context.N_AVAILABLE_TIMESTAMPS: [1380, 1320, 0, 1259, 61],
        Context.NAN_DENSITY_IN_AVAILABLE: [0.8899, 0.333, np.nan, 0.3328, 0.344]},
        index=pd.date_range(pd.to_datetime('2020-01-01'), pd.to_datetime('2020-01-05'), freq="1D"))

    pd.testing.assert_frame_equal(actual, expected, check_freq=False, atol=1e-3)


def test_change_sensor_unit_returns_data():
    # Wrong sensor unit can lead to NaN values (e.g. te_amb defined in degC, when measurements are in K).
    # These NaN values should not overwrite the Context dataframe.
    # After changing the unit (to K in this case), the correct values (as parsed at data upload) should be recoverable.
    with open(sunpeek.demo.DEMO_CONFIG_PATH) as f:
        conf = json.load(f)
    assert conf['plant']['raw_sensors'][0]['raw_name'] == 'te_amb'
    assert conf['plant']['raw_sensors'][0]['native_unit'] == 'K'

    # Suppose a user defined the wrong unit
    conf['plant']['raw_sensors'][0]['native_unit'] = 'degC'
    plant = config_parser.make_full_plant(conf)
    use_csv(plant, csv_files=sunpeek.demo.DEMO_DATA_PATH_2DAYS, timezone='UTC', datetime_template='year_month_day',
            calculate_virtuals=False)
    assert plant.context.df['te_amb'].isna().sum() == 0
    assert plant.te_amb.data.notna().sum() == 0

    # Change unit, assert plant.te_amb now has not-NaN data
    plant.te_amb.native_unit = 'K'
    assert plant.te_amb.data.isna().sum() == 0

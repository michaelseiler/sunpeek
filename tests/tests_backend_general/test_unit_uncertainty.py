""" Tests for working with units & uncertainties.
These tests are far from being complete, but should test some basic functionality.
"""

import pytest
import os
import numpy as np
import pandas as pd
import pint

from sunpeek.common import utils
from sunpeek.common import unit_uncertainty as uu
from sunpeek.common.common_units import Dimensionality


units = uu.units
Q = uu.Q


@pytest.fixture
def df(resources_dir):
    # Minimum example dataframe
    csv_file = os.path.join(resources_dir, 'data', 'unit_uncertainty_min_example.csv')
    df = pd.read_csv(csv_file, sep=';', header=0)
    df.index = pd.date_range("2018-01-01 12:00", periods=4, freq="min")
    # print(df)
    return df


@pytest.fixture
def x(df):
    pobj = uu.to_pint(df['x'], unit='mm',
                      # absolute_uncertainty=Q(1, 'mm'),
                      # relative_uncertainty=Q(10, 'percent')
                        )
    return pobj


@pytest.fixture
def y(df):
    unc = Q([0.001, 0.003, np.nan, 0.001], 'kW / m**2')
    pobj = uu.to_pint(df['y'], unit='W m**-2',
                      # absolute_uncertainty=unc
                      )
    return pobj


@pytest.fixture
def a(df):
    pobj = uu.to_pint(df['a'], unit='km')
    return pobj


@pytest.fixture
def b(df):
    unc = Q([0.1, 0.3, np.nan, 0.2], 'J kg**-1')
    pobj = uu.to_pint(df['b'], unit='J kg**-1',
                      # absolute_uncertainty=unc
                      )
    return pobj


@pytest.fixture
def df2():
    # Real example dataframe, Arcon South 1 day 2017-05-20
    csv_file = os.path.join(utils.ROOT_DIR, 'tests/resources/data/unit_uncertainty_ArcS_1day.csv')
    df = pd.read_csv(csv_file, sep=';', header=0, index_col=0)
    df.index = pd.DatetimeIndex(df.index)
    # print(df)
    return df


@pytest.fixture
def z(df2):
    # Return global radiation
    z = df2['rd_gti']
    return z


def test_creation(df, x, y):
    return True


def test_to_pint(df):
    # Construct pint Quantity objects (= unit & uncertainy aware)
    z = uu.to_pint(df['z'], unit='degC',
                   # absolute_uncertainty=Q(1, 'degC'),
                   # relative_uncertainty=Q(10, 'percent'),
                   # relative_unit='degC'
                    )
    return True


# @pytest.mark.skip(reason="Not required because we're not using the uncertainty system for now.")
# def test_to_series(df, x):
#     # Convert to pandas series
#     # (no uncertainty, only numeric values in given unit: nominal values & uncertainty combined)
#     s = uu.to_series(x)
#     s = uu.to_series(x, index=df.index, name='x')
#     s = uu.to_series(x, direction='plus', k=1, index=df.index, name='x')
#     s = uu.to_series(x, output_unit='mm', direction='plus', k=1, index=df.index, name='x')
#     s = uu.to_series(x, output_unit='mm', direction='minus', k=2, index=df.index, name='x')
#
#
# @pytest.mark.skip(reason="Not required because we're not using the uncertainty system for now.")
# def test_nominal_values(df, x):
#     # Access nominal values (no uncertainty), as series
#     uu.nominal_values(x)
#     uu.nominal_values(x, output_unit='m')
#     uu.nominal_values(x, output_unit='m', index=df.index)
#     uu.nominal_values(x, output_unit='m', index=df.index, name='x')
#
#
# @pytest.mark.skip(reason="Not required because we're not using the uncertainty system for now.")
# def test_std_devs(df, x):
#     # Access only uncertainty (std. deviation), as series
#     uu.std_devs(x)
#     uu.std_devs(x, output_unit='m')
#     uu.std_devs(x, output_unit='m', index=df.index)
#     uu.std_devs(x, output_unit='m', index=df.index, name='x')
#
#
# @pytest.mark.skip(reason="Not required because we're not using the uncertainty system for now.")
# def test_isna(df, b):
#     is_na = uu.isna(b)
#     assert all(is_na == [False, True, True, False])


# @pytest.mark.skip(reason="Development plot")
# def test_mean_uncertainty(z):
#     from plotly.subplots import make_subplots
#     """Compare different strategies to calculate uncertainty of averages."""
#
#     unc = lambda x: 0.1 * x + 30
#     to_pint = lambda z: uu.to_pint(z, unit='W m**-2',
#                    absolute_uncertainty=Q(30, 'W m**-2'),
#                    relative_uncertainty=Q(10, 'percent'))
#     zu = to_pint(z)
#     res = z.resample('10Min')
#
#     def add_unc(fig, y, unc, x=None, row=1, col=1):
#         if x is None:
#             x = y.index
#         fig.add_scatter(y=y + unc, x=x, mode='lines', line_color='grey', line_width=0.5,
#                         row=row, col=col, showlegend=False)
#         fig.add_scatter(y=y - unc, x=x, mode='lines', line_color='grey', line_width=0.5, fill='tonexty',
#                         row=row, col=col, showlegend=False)
#
#     fig = make_subplots(rows=3, shared_xaxes=True, vertical_spacing=0.02)
#
#     # Original data
#     add_unc(fig, y=z, unc=unc(z))
#     fig.add_scatter(y=z, x=z.index, mode='lines', line_width=3, line_color='black', row=1, col=1,
#                     name='Original data', legendrank=1)
#
#     # Resample, then uncertainty
#     zr = res.mean()
#     add_unc(fig, y=zr, unc=unc(zr), row=2)
#     fig.add_scatter(y=zr, x=zr.index, mode='lines', line_color='red', line_width=3, row=2, col=1,
#                     name='Resample first, then uncertainty', legendrank=2)
#
#     # Uncertainty, then resample
#     n = np.array([])
#     u = np.array([])
#     for r in res:
#         ru = to_pint(r[1])
#         n = np.append(n, uu.nominal_values(ru.mean(), to_numpy=True))
#         u = np.append(u, uu.std_devs(ru.mean(), to_numpy=True))
#
#     ru = uu.to_pint_mu(n, u, "W m**-2")
#     add_unc(fig, y=uu.nominal_values(ru), unc=uu.std_devs(ru), x=zr.index, row=3)
#     fig.add_scatter(y=uu.nominal_values(ru), x=zr.index, mode='lines', line_color='blue', line_width=3, row=3, col=1,
#                     name='pint propagation (uncertainty first, then resample)', legendrank=3)
#
#
#     fig.update_layout(dragmode='pan')
#     # fig.update_layout(dragmode='pan', legend_traceorder='reversed')
#     config = {'scrollZoom': True,
#               'displayModeBar': True,
#               'modeBarButtonsToRemove': ['zoom', 'zoomIn', 'zoomOut'],
#               'modeBarButtonsToAdd': ['drawline',
#                                       'drawrect',
#                                       'eraseshape'
#                                       ]}
#     fig.show(config=config)
#
#
#     # def test_isna_dict(df, x, y, b):
#     #     pobj_dict = dict(x=x, y=y, b=b)
#     #     is_na = uu.isna_dict(pobj_dict)
#     #     assert all(is_na == [False, True, True, False])


def test_operations_ok(x, y, a):
    # Simple operations are uncertainty-aware and unit-checked
    x * y
    # Can sum compatible units
    x + a


def test_operations_fail(x, y, a):
    # Simple operations are uncertainty-aware and unit-checked: Cannot sum incompatible units
    try:
        x + y
    except pint.errors.DimensionalityError:
        assert True


def test_mean(x):
    x.mean()


def test_compatibility():
    x = Q(1, 'm')
    y = Q(1, 'km')
    x.is_compatible_with(y)
    units('m').is_compatible_with('cm')
    units('').is_compatible_with(None)
    units(None).is_compatible_with(None)
    units('').is_compatible_with('')
    units('').is_compatible_with(None)


def test_dimensionality():
    m = Dimensionality(['g', 'kg', 'tonne', 'lbs', 'ton'])
    t = Dimensionality(['s', 'min', 'hour', 'day', 'week', 'year'])
    p = Dimensionality(['W', 'kW', 'MW', 'GW', 'BTU/h'])

    assert list(m) == ['g', 'kg', 'tonne', 'lbs', 'ton']
    assert repr(m) == "Dimensionality(['g', 'kg', 'tonne', 'lbs', 'ton'])"
    assert list(m/t) == ['g/s', 'g/min', 'g/hour', 'g/day', 'g/week', 'g/year',
                   'kg/s', 'kg/min', 'kg/hour', 'kg/day', 'kg/week', 'kg/year',
                   'tonne/s', 'tonne/min', 'tonne/hour', 'tonne/day', 'tonne/week', 'tonne/year',
                   'lbs/s', 'lbs/min', 'lbs/hour', 'lbs/day', 'lbs/week', 'lbs/year',
                   'ton/s', 'ton/min', 'ton/hour', 'ton/day', 'ton/week', 'ton/year']
    assert list(p*t) == ['W·s', 'W·min', 'W·hour', 'W·day', 'W·week', 'W·year',
                   'kW·s', 'kW·min', 'kW·hour', 'kW·day', 'kW·week', 'kW·year',
                   'MW·s', 'MW·min', 'MW·hour', 'MW·day', 'MW·week', 'MW·year',
                   'GW·s', 'GW·min', 'GW·hour', 'GW·day', 'GW·week', 'GW·year',
                   'BTU/h·s', 'BTU/h·min', 'BTU/h·hour', 'BTU/h·day', 'BTU/h·week', 'BTU/h·year']
    assert str(p) == "['W', 'kW', 'MW', 'GW', 'BTU/h']"


# Low level access methods
# print(type(x))      # numbers (magnitude)
# print(x.magnitude)  # numpy.ndarray
# print(x.m)
# print(x.units)      # unit: pint.unit.build_unit_class.<locals>.Unit
# print(x.u)
#
# print(x.unitless)
# print(x.tolist())
# # x.visualize()
# print(x.to_root_units())
# print(x.ito_root_units())  # inplace
#
# # Low level access to nominal values and uncertainties
# # (same as uu.nominal_values and uu.std_devs)
# print(unumpy.nominal_values(x.magnitude))
# print(unumpy.nominal_values(x.to(units('m')).magnitude))
# print(unumpy.std_devs(x.magnitude))


# Real FHW data
# csv_file = os.path.join(utils.ROOT_DIR, 'tests', 'resources', 'data', 'pc_method',
#              'FHW_2017_1m__collField_ArcS__2017-03-01_2017-11-01.csv'),
# df = pd.read_csv(csv_file, index_col=0, sep=';')


# pint.Quantity
# pint.Measurement
# not working...
# pint.Quantity([1, 2], "kg").plus_minus(pint.Quantity([0.1, 0.2], "kg"))
# https://github.com/hgrecco/pint/issues/918

import pytest

import sunpeek.components as cmp
import sunpeek.core_methods.virtuals as virtuals


def test_map_unmap_real_virtual_sensor(example_plant):
    # Test in response to this bug report: https://gitlab.com/sunpeek/sunpeek/-/issues/451
    # If a slot is mapped to a real sensor, but was previously mapped to a virtual sensor, that virtual sensor should
    # be deleted from plant.raw_sensors.
    p = example_plant
    a = p.arrays[0]

    # Without a real 'is_shadowed' sensor, make sure 'is_shadowed' can be calculated as a virtual sensor.
    virtuals.config_virtuals(p)
    assert a.is_shadowed.is_virtual
    assert a.is_shadowed.can_calculate

    # Now map a real sensor
    a.is_shadowed = cmp.Sensor(raw_name="is_shadowed", native_unit='dimensionless')

    virtuals.config_virtuals(p)
    assert not a.is_shadowed.is_virtual

    # Make sure that, by mapping the real sensor, the virtual sensor got removed from plant.raw_sensors
    sensor_names = p.get_raw_names(only_virtuals=True)
    is_shadowed_list = [s for s in sensor_names if s.startswith('is_shadowed__virtual')]

    assert len(is_shadowed_list) == 0

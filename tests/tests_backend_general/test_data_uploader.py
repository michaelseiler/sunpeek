import pytest
import json
import os
import pytz
import pandas as pd
import numpy as np
import pandas.api.types
from io import BytesIO
import datetime as dt

import sunpeek.demo
from sunpeek.common import config_parser
from sunpeek.data_handling.wrapper import use_csv
from sunpeek.data_handling.data_uploader import DatetimeTemplates, DataUploadResponse, UploadHistory
from sunpeek.common.errors import DataProcessingError
from sunpeek.data_handling.data_uploader import DataUploader_df, DataUploader_pq
import sunpeek.common.time_zone as tz
from sunpeek.common.utils import to_utc, to_unix_str, utc_str, json_to_df


## --- fixtures

@pytest.fixture
def raw_plant():
    """FHW plant without fluid, with data, virtuals configured"""
    with open(sunpeek.demo.DEMO_CONFIG_PATH) as f:
        conf = json.load(f)
    return config_parser.make_full_plant(conf)


@pytest.fixture
def get_csv(fn, resources_dir):
    full_fn = lambda x: os.path.join(os.path.join(resources_dir, 'data', 'data_uploader'), x + '.csv')
    if isinstance(fn, str):
        fn = [fn]
    all_fn = [full_fn(x) for x in fn]
    return all_fn


@pytest.fixture
def get_large_csv():
    return sunpeek.demo.DEMO_DATA_PATH_1MONTH


@pytest.fixture
def raw_sensor_names():
    return ['rd_gti', 'rd_bti', 'rd_dti', 'rd_dni', 'vf', 'te_out', 'te_in', 've_wind', 'te_amb',
            'rh_amb', 'is shadowed']


# info on the various csv files
file_info = {
    'csv1': 'Normal data, chunk 1',
    'csv2': 'Normal data, chunk 2, chronologically after 1',
    'csv3': 'Normal data, chunk 3, chronologically after 2',
    'csv1_2': 'Normal data, overlapping with chunk 1',
    'dup_2': 'Duplicate timestamps, 2 non-duplicate rows remaining',
    'dup_1': 'Duplicate timestamps, 1 non-duplicate row remaining',
    'rows_3': 'Normal data, no extra columns, only 1 row of data',
    "rows_2": 'Normal data, no extra columns, only 2 rows of data',
    "rows_1": 'Normal data, no extra columns, only 1 row of data',
    "headers_only": 'No data, only headers',
    "cols_missing": 'Sorted data, some required columns missing.',
    "unsorted": 'Unsorted data, timestamps are not in chronological order',
    "empty": 'No data, no headers, empty file',
    "extra_cols": 'Normal data, with some extra columns',
    "only_extra_cols": 'Only columns that are not needed for the plant',
    "no_cols": 'Only timestamps, no data',
    "strings": 'Data with some strings',
    "crap_rows": 'Normal rows mixed with crap rows to be skipped',
    "all_crap": 'Contains 2 crap rows, no readable data',
    "tz_utc": 'Some data, timestamps in utc e.g. "30.04.2017  00:00:00+00"',
    "tz_utc_1": 'Some data, timestamps in utc + 1 e.g. "30.04.2017  00:00:00+01"',
    "tz_mixed": 'Some data, timestamps all have timezone information, but the timezones are mixed',
    "tz_no_yes": 'Some data, some timestamps have timezones, others dont',
    "datetime_format": 'Non-default datetime format',
    "index_col": 'Timestamps not in first column',
}


## --- tests without database


@pytest.mark.parametrize('fn', ['csv1', 'csv2', 'csv3', 'csv1_2', 'rows_3', 'rows_2', 'extra_cols', 'strings',
                                'unsorted', 'cols_missing', 'no_cols'])
def test_readable_files(raw_plant, fn, get_csv):
    use_csv(raw_plant, get_csv, timezone='Europe/Vienna', calculate_virtuals=False,
            datetime_template=DatetimeTemplates.year_month_day)


@pytest.mark.skip('Basically just to get a feeling for data_uploader performance')
def test_large_csv(raw_plant, get_large_csv):
    use_csv(raw_plant, get_large_csv, timezone='Europe/Vienna', calculate_virtuals=False,
            datetime_template=DatetimeTemplates.year_month_day)
    assert raw_plant.context.df.shape == (525361, 11)


@pytest.mark.parametrize('fn', [['csv1', 'csv2', 'csv3']])
def test_multiple_files(raw_plant, fn, get_csv, raw_sensor_names):
    use_csv(raw_plant, get_csv, timezone='Europe/Vienna', calculate_virtuals=False,
            datetime_template=DatetimeTemplates.year_month_day)
    df = raw_plant.context.df

    assert df.shape == (16, 11)
    assert set(df.columns) == set(raw_sensor_names)
    assert all([pandas.api.types.is_numeric_dtype(df[col]) for col in df.columns])


@pytest.mark.parametrize('fn', ['csv1'])
def test_local_timezone_with_DST(raw_plant, fn, get_csv):
    use_csv(raw_plant, get_csv, timezone=tz.TIMEZONE_LOCAL_WITH_DST, calculate_virtuals=False,
            datetime_template=DatetimeTemplates.year_month_day)
    df = raw_plant.context.df
    expected = pd.to_datetime('2017-05-01 00:00:00').tz_localize(tz='Europe/Vienna')

    assert df.index[0] == expected
    assert df.index.tzinfo is not None
    assert df.index.tzinfo == dt.timezone.utc
    assert raw_plant.tz_data == pytz.FixedOffset(60)


@pytest.mark.parametrize('fn', ['csv1'])
def test_local_timezone_without_DST(raw_plant, fn, get_csv):
    use_csv(raw_plant, get_csv, timezone=tz.TIMEZONE_LOCAL_NO_DST, calculate_virtuals=False,
            datetime_template=DatetimeTemplates.year_month_day)
    df = raw_plant.context.df

    assert df.index.tzinfo == dt.timezone.utc
    assert raw_plant.tz_data == pytz.FixedOffset(60)


@pytest.mark.parametrize('fn', ['tz_utc'])
def test_explicit_none_timezone(raw_plant, fn, get_csv):
    use_csv(raw_plant, get_csv, timezone=tz.TIMEZONE_NONE, calculate_virtuals=False,
            datetime_template=DatetimeTemplates.year_month_day)
    df = raw_plant.context.df

    assert df.index.tzinfo is not None
    assert raw_plant.tz_data == pytz.FixedOffset(60)


@pytest.mark.parametrize('fn', ['cols_missing'])
def test_cols_missing(raw_plant, fn, get_csv, raw_sensor_names):
    use_csv(raw_plant, get_csv, timezone='Europe/Vienna', calculate_virtuals=False,
            datetime_template=DatetimeTemplates.year_month_day)

    assert set(raw_plant.context.df.columns) == set(raw_sensor_names)


@pytest.mark.parametrize('fn', ['tz_utc', 'tz_utc_1'])
def test_ambiguous_timezone(raw_plant, fn, get_csv):
    with pytest.warns(UserWarning, match='Reading csv files resulted in a DataFrame with less than 2 rows'):
        use_csv(raw_plant, get_csv, timezone='Europe/Vienna', calculate_virtuals=False,
                datetime_template=DatetimeTemplates.year_month_day)


@pytest.mark.parametrize('fn', ['rows_1'])
def test_less_2rows__(raw_plant, fn, get_csv):
    with pytest.warns(UserWarning, match='resulted in a DataFrame with less than 2 rows'):
        use_csv(raw_plant, get_csv, timezone='Europe/Vienna', calculate_virtuals=False,
                datetime_template=DatetimeTemplates.year_month_day)


@pytest.mark.parametrize('fn', ['rows_1', 'headers_only', 'dup_1'])
def test_less_2rows(raw_plant, fn, get_csv):
    with pytest.warns(UserWarning, match='resulted in a DataFrame with less than 2 rows'):
        use_csv(raw_plant, get_csv, timezone='Europe/Vienna', calculate_virtuals=False,
                datetime_template=DatetimeTemplates.year_month_day)


@pytest.mark.parametrize('fn', ['dup_2'])
def test_duplicate_rows(raw_plant, fn, get_csv):
    # 2 rows remaining after removing duplicates
    output = use_csv(raw_plant, get_csv, timezone='Europe/Vienna', calculate_virtuals=False,
                     datetime_template=DatetimeTemplates.year_month_day)

    assert len(raw_plant.context.df) == 2
    assert output.n_duplicates_index == 2


@pytest.mark.parametrize('fn', [['csv3', 'tz_utc']])
def test_mixing_tz_csv_string(raw_plant, fn, get_csv):
    # mix non-tz and tz-aware files: not allowed
    # only imports csv3, tz_utc is skipped
    output = use_csv(raw_plant, get_csv, timezone='Europe/Vienna', calculate_virtuals=False,
                     datetime_template=DatetimeTemplates.year_month_day)

    assert len(raw_plant.context.df) == 6
    assert 'Ambiguous timezone information found' in output.response_per_file[1].error_cause


@pytest.mark.parametrize('fn', [['tz_utc', 'csv3']])
def test_mixing_tz_csv_string__no_timezone(raw_plant, fn, get_csv):
    # mix non-tz and tz-aware files
    output = use_csv(raw_plant, get_csv, calculate_virtuals=False, datetime_template=DatetimeTemplates.year_month_day)
    # only imports tz_utc, csv3 is skipped
    assert len(raw_plant.context.df) == 3
    assert isinstance(output, DataUploadResponse)
    assert isinstance(output.response_per_file, list)
    assert len(output.response_per_file) == 2
    assert isinstance(output.response_per_file[0], UploadHistory)

    out1 = output.response_per_file[0]
    out2 = output.response_per_file[1]
    assert out1.name == 'tz_utc.csv'
    assert out2.name == 'csv3.csv'
    assert out1.error_cause is None
    assert 'No timezone information found' in out2.error_cause


@pytest.mark.parametrize('fn', ['tz_utc'])
def test_upload_response(raw_plant, fn, get_csv):
    output = use_csv(raw_plant, get_csv, calculate_virtuals=False, datetime_template=DatetimeTemplates.year_month_day)

    assert len(raw_plant.context.df) == 3
    assert isinstance(output, DataUploadResponse)


@pytest.mark.parametrize('fn', ['i_dont_exist'])
def test_file_not_found(raw_plant, fn, get_csv):
    with pytest.raises(FileNotFoundError, match='Cannot find file'):
        use_csv(raw_plant, get_csv, timezone='Europe/Vienna', calculate_virtuals=False,
                datetime_template=DatetimeTemplates.year_month_day)


@pytest.mark.parametrize('fn', [['tz_utc', 'tz_utc_1']])
def test_mixing_different_csv_tz(raw_plant, fn, get_csv):
    # mix csv files which all have timezone-aware timestamps but they don't use the same timezone
    with pytest.raises(DataProcessingError, match='Cannot concatenate DataFrames with mixed timezones'):
        use_csv(raw_plant, get_csv, calculate_virtuals=False, datetime_template=DatetimeTemplates.year_month_day)


@pytest.mark.parametrize('fn', ['crap_rows'])
def test_crap_rows_deleted(raw_plant, fn, get_csv):
    use_csv(raw_plant, get_csv, timezone='Europe/Vienna', calculate_virtuals=False,
            datetime_template=DatetimeTemplates.year_month_day)

    assert raw_plant.context.df.index[1] == pd.to_datetime('2017-05-01 00:02:00').tz_localize(tz='Europe/Vienna')


@pytest.mark.parametrize('fn', [['crap_rows', 'csv3']])
def test_combine_nonreadable_readable(raw_plant, fn, get_csv):
    use_csv(raw_plant, get_csv, timezone='Europe/Vienna', calculate_virtuals=False,
            datetime_template=DatetimeTemplates.year_month_day)


@pytest.mark.parametrize('fn', ['all_crap'])
def test_all_crap(raw_plant, fn, get_csv):
    with pytest.warns(UserWarning, match='Reading csv files resulted in a DataFrame with less than 2 rows'):
        use_csv(raw_plant, get_csv, calculate_virtuals=False, datetime_template=DatetimeTemplates.year_month_day)


@pytest.mark.parametrize('fn', ['extra_cols'])
def test_extra_cols_dropped(raw_plant, fn, get_csv):
    use_csv(raw_plant, get_csv, timezone='Europe/Vienna', calculate_virtuals=False,
            datetime_template=DatetimeTemplates.year_month_day)

    assert 'tp_mequso' not in raw_plant.context.df.columns


@pytest.mark.parametrize('fn', ['tz_no_yes', 'tz_mixed'])
def test_tz_inconsistent(raw_plant, fn, get_csv):
    with pytest.warns(UserWarning, match='Reading csv files resulted in a DataFrame with less than 2 rows'):
        use_csv(raw_plant, get_csv, calculate_virtuals=False, datetime_template=DatetimeTemplates.day_month_year)


@pytest.mark.parametrize('fn', [['tz_no_yes', 'csv3'], ['tz_mixed', 'csv3']])
def test_tz_inconsistent_plus_normalcsv(raw_plant, fn, get_csv):
    # first file is skipped, second one is read ok
    use_csv(raw_plant, get_csv, timezone='Europe/Vienna', calculate_virtuals=False,
            datetime_template=DatetimeTemplates.year_month_day)

    assert len(raw_plant.context.df) == 6


@pytest.mark.parametrize('fn', ['empty'])
def test_empty(raw_plant, fn, get_csv):
    with pytest.warns(UserWarning, match='Reading csv files resulted in a DataFrame with less than 2 rows'):
        use_csv(raw_plant, get_csv, calculate_virtuals=False, datetime_template=DatetimeTemplates.year_month_day)


@pytest.mark.parametrize('fn', ['strings'])
def test_strings_to_nan(raw_plant, fn, get_csv):
    use_csv(raw_plant, get_csv, timezone='Europe/Vienna', calculate_virtuals=False,
            datetime_template=DatetimeTemplates.year_month_day)

    assert np.isnan(raw_plant.context.df.iloc[0, 0])
    assert ~np.isnan(raw_plant.context.df.iloc[0, 1])
    assert ~np.isnan(raw_plant.context.df.iloc[1, 0])


@pytest.mark.parametrize('fn', ['unsorted'])
def test_sort_timestamps(raw_plant, fn, get_csv):
    use_csv(raw_plant, get_csv, timezone='Europe/Vienna', calculate_virtuals=False,
            datetime_template=DatetimeTemplates.year_month_day)

    assert raw_plant.context.df.index.is_monotonic_increasing


@pytest.mark.parametrize('fn', ['no_cols'])
def test_no_cols(raw_plant, fn, get_csv, raw_sensor_names):
    use_csv(raw_plant, get_csv, timezone='Europe/Vienna', calculate_virtuals=False,
            datetime_template=DatetimeTemplates.year_month_day)

    assert raw_plant.context.df is None


@pytest.mark.parametrize('fn', ['datetime_template_DMY'])
def test_datetime_template_or_format_set(raw_plant, fn, get_csv, raw_sensor_names):
    with pytest.raises(DataProcessingError,
                       match='Either "datetime_template" or "datetime_format" needs to be specified.'):
        use_csv(raw_plant, get_csv, timezone='Europe/Vienna', calculate_virtuals=False)


@pytest.mark.parametrize('fn', ['datetime_template_DMY'])
def test_dataframe_utc(raw_plant, fn, get_csv, raw_sensor_names):
    use_csv(raw_plant, get_csv, timezone='Europe/Vienna', calculate_virtuals=False,
            datetime_template=DatetimeTemplates.day_month_year)
    t_expected = pd.to_datetime('2017-04-30 23:00:00').tz_localize(tz='Europe/Vienna')

    assert raw_plant.context.df.index.tzinfo == dt.timezone.utc
    assert raw_plant.context.df.index[0] == t_expected


@pytest.mark.parametrize('fn', ['datetime_template_DMY'])
def test_datetime_template_dmy(raw_plant, fn, get_csv, raw_sensor_names):
    use_csv(raw_plant, get_csv, timezone='Europe/Vienna', calculate_virtuals=False,
            datetime_template=DatetimeTemplates.day_month_year)
    t_expected = pd.to_datetime('2017-04-30 23:00:00').tz_localize(tz='Europe/Vienna')

    assert raw_plant.context.df.index[0] == t_expected
    assert raw_plant.time_index[0] == t_expected.tz_convert(raw_plant.tz_data)
    assert raw_plant.context.df['ve_wind'][0] == 0.235


@pytest.mark.parametrize('fn', ['datetime_template_MDY'])
def test_datetime_template_mdy(raw_plant, fn, get_csv, raw_sensor_names):
    use_csv(raw_plant, get_csv, timezone='Europe/Vienna', calculate_virtuals=False,
            datetime_template=DatetimeTemplates.month_day_year)
    t_expected = pd.to_datetime('2017-04-30 23:00:00').tz_localize(tz='Europe/Vienna')
    assert raw_plant.context.df.index[0] == t_expected
    assert raw_plant.time_index[0] == t_expected.tz_convert(raw_plant.tz_data)
    assert raw_plant.context.df['ve_wind'][0] == 0.235


@pytest.mark.parametrize('fn', ['datetime_template_DMY'])
def test_datetime_format(raw_plant, fn, get_csv, raw_sensor_names):
    use_csv(raw_plant, get_csv, timezone='Europe/Vienna', calculate_virtuals=False,
            datetime_format='%d.%m.%Y %H:%M')
    assert raw_plant.context.df.index[0] == pd.to_datetime('2017-04-30 23:00:00').tz_localize(tz='Europe/Vienna')
    assert raw_plant.context.df['ve_wind'][0] == 0.235


@pytest.mark.parametrize('fn', ['index_col'])
def test_index_col(raw_plant, fn, get_csv, raw_sensor_names):
    use_csv(raw_plant, get_csv, timezone='Europe/Vienna', calculate_virtuals=False,
            datetime_format='%d.%m.%Y %H:%M',
            index_col=1)
    assert raw_plant.context.df.index[0] == pd.to_datetime('2017-04-30 23:00:00').tz_localize(tz='Europe/Vienna')
    assert raw_plant.context.df['ve_wind'][0] == 0.235


@pytest.mark.parametrize('fn', ['csv_decimal'])
def test_csv_decimal(raw_plant, fn, get_csv, raw_sensor_names):
    use_csv(raw_plant, get_csv, timezone='Europe/Vienna', calculate_virtuals=False,
            datetime_template=DatetimeTemplates.year_month_day,
            csv_decimal=',')
    assert raw_plant.context.df['ve_wind'][0] == 0.235


def getBio(get_csv):
    csv = get_csv
    with open(csv[0], 'rb') as f:
        bio = BytesIO(f.read())
    return bio


def test_dataUploader_getSettings(raw_plant):
    dataUploader = DataUploader_df(raw_plant, datetime_template=DatetimeTemplates.year_month_day,
                                   timezone="Europe/Vienna")
    settings = dataUploader.get_settings()

    # check method works
    assert settings["datetime_template"] == DatetimeTemplates.year_month_day
    assert settings["csv_decimal"] == "."
    assert settings["datetime_format"] is None
    assert settings["timezone"] == pytz.timezone("Europe/Vienna")
    assert settings["csv_separator"] == ";"
    assert settings["csv_encoding"] == "utf-8"
    assert settings["index_col"] == 0


@pytest.mark.parametrize('fn', ['csv1', "csv2", "csv3"])
def test_dataUploader_getIndexName(raw_plant, fn, get_csv):
    # read file
    bio = getBio(get_csv)

    # execution
    dataUploader = DataUploader_df(raw_plant, datetime_template=DatetimeTemplates.year_month_day,
                                   timezone="Europe/Vienna")
    index = dataUploader.get_index_name(files=[bio])

    # check method works
    expected_index = "timestamps_UTC"
    assert index == expected_index


@pytest.mark.parametrize('fn', ['csv1', "csv2", "csv3"])
def test_dataUploader_do_inspection(raw_plant, fn, get_csv):
    # read file
    bio = getBio(get_csv)

    # execution
    dataUploader = DataUploader_df(raw_plant, datetime_template=DatetimeTemplates.year_month_day,
                                   timezone=tz.TIMEZONE_LOCAL_NO_DST)
    df = dataUploader.do_inspection(files=[bio])

    # check method works
    assert isinstance(df, pd.DataFrame)


@pytest.mark.parametrize('fn', ['csv1'])
def test_dataUploader_do_inspection_wrongDatetime(raw_plant, fn, get_csv):
    bio = getBio(get_csv)
    with pytest.raises(DataProcessingError):
        dataUploader = DataUploader_df(raw_plant, datetime_format="%Y.%m.%d WRONG %H:%M",
                                       timezone=tz.TIMEZONE_LOCAL_NO_DST)
        dataUploader.do_inspection(files=[bio])


@pytest.mark.parametrize('fn', ['csv1'])
def test_dataUploader_do_inspection_unknownEncoding(raw_plant, fn, get_csv):
    bio = getBio(get_csv)
    with pytest.raises(ValueError):
        dataUploader = DataUploader_df(raw_plant, datetime_template=DatetimeTemplates.month_day_year,
                                       csv_encoding="undefined", timezone=tz.TIMEZONE_LOCAL_NO_DST)
        dataUploader.do_inspection(files=[bio])


@pytest.mark.parametrize('fn', ['csv1_latin1'])
def test_dataUploader_do_inspection_wrongEncoding(raw_plant, fn, get_csv):
    bio = getBio(get_csv)
    with pytest.raises(DataProcessingError):
        dataUploader = DataUploader_df(raw_plant, datetime_template=DatetimeTemplates.month_day_year,
                                       timezone=tz.TIMEZONE_LOCAL_NO_DST)
        dataUploader.do_inspection(files=[bio])


@pytest.mark.parametrize('uploader', [DataUploader_pq, DataUploader_df])
def test_dataUploader_parsesTimezone(raw_plant, uploader):
    for zone in [tz.TIMEZONE_LOCAL_WITH_DST, tz.TIMEZONE_NONE, tz.TIMEZONE_LOCAL_NO_DST, "UTC+10"]:
        dataUploader = uploader(raw_plant, datetime_format=DatetimeTemplates.year_month_day, timezone=zone)
        timezone = dataUploader.timezone
        assert timezone == tz.process_timezone(zone, plant=raw_plant)


@pytest.mark.parametrize('fn', ['no_sensors'])
def test_no_valid_sensors__raise_pq(raw_plant, fn, get_csv):
    # Test for https://gitlab.com/sunpeek/sunpeek/-/issues/525
    with pytest.raises(ValueError, match='Uploaded file contains no data columns that match with sensor names'):
        up = DataUploader_pq(plant=raw_plant,
                             timezone='UTC',
                             datetime_template=DatetimeTemplates.year_month_day,
                             on_file_error='raise')
        up.do_upload(files=get_csv, calculate_virtuals=False)


@pytest.mark.parametrize('fn', ['no_sensors'])
def test_no_valid_sensors__report_pq(raw_plant, fn, get_csv):
    # Test for https://gitlab.com/sunpeek/sunpeek/-/issues/525
    up = DataUploader_pq(plant=raw_plant,
                         timezone='UTC',
                         datetime_template=DatetimeTemplates.year_month_day)
    up.do_upload(files=get_csv, calculate_virtuals=False)


@pytest.mark.parametrize('fn', ['no_sensors'])
def test_no_valid_sensors__raise(raw_plant, fn, get_csv):
    # Test for https://gitlab.com/sunpeek/sunpeek/-/issues/525
    with pytest.raises(ValueError, match='Uploaded file contains no data columns that match with sensor names'):
        use_csv(raw_plant, get_csv, timezone='UTC', calculate_virtuals=False,
                datetime_template=DatetimeTemplates.year_month_day,
                on_file_error='raise')


@pytest.mark.parametrize('fn', ['no_sensors'])
def test_no_valid_sensors__report(raw_plant, fn, get_csv):
    # Test for https://gitlab.com/sunpeek/sunpeek/-/issues/525
    use_csv(raw_plant, get_csv, timezone='UTC', calculate_virtuals=False,
            datetime_template=DatetimeTemplates.year_month_day)

    assert raw_plant.context.df is None


# Test delete data with both dataframe and parquet datasources  -------------------
# see also integration tests in test_files_api.py

@pytest.fixture(params=[  # datasource
    'df',
    'pq',
])
def plant_for_delete_data(request, raw_plant):
    # Upload data using specified datasource
    p = raw_plant
    up_class = DataUploader_df if request.param == 'df' else DataUploader_pq
    up = up_class(plant=p, timezone='UTC', datetime_template=DatetimeTemplates.year_month_day)
    up.do_upload(files=sunpeek.demo.DEMO_DATA_PATH_2DAYS, calculate_virtuals=True)

    # Assert data before deletion
    df = p.te_amb.data
    print(f"Data uploaded. {df.index[0]} --> {df.index[-1]}")

    t = pd.to_datetime("2017-05-01 12:00:00", utc=True)
    print(f"- Assert has data: {utc_str(t)}")
    assert t in df.index
    assert not np.isnan(df[t].m)

    return p


def test_delete_data__in_between(plant_for_delete_data):
    # Delete some data in the middle of available data
    plant = plant_for_delete_data
    plant.context.delete_sensor_data(start=pd.to_datetime("2017-05-01 10:00:00", utc=True),
                                     end=pd.to_datetime("2017-05-01 14:00:00", utc=True))

    # Data outside deleted period ok?
    t_ok = ["2017-05-01 09:59:00", "2017-05-01 14:01:00"]
    for t_ in t_ok:
        t = pd.to_datetime(t_, utc=True)
        print(f"- Assert has data: {utc_str(t)}.")
        assert t in plant.time_index
        assert not np.isnan(plant.te_amb.data.loc[t].m)

    # Data actually deleted?
    t_deleted = ["2017-05-01 10:00:00", "2017-05-01 12:00:00", "2017-05-01 14:00:00"]
    for t_ in t_deleted:
        t = pd.to_datetime(t_, utc=True)
        print(f"- Assert has NO data: {utc_str(t)}.")
        assert t not in plant.time_index

    # Data start & end correct?
    expected = pd.to_datetime("2017-04-30 23:00:00", utc=True)
    assert plant.time_index[0] == expected
    expected = pd.to_datetime("2017-05-02 22:59:00", utc=True)
    assert plant.time_index[-1] == expected


@pytest.mark.parametrize("start, end, t_ok, t_deleted, expected", [
    ("2017-05-01 10:00:00", "2017-05-03 14:00:00",
     ["2017-05-01 09:59:00", "2017-04-30 23:59:00"],  # t_ok
     ["2017-05-01 10:00:00", "2017-05-01 10:01:00", "2017-05-02 22:59:00"],  # t_deleted
     ["2017-04-30 23:00:00", "2017-05-01 09:59:00"],  # expected start end
     ),
    ("2017-04-01 10:00:00", "2017-05-01 10:00:00",
     ["2017-05-01 10:01:00"],
     ["2017-05-01 09:59:00"],
     ["2017-05-01 10:01:00", "2017-05-02 22:59:00"],
     ),
])  # all timestamps interpreted as UTC
def test_delete_data__overlapping_start_end(plant_for_delete_data, start, end, t_ok, t_deleted, expected):
    # Delete some data overlapping with start / end of available data
    plant = plant_for_delete_data
    plant.context.delete_sensor_data(start=pd.to_datetime(start, utc=True),
                                     end=pd.to_datetime(end, utc=True))

    # Data outside deleted period ok?
    for t_ in t_ok:
        t = pd.to_datetime(t_, utc=True)
        print(f"- Assert has data: {utc_str(t)}.")
        assert t in plant.time_index
        assert not np.isnan(plant.te_amb.data.loc[t].m)

    # Data actually deleted?
    for t_ in t_deleted:
        t = pd.to_datetime(t_, utc=True)
        print(f"- Assert has NO data: {utc_str(t)}.")
        assert t not in plant.time_index

    # Data start & end correct?
    assert plant.time_index[0] == pd.to_datetime(expected[0], utc=True)
    assert plant.time_index[-1] == pd.to_datetime(expected[1], utc=True)


@pytest.mark.parametrize("start, end", [
    ("2017-04-30 23:00:00", "2017-05-02 22:59:00"),  # exact start & end dates of data
    ("2017-01-01 00:00:00", "2017-05-02 22:59:00"),  # starting earlier
    ("2017-04-30 23:00:00", "2017-05-03 22:59:00"),  # ending later
    ("2017-01-01 00:00:00", "2018-01-01 00:00:00"),  # starting earlier, ending later
])  # all timestamps interpreted as UTC
def test_delete_data__all(plant_for_delete_data, start, end):
    # Delete all data
    plant = plant_for_delete_data
    plant.context.delete_sensor_data(start=pd.to_datetime(start, utc=True),
                                     end=pd.to_datetime(end, utc=True))

    assert plant.time_index is None


def test_delete_data__inverted_fails(plant_for_delete_data):
    plant = plant_for_delete_data
    start, end = ("2018-01-01 00:00:00", "2017-01-01 00:00:00")
    with pytest.raises(ValueError, match='Timestamp "start" must be equal or less than "end"'):
        plant.context.delete_sensor_data(start=pd.to_datetime(start, utc=True),
                                         end=pd.to_datetime(end, utc=True))

from sunpeek.common.unit_uncertainty import Q
from sunpeek.components.components_factories import CollectorSST, CollectorQDT, Collector, CollectorTypes
from sunpeek.components.iam_methods import IAM_Interpolated


def test_SST_Collector():
    # using 011-7S2688 F
    # https://www.dincertco.tuv.com/registrations/60115007?locale=en
    iam_method = IAM_Interpolated(
        aoi_reference=Q([10, 20, 30, 40, 50, 60, 70, 80, 90], 'deg'),
        iam_reference=Q([
            [1.00, 1.00, 1.00, 0.99, 0.97, 0.91, 0.75, 0.42, 0.0],
            [1.00, 1.00, 1.00, 0.99, 0.98, 0.94, 0.84, 0.59, 0.0],
        ]),
    )
    col = CollectorSST(
        test_reference_area="gross",
        eta0hem=Q(0.812),
        a1=Q(2.936, "W/(m².K)"),
        a2=Q(0.009, "W/(m²K²)"),
        a8=Q(0, 'W m**-2 K**-4'),
        ceff=Q(10.2, "kJ/(K.m²)"),
        iam_method=iam_method,
        gross_length=Q(2591, "mm"),
        gross_width=Q(6158, "mm"),
        gross_height=Q(213, "mm"),
        name="SF500-15",
        area_gr=Q(15.96, "m²"),
        licence_number="011-7S2688 F",
        collector_type=CollectorTypes.flat_plate.value
    )
    assert isinstance(col, Collector)
    assert col.test_type == "SST"
    assert col.eta0hem == Q(0.812)


def test_QDT_Collector_K5GigaPlus():
    # using: 011-7S2437
    iam_method = IAM_Interpolated(
        aoi_reference=Q([10, 20, 30, 40, 50, 60, 70, 80, 90], 'deg'),
        iam_reference=Q([1.0, 1.0, 0.99, 0.98, 0.94, 0.83, 0.56, 0.28, 0.0]),
    )
    col = CollectorQDT(
        name="K5Giga+",
        manufacturer_name="KBB Kollektorbau GmbH",
        licence_number="011-7S2437",
        area_gr=Q(12.42, "m²"),
        gross_width=Q(5750, "mm"),
        gross_length=Q(2160, "mm"),
        gross_height=Q(119, "mm"),
        eta0b=Q(0.752),
        a1=Q(2.416, "W/(m².K)"),
        a2=Q(0.008, "W/(m².K²)"),
        a5=Q(8.3, "kJ/(K.m²)"),
        a8=Q(0, 'W m**-2 K**-4'),
        kd=Q(0.964),
        test_reference_area="gross",
        iam_method=iam_method,
        collector_type=CollectorTypes.flat_plate.value
    )
    assert isinstance(col, Collector)
    assert col.test_type == "QDT"
    assert col.eta0b == Q(0.752)


import pytest
import numpy as np
from numpy.testing import assert_array_almost_equal

from sunpeek.common.unit_uncertainty import Q, to_s, to_numpy
from sunpeek.components.iam_methods import IAM_Interpolated, IAM_ASHRAE, IAM_K50
from sunpeek.components.physical import Array
from sunpeek.components.components_factories import CollectorQDT, CollectorTypes

def to_numpy_(s):
    return to_numpy(s, '')


# ------------
# TEST ASHRAE
# ------------


def test_iam_ashrae_aoi_90deg():
    b0 = Q(0.05)
    aoi = Q(90, "deg")
    iam_method = IAM_ASHRAE(b=b0)
    iam_res = iam_method.get_iam(aoi=aoi)
    expected_iam = 0
    assert to_numpy_(iam_res) == pytest.approx(expected_iam)


def test_iam_ashrae_aoi_0deg():
    b0 = Q(0.05)
    aoi = Q(0, "deg")
    iam_method = IAM_ASHRAE(b=b0)
    iam_res = iam_method.get_iam(aoi=aoi)
    expected_iam = 1
    assert to_numpy_(iam_res) == pytest.approx(expected_iam)


# ------------
# TEST ASHRAE (with k50)
# ------------

def test_iam_k50_aoi_90deg():
    k50 = Q(0.94)
    angle_of_incidence = Q(90, "deg")
    iam_method = IAM_K50(k50=k50)
    iam_res = iam_method.get_iam(aoi=angle_of_incidence)
    expected_iam = 0
    assert to_numpy_(iam_res) == pytest.approx(expected_iam)


def test_iam_k50_aoi_0deg():
    k50 = Q(0.94)
    angle_of_incidence = Q(0, "deg")
    iam_method = IAM_K50(k50=k50)
    iam_res = iam_method.get_iam(aoi=angle_of_incidence)
    expected_iam = 1
    assert to_numpy_(iam_res) == pytest.approx(expected_iam)


def test_iam_k50_aoi_50deg():
    k50 = 0.94
    angle_of_incidence = Q(50, "deg")
    iam_method = IAM_K50(k50=Q(k50))
    iam_res = iam_method.get_iam(aoi=angle_of_incidence)
    expected_iam = k50
    assert to_numpy_(iam_res) == pytest.approx(expected_iam)


def test_iam_k50_aoi_k5Giga():
    # using K5 giga as example. However, curves do not align well for aoi>50°
    k50 = 0.94
    angle_of_incidence = Q([0, 10, 20, 30, 40, 50], 'deg')
    expected_iam = Q([1.00, 1.00, 1.00, 0.99, 0.98, 0.94])
    iam_method = IAM_K50(k50=Q(k50))
    iam_res = iam_method.get_iam(aoi=angle_of_incidence)
    assert to_numpy_(iam_res) == pytest.approx(to_numpy_(expected_iam), 0.02)


# -----------------
# TEST interpolated
# -----------------

# add boundaries

from sunpeek.components.iam_methods import _add_boundaries


def test_iam_interpolated_add_boundaries_works():
    aoi_reference = [10, 20, 30]
    iam_reference = [0.5, 0.5, 0.5]
    aoi, iam = _add_boundaries(aoi_reference=aoi_reference, iam_reference=iam_reference)
    assert aoi == [0] + aoi_reference + [90]
    assert iam == [1.00] + iam_reference + [0.00]


def test_iam_interpolated_add_boundaries_is_applied():
    aoi_reference = Q([10, 20, 30], 'deg')
    iam_reference = Q([0.5, 0.5, 0.5])
    method = IAM_Interpolated(aoi_reference=aoi_reference, iam_reference=iam_reference)
    assert 1.00 in method.iam_reference[0]
    assert 0.00 in method.iam_reference[0]
    assert Q(90, 'deg') in method.aoi_reference[0]
    assert Q(0, 'deg') in method.aoi_reference[0]
    assert 1.00 in method.iam_reference[1]
    assert 0.00 in method.iam_reference[1]
    assert Q(90, 'deg') in method.aoi_reference[1]
    assert Q(0, 'deg') in method.aoi_reference[1]


# to 2D

from sunpeek.components.iam_methods import _to_2D_list


def test_iam_interpolated_to_2D():
    aoi_reference = [10, 20, 30]
    iam_reference = [0.5, 0.5, 0.5]
    aoi, iam = _to_2D_list(aoi_reference=aoi_reference, iam_reference=iam_reference)
    assert aoi == [aoi_reference, aoi_reference]
    assert iam == [iam_reference, iam_reference]


def test_iam_interpolated_to_2D_numpy():
    aoi_reference = [10, 20, 30]
    iam_reference = [0.5, 0.5, 0.5]
    aoi, iam = _to_2D_list(aoi_reference=np.array(aoi_reference), iam_reference=np.array(iam_reference))
    assert aoi == [aoi_reference, aoi_reference]
    assert iam == [iam_reference, iam_reference]


def test_iam_interpolated_to_2D_only_aoi():
    aoi_reference = [10, 20, 30]
    iam_reference = [[0.5, 0.5, 0.5], [0.5, 0.5, 0.5]]
    aoi, iam = _to_2D_list(aoi_reference=aoi_reference, iam_reference=iam_reference)
    assert aoi == [aoi_reference, aoi_reference]
    assert iam == iam_reference


def test_iam_interpolated_to_2D_no_change_if_2D():
    aoi_reference = [[10, 20, 30], [10, 20, 30]]
    iam_reference = [[0.5, 0.5, 0.5], [0.5, 0.5, 0.5]]
    aoi, iam = _to_2D_list(aoi_reference=aoi_reference, iam_reference=iam_reference)
    assert aoi == aoi_reference
    assert iam == iam_reference


# test angle conversion

from sunpeek.components.iam_methods import _get_aoi_longitudinal, _get_aoi_transversal


def test_get_theta_longitudinal_g0():
    aoi = np.linspace(0, 90, 90, endpoint=False)
    gamma = 0
    res = _get_aoi_longitudinal(aoi=aoi, azimuth_diff=gamma)
    assert_array_almost_equal(0, res)


def test_get_theta_longitudinal_g90():
    aoi = np.linspace(0, 90, 90, endpoint=False)
    gamma = 90
    res = _get_aoi_longitudinal(aoi=aoi, azimuth_diff=gamma)
    assert_array_almost_equal(aoi, res)


def test_get_theta_longitudinal_g180():
    aoi = np.linspace(0, 90, 90, endpoint=False)
    gamma = 180
    res = _get_aoi_longitudinal(aoi=aoi, azimuth_diff=gamma)
    assert_array_almost_equal(0, res)


def test_get_theta_longitudinal_g270():
    aoi = np.linspace(0, 90, 90, endpoint=False)
    gamma = 270
    res = _get_aoi_longitudinal(aoi=aoi, azimuth_diff=gamma)
    assert_array_almost_equal(aoi, res)


def test_get_theta_transversal_g0():
    aoi = np.linspace(0, 90, 90, endpoint=False)
    gamma = 0
    res = _get_aoi_transversal(aoi=aoi, azimuth_diff=gamma)
    assert_array_almost_equal(aoi, res)


def test_get_theta_transversal_g90():
    aoi = np.linspace(0, 90, 90, endpoint=False)
    gamma = 90
    res = _get_aoi_transversal(aoi=aoi, azimuth_diff=gamma)
    assert_array_almost_equal(0, res)


def test_get_theta_transversal_g180():
    aoi = np.linspace(0, 90, 90, endpoint=False)
    gamma = 180
    res = _get_aoi_transversal(aoi=aoi, azimuth_diff=gamma)
    assert_array_almost_equal(aoi, res)


def test_get_theta_transversal_g270():
    aoi = np.linspace(0, 90, 90, endpoint=False)
    gamma = 270
    res = _get_aoi_transversal(aoi=aoi, azimuth_diff=gamma)
    assert_array_almost_equal(0, res)


# get iam from interpolated values

def test_iam_interpolated_get_iam_0degree():
    aoi_array = Q([0, 10, 20, 30], 'deg')
    iam_array = Q([1, 1, 0.8, 0.3])
    iam_method = IAM_Interpolated(aoi_reference=aoi_array, iam_reference=iam_array)

    aoi = to_s(0, 'deg')
    azimuth_diff = to_s(0, 'deg')
    result = iam_method.get_iam(aoi=aoi, azimuth_diff=azimuth_diff)
    expected_res = 1

    assert to_numpy_(result) == pytest.approx(expected_res)


def test_iam_interpolated_get_iam_inbetween():
    aoi_array = Q([0, 10], 'deg')
    iam_array = Q([1, 0.8])
    iam_method = IAM_Interpolated(aoi_reference=aoi_array, iam_reference=iam_array)

    aoi = to_s(5, 'deg')
    result = iam_method.get_iam(aoi=aoi, azimuth_diff=to_s(0, 'deg'))
    expected_res = 0.9

    assert to_numpy_(result) == pytest.approx(expected_res)


def test_iam_interpolated_get_iam_out_of_bounds_max():
    aoi_array = Q([10, 20], 'deg')
    iam_array = Q([1, 0.8])
    iam_method = IAM_Interpolated(aoi_reference=aoi_array, iam_reference=iam_array)

    aoi = to_s(0, 'deg')
    result = iam_method.get_iam(aoi=aoi, azimuth_diff=to_s(0, 'deg'))
    expected_res = 1

    assert to_numpy_(result) == pytest.approx(expected_res)


def test_iam_interpolated_get_iam_out_of_bounds_min():
    aoi_array = Q([10, 20], 'deg')
    iam_array = Q([1, 0.8])
    iam_method = IAM_Interpolated(aoi_reference=aoi_array, iam_reference=iam_array)

    aoi = to_s(90, 'deg')
    result = iam_method.get_iam(aoi=aoi, azimuth_diff=to_s(0, 'deg'))
    expected_res = 0

    assert to_numpy_(result) == pytest.approx(expected_res)


def test_iam_interpolated_get_iam_array():
    aoi_array = Q(np.array([10, 20, 30, 40, 50, 60, 70, 80]), 'deg')
    iam_array = Q(np.array([1.00, .99, .98, .96, .91, .82, .53, .27]))
    iam_method = IAM_Interpolated(aoi_reference=aoi_array, iam_reference=iam_array)

    aoi = to_s(aoi_array, 'deg')
    result = iam_method.get_iam(aoi=aoi, azimuth_diff=to_s(0, 'deg'))
    expected_res = to_s(iam_array)

    assert to_numpy_(result) == pytest.approx(to_numpy_(expected_res))


def test_iam_interpolated_get_iam_independent_of_gamma_if_symmetric():
    aoi_array = Q(np.array([
        [10, 20, 30, 40, 50, 60, 70],
        [10, 20, 30, 40, 50, 60, 70],
    ]), 'deg')
    iam_array = Q(np.array([
        [1.00, 1.00, 0.99, 0.97, 0.94, 0.86, 0.72],
        [1.00, 1.00, 0.99, 0.97, 0.94, 0.86, 0.72]
    ]))
    iam_method = IAM_Interpolated(aoi_reference=aoi_array, iam_reference=iam_array)

    aoi = to_s([50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50], 'deg')
    azimuth_diff = to_s([10, 20, 40, 60, 80, 90, 100, 110, 120, 130, 200, 350], 'deg')
    expected_res = to_s([0.94, 0.94, 0.94, 0.94, 0.94, 0.94, 0.94, 0.94, 0.94, 0.94, 0.94, 0.94])
    result = iam_method.get_iam(aoi=aoi, azimuth_diff=azimuth_diff)

    assert to_numpy_(result) == pytest.approx(to_numpy_(expected_res), 0.01)


def test_iam_interpolated_get_iam_independent_of_gamma_if_1d():
    aoi_array = Q([10, 20, 30, 40, 50, 60, 70], 'deg')
    iam_array = Q([1.00, 1.00, 0.99, 0.97, 0.94, 0.86, 0.72])
    iam_method = IAM_Interpolated(aoi_reference=aoi_array, iam_reference=iam_array)

    aoi = to_s([50, 50, 50, 50, 50, 50], 'deg')
    azimuth_diff = to_s([10, 20, 40, 60, 80, 90], 'deg')
    expected_res = [0.94, 0.94, 0.94, 0.94, 0.94, 0.94]
    result = iam_method.get_iam(aoi=aoi, azimuth_diff=azimuth_diff)

    assert to_numpy_(result) == pytest.approx(expected_res, 0.01)


# Test in combination with CollectorArray Components

@pytest.fixture
def example_array():
    iam_method = IAM_Interpolated(
        aoi_reference=Q([10, 20, 30, 40, 50, 60, 70, 80, 90], 'deg'),
        iam_reference=Q([1., 0.99, 0.98, 0.96, 0.91, 0.82, 0.53, 0.27, 0.]),
    )
    collector = CollectorQDT(eta0b=Q(0.814),
                             a1=Q(2.102, "W/m²/K"),
                             a2=Q(0.016, "W/m²/K²"),
                             a5=Q(9.664, "kJ/(m².K)"),
                             a8=Q(0, 'W m**-2 K**-4'),
                             kd=Q(0.931),
                             iam_method=iam_method,
                             area_ap=Q(12.35, "m²"),
                             area_gr=Q(13.17, "m²"),
                             gross_length=Q(2.224, "m"),
                             test_reference_area="gross",
                             collector_type=CollectorTypes.flat_plate.value
                             )
    array = Array(collector=collector, area_gr=Q(10, "m²"))

    return array


def test_get_kb_with_collector(example_array):
    aoi = Q(30, 'deg')
    result = example_array.collector.iam_method.get_iam(aoi=aoi, azimuth_diff=to_s(0, 'deg'))
    expected_res = .98

    assert to_numpy_(result) == pytest.approx(expected_res)


def test_get_kb_with_array_with_collector(example_array):
    aoi = Q(np.array([10, 20, 30, 40, 50, 60, 70, 80]), 'deg')
    expected_res = Q(np.array([1.00, .99, .98, .96, .91, .82, .53, .27]))
    result = example_array.collector.iam_method.get_iam(aoi=aoi, azimuth_diff=to_s(0, 'deg'))

    assert to_numpy_(result) == pytest.approx(to_numpy_(expected_res))

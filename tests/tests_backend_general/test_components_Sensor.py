import pytest
import pandas as pd
import datetime as dt

from sunpeek.common.unit_uncertainty import Q
import sunpeek.components as cmp
from sunpeek.components.helpers import AccuracyClass
from sunpeek.components import sensor_types as st
import sunpeek.common.errors


# --------------------------
# Tests for Setup
# --------------------------

def test_all_sensor_types():
    # Test that sensor types have required fields (not None, not '')
    for t in st.all_sensor_types:
        assert t.name
        assert t.compatible_unit_str
        assert t.description


def test_sensor_types():
    assert len(st.all_sensor_types) >= 20
    assert isinstance(st.ambient_temperature, cmp.SensorType)


def test_sensor_type_common_units():
    assert st.ambient_temperature.common_units == ["°C", "K", "°F"]
    assert st.direct_radiation.common_units == ["W/m²", "W/ft²", "kW/m²", "kW/ft²", "MW/m²", "MW/ft²", "GW/m²",
                                                "GW/ft²", "BTU/h/m²", "BTU/h/ft²"]
    assert st.thermal_power.common_units == ["W", "kW", "MW", "GW", "BTU/h"]


def test_sensor_creation():
    s = cmp.Sensor(raw_name='s_amb', native_unit='degC', accuracy_class='medium',
                   installation_condition='perfect')
    assert s.raw_name == 's_amb'
    assert s.native_unit == 'degree_Celsius'
    assert s.accuracy_class == AccuracyClass['medium']
    assert s.installation_condition == 'perfect'
    assert s._accuracy_class == AccuracyClass['medium']

    # mixed case name
    s = cmp.Sensor(raw_name='tAmb', native_unit='degC')
    assert s.raw_name == 'tAmb'


def test_map_sensor():
    plant = cmp.Plant("Test", latitude=Q(47.0471, 'deg'), longitude=Q(15.43736, 'deg'))
    s1 = cmp.Sensor(raw_name='s_amb', native_unit='degC', accuracy_class='medium',
                    installation_condition='perfect')
    s2 = cmp.Sensor(raw_name='tp', native_unit='kW', accuracy_class='medium', installation_condition='perfect')

    cmp.SensorMap('te_amb', s1, 'ambient_temperature', component=plant)
    cmp.SensorMap('tp', s2, 'thermal_power', component=plant)

    assert plant.te_amb is not None
    assert plant.tp is not None
    assert plant.sensors['te_amb'].native_unit == 'degree_Celsius'
    assert plant.sensors['tp'].raw_name == 'tp'
    assert plant.sensors['te_amb'].sensor_type == st.ambient_temperature


def test_cant_set_invalid_sensor_type():
    plant = cmp.Plant("Test", latitude=Q(47.0471, 'deg'), longitude=Q(15.43736, 'deg'))
    s1 = cmp.Sensor(raw_name='s_amb', native_unit='degC', accuracy_class='medium',
                    installation_condition='perfect')
    cmp.SensorMap('te_amb', s1, 'ambient_temperature', component=plant)
    assert plant.sensors['te_amb'].sensor_type == st.ambient_temperature
    with pytest.raises(sunpeek.common.errors.ConfigurationError, match='Cannot change the "sensor_type" of a mapped sensor. Mapped sensors get their sensor type from the component slot they are mapped to.'):
        plant.sensors['te_amb'].sensor_type = st.fluid_temperature


def test_cant_set_invalid_unit():
    plant = cmp.Plant("Test", latitude=Q(47.0471, 'deg'), longitude=Q(15.43736, 'deg'))
    s1 = cmp.Sensor(raw_name='s_amb', native_unit='degC', accuracy_class='medium',
                    installation_condition='perfect')
    cmp.SensorMap('te_amb', s1, 'ambient_temperature', component=plant)

    assert plant.sensors['te_amb'].native_unit == 'degree_Celsius'

    with pytest.raises(ValueError, match='not compatible with sensor_type unit'):
        plant.sensors['te_amb'].native_unit = 'watt'

# def test_cant_set_invalid_unit_except_for_none():
#     plant = cmp.Plant("Test", latitude=Q(47.0471, 'deg'), longitude=Q(15.43736, 'deg'))
#     s1 = cmp.Sensor(raw_name='s_amb', native_unit='degC', accuracy_class='medium',
#                     installation_condition='perfect')
#     cmp.SensorMap('te_amb', s1, 'ambient_temperature', component=plant)
#     assert plant.sensors['te_amb'].native_unit == 'degree_Celsius'
#     plant.sensors['te_amb'].native_unit = None
#     assert plant.sensors['te_amb'].native_unit is None


def test_can_set_same_unit_when_mapped():
    plant = cmp.Plant("Test", latitude=Q(47.0471, 'deg'), longitude=Q(15.43736, 'deg'))
    s1 = cmp.Sensor(raw_name='s_amb', native_unit='degC', accuracy_class='medium',
                    installation_condition='perfect')
    cmp.SensorMap('te_amb', s1, 'ambient_temperature', component=plant)
    assert plant.sensors['te_amb'].native_unit == 'degree_Celsius'
    plant.sensors['te_amb'].native_unit = "degree_Celsius"
    assert plant.sensors['te_amb'].native_unit == 'degree_Celsius'


def test_can_set_valid_sensor_type():
    plant = cmp.Plant("Test", latitude=Q(47.0471, 'deg'), longitude=Q(15.43736, 'deg'))
    s1 = cmp.Sensor(raw_name='s_amb', native_unit='degC', accuracy_class='medium',
                    installation_condition='perfect')
    cmp.SensorMap('te_amb', s1, 'ambient_temperature', component=plant)
    assert plant.sensors['te_amb'].sensor_type == st.ambient_temperature
    plant.sensors['te_amb'].sensor_type = st.ambient_temperature


def test_sensor_creation2():
    sensor = cmp.Sensor(raw_name='leistung', native_unit='kW', installation_condition='good')


def test_sensor_formatted_unit():
    sensor = cmp.Sensor(raw_name='sensor_name', native_unit='kW')
    assert sensor.formatted_unit == "kW"

    sensor = cmp.Sensor(raw_name='sensor_name', native_unit="W / m**2")
    assert sensor.formatted_unit == "W/m²"

    sensor = cmp.Sensor(raw_name='sensor_name', native_unit=None)
    assert sensor.formatted_unit is None

    sensor = cmp.Sensor(raw_name='sensor_name', native_unit="")
    assert sensor.formatted_unit == "None"

    sensor = cmp.Sensor(raw_name='sensor_name', native_unit="dimensionless")
    assert sensor.formatted_unit == "None"


def test_native_unit_due_to_mapping():
    plant = cmp.Plant("Test", latitude=Q(47.0471, 'deg'), longitude=Q(15.43736, 'deg'))
    sensor = cmp.Sensor(raw_name='sensor_name', native_unit=None)
    assert sensor.formatted_unit is None

    cmp.SensorMap('te_amb', sensor, 'ambient_temperature', component=plant)
    assert sensor.formatted_unit == "°C"



@pytest.mark.parametrize('tilt, azim', [(Q(35, 'deg'), Q(10, 'deg')), ([35, 'deg'], [10, 'deg'])])
def test_sensor_info(tilt, azim):
    example_plant = cmp.Plant('Test')
    rd_gti = cmp.Sensor(raw_name='rd_gti', native_unit='W/m**2', info={'tilt': tilt, 'azim': azim},
                        plant=example_plant)
    example_plant.set_sensors(in_global=rd_gti)
    assert isinstance(example_plant.in_global.info['tilt'], Q)
    assert example_plant.in_global.info['tilt'].magnitude == 35


def test_sensor_is_assigned_plant_on_map():
    example_plant = cmp.Plant('Test Plant')
    example_array = cmp.Array('Test Array')
    example_plant.arrays.append(example_array)

    sensor = cmp.Sensor(raw_name='leistung', native_unit='kW', installation_condition='good')
    assert sensor.plant is None

    cmp.SensorMap('tp', sensor, 'thermal_power', example_array)

    assert sensor.plant == example_plant


def test_is_mapped():
    example_plant = cmp.Plant('Test Plant')
    s = cmp.Sensor('Test Sensor', 'K')
    assert not s.is_mapped
    s.plant = example_plant
    assert not s.is_mapped
    cmp.SensorMap('te_amb', s, 'ambient_temperature', example_plant)
    assert s.is_mapped


def test_sensor_map_removed_from_sensor_mappings_on_unmap():
    p = cmp.Plant('Test')
    assert p.te_amb is None

    t2 = cmp.Sensor('Test sensor 2', 'K')
    p.te_amb = t2
    assert len(t2.mappings) == 1

    p.te_amb = None
    assert len(t2.mappings) == 0


def test_given_sensor_type_retained_after_unmap():
    p = cmp.Plant('Test')
    assert p.te_amb is None

    t2 = cmp.Sensor('Test sensor 2', 'K', sensor_type=st.ambient_temperature)
    assert t2.sensor_type == st.ambient_temperature

    p.te_amb = t2
    assert t2.sensor_type == st.ambient_temperature

    p.te_amb = None
    assert t2.sensor_type == st.ambient_temperature


def test_mapping_sensor_does_not_fix_sensor_type_after_unmap():
    p = cmp.Plant('Test')
    assert p.te_amb is None

    t2 = cmp.Sensor('Test sensor 2', 'K')
    assert t2.sensor_type is None

    p.te_amb = t2
    assert t2.sensor_type == st.ambient_temperature

    p.sensor_map = {'te_amb': None}
    assert t2.sensor_type is None

    p.sensor_map = {'te_in': 'Test sensor 2'}
    assert t2.sensor_type == st.fluid_temperature


def test_duplicate_sensor_names_not_allowed():
    p = cmp.Plant('Test')

    t1 = cmp.Sensor('Test sensor 1', 'K')
    p.map_sensor(t1, 'te_amb')

    t2 = cmp.Sensor('test sensor 1', 'K')

    with pytest.raises(sunpeek.common.errors.DuplicateNameError):
        p.map_sensor(t2, 'te_dew_amb')


def test_native_unit_from_type():
    s = cmp.Sensor('test', None)
    s.sensor_type = st.ambient_temperature
    assert s.native_unit == Q(1, 'degC')


def test_mapping_sensors_combines_plant_map_sensor():
    p = cmp.Plant('Test')
    s = cmp.Sensor('Test sensor 1', 'K')

    # initial
    assert s not in p.raw_sensors
    assert s.plant is None
    assert "te_amb" not in p.sensors
    assert "te_amb" not in p.sensor_map

    p.map_sensor(s, 'te_amb')

    # sensor mapping combines sensor, plant, and mapping
    assert s in p.raw_sensors
    assert s.plant == p
    assert p.sensors["te_amb"] == s
    assert p.sensor_map["te_amb"].sensor == s


def test_delete_sensor_via_plant_removes_mapping():
    p = cmp.Plant('Test')
    s = cmp.Sensor('Test sensor 1', 'K')
    p.map_sensor(s, 'te_amb')

    # sensor mapping combines sensor, plant, and mapping
    assert s in p.raw_sensors
    assert s.plant == p
    assert p.sensors["te_amb"] == s
    assert p.sensor_map["te_amb"].sensor == s

    # removing sensor (deleting from plant) reverts that
    p.raw_sensors.remove(s)

    assert s not in p.raw_sensors
    assert s.plant is None
    assert "te_amb" not in p.sensors
    assert "te_amb" not in p.sensor_map


def test_sensor_remove_references():
    p = cmp.Plant('Test')
    s = cmp.Sensor('Test sensor 1', 'K')
    p.map_sensor(s, 'te_amb')

    # sensor mapping combines sensor, plant, and mapping
    assert s in p.raw_sensors
    assert s.plant == p
    assert p.sensors["te_amb"] == s
    assert p.sensor_map["te_amb"].sensor == s

    # removing_references reverts that
    s.remove_references()

    assert s not in p.raw_sensors
    assert s.plant is None
    assert "te_amb" not in p.sensor_map
    assert "te_amb" not in p.sensors


def test_sensor_data():
    p = cmp.Plant("Test", latitude=Q(45,'°'), longitude=Q(15,'°'))
    s = cmp.Sensor("te_amb", '°C')
    p.map_sensor(s, 'te_amb')

    df = pd.DataFrame(columns=["te_amb"], index=[dt.datetime(2022, 1, 1, minute=0), dt.datetime(2022, 1, 1, minute=0), dt.datetime(2022, 1, 1, minute=1)], data=[0, 1, 2])
    df.index = pd.to_datetime(df.index, utc=True)

    p.context._datasource = 'df'
    p.context._df = df

    assert len(s.data) == 2




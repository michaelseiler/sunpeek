import pint
import pytest

from sunpeek.common.unit_uncertainty import Q
import sunpeek.components as cmp
from sunpeek.components import Array, Collector, CollectorTypes
import sunpeek.components.iam_methods as iam


@pytest.fixture()
def collector_static():
    return Collector(
        test_reference_area="gross",
        test_type='SST',
        eta0hem=Q(0.81),
        a1=Q(2.71, "W m**-2 K**-1"),
        a2=Q(0.01, "W m**-2 K**-2"),
        a5=Q(14.7, "kJ m**-2 K**-1"),
        a8=Q(0, 'W m**-2 K**-4'),
        kd=Q(0.983),
        gross_length=Q(1, "m"),
        area_gr=Q(3, 'm**2'),
        name="Ökotech HT 2009 Spezial 2",
        iam_method=iam.IAM_K50(k50=Q(0.97)),
        collector_type=CollectorTypes.flat_plate.value
    )


@pytest.fixture
def collector_dynamic():
    return Collector(name='configured_collector',
                     gross_length=Q(2.3, 'm'),
                     test_type='QDT',
                     test_reference_area='gross',
                     area_gr=Q(5, 'm**2'),
                     a1=Q(2.3, 'W m**-2 K**-1'),
                     a2=Q(0.01, 'W m**-2 K**-2'),
                     a5=Q(7500, 'J m**-2 K**-1'),
                     a8=Q(0, 'W m**-2 K**-4'),
                     eta0b=Q(0.81, ''),
                     kd=Q(0.93, ''),
                     iam_method=iam.IAM_ASHRAE(b=Q(0.15)),
                     collector_type=CollectorTypes.flat_plate.value
                     )


@pytest.fixture()
def example_array(collector_static):
    return Array(collector=collector_static, area_gr=Q(100, "m²"), tilt=Q(0, "deg"), azim=Q(180, "deg"))


# TESTS


def test_array_exists(collector_dynamic):
    Array(collector=collector_dynamic, area_gr=Q(100, "m²"), tilt=Q(0))
    return


def test_array_defaults(collector_dynamic):
    a = Array(collector=collector_dynamic, area_gr=Q(100, "m²"), tilt=Q(0))

    assert a.sensor_map == {}
    assert a.ground_tilt.m == 0
    assert a.mounting_level.m == 0
    assert a.rho_colsurface.m == 0
    assert a.max_aoi_shadow.m == 80


def test_array_has_required_attributes(example_array):
    assert example_array.tilt is not None
    assert example_array.azim is not None


def test_array_has_required_sensor_names(example_array):
    required_attributes = ["te_in", "te_out", "tp"]
    for required_attr in required_attributes:
        assert getattr(example_array, required_attr) is None


def test_array_has_required_module_attributes_set(example_array):
    coll = example_array.collector

    assert coll.a1 is not None
    assert coll.a2 is not None
    assert coll.a5 is not None
    assert coll.a8 is not None
    assert coll.eta0hem is not None
    assert coll.kd is not None


def test_array_creation(collector_dynamic):
    array = cmp.Array('array3', collector=collector_dynamic, area_gr=Q(500, 'm**2'),
                      azim=Q(180, 'deg'), tilt=Q(30, 'deg'))

    assert array.collector.name == 'configured_collector'
    assert isinstance(array.area_gr, pint.Quantity)
    assert array._azim_mag == 180, array._azim_unit == 'degree'

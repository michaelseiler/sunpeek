import uuid

import sunpeek.components as cmp
from sunpeek.components.helpers import ResultStatus


def test_job(example_plant):
    job = cmp.Job(status=ResultStatus.pending, plant=example_plant)
    assert isinstance(job.id, uuid.UUID)
    assert uuid.UUID(str(job.id)).version == 4
    assert str(job.status.value) == 'pending'

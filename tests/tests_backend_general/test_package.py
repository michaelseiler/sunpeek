import os
from os.path import exists
import pytest
import importlib
import importlib.metadata
from pathlib import Path
import tomli

import sunpeek


@pytest.fixture()
def env_version():
    if os.environ.get('SUNPEEK_VERSION') is not None:
        v = os.environ['SUNPEEK_VERSION']
        os.environ['SUNPEEK_VERSION'] = '9.9.9'
        yield True
        os.environ['SUNPEEK_VERSION'] = v
        importlib.reload(sunpeek)
    else:
        yield False


def test_version(env_version):
    try:
        print('testing version from package metadata')
        assert sunpeek.__version__ == importlib.metadata.metadata('sunpeek')['version']
        return
    except importlib.metadata.PackageNotFoundError:
        pass

    if exists(Path(__file__).parent.parent.with_name('pyproject.toml')):
        print('testing version from pyproject')
        with open(Path(__file__).parent.parent.with_name('pyproject.toml'), 'rb') as f:
            t = tomli.load(f)
        assert sunpeek.__version__ == t['tool']['poetry']['version']  # Test in "dev" state
        return

    if env_version:
        importlib.reload(sunpeek)
        print('testing version from environment variable')
        # Test with env var (as in container)
        assert sunpeek.__version__ == '9.9.9'

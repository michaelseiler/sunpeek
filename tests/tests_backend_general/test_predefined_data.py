import sunpeek.components as cmp


def test_predefined_collectors():
    from sunpeek.definitions.collectors import all_definitions

    assert isinstance(all_definitions, list)
    assert len(all_definitions) >= 14
    for tpe in all_definitions:
        assert isinstance(tpe, cmp.Collector)


def test_predefined_fluids():
    from sunpeek.definitions.fluid_definitions import all_definitions, wpd_fluids, coolprop_fluids

    assert isinstance(all_definitions, list)
    assert len(all_definitions) > 1
    for fluid in wpd_fluids:
        assert isinstance(fluid, cmp.WPDFluidDefinition)

    for fluid in coolprop_fluids:
        assert isinstance(fluid, cmp.CoolPropFluidDefinition)

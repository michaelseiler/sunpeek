import pytest
from sqlalchemy import Column, Integer, Identity, ForeignKey
from pydantic import ValidationError

from sunpeek.common.unit_uncertainty import Q
import sunpeek.components as cmp
import sunpeek.components.helpers
from sunpeek.components.helpers import ComponentParam, ORMBase
from sunpeek.common.errors import ConfigurationError
import sunpeek.serializable_models as smodels


def test_scalar_component_param():
    class TestComp(cmp.Component, ORMBase):
        __tablename__ = 'test'

        __mapper_args__ = {
            "polymorphic_identity": "test_component"
        }
        id = Column(Integer, ForeignKey('components.component_id'), primary_key=True)
        test_param = ComponentParam('m', 0, 100)

    sunpeek.components.helpers.AttrSetterMixin.define_component_attrs()

    t = TestComp(test_param=Q(50, 'm'))
    assert t.test_param.magnitude == 50

    with pytest.raises(ConfigurationError):
        t.test_param = Q(500, 'm')
    with pytest.raises(ConfigurationError):
        t.test_param = Q(-100, 'm')
    with pytest.raises(ConfigurationError):
        t.test_param = Q([10,50], 'm')


def test_array_component_param():
    class TestComp(cmp.Component, ORMBase):
        __tablename__ = 'test2'
        id = Column(Integer, ForeignKey('components.component_id'), primary_key=True)
        test_param = ComponentParam('m', 0, 100, param_type='array')

    sunpeek.components.helpers.AttrSetterMixin.define_component_attrs()

    t = TestComp(test_param=Q([50], 'm'))
    assert t.test_param[0].magnitude == 50

    with pytest.raises(ConfigurationError):
        t.test_param = Q([500, 20], 'm')
    with pytest.raises(ConfigurationError):
        t.test_param = Q([-1, 20], 'm')
    with pytest.raises(ConfigurationError):
        t.test_param = Q(10, 'm')


def test_pydantic_model_strictness(fhw):
    plant = fhw
    array = fhw.arrays[0]
    collector = fhw.arrays[0].collector

    assert isinstance(smodels.Plant.from_orm(plant), smodels.Plant)
    with pytest.raises(ValidationError):
        smodels.Array.from_orm(plant)
    with pytest.raises(ValidationError):
        smodels.Collector.from_orm(plant)

    assert isinstance(smodels.Array.from_orm(array), smodels.Array)
    with pytest.raises(ValidationError):
        smodels.Plant.from_orm(array)
    with pytest.raises(ValidationError):
        smodels.Collector.from_orm(array)

    assert isinstance(smodels.Collector.from_orm(collector), smodels.Collector)
    with pytest.raises(ValidationError):
        smodels.Plant.from_orm(collector)
    with pytest.raises(ValidationError):
        smodels.Array.from_orm(collector)

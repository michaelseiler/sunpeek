from sunpeek.common.unit_uncertainty import Q
from sunpeek.common import config_parser
import sunpeek.components as cmp


def test_make_raw_sensors(conf):
    sensors = []
    for sensor in conf['plant']['raw_sensors']:
        sensors.append(cmp.Sensor(**sensor))
    assert len(sensors) == 11
    expected = ['te_amb', 'rh_amb', 've_wind', 'is shadowed', 'rd_bti', 'rd_dti', 'rd_gti', 'rd_dni', 'vf',
                'te_out', 'te_in']
    assert [s.raw_name for s in sensors] == expected


def test_make_full_plant(conf):
    p = config_parser.make_full_plant(conf)
    assert p.name == "FHW Arcon South _Test_"
    # assert isinstance(p.fluid_solar, WPDFluidPure)
    assert p.arrays[0].te_in.sensor_type.name == 'fluid_temperature'
    assert isinstance(p.arrays[0], cmp.Array)
    assert p.arrays[0].collector.name == "Arcon 3510"


def test_sensor_info_conf(conf):
    p = cmp.Plant('Test')
    for sensor_conf in conf['plant']['raw_sensors']:
        if sensor_conf['raw_name'] == 'rd_bti':
            break
    sens = cmp.Sensor(**sensor_conf, plant=p)
    assert isinstance(sens.info, cmp.SensorInfo)
    assert sens.info['tilt'] == Q(30, 'deg')

import datetime
import pytz
import pytest

from sunpeek.components import OperationalEvent
from sunpeek.common.errors import ConfigurationError, TimeZoneError


def test_create_operational_event(example_plant):
    example_plant.add_operational_event('2017-7-1 00:00+01', '2017-7-2 10:00+01', description="test error")
    assert len(example_plant.operational_events) >= 1

    OperationalEvent(example_plant, '2017-7-22 00:00+01')
    assert len(example_plant.operational_events) >= 2
    assert example_plant.operational_events[-1].event_end is None

    with pytest.raises(ConfigurationError):
        OperationalEvent(example_plant, '2017-7-22 10:00+01', '2017-7-2 00:00+01')

    OperationalEvent(example_plant, '2017-7-25 10:00+02', '2017-7-28 00:00+02', tz='Europe/Vienna')
    with pytest.raises(TimeZoneError):
        OperationalEvent(example_plant, '2017-7-22 10:00+01', '2017-7-2 00:00+01', tz='Europe/Vienna')


def test_ignored_ranges(example_plant):
    example_plant.add_operational_event('2017-7-1 00:00+01', '2017-7-2 10:00+01', description="test error",
                                        ignored_range=True)
    example_plant.add_operational_event('2017-8-5 00:15', '2017-8-5 13:07', tz='Europe/Vienna', description="test error",
                                        ignored_range=True)
    assert len(example_plant.ignored_ranges) >= 1
    assert example_plant.is_ignored('2017-8-5 12:00+01')
    assert example_plant.is_ignored('2017-7-2 00:00+02')
    assert not example_plant.is_ignored(datetime.datetime(2017, 8, 5, 14, 7, tzinfo=pytz.timezone('Europe/Vienna')))

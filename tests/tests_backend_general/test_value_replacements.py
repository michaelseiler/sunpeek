import pytest
import copy

from sunpeek.common.unit_uncertainty import Q
from sunpeek.components import Sensor, sensor_types


## --- fixtures

@pytest.fixture
def rep():
    return {'lower': (Q(-10, 'degC'), Q(0, 'degC'), Q(0, 'degC')),
            'upper': (Q(100, 'degC'), Q(120, 'degC'), Q(123, 'K'))
            }


@pytest.fixture
def s(rep):
    return Sensor('test', native_unit='K', value_replacements=rep)


## --- basic functionality

def test_sensor_value_replacements(s):
    assert s.value_replacements == {'upper': (Q(100, 'degC'), Q(120, 'degC'), Q(123, 'K')),
                                    'lower': (Q(-10, 'degC'), Q(0, 'degC'), Q(0, 'degC'))}


def test_sensortype_value_replacements():
    s_type = copy.deepcopy(sensor_types.fluid_temperature)
    s_type.lower_replace_min = Q(-10, 'degC')
    s_type.lower_replace_max = Q(0, 'degC')
    s_type.lower_replace_value = Q(0, 'degC')
    s_type.upper_replace_min = Q(100, 'degC')
    s_type.upper_replace_max = Q(120, 'degC')
    s_type.upper_replace_value = Q(100, 'degC')

    s = Sensor('test', native_unit='K', sensor_type=s_type)
    assert s.value_replacements == {'upper': (Q(100, 'degC'), Q(120, 'degC'), Q(100, 'degC')),
                                    'lower': (Q(-10, 'degC'), Q(0, 'degC'), Q(0, 'degC'))}


## --- invalid replacement intervals

def test_compatible_unit(s):
    with pytest.raises(ValueError, match='unit is not compatible with sensor native_unit'):
        s.value_replacements = {'upper': (Q(100, 'm'), Q(120, 'degC'), Q(100, 'degC')),
                                'lower': (Q(-10, 'degC'), Q(0, 'degC'), Q(0, 'degC'))}
    with pytest.raises(ValueError, match='unit is not compatible with sensor native_unit'):
        s.value_replacements = {'upper': (Q(100, 'degC'), Q(120, 'degC'), Q(100, 'degC')),
                                'lower': (Q(-10, 'degC'), Q(0, 'degC'), Q(0, 'W'))}
    with pytest.raises(ValueError, match='unit is not compatible with sensor native_unit'):
        s.value_replacements = {'upper': (None, None, Q(100, 'm'))}


def test_tuple(s):
    with pytest.raises(ValueError, match='pass a dictionary with a 3-value tuple representing the left and right '
                                         'interval boundaries and the replacement'):
        s.value_replacements = {'lower': (Q(80, 'degC'), Q(100, 'degC'))}


def test_boundary_left_right(s):
    with pytest.raises(ValueError, match='lower limit cannot be larger than the upper limit'):
        s.value_replacements = {'lower': (Q(80, 'degC'), Q(60, 'degC'), None)}


def test_boundary_not_set(s):
    with pytest.raises(ValueError, match='both left or right interval boundaries are None'):
        s.value_replacements = {'lower': (None, None, Q(100, 'degC'))}


def test_boundary_replacement_none(s):
    with pytest.raises(ValueError, match='replacement is None while both interval limits are not None'):
        s.value_replacements = {'lower': (Q(100, 'degC'), Q(120, 'degC'), None)}


def test_lower_right_missing(s):
    with pytest.raises(ValueError, match='right interval boundary is None '
                                         'while left interval limit and replacement value are not None'):
        s.value_replacements = {'lower': (Q(100, 'degC'), None, Q(10, 'degC'))}


def test_upper_left_missing(s):
    with pytest.raises(ValueError, match='left interval boundary is None '
                                         'while right interval limit and replacement value are not None'):
        s.value_replacements = {'upper': (None, Q(100, 'degC'), Q(10, 'degC'))}


## --- test None

def test_none_if_no_key_upper(s):
    s.value_replacements = {'upper': (Q(80, 'degC'), Q(100, 'degC'), Q(10, 'degC'))}
    assert s._lower_replace_min is None
    assert s._lower_replace_max is None
    assert s.lower_replace_value is None


def test_none_if_no_key_lower(s):
    s.value_replacements = {'lower': (Q(80, 'degC'), Q(100, 'degC'), Q(10, 'degC'))}
    assert s._upper_replace_min is None
    assert s._upper_replace_max is None
    assert s.upper_replace_value is None


def test_all_none_is_ok(s):
    s.value_replacements = {'upper': (None, None, None),
                            'lower': (None, None, None)}


def test_only_outer_limit_given(s):
    s.value_replacements = {'upper': (None, Q(120, 'degC'), None),
                            'lower': (Q(-10, 'degC'), None, None)}


def test_only_inner_limit_given(s):
    s.value_replacements = {'upper': (Q(120, 'degC'), None, None),
                            'lower': (None, Q(-10, 'degC'), None)}


def test_outer_limit_none(s):
    s.value_replacements = {'upper': (Q(120, 'degC'), None, Q(100, 'degC')),
                            'lower': (None, Q(-10, 'degC'), Q(100, 'degC'))}

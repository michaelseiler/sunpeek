import pandas as pd
import pytest
import pint
import numpy as np

import sunpeek.demo
from sunpeek.data_handling.wrapper import use_dataframe
from sunpeek.common.unit_uncertainty import Q
from sunpeek.data_handling.context import Context
from sunpeek.components import Plant, Array, Collector, CollectorTypes, Sensor, SensorMap
import sunpeek.components.iam_methods as iam
from sunpeek.common.errors import TimeZoneError


@pytest.fixture()
def minimal_collector():
    return Collector(
        test_reference_area="gross",
        test_type='SST',
        eta0hem=Q(0.81),
        a1=Q(2.71, " W m**-2 K**-1"),
        a2=Q(0.01, " W m**-2 K**-2"),
        a5=Q(14.7, "kJ m**-2 K**-1"),
        a8=Q(0, 'W m**-2 K**-4'),
        kd=Q(0.983),
        gross_length=Q(1, "m"),
        area_gr=Q(3, "m**2"),
        iam_method=iam.IAM_K50(k50=Q(0.97)),
        collector_type=CollectorTypes.flat_plate.value
    )


@pytest.fixture()
def minimal_array(minimal_collector):
    return Array(collector=minimal_collector, area_gr=Q(100, "m²"), tilt=Q(0, "deg"), azim=Q(180, "deg"))


@pytest.fixture()
def minimal_plant(minimal_array):
    return Plant('Test', longitude=Q(14.5, "deg"), latitude=Q(14.5, "deg"))


@pytest.fixture
def configured_collector():
    return Collector(name='configured_collector',
                     gross_length=Q(2.3, 'm'),
                     test_type='QDT',
                     test_reference_area='gross',
                     area_gr=Q(5, 'm**2'),
                     a1=Q(2.3, 'W m**-2 K**-1'),
                     a2=Q(0.01, 'W m**-2 K**-2'),
                     a5=Q(7500, 'J m**-2 K**-1'),
                     a8=Q(0, 'W m**-2 K**-4'),
                     eta0b=Q(0.81, ''),
                     iam_method=iam.IAM_ASHRAE(b=Q(0.15)),
                     kd=Q(0.93, ''),
                     collector_type=CollectorTypes.flat_plate.value
                     )


@pytest.fixture()
def configured_plant(configured_collector):
    plant = Plant("Test", latitude=Q(47.0471, 'deg'), longitude=Q(15.43736, 'deg'), elevation=Q(172, 'm'))
    # plant.set_sensors(tp="pwSolHtmHst", in_global="rd_gti", te_amb="teAmb")
    array = Array('array3', plant, collector=configured_collector,
                  area_gr=Q(500, 'm**2'), area_ap=Q(490, 'm**2'),
                  azim=Q(180, 'deg'), tilt=Q(30, 'deg'), row_spacing=Q(3, 'm'))
    plant.arrays.append(array)
    return plant


# =====================
# TEST PLANT CREATION
# =====================


def test_plant_creation_defaults():
    longitude = 14.5
    latitude = 14.5
    p = Plant('Test', longitude=Q(longitude, "deg"), latitude=Q(latitude, "deg"))

    assert p.elevation.m == 100


def test_plant_creation():
    plant = Plant('FHW', operator='SOLID', latitude=Q(47.0471, 'deg'), longitude=Q(15.43736, 'deg'))
    assert plant.name == 'FHW'
    assert plant.operator == 'SOLID'
    assert isinstance(plant.latitude, pint.Quantity) and isinstance(plant.longitude, pint.Quantity)
    assert plant.__latitude_mag == 47.0471 and plant.__latitude_unit == 'degree'
    assert plant.__longitude_mag == 15.43736 and plant.__longitude_unit == 'degree'


def test_plant_has_required_attributes(minimal_plant):
    assert minimal_plant.latitude is not None
    assert minimal_plant.longitude is not None
    assert minimal_plant.elevation is not None
    # assert minimal_plant.name is not None
    # assert minimal_plant.timezone is not None


def test_plant_class_arrays_default_is_empty_list():
    longitude = 14.5
    latitude = 14.5
    plant = Plant(longitude=Q(longitude, "deg"), latitude=Q(latitude, "deg"))
    arrays = plant.arrays
    assert arrays is not None
    iter(arrays)


def test_printing_Plant_does_not_raise_error():
    # this test is there because initially, pvlib.Plant() is used, and - if not specifying any array - this causes a problem when printing the array.
    plant = Plant("Test", latitude=Q(47.0471, 'deg'), longitude=Q(15.43736, 'deg'))
    print(plant)


# def test_create_plant_setPlantSensors_failsIfRequiredSensorIsMissing():
#     plant = Plant("Test", latitude=Q(47.0471, 'deg'), longitude=Q(15.43736, 'deg'))
#     te_amb = Sensor(raw_name='te_amb', native_unit='degC')
#     G_hem = Sensor(raw_name='rd_gti', native_unit='W m**-2')
#     tp = Sensor(raw_name='pwSolHtmHst', native_unit='kWh')
#     with pytest.raises(Exception):
#         plant.set_sensors(te_amb=te_amb, tp=tp)
#
#     with pytest.raises(Exception):
#         # plant.set_sensors(te_amb=te_amb, rd_ghi=G_hem)
#         plant.set_sensors(te_amb=te_amb, rd_ghi=G_hem)
#
#     with pytest.raises(Exception):
#         plant.set_sensors(rd_ghi=G_hem, tp=tp)


def test_create_plant_setPlantSensors_works_minimal():
    plant = Plant("Test", latitude=Q(47.0471, 'deg'), longitude=Q(15.43736, 'deg'))
    te_amb = Sensor(raw_name='te_amb', native_unit='degC')
    # G_hem = Sensor(raw_name='rd_gti', native_unit='W m**-2')
    tp = Sensor(raw_name='pwSolHtmHst', native_unit='kWh')
    # plant.set_sensors(te_amb=te_amb, tp=tp, rd_ghi=G_hem)
    plant.set_sensors(te_amb=te_amb, tp=tp)


def test_create_plant_setPlantSensors_worksForBeamAndDiffuse():
    plant = Plant("Test", latitude=Q(47.0471, 'deg'), longitude=Q(15.43736, 'deg'))
    te_amb = Sensor(raw_name='te_amb', native_unit='degC')
    tp = Sensor(raw_name='pwSolHtmHst', native_unit='kWh')
    rd_dti = Sensor(raw_name='rd_dti', native_unit='W m**-2')
    rd_bti = Sensor(raw_name='rd_bti', native_unit='W m**-2')
    plant.set_sensors(te_amb=te_amb, tp=tp, in_beam=rd_bti, in_diffuse=rd_dti)


def test_create_plant_setArrays_works(configured_collector):
    plant = Plant("Test", latitude=Q(47.0471, 'deg'), longitude=Q(15.43736, 'deg'))
    array = Array('array3', plant, collector=configured_collector,
                  area_gr=Q(500, 'm**2'), azim=Q(180, 'deg'), tilt=Q(30, 'deg'))
    plant.arrays.append(array)


def test_plant_class_arrays_are_not_class_variables(configured_collector):
    longitude = Q(14.5, "deg")
    latitude = Q(14.5, "deg")

    plant = Plant(longitude=longitude, latitude=latitude)
    array = Array('array3', plant, collector=configured_collector,
                  area_gr=Q(500, 'm**2'), azim=Q(180, 'deg'), tilt=Q(30, 'deg'))
    plant.arrays.append(array)
    plant2 = Plant(longitude=longitude, latitude=latitude)

    assert len(plant2.arrays) == 0
    assert array not in plant2.arrays


def test_create_plant_addArrays_works(configured_collector):
    plant = Plant("Test", latitude=Q(47.0471, 'deg'), longitude=Q(15.43736, 'deg'))
    array = Array('array3', plant, collector=configured_collector,
                  area_gr=Q(500, 'm**2'), azim=Q(180, 'deg'), tilt=Q(30, 'deg'))
    plant.arrays.append(array)


def test_set_sensors_automatically_sets_plant():
    plant = Plant("Test", latitude=Q(0, "°"), longitude=Q(0, "°"))
    sensor_1 = Sensor("teAmb", "°C")
    assert sensor_1.plant == None
    plant.set_sensors(te_amb=sensor_1)
    assert sensor_1.plant == plant


def test_add_array_automatically_sets_plant(configured_collector):
    plant = Plant("Test", latitude=Q(0, "°"), longitude=Q(0, "°"))
    array = Array("array", collector=configured_collector,
                  area_gr=Q(500, 'm**2'), azim=Q(180, 'deg'), tilt=Q(30, 'deg'))

    assert array.plant == None
    plant.add_array(array)
    assert plant.arrays[0] == array
    assert array.plant == plant


def test_plant_kwarg_automatically_sets_plant(configured_collector):
    plant = Plant("Test", latitude=Q(0, "°"), longitude=Q(0, "°"))
    array = Array(plant=plant, name="array", collector=configured_collector,
                  area_gr=Q(500, 'm**2'), azim=Q(180, 'deg'), tilt=Q(30, 'deg'))

    assert plant.arrays[0] == array
    assert array.plant == plant


def test_add_array_sets_plant_for_sensors(configured_collector):
    sensor_2 = Sensor("teIn", "°C")
    plant = Plant("Test", latitude=Q(0, "°"), longitude=Q(0, "°"))
    array = Array("array", collector=configured_collector,
                  area_gr=Q(500, 'm**2'), azim=Q(180, 'deg'), tilt=Q(30, 'deg'))
    plant.add_array(array)
    array.set_sensors(te_in=sensor_2)

    assert array.te_in == sensor_2
    assert sensor_2.plant == plant


def test_set_array_sensors_sets_plant(configured_collector):
    sensor_2 = Sensor("teIn", "°C")
    plant = Plant("Test", latitude=Q(0, "°"), longitude=Q(0, "°"))
    array = Array("array", collector=configured_collector,
                  area_gr=Q(500, 'm**2'), azim=Q(180, 'deg'), tilt=Q(30, 'deg'))
    plant.add_array(array)

    assert sensor_2.plant == None
    array.set_sensors(te_in=sensor_2)
    assert array.te_in == sensor_2
    assert sensor_2.plant == plant


# ===========================
# TEST using "minimal" PLANT
# ===========================
# meaning that no sensors have been set yet
# meaning that no arrays have been set yet


def test_plant_add_sensors(minimal_plant):
    minimal_plant.set_sensors(in_global=Sensor("rd_gti", "W m**-2"), in_diffuse=Sensor("rd_dti", "W m**-2"),
                              te_amb=Sensor("te_amb", "degC"), tp=Sensor("tp", "W"))
    assert minimal_plant.in_global is not None
    assert minimal_plant.in_diffuse is not None
    assert minimal_plant.te_amb is not None
    assert minimal_plant.tp is not None


def test_plant_arrays_are_iterable(minimal_plant, minimal_array):
    minimal_plant.arrays.append(minimal_array)
    minimal_plant.arrays.append(minimal_array)
    arrays = minimal_plant.arrays
    assert arrays is not None
    assert arrays != []
    iter(arrays)
    assert arrays[0] == minimal_array
    assert arrays[1] == minimal_array


def test_plant_get_area_works(configured_plant):
    assert configured_plant.area_ap is not None


@pytest.fixture
def df():
    return pd.read_csv(sunpeek.demo.DEMO_DATA_PATH_2DAYS,
                       sep=';', index_col=0, parse_dates=True)


def test_dataframe_datasource(configured_plant, df):
    s = Sensor('te_amb', 'degC', plant=configured_plant)
    SensorMap('te_amb', s, 'ambient_temperature', configured_plant)
    use_dataframe(configured_plant, df, timezone='UTC', calculate_virtuals=False)
    assert configured_plant.context.datasource == 'df'
    assert isinstance(configured_plant.te_amb.data, pd.Series)
    assert isinstance(configured_plant.time_index, pd.DatetimeIndex)
    assert len(configured_plant.time_index) == len(configured_plant.te_amb.data)


def test_dataframe_datasource_with_init(configured_plant, df):
    s = Sensor('te_amb', 'degC')
    configured_plant.set_sensors(te_amb=s)
    use_dataframe(configured_plant, df, timezone='UTC', calculate_virtuals=False)

    assert configured_plant.context.datasource == 'df'
    assert isinstance(configured_plant.te_amb.data, pd.Series)
    assert isinstance(configured_plant.time_index, pd.DatetimeIndex)
    assert len(configured_plant.time_index) == len(configured_plant.te_amb.data)


def test_sensors_are_assigned_to_plant_automatically(minimal_plant):
    s1 = Sensor('te_amb', 'degC')
    s2 = Sensor('rd_gti', 'W m**-2')
    minimal_plant.set_sensors(te_amb=s1, in_global=s2)
    assert minimal_plant.te_amb.plant == minimal_plant
    assert minimal_plant.in_global.plant == minimal_plant


def test_array_sensors_are_assigned_to_plant_automatically(minimal_plant, minimal_array):
    minimal_plant.arrays.append(minimal_array)
    minimal_array.set_sensors(te_in=Sensor("te_in", "degC"), te_out=Sensor("te_out", "degC"))
    assert minimal_plant.arrays[0].te_in.plant == minimal_plant


def test_array_sensors_are_assigned_to_plant_array_created_first(minimal_plant):
    a = Array('Test Array')
    minimal_plant.add_array(a)
    a.set_sensors(te_in=Sensor("te_in", "degC"),
                  te_out=Sensor("te_out", "degC"))
    assert minimal_plant.arrays[0].te_in.plant == minimal_plant
    assert minimal_plant.arrays[0].te_out.plant == minimal_plant


def test_array_sensors_are_assigned_to_plant_array_created_first_no_addarray(minimal_plant):
    a = Array(name='Test Array', plant=minimal_plant)
    a.set_sensors(te_in=Sensor("te_in", "degC"), te_out=Sensor("te_out", "degC"))
    assert minimal_plant.arrays[0].te_in.plant == minimal_plant
    assert minimal_plant.arrays[0].te_out.plant == minimal_plant


def test_set_sensors_are_assigned_to_plant(minimal_plant):
    a1 = Array(name='Test Array', plant=minimal_plant)
    a1.set_sensors(te_in=Sensor("te_in", "degC"))
    a2 = Array(name='Test Array 2', plant=minimal_plant)
    a2.set_sensors(te_out=Sensor("te_out", "degC"))
    assert minimal_plant.arrays[0].te_in.plant == minimal_plant
    assert minimal_plant.arrays[1].te_out.plant == minimal_plant


def test_start_end(configured_plant, df):
    s = Sensor('te_amb', 'degC')
    p = configured_plant
    p.set_sensors(te_amb=s)
    df = pd.DataFrame(np.random.randn(2, 1), index=
    pd.to_datetime((pd.to_datetime('2017-4-30', utc=True), pd.to_datetime('2017-5-1', utc=True))))

    with pytest.raises(TimeZoneError, match='Ambiguous timezone information found'):
        p.context = Context(plant=p, datasource='df', dataframe=df, df_timezone='UTC')


def test_start_end_no_timezone(configured_plant, df):
    s = Sensor('te_amb', 'degC')
    p = configured_plant
    p.set_sensors(te_amb=s)
    df = pd.DataFrame(np.random.randn(2, 1), columns=['te_amb'], index=
    pd.to_datetime((pd.to_datetime('2017-4-30', utc=True), pd.to_datetime('2017-5-1', utc=True))))
    p.context = Context(plant=p, datasource='df', dataframe=df)

    assert p.te_amb.data.index[0] == pd.to_datetime('2017-04-30 00:00', utc=True)
    assert p.te_amb.data.index[-1] == pd.to_datetime('2017-05-01 00:00', utc=True)


def test_time_index_access(configured_plant, df):
    p = configured_plant
    df = pd.DataFrame(np.random.randn(2, 1), columns=['te_amb'], index=
    pd.to_datetime((pd.to_datetime('2017-4-30', utc=True), pd.to_datetime('2017-5-1', utc=True))))
    p.context = Context(plant=p, datasource='df', dataframe=df)

    assert isinstance(p.time_index, pd.DatetimeIndex)
    assert p.time_index[0] == pd.to_datetime('2017-04-30 00:00', utc=True)
    assert p.time_index[-1] == pd.to_datetime('2017-05-01 00:00', utc=True)


def test_time_plant_has_upload_history(configured_plant):
    p = configured_plant
    assert isinstance(p.upload_history, list)

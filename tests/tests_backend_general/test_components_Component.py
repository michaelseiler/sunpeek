import pytest
import sunpeek.common.errors
import sunpeek.components as cmp
import sunpeek.core_methods.virtuals.main
import sunpeek.core_methods.virtuals as virtuals


## test get_display_name()


@pytest.fixture
def fhw_comp(fhw):
    virtuals.config_virtuals(fhw)

    return fhw

# def _assert_display_name(component, *args):
#     """For given tuple, check if component.get_display_name() returns expected results"""
#     (slot_name, descriptive_name, unit_str, raw_str) = args
#     fun = lambda fmt: component.get_display_name(slot_name, fmt=fmt)
#
#     descriptive = fun('d')
#     assert descriptive == fun('descriptive')
#     assert descriptive == descriptive_name
#
#     unit = fun('u')
#     assert unit == fun('unit')
#     assert unit == unit_str
#
#     raw = fun('r')
#     assert raw == fun('raw')
#     assert raw == raw_str
#
#
# def test_display_none(fhw):
#     assert fhw.get_display_name('non_existent_sensor') is None
#
#
# # Tuples define: slot_name, descriptive_name, unit string, raw_name string
# @pytest.mark.parametrize('item', [
#     ('tp', 'Thermal power', '[None]', '(tp)'),
#     # ('rd_ghi', 'Global horizontal irradiance', '[W/m²]', 'rd_ghi'),  # currently skipped in Plant
#     ('sun_zenith', 'Solar zenith angle', '[None]', '(sun_zenith)'),
# ])
# def test_display_Plant_FHW(fhw, item):
#     _assert_display_name(fhw, *item)
#
#
# @pytest.mark.parametrize('item', [
#     ('vf', 'Volume flow', '[m³/s]', '(vf)'),
#     ('te_in', 'Inlet temperature', '[K]', '(te_in)'),
#     ('is_shadowed', 'Array is shadowed', '[]', '(is_shadowed)'),
#     ('in_global', 'Global radiation input', '[W/m²]', '(rd_gti)'),
#     ('rd_gti', 'Global radiation', '[None]', '(rd_gti)'),
#     ('aoi', 'Angle of incidence', '[None]', '(aoi)'),
# ])
# def test_display_Array(fhw, item):
#     _assert_display_name(fhw.arrays[0], *item)
#
#
# @pytest.mark.parametrize('item', [
#     ('tp', 'Thermal power', '[kW]', '(tp)'),
#     ('sun_zenith', 'Solar zenith angle', '[deg]', '(sun_zenith)'),
# ])
# def test_display_Plant_FHW_virtuals_calculated(fhw_calculated, item):
#     _assert_display_name(fhw_calculated, *item)
#
#
# @pytest.mark.parametrize('item', [
#     ('rd_gti', 'Global radiation', '[W/m²]', '(rd_gti)'),
#     ('aoi', 'Angle of incidence', '[deg]', '(aoi)'),
# ])
# def test_display_Array_virtuals_calculated(fhw_calculated, item):
#     _assert_display_name(fhw_calculated.arrays[0], *item)


def test_get_raw_sensors(fhw_comp):
    assert fhw_comp.get_raw_sensor('te_amb').raw_name == 'te_amb'
    assert fhw_comp.get_raw_sensor('foo') is None
    with pytest.raises(sunpeek.common.errors.SensorNotFoundError):
        fhw_comp.get_raw_sensor('foo', raise_if_not_found=True)


def test_set_slot_attr_map_sensor():
    p = cmp.Plant('Test')
    assert p.te_amb is None

    t2 = cmp.Sensor('Test sensor 2', 'K')
    p.te_amb = t2
    assert t2 in p.sensors.values()

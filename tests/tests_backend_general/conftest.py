import json
import pytest

from sunpeek.common.unit_uncertainty import Q
import sunpeek.components as cmp
from sunpeek.components.iam_methods import IAM_ASHRAE
from sunpeek.components.types import CollectorTypes
from sunpeek import demo


@pytest.fixture(scope='session')
def conf():
    """Returns FHW plant config JSON file"""
    with open(demo.DEMO_CONFIG_PATH) as f:
        return json.load(f)


@pytest.fixture
def great_collector():
    return cmp.Collector(name='great_collector', test_reference_area='gross', gross_length=Q(2.3, 'm'),
                         test_type='QDT', area_gr=Q(5, 'm**2'),
                         a1=Q(2.3, 'W m**-2 K**-1'), a2=Q(0.01, 'W m**-2 K**-2'), a5=Q(7500, 'J m**-2 K**-1'),
                         a8=Q(0, 'W m**-2 K**-4'),
                         eta0b=Q(0.81, ''), kd=Q(0.93, ''),
                         iam_method=IAM_ASHRAE(b=Q(0.15)), collector_type=CollectorTypes.flat_plate.value)


@pytest.fixture
def example_plant(great_collector):
    plant = cmp.Plant("Test", latitude=Q(47.0471, 'deg'), longitude=Q(15.43736, 'deg'))
    tp = cmp.Sensor(raw_name="pwSolHtmHst", native_unit='kW')
    in_global = cmp.Sensor(raw_name="rd_gti", native_unit='W/m**2')
    te_amb = cmp.Sensor(raw_name="teAmb", native_unit='degC')
    plant.set_sensors(tp=tp, in_global=in_global, te_amb=te_amb)
    array = cmp.Array('array3', plant,
                      collector=great_collector,
                      area_gr=Q(500, 'm**2'), area_ap=Q(490, 'm**2'),
                      azim=Q(180, 'deg'), tilt=Q(30, 'deg'),
                      row_spacing=Q(3, 'm'))

    return plant

# Contributing to the SunPeek Project <!-- omit in toc -->

Thank you for thinking about investing your time in contributing to our project!

Read our [Code of Conduct](https://gitlab.com/sunpeek/sunpeek/-/blob/main/CODE_OF_CONDUCT.md) to keep our community safe, approachable and inclusive.

In this guide you will get an overview of the contribution workflow from opening an issue, creating a MR, reviewing, 
and merging the MR, depending on how you are trying to contribute.

## License - IMPORTANT READ THIS FIRST
By submitting contributions to SunPeek, you are agreeing to release those contributions under the same license as 
that applied to the repository you are submitting to. You are also agreeing to the other statements contained in the 
[Developer Certificate of Origin](https://developercertificate.org/). To explicit signal acknowledgment of this, **we require
all commits to carry the `Signed-off-by` trailer**, which will be interpreted as explicit agreement to the terms in the DCO. 
The email address used for this should match an email address on your gitlab account. You can add this trailer by using the 
`-s` flag with the `git commit` command, most git GUI clients include a configuration option to do this as well. **NOTE: 
even in the absence of the `Signed-off-by` trailer, by submitting work for inclusion in the project, you acknowledge and
agree to the terms in the DCO.**

### **Did you find a bug?**

* **STOP, is it a security vulrability in SunPeek?** in this case please either email the maintainers (see the [README](./README.md)),
or open a GitLab issue and **ensure you tick the confidential checkbox**

* **Ensure the bug was not already reported** by searching on GitLab under [Issues](https://gitlab.com/sunpeek/sunpeek/issues/).

* If you're unable to find an open issue addressing the problem, [open a new one](https://gitlab.com/sunpeek/sunpeek/issues/new). 
Be sure to include a **title and clear description**, as much relevant information as possible, and a **code sample** or 
an **anonymised configuration sample** demonstrating the expected behavior that is not occurring.
* If possible, use the relevant bug report templates to create the issue. Simply select the most appropriate template from the dropdown, and fill in the requested information.

### **Did you write a patch that fixes a bug?**

* Open a new GitLab merge request with the patch. Please see [Workflow for making changes](#workflow-for-making-changes) 
for a description of the workflow for this.
* Ensure the MR description clearly describes the problem and solution. Include the relevant issue number if applicable.

### **Do you intend to add a new feature or change an existing one?**

* Check if your change has been suggested already, by searching under [Issues](https://gitlab.com/sunpeek/sunpeek/issues/).
* If it has not already been proposed, suggest your change in [a new issue](https://gitlab.com/sunpeek/sunpeek/issues/new).
Please use appropriate labels to allow us to triage suggestions.
* Ideally after your proposal has been discussed and agreed, create a fork of the repository, commit your changes to it 
and open a Merge Request following the procedure in [Workflow for making changes](#workflow-for-making-changes)

## New contributor resources

To get an overview of the project, read the [README](README.md). Here are some resources to help you get started with open source contributions:

- [A simple guide to contributing to open source](https://opensource.guide/how-to-contribute/)
- [Set up Git](https://docs.gitlab.com/ee/topics/git/how_to_install_git/index.html), or use a 3rd party git application like [Fork](fork.dev)
- [Collaborating with pull requests](https://docs.github.com/en/github/collaborating-with-pull-requests)


## Workflow for making changes

1. [Fork the repository](https://docs.gitlab.com/ee/user/project/repository/forking_workflow.html#creating-a-fork), 
and optionally clone the fork to your local machine.
2. Follow the instructions in the [README](./README.md) for setting up the development environment.
3. Create a working branch in your fork and start with your changes!
4. Write Tests
5. Run the full test suites.

### Commit your updates
Commit changes as you work. Please try and use short but descriptive commit messages and commit related changes together,
this will help with the review process.

### Merge Request

When you're finished with the changes, [create a merge request](https://docs.gitlab.com/ee/user/project/repository/forking_workflow.html#merging-upstream),
also known as an MR (if you are used to GitHub, these are the same as PRs there). 
- Fill the "Ready for review" template so that we can review your MR. This template helps reviewers understand your changes as well as the purpose of your pull request.
- Don't forget to include a reference to the issue like closes #556 (if the MR fully addresses the issue) or (initial work on #556)
- You can open an MR before you are completly ready for the changes to be merged, but please make sure that you hit the Mark Ready button when the work is finished.
- After you open the MR, and when you add changes to it, automated tests will run
- Please make sure that your merge request can be merged, with no conflicts, before marking it ready. 
This may require you to first update your fork from the upstream project and merge the `main` branch back into your working branch.

Once you submit your MR and mark it ready, a Maintainer will review it. If they are accepted, the MR will be merged.
- We may ask for changes to be made before a MR can be merged, either using [suggested changes](https://docs.gitlab.com/ee/user/project/merge_requests/reviews/suggestions.html) or pull request comments. You can apply suggested changes directly through the UI. You can make any other changes in your fork, then commit them to your branch.
- As you update your MR and apply changes, mark each conversation as [resolved](https://docs.github.com/en/github/collaborating-with-issues-and-pull-requests/commenting-on-a-pull-request#resolving-conversations).
- If you run into any merge issues, checkout this [git tutorial](https://github.com/skills/resolve-merge-conflicts) to help you resolve merge conflicts and other issues.

## Tests
The easiest way to make sure code works is to have unit tests that our CI pipeline can run automatically. Examples can be found here in the tests directory.
To run tests, in a terminal in the project virtual env run `python -m pytest --cov-config=.coveragerc --cov=./ ./tests/`
The goal is to have test coverage after a commit be at least as high as the last time the test was run. 
Therefore please try and write tests so that when you run the above command it results in coverage better than
[![coverage report](https://gitlab.com/sunpeek/sunpeek/badges/main/coverage.svg)](https://gitlab.com/sunpeek/sunpeek/-/commits/main)

### Database Tests
For integration tests that depend on the database the easiest solution is to use the timescaledb docker image, see [Getting a local development database](#Getting a local development database) for 
pre-requisits. In order to ensure that tests run both locally and in the automatic tests CI jobs, 
please use the provided test fixtures: `session` to obtain an [SQLAlchemy session object](https://docs.sqlalchemy.org/en/14/orm/session.html)
(probably what you need in most cases), or `psycopg2_connection` for a [psycopg2 connection object](https://www.psycopg.org/docs/connection.html#connection)
(if you need to test directly against raw data tables)

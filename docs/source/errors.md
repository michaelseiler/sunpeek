# Troubleshooting

## SensorDataNotFoundError
This error is raised when a sensor is configured, but the underlying data can't be accessed, normally due to the column 
in the underlying datastore being missing or misnamed.

### SensorDataNotFoundError when using the database datastore
This is the most likely case if you are using SunPeek as a web application. It is most likely due to a sensor being added
in configuration, but no data having been uploaded for it yet. 

```{tip}  
Database columns are only created for sensors once data is uploaded for them. If you are seeing this error and have
recently added sensors to a plant configuration, make sure that some data has been uploaded before trying to perform 
any analysis.  
```

## Unexpected calculation error

Unexpected calculation errors may occur during calculation of internal core methods, like virtual sensors, 
the Performance Check method or some D-CAT method. 
The cause is most likely related to some unexpected numeric problem, or a problem that involves some external library 
used by the specific calculation. 

Please report the issue to the maintainers of SunPeek to help them improve the robustness of the software. 
The full error trace has been recorded in the log files. Please contact your system administrator to inspect these logs.


# Configuration file for the Sphinx documentation builder.
#
# This file only contains a selection of the most common options. For a full
# list see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Path setup --------------------------------------------------------------

# If extensions (or modules to document with autodoc) are in another directory,
# add these directories to sys.path here. If the directory is relative to the
# documentation root, use os.path.abspath to make it absolute, like shown here.
#
import os
import sys
import json

sys.path.insert(0, os.path.abspath('../../'))
import sunpeek.common
import sunpeek.api.main
# import sunpeek.api
import sunpeek.components
from sunpeek import base_model

# -- Project information -----------------------------------------------------
project = 'SunPeek'
copyright = '2020-2022, HarvestIT Consortium; 2023, SunPeek Open Source Contributors'
author = "Philip Ohnewein, Daniel Tschopp, Lukas Feierl, Marnoch Hamilton-Jones, Jonathan Cazco"
version = os.environ.get('SUNPEEK_VERSION', '0.0.0.dev0')
html_title = 'SunPeek Documentation'


# -- General configuration ---------------------------------------------------

# Add any Sphinx extension module names here, as strings. They can be
# extensions coming with Sphinx (named 'sphinx.ext.*') or your custom
# ones.

html_favicon = '_static/icon.svg'
html_static_path = ['_static']

html_theme_options = {
   "logo": {
      "image_light": "_static/Logo_Blue_wide.svg",
      "image_dark": "_static/Logo_Transparent_wide.svg",
   }
}

extensions = [
    'sphinx.ext.autodoc', 
    'sphinx.ext.mathjax', 
    # 'sphinx.ext.viewcode',
    # 'numpydoc',
    'sphinx.ext.autosummary',
    'sphinx.ext.doctest',
    # 'sphinx.ext.inheritance_diagram',
    'myst_parser',
    # 'sphinx.ext.viewcode'
    'sphinxcontrib.redoc',
    'sphinx.ext.intersphinx',
    'sphinx.ext.napoleon',
    'sphinx_autodoc_typehints',
]

# master_doc = "contents"

autosummary_generate = True  # Turn on sphinx.ext.autosummary

default_role = 'py:obj'

show_authors = True

myst_heading_anchors = 4

napoleon_use_param = True
napoleon_use_ivar = True
napoleon_use_admonition_for_notes = True
napoleon_preprocess_types = True
napoleon_attr_annotations = True

napoleon_type_aliases = {
    "Quantity": "pint.Quantity",
    "Q": "pint.Quantity",
    "pd": "pandas"
}

intersphinx_mapping = {'python': ('https://docs.python.org/3', None),
                       'pint': ('https://pint.readthedocs.io/en/0.19.2', None),
                       'pvlib': ('https://pvlib-python.readthedocs.io/en/stable', None),
                       'pandas': ('https://pandas.pydata.org/pandas-docs/stable', None),
                       'pytz': ('https://pythonhosted.org/pytz/', None),
                       }

# Add any paths that contain templates here, relative to this directory.
templates_path = ['_templates']

# List of patterns, relative to source directory, that match files and
# directories to ignore when looking for source files.
# This pattern also affects html_static_path and html_extra_path.
exclude_patterns = []


# -- Options for HTML output -------------------------------------------------

# The theme to use for HTML and HTML Help pages.  See the documentation for
# a list of builtin themes.
#
# html_theme = 'sphinx_rtd_theme'
html_theme = "pydata_sphinx_theme"
# Add any paths that contain custom static files (such as style sheets) here,
# relative to this directory. They are copied after the builtin static files,
# so a file named "default.css" will overwrite the builtin "default.css".
# html_static_path = ['_static']

with open('./api-spec.json', 'w') as f:
    json.dump(sunpeek.api.main.app.openapi(), f)

redoc = [
    {
        'name': 'SunPeek REST API',
        'page': 'api-docs',
        'spec': 'api-spec.json',
        'embed': True
    }
]

redoc_uri = 'https://unpkg.com/redoc@2.0.0-rc.66/bundles/redoc.standalone.js'

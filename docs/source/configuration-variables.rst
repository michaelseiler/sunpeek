=======================
Configuration Variables
=======================

====================== =============    =======
Variable               Default Value    Note
====================== =============    =======
HIT_DB_TYPE            postgresql       currently only postgresql is supported
HIT_DB_HOST            localhost        the hostname where the database server is accessible
HIT_DB_NAME            harvestit        ..
HIT_DB_USER            postgres         This will be changed to 'harvestit'
HIT_DB_PW                               A common value is configured for unit tests, in all other cases, this must be set by the user
HIT_HOST               localhost        The external FQDN for the application
SUNPEEK_RAW_DATA_PATH  ./raw_data       The path where raw data uploaded for each plant is stored
SUNPEEK_CALC_DATA_PATH ./calc_data      The path where calculated data for virtual sensors is stored
VUE_APP_BASE_URL       localhost        The internal hostname for the API, used to tell the frontend where the backend is available - in the default compose setup, this would be "api"
SUNPEEK_API_ROOT_PATH  localhost        The internal hostname for the API - in the default compose setup, this would be "/api/v1"
SUNPEEK_API_PORT       8000             The port to expose the API on.
SUNPEEK_VERSION        0.0.0.dev0       This baked in to the Docker image during build, do not set manually
====================== =============    =======

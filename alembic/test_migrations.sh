#!/bin/bash

cd "$(dirname ${BASH_SOURCE[0]})"

# Using current docker image to get old DB schema, use sqlite backend
docker pull sunpeek/sunpeek:dev
docker run \
  --mount type=bind,source="$(pwd)"/..,target=/new_code \
  -v sunpeek_temp_alembic:/db_data \
  -e HIT_DB_HOST=////home/schema_check_db.sqlite -e HIT_DB_TYPE=sqlite -e SUNPEEK_TEST_DB_MIGRATION=1\
  --name sunpeek-alembic -d sunpeek/sunpeek:dev sleep infinity

docker exec sunpeek-alembic python -c 'from sunpeek.db_utils import init_db; init_db.init_db()'
docker exec sunpeek-alembic bash -c 'cd /new_code; alembic upgrade head'
docker exec sunpeek-alembic bash -c 'cd /new_code; python -m pytest -c ./tests/pytest_nohost.ini ./tests/tests_integration'

docker stop sunpeek-alembic
docker rm sunpeek-alembic
read -p "Press enter to continue"

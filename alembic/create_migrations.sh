#!/bin/bash

cd "$(dirname ${BASH_SOURCE[0]})"

# Using current docker image to get old DB schema, use sqlite backend
docker pull sunpeek/sunpeek
docker run \
  --mount type=bind,source="$(pwd)",target=/new_code/alembic \
  --mount type=bind,source="$(pwd)"/../sunpeek,target=/new_code/sunpeek \
  -e HIT_DB_HOST=////home/schema_check_db.sqlite -e HIT_DB_TYPE=sqlite \
  -d --name sunpeek-alembic sunpeek/sunpeek sleep infinity

# Init DB
docker exec sunpeek-alembic python -c 'from sunpeek.db_utils import init_db; init_db.init_db()'

# Copy in alembic config file
docker cp ../alembic.ini sunpeek-alembic:/new_code/alembic.ini

# Auto generate revisions
docker exec sunpeek-alembic bash -c 'cd /new_code; alembic revision --autogenerate'

# Issue warning about reviewing changes
echo "An alembic revision file has been created in the alembic/revisions directory. You MUST manually review the
migrations to verify they reflect the intent of your changes"

# Remove container and volumes
docker stop sunpeek-alembic
docker rm sunpeek-alembic

read -p "Press enter to continue"

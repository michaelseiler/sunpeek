"""empty message

Revision ID: 1e1f334238bc
Revises: b5b8aa9a2c34
Create Date: 2023-08-10 09:58:13.409935

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '1e1f334238bc'
down_revision = 'b5b8aa9a2c34'
branch_labels = None
depends_on = None


def upgrade() -> None:
    # ### commands auto generated by Alembic - please adjust! ###
    pass
    # ### end Alembic commands ###


def downgrade() -> None:
    # ### commands auto generated by Alembic - please adjust! ###
    pass
    # ### end Alembic commands ###

"""empty message

Revision ID: 01f70848fab3
Revises: 1e1f334238bc
Create Date: 2023-08-24 15:29:03.543064

"""
from alembic import op
import sqlalchemy as sa

from sqlalchemy import orm
from sunpeek.components import Plant
from sunpeek.components.helpers import PCSettingsDefaults

# revision identifiers, used by Alembic.
revision = '01f70848fab3'
down_revision = '1e1f334238bc'
branch_labels = None
depends_on = None


def upgrade() -> None:
    # ### commands auto generated by Alembic - please adjust! ###
    op.create_table('pc_settings_defaults',
        sa.Column('id', sa.Integer(), sa.Identity(always=0), nullable=False),
        sa.Column('plant_id', sa.Integer(), nullable=False),
        sa.Column('evaluation_mode', sa.Enum('ISO', 'extended', name='pc_evaluation_modes2'), nullable=True),
        sa.Column('equation', sa.Integer(), nullable=True),
        sa.Column('wind_used', sa.Boolean(), nullable=True),
        sa.Column('safety_uncertainty', sa.Float(), nullable=True),
        sa.Column('safety_pipes', sa.Float(), nullable=True),
        sa.Column('safety_others', sa.Float(), nullable=True),
        sa.ForeignKeyConstraint(['plant_id'], ['plant.id'], name=op.f('fk_pc_settings_defaults_plant_id_plant'), ondelete='CASCADE'),
        sa.PrimaryKeyConstraint('id', name=op.f('pk_pc_settings_defaults')),
        sa.UniqueConstraint('plant_id', name=op.f('uq_pc_settings_defaults_plant_id'))
    )
    with op.batch_alter_table('collector_types', schema=None) as batch_op:
        batch_op.create_unique_constraint(batch_op.f('uq_collector_types_name'), ['name'])

    with op.batch_alter_table('fluid_definitions', schema=None) as batch_op:
        batch_op.create_unique_constraint(batch_op.f('uq_fluid_definitions_name'), ['name'])

    with op.batch_alter_table('plant', schema=None) as batch_op:
        batch_op.create_unique_constraint(batch_op.f('uq_plant_name'), ['name'])

    with op.batch_alter_table('sensors', schema=None) as batch_op:
        batch_op.create_unique_constraint(batch_op.f('uq_sensors_raw_name'), ['raw_name', 'plant_id'])

    # update all existing plants and enter a PCSettingDefault
    bind = op.get_bind()
    session = orm.Session(bind=bind)

    print("test")
    plants = session.query(Plant).all()
    for plant in plants:
        print("inserting PC-method defaults for each plant", plant)
        plant.pc_settings_defaults = PCSettingsDefaults()

    session.commit()
    # ### end Alembic commands ###


def downgrade() -> None:
    # ### commands auto generated by Alembic - please adjust! ###
    with op.batch_alter_table('sensors', schema=None) as batch_op:
        batch_op.drop_constraint(batch_op.f('uq_sensors_raw_name'), type_='unique')

    with op.batch_alter_table('plant', schema=None) as batch_op:
        batch_op.drop_constraint(batch_op.f('uq_plant_name'), type_='unique')

    with op.batch_alter_table('fluid_definitions', schema=None) as batch_op:
        batch_op.drop_constraint(batch_op.f('uq_fluid_definitions_name'), type_='unique')

    with op.batch_alter_table('collector_types', schema=None) as batch_op:
        batch_op.drop_constraint(batch_op.f('uq_collector_types_name'), type_='unique')

    op.drop_table('pc_settings_defaults')
    # ### end Alembic commands ###

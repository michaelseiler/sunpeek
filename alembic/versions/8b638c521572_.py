"""empty message

Revision ID: 8b638c521572
Revises: f8d97da3db10
Create Date: 2023-09-11 09:04:20.147814

"""
from alembic import op
import sqlalchemy as sa

# revision identifiers, used by Alembic.
revision = '8b638c521572'
down_revision = '0ed137addd6f'
branch_labels = None
depends_on = None


def upgrade() -> None:
    op.rename_table('collector_types', 'collectors')

    with op.batch_alter_table('collectors', schema=None) as batch_op:
        collectortypes = sa.Enum('flat_plate', 'concentrating', name='collectortypes')
        collectortypes.create(op.get_bind())
        op.add_column('collectors',
                      sa.Column('collector_type', collectortypes, nullable=False, server_default='flat_plate'))
        batch_op.alter_column('collector_type', server_default=None)
        batch_op.create_unique_constraint(batch_op.f('uq_collectors_name'), ['name'])
        batch_op.drop_constraint(batch_op.f('uq_collector_types_name'))
        batch_op.add_column(sa.Column('_concentration_ratio_mag', sa.Float(), nullable=True))
        batch_op.add_column(sa.Column('_concentration_ratio_unit', sa.String(), nullable=True))
        batch_op.add_column(sa.Column('_a8_mag', sa.Float(), nullable=True))
        batch_op.add_column(sa.Column('_a8_unit', sa.String(), nullable=True))

    with op.batch_alter_table('arrays', schema=None) as batch_op:
        batch_op.alter_column('collector_type_id', new_column_name='collector_id')

    with op.batch_alter_table('iam_methods', schema=None) as batch_op:
        batch_op.alter_column('collector_type_id', new_column_name='collector_id')


def downgrade() -> None:
    with op.batch_alter_table('iam_methods', schema=None) as batch_op:
        batch_op.alter_column('collector_id', new_column_name='collector_type_id')

    with op.batch_alter_table('arrays', schema=None) as batch_op:
        batch_op.alter_column('collector_id', new_column_name='collector_type_id')

    op.rename_table('collectors', 'collector_types')
    op.drop_column('collector_types', 'collector_type')
    with op.batch_alter_table('collectors', schema=None) as batch_op:
        batch_op.create_unique_constraint(batch_op.f('uq_collector_types_name'), ['name'])
        batch_op.drop_constraint(batch_op.f('uq_collectors_name'))
        batch_op.drop_column('_concentration_ratio_mag')
        batch_op.drop_column('_concentration_ratio_unit')
        batch_op.drop_column('_a8_mag')
        batch_op.drop_column('_a8_unit')

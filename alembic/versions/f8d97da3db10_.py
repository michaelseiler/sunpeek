"""empty message

Revision ID: f8d97da3db10
Revises: e665be32adf9
Create Date: 2023-09-05 13:48:11.218342

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'f8d97da3db10'
down_revision = 'e665be32adf9'
branch_labels = None
depends_on = None


def upgrade() -> None:
    # ### commands auto generated by Alembic - please adjust! ###
    with op.batch_alter_table('pc_method_outputs', schema=None) as batch_op:
        batch_op.alter_column('equation', new_column_name='formula')

    with op.batch_alter_table('pc_settings_defaults', schema=None) as batch_op:
        batch_op.alter_column('equation', new_column_name='formula')

    with op.batch_alter_table('plant', schema=None) as batch_op:
        batch_op.alter_column('_altitude_mag', new_column_name='_elevation_mag')
        batch_op.alter_column('_altitude_unit', new_column_name='_elevation_unit')

    # ### end Alembic commands ###


def downgrade() -> None:
    # ### commands auto generated by Alembic - please adjust! ###
    with op.batch_alter_table('plant', schema=None) as batch_op:
        batch_op.alter_column('_elevation_unit', new_column_name='_altitude_unit')
        batch_op.alter_column('_elevation_mag', new_column_name='_altitude_mag')

    with op.batch_alter_table('pc_settings_defaults', schema=None) as batch_op:
        batch_op.alter_column('formula', new_column_name='equation')

    with op.batch_alter_table('pc_method_outputs', schema=None) as batch_op:
        batch_op.alter_column('formula', new_column_name='equation')

    # ### end Alembic commands ###

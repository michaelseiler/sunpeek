"""empty message

Revision ID: b56243a3dcde
Revises: 0be4ac5f1227
Create Date: 2023-10-11 16:04:27.926248

"""
from alembic import op
import sqlalchemy as sa

# revision identifiers, used by Alembic.
revision = 'b56243a3dcde'
down_revision = 'ee50cf887a01'
branch_labels = None
depends_on = None


def upgrade() -> None:
    op.alter_column('plant', '_fluid_vol_unit', new_column_name='_fluidvol_total_unit')
    op.alter_column('plant', '_fluid_vol_mag', new_column_name='_fluidvol_total_mag')


def downgrade() -> None:
    op.alter_column('plant', '_fluidvol_total_unit', new_column_name='_fluid_vol_unit')
    op.alter_column('plant', '_fluidvol_total_mag', new_column_name='_fluid_vol_mag')

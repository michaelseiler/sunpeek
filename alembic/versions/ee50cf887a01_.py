"""empty message

Revision ID: ee50cf887a01
Revises: 0be4ac5f1227
Create Date: 2023-10-16 10:30:13.257191

"""
from alembic import op
import sqlalchemy as sa

# revision identifiers, used by Alembic.
revision = 'ee50cf887a01'
down_revision = '0be4ac5f1227'
branch_labels = None
depends_on = None


def upgrade() -> None:
    with op.batch_alter_table('plant', schema=None) as batch_op:
        batch_op.alter_column('_tz_data_offset', new_column_name='tz_data_offset')


def downgrade() -> None:
    with op.batch_alter_table('plant', schema=None) as batch_op:
        batch_op.alter_column('tz_data_offset', new_column_name='_tz_data_offset')

"""empty message

Revision ID: 0ed137addd6f
Revises: f8d97da3db10
Create Date: 2023-09-22 12:16:12.894860

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '0ed137addd6f'
down_revision = 'f8d97da3db10'
branch_labels = None
depends_on = None


def upgrade() -> None:
    with op.batch_alter_table('plant', schema=None) as batch_op:
        batch_op.add_column(sa.Column('virtuals_calculation_uptodate', sa.Boolean(), nullable=True))


def downgrade() -> None:
    with op.batch_alter_table('plant', schema=None) as batch_op:
        batch_op.drop_column('virtuals_calculation_uptodate')


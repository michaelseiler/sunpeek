"""empty message

Revision ID: d427e7b0758a
Revises: 8b638c521572
Create Date: 2023-10-02 12:36:11.082736

"""
from alembic import op
import sqlalchemy as sa

# revision identifiers, used by Alembic.
revision = 'd427e7b0758a'
down_revision = '8b638c521572'
branch_labels = None
depends_on = None


def upgrade() -> None:
    with op.batch_alter_table('collectors', schema=None) as batch_op:
        batch_op.add_column(sa.Column('aperture_parameters', sa.JSON(), nullable=True))

        collectortesttypes = sa.Enum('SST', 'QDT', name='collectortesttypes')
        collectortesttypes.create(op.get_bind())
        batch_op.drop_column('test_type')
        batch_op.add_column(sa.Column('test_type', collectortesttypes))


def downgrade() -> None:
    with op.batch_alter_table('collectors', schema=None) as batch_op:
        batch_op.alter_column('test_type',
                              existing_type=sa.Enum('SST', 'QDT', name='collectortesttypes'),
                              type_=sa.VARCHAR(length=7),
                              existing_nullable=True)
        batch_op.drop_column('aperture_parameters')

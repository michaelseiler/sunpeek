"""empty message

Revision ID: 0be4ac5f1227
Revises: d427e7b0758a
Create Date: 2023-10-04 21:04:48.231092

"""
from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects import sqlite

# revision identifiers, used by Alembic.
revision = '0be4ac5f1227'
down_revision = 'd427e7b0758a'
branch_labels = None
depends_on = None


def upgrade() -> None:
    with op.batch_alter_table('collectors', schema=None) as batch_op:
        batch_op.alter_column('calculated_parameters', new_column_name='calculation_info')


def downgrade() -> None:
    with op.batch_alter_table('collectors', schema=None) as batch_op:
        batch_op.alter_column('calculation_info', new_column_name='calculated_parameters')


import uvicorn
import os
import sqlalchemy.exc
import alembic

from sunpeek.db_utils import init_db
import sunpeek.common.errors
from sunpeek.common.utils import sp_logger


def initialise_database():
    """initialises the database if it is not yet created"""
    try:
        init_db.init_db()
    except (sqlalchemy.exc.ProgrammingError, sunpeek.common.errors.DatabaseAlreadyExistsError) as e:
        if "already exists" in str(e):
            os.chdir(os.path.dirname(os.path.realpath(__file__)))
            alembic.config.main(argv=['--raiseerr', 'upgrade', 'head'])

        else:
            if "Timescaledb extension must be installed" in str(e):
                raise sunpeek.common.errors.ConfigurationError(str(e))
    except sqlalchemy.exc.OperationalError as e:
        sp_logger.error(f"Could not connect to database: {str(e)}")


# def get_port():
#     """returns the port where the API should be run, depending on the set environment variable"""
#     root_path = os.environ.get('SUNPEEK_API_ROOT_PATH', 'localhost:8000')
#     if os.environ.get('SUNPEEK_API_PORT') is not None:
#         return os.environ.get('SUNPEEK_API_PORT')
#     if root_path is not None:
#         try:
#             port = int(root_path.split(':')[1])
#         except IndexError:
#             port = 80
#     else:
#         port = 8000
#     return port


def run_sunpeek():
    initialise_database()
    port = os.environ.get('SUNPEEK_API_PORT', 8000)
    uvicorn.run("sunpeek.api.main:app", host='0.0.0.0', port=port)


if __name__ == '__main__':
    run_sunpeek()

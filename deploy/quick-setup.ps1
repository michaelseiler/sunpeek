Add-Type -AssemblyName System.Web

Copy-Item 'api.env.template' 'api.env'
$db_pw = [System.Web.Security.Membership]::GeneratePassword(24,2)
$regex = '(?<=HIT_DB_PW=)[^"]*'
(Get-Content 'api.env') -replace $regex, ($db_pw) | Set-Content 'api.env'

Copy-Item 'db.env.template' 'db.env'
$regex = '(?<=POSTGRES_PASSWORD=)[^"]*'
(Get-Content 'db.env') -replace $regex, ($db_pw) | Set-Content 'db.env'

Copy-Item 'ui.env.template' 'ui.env'
$default=(Get-Content 'ui.env')
$default=(Select-String -Path 'ui.env' -Pattern 'HIT_API_BASE_URL=').Line
$default = $default.Trim('HIT_API_BASE_URL=').Trim('/api/v1')
$ext_name = Read-Host -Prompt "Provide the externally accessible FQDN (URL) for the application [$default]"
if ([string]::IsNullOrWhiteSpace($ext_name)){
    $ext_name = $default
}
(Get-Content 'ui.env') -replace '(?<=HIT_API_BASE_URL=)[^"]*', ($ext_name+'/api/v1') | Set-Content 'ui.env'

Write-Output "Configuration Complete. You can run 'docker compose up -d' to start it and 'docker compose down' to stop it"

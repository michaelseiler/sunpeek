import os
from deploy import quick_setup_utils


here = os.path.dirname(__file__)


def create_ui_env(url=''):
    ui_conf = []

    if url != '':
        ui_conf.append('HIT_API_BASE_URL=' + url + '/api/v1\n')
    else:
        ui_conf.append('HIT_API_BASE_URL=' + quick_setup_utils.get_default_url() + '/api/v1\n')

    with open(os.path.join(quick_setup_utils.here, 'ui.env'), 'w') as f:
        f.writelines(ui_conf)


def create_db_and_api_env():
    with open(os.path.join(here, 'api.env.template')) as f:
        api_conf = f.readlines()

    api_conf.pop(quick_setup_utils.index_conf_entry('#HIT_DB_PW', api_conf))
    api_conf.pop(quick_setup_utils.index_conf_entry('#HIT_DB_USER', api_conf))
    api_conf[quick_setup_utils.index_conf_entry('HIT_DB_HOST', api_conf)] = \
        'HIT_DB_HOST=' + '//var/lib/sunpeek/data/sunpeek_db.sqlite\n'
    # api_conf.append('HIT_DB_TYPE=sqlite')

    with open(os.path.join(here, 'api.env'), 'w') as f:
        f.writelines(api_conf)


if __name__ == '__main__':
    create_ui_env(url=input(f"Provide the externally accessible FQDN (URL) for the application["
                            f"{quick_setup_utils.get_default_url()}]: "))
    create_db_and_api_env()


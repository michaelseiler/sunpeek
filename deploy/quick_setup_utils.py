
import os


here = os.path.dirname(__file__)


def index_conf_entry(var_name, conf_lines):
    return [entry.split('=')[0] for entry in conf_lines].index(var_name)


def get_default_url():
    with open(os.path.join(here, 'ui.env.template')) as f:
        ui_conf = f.readlines()

    return ui_conf[index_conf_entry('HIT_API_BASE_URL', ui_conf)].split('=')[1].strip('api/v1\n')

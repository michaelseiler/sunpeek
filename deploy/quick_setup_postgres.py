from deploy import quick_setup_utils
import os
import string
import secrets

here = os.path.dirname(__file__)


def create_ui_env(url=''):
    with open(os.path.join(quick_setup_utils.here, 'ui.env.template')) as f:
        ui_conf = f.readlines()

    if url != '':
        ui_conf[quick_setup_utils.index_conf_entry('HIT_API_BASE_URL', ui_conf)] = 'HIT_API_BASE_URL=' + url + '/api/v1\n'
    else:
        ui_conf[quick_setup_utils.index_conf_entry('HIT_API_BASE_URL', ui_conf)] = 'HIT_API_BASE_URL=' + quick_setup_utils.get_default_url() + '/api/v1\n'

    with open(os.path.join(quick_setup_utils.here, 'ui.env'), 'w') as f:
        f.writelines(ui_conf)


def create_db_and_api_env():
    with open(os.path.join(here, 'api.env.template')) as f:
        api_conf = f.readlines()

    with open(os.path.join(here, 'db.env.template')) as f:
        db_conf = f.readlines()

    alphabet = string.ascii_letters + string.digits + '_' + '-' + '#' + '€' + '§'
    db_pw = ''.join(secrets.choice(alphabet) for i in range(36))

    api_conf[quick_setup_utils.index_conf_entry('HIT_DB_PW', api_conf)] = 'HIT_DB_PW=' + db_pw + '\n'
    db_conf[quick_setup_utils.index_conf_entry('POSTGRES_PASSWORD', db_conf)] = 'POSTGRES_PASSWORD=' + db_pw + '\n'

    with open(os.path.join(here, 'api.env'), 'w') as f:
        f.writelines(api_conf)

    with open(os.path.join(here, 'db.env'), 'w') as f:
        f.writelines(db_conf)


if __name__ == '__main__':
    quick_setup_utils.create_db_and_api_env()
    create_ui_env(url=input(f"Provide the externally accessible FQDN (URL) for the application["
                            f"{quick_setup_utils.get_default_url()}]: "))

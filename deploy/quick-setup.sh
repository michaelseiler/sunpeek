#!/bin/bash

curl https://gitlab.com/sunpeek/sunpeek/-/raw/main/deploy/api.env.template -o api.env.template
curl https://gitlab.com/sunpeek/sunpeek/-/raw/main/deploy/ui.env.template -o ui.env.template
curl https://gitlab.com/sunpeek/sunpeek/-/raw/main/deploy/docker-compose.yml -o docker-compose.yml
mkdir traefik_dynamic_conf
curl https://gitlab.com/sunpeek/sunpeek/-/raw/main/deploy/traefik_dynamic_conf/conf.toml -o ./traefik_dynamic_conf/conf.toml

read -p "Enter the externally accessible URL for the application [http://localhost/] (without trailing /)" url

if [ -z "$url" ]
then
  url=http://localhost
fi

cp api.env.template api.env
cp ui.env.template ui.env

url=$(echo $url | sed -r 's/[/]/\\\//g')
sed -i -e "s/HIT_API_BASE_URL=http:\/\/localhost\/api\/v1/HIT_API_BASE_URL=""$url""\/api\/v1/g" ui.env

import os
import sqlalchemy.exc

from sunpeek.db_utils import init_db
from sunpeek.common import utils
from sunpeek.demo import demo_plant


try:
    init_db.init_db()
except sqlalchemy.exc.OperationalError as e:
    if " Connection refused " in str(e):
        raise ConnectionError("Could not connect to DB, run 'docker-compose up -d' from a terminal in the 'tests' "
                              "foilder to start a DB container.")
    else:
        raise

except AssertionError as e:
    if "Timescaledb extension must be installed and available" in str(e):
        raise
    else:
        pass

with utils.S() as session:
    plant = demo_plant.create_demoplant(session)
    demo_plant.add_demo_data(plant, session=session)

from tkinter import filedialog, Tk, PhotoImage, Toplevel
# from tkinter import
from tkinter import ttk
import shutil
import os
from deploy import quick_setup_sqlite as qs

here = os.path.dirname(__file__)

try:
    import pyi_splash
    pyi_splash.update_text('Setup files extracted, starting setup UI...')
    pyi_splash.close()
except:
    pass


def copy_files(dest):
    for file in ['./deploy/api.env', './deploy/ui.env', './deploy/docker-compose.yml', './deploy/start.bat']:
        try:
            shutil.copy(os.path.join(here, file), dest)
        except FileExistsError:
            pass

    try:
        shutil.copy(os.path.join(here, './deploy/db.env'), dest)
    except (FileExistsError, FileNotFoundError):
        pass

    try:
        shutil.copytree(os.path.join(here, './deploy/traefik_dynamic_conf'), os.path.join(dest, 'traefik_dynamic_conf'))
    except FileExistsError:
        pass

def popup_bonus(conf_path, root):
    win = Toplevel()
    win.wm_title("Window")

    l = ttk.Label(win, text=f"Setup complete. Please open {conf_path} and double click on start.bat to start the application \n"
                            f"You must install Docker Desktop first, see https://docs.docker.com/desktop/install/windows-install/, "
                            f"start it and wait until it is running \n"
                            f"For more information, visit docs.sunpeek.org")
    l.grid(row=0, column=0)

    b = ttk.Button(win, text="OK", command=root.destroy)
    b.grid(row=1, column=0)


def setup(url, root, conf_dir):
    qs.create_db_and_api_env()
    qs.create_ui_env(url=url)
    copy_files(conf_dir)
    popup_bonus(conf_dir, root)


def set_config_dir(field):
    dir = filedialog.askdirectory()
    field.delete(0,"end")
    field.insert(0, dir)


root = Tk()
root.title('SunPeek Easy Setup')
icon = PhotoImage(file=os.path.join(here,  'static_assets', 'icon.png'))
root.iconphoto(False, icon)
frm = ttk.Frame(root, padding=10)
frm.grid()
# ttk.Label(frm, text="Please set the following 2 options, then we'll do the rest").grid(column=0, row=0)

ttk.Label(frm, text="Please select the directory to store configuration files: ").grid(column=0, row=1)
conf_dir_input = ttk.Entry(frm)
conf_dir_input.grid(column=2, row=1)
image1 = PhotoImage(file=os.path.join(here,  'static_assets', 'Mimetypes-inode-directory-icon.png'))
ttk.Button(frm, image=image1, command=lambda: set_config_dir(conf_dir_input)).grid(column=3, row=1)

ttk.Label(frm, text="Please provide the URL for the application. For local only installation, accept the default: ").grid(column=0, row=2)
inputtxt = ttk.Entry(frm)
inputtxt.grid(column=2, row=2)
inputtxt.insert(0, qs.quick_setup_utils.get_default_url())

ttk.Button(frm, text="Quit", command=root.destroy).grid(column=0, row=3)
ttk.Button(frm, text="Setup", command=lambda: setup(inputtxt.get(), root, conf_dir_input.get())).grid(column=1, row=3)

root.mainloop()
